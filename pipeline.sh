#Move repo's source
shopt -s extglob
mkdir mod
mv !(mod|password) mod

#Create dirs
mkdir tools
mkdir bin

#Go to tools
cd tools

#NWNSC
wget https://github.com/nwneetools/nwnsc/releases/download/v1.1.2/nwnsc-linux-v1.1.2.zip
unzip nwnsc-linux-v1.1.2.zip

#NWSync tools
wget https://github.com/niv/nwsync/releases/download/svc-11/nwsync.linux.amd64.zip
unzip nwsync.linux.amd64.zip

#Core scripts
wget 'https://docs.google.com/uc?export=download&id=1AnXuAUXdmsa_Olwouxc1p7br3MA_3N7w' -O nwnsc-data.zip
unzip nwnsc-data.zip

#Nim tools
wget https://github.com/niv/neverwinter.nim/releases/download/1.3.1/neverwinter.linux.amd64.zip
unzip neverwinter.linux.amd64.zip

#Nasher
wget https://github.com/squattingmonk/nasher/releases/download/0.16.3/nasher_linux.tar.gz
tar -xzf nasher_linux.tar.gz
mv nasher_linux/nasher nasher

#Clean
rm *.zip
rm *.tar.gz
rm nasher_linux/*
rmdir nasher_linux

#Add to path
export PATH=$PATH:`pwd`

#Go back to main directory
cd ..

#Configure Nasher
nasher config --installDir:"`pwd`/bin"
nasher config --nssFlags:"-n `pwd`/tools/nwnsc-data"

#Install module and hak
cd mod
nasher install release --yes
nasher install hakpak --yes
cd ..

#Remove created Realms directory, as it is not needed
rm -rf bin/modules/Realms

#Move files
mv bin/modules/Realms.mod bin/Realms.mod
mv bin/hak/realms.hak bin/realms.hak
rmdir bin/modules
rmdir bin/hak

#Get module version
VERSION=$(awk -F "=" '/version/ {print $2}' mod/nasher.cfg | tr -d ' ' | tr -d '"' | sed -r 's/[ ]+/-/g')

#Copy the password file to bin and ensure appropriate permissions
cp password bin/password
chmod 700 bin/password

#Get timestamp - using the same timestamp in both builds will get rid of the "Update available" notification when using a Polish version
TIMESTAMP=`date +"%s"`

#Install English tlk
cd mod
nasher install tlk-en --yes
cd ..
mv bin/tlk/realms.tlk bin/realms.tlk
rmdir bin/tlk

#Package English offline build
VEREN="${VERSION}-en"
cd bin
echo -e "Realms ${VEREN}\n---\n\nA random adventure generator for Neverwinter Nights: Enhanced Edition.\n\nInvite some friends or create customized NPC companions, form a party and generate a random Neverwinter Nights adventure to play through.\nExplore your unique realm filled with dangers and treasures, complete (or fail) quests and try to win the game by completing the main objective.\nAnd once you do, why not note down your adventure score for bragging rights or share the generated adventure with others to see how they will fare?\n\nGameplay Length: about 1 hr per map\nNumber Players: 1-5\nLanguage: English, Polish\nLevel Range: 1\nRaces: Any\nTricks & Traps: Medium\nRoleplay: None\nHack & Slash: Heavy\nClasses: Any\nAlignments: Any\nScope: Small\nDM Needed: No DM Required\nSingle or Multiplayer: Both\nContent Rating: Teen\n\nThis is an offline package containing the 'Realms' module. To install it, put included files in appropriate folders in your NWN user directory folder (located in 'My Documents' by default):\n-'Realms.mod' in 'modules'\n-'realms.hak' in 'hak'\n-'realms.tlk' in 'tlk'\n\nPlease note that the recommended way of installing the module is to add an NWSync repository in your game. This package is provided for those who wish to have a backup of the module.\nNWSync makes installation very simple and lets you have notifications for whenever module updates are available, as well as lets you update with a single click inside NWN! To install 'Realms' this way:\n1. Copy this URL: https://sync.neverwintervault.org/~taro94\n2. Launch 'Neverwinter Nights: Enhanced Edition'\n3. Go to the new game menu\n4. Choose 'Manage' and paste the copied URL into the 'Add repository by URL' input field using CTRL+V\n5. Confirm when asked if it looks OK\n6. Go back to the new game menu and go to the 'Community' tab\n7. Find 'Realms' on the list of modules under 'taro94' and click 'Download', then confirm the downloading process\n8. Once the download is complete, you are ready to launch the game by clicking 'Play'.\n\nModule made by: Stanisław Jasiewicz (Taro94)\nProject git repository: https://gitlab.com/Taro94/realms" >> readme.txt
zip ../"Realms-${VEREN}.zip" readme.txt realms.hak Realms.mod realms.tlk
rm readme.txt
cd ..

#Deploy English build
cd bin
nwsync_remote --url https://sync.neverwintervault.org --username taro94 --password-file ./password manifest-create --path . --module "Realms.mod" > manifest_output 2>&1
NWSYNC_MANIFEST=`tail -2 manifest_output | head -1 | grep -o '[^ ]*$'`
rm manifest_output
nwsync_remote --url https://sync.neverwintervault.org --username taro94 --password-file ./password module-version-set $NWSYNC_MODULE $NWSYNC_MANIFEST $VEREN Realms $TIMESTAMP
cd ..

#Remove tlk
rm bin/realms.tlk

#Install Polish tlk
cd mod
nasher install tlk-pl --yes
cd ..
mv bin/tlk/realms.tlk bin/realms.tlk
rmdir bin/tlk

#Package Polish offline build
VERPL="${VERSION}-pl"
cd bin
echo -e "Realms ${VERPL}\n---\n\nGenerator losowych przygód do Neverwinter Nights: Enhanced Edition.\n\nZaproś znajomych lub stwórz własnych towarzyszy NPC, uformuj drużynę i wygeneruj losową przygodę do przejścia.\nPrzemierzaj swoją niepowtarzalną krainę wypełnioną skarbami i zagrożeniami, wykonuj (bądź nie) zadania i postaraj się wygrać grę wypełniając główną misję.\nA gdy ci się uda, dlaczego by nie pochwalić się innym graczom wynikiem otrzymanym za ukończenie przygody albo nie podzielić się z nimi wygenerowaną mapą, aby przekonać się, jak sobie poradzą?\n\nDługość Gry: około 1 godzina na mapę\nLiczba Graczy: 1-5\nJęzyk: Polski, Angielski\nZakres Poziomów: 1\nRasy: Dowolne\nZagadki i pułapki: Średnie\nOdgrywanie ról: Brak\nIlość Walki: Duża\nKlasy: Dowolne\nCharaktery: Dowolne\nRozmiar: Mały\nPotrzebny MG: Nie\nJeden czy wielu graczy: Dowolnie\nOcena Treści: Dla nastolatków\n\nTo jest paczka offline z modułem 'Realms'. Aby ją zainstalować, przenieś załączone pliki do odpowiednich folderów w swoim katalogu użytkownika NWN (domyślnie znajdującym się w folderze 'Moje Dokumenty'):\n-'Realms.mod' do 'modules'\n-'realms.hak' do 'hak'\n-'realms.tlk' do 'tlk'\n\nZwróć uwagę, że rekomendowanym sposobem instalacji modułu jest dodać repozytorium NWSync w swojej kopii gry. Ta paczka przygotowana została z myślą o tych, którzy chcą mieć kopię zapasową modułu offline.\nNWSync czyni instalację wyjątkowo prostą, zapewnia też powiadomienia w przypadku pojawienia się aktualizacji oraz pozwala wykonywać owe aktualizacje pojedynczym kliknięciem z wnętrza NWN! Aby zainstalować 'Realms' w ten sposób:\n1. Skopiuj następujący adres URL: https://sync.neverwintervault.org/~taro94\n2. Uruchom 'Neverwinter Nights: Enhanced Edition'\n3. Przejdź do menu nowej gry\n4. Wybierz 'Zarządzaj' i wklej skopiowany URL w pole 'Dodaj repozytorium po URL' przy użyciu CTRL+V\n5. Potwierdź, gdy gra zapyta cię, czy dane wyglądają właściwie\n6. Wróć do menu nowej gry i przejdź do zakładki 'Społeczność'\n7. Znajdź 'Realms' na liście modułów pod 'taro94' i naciśnij 'Zarządzaj', wybierz najnowszą wersję z końcówką 'pl', kliknij 'Pobierz', następnie potwierdź pobieranie (nie klikaj 'Pobierz' dostępnego obok 'Zarządzaj', ponieważ zainstaluje to angielską wersję modułu)\n8. Gdy pobieranie będzie zakończone, jesteś gotowy do rozpoczęcia gry przez naciśnięcie 'Graj'.\n\nAutor modułu: Stanisław Jasiewicz (Taro94)\nRepozytorium git projektu: https://gitlab.com/Taro94/realms" >> readme.txt
zip ../"Realms-${VERPL}.zip" readme.txt realms.hak Realms.mod realms.tlk
rm readme.txt
cd ..

#Deploy Polish build
cd bin
nwsync_remote --url https://sync.neverwintervault.org --username taro94 --password-file ./password manifest-create --path . --module "Realms.mod" > manifest_output 2>&1
NWSYNC_MANIFEST=`tail -2 manifest_output | head -1 | grep -o '[^ ]*$'`
rm manifest_output
nwsync_remote --url https://sync.neverwintervault.org --username taro94 --password-file ./password module-version-set $NWSYNC_MODULE $NWSYNC_MANIFEST $VERPL Realms $TIMESTAMP
cd ..

#Cleanup
rm -rf tools
rm -rf bin
rm -rf mod

#Version to file
echo $VERSION > version
