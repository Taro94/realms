# Realms

Realms is an unofficial module/expansion for Neverwinter Nights: Enhanced Edition.

It aims to provide NWN players with an unlimited number of randomly generated adventures, for singleplayer and multiplayer alike.

## Main features

- Different map, encounters and quests every playthrough,
- Challenging fights that will force you to make good use of consumables and party tactics,
- Highly customizable difficulty and ruleset, like value of quest rewards,
- Balanced gameplay in which every class, feat, skill and spell has its use (yes, even Improved Initiative),
- Cooperative multiplayer for up to 5 players officially supported (but more may work if you adjust difficulty and ruleset),
- AI companion creator that allows you to create your own party from scratch,
- Horse riding support, including quality of life features like auto-mount and auto-dismount on area transitions from and into buildings,
- Statistics and final score screen after every playthrough allowing for competition between players,
- Full English and Polish localizations

## Installation

There are two ways of installing the module at this time.

The recommended option is to use the Neverwinter Vault's public NWSync repository. This comes with module update notifications and general ease of "Realms" installation and updating. To do this:
1. Copy the following URL: https://sync.neverwintervault.org/~taro94
2. Add a new NWSync repository in the new game menu in NWN:EE, pasting the copied address as its URL
3. Install the "Realms" module from the "Community" tab in the new game menu (you need to manually select a Polish version if you want it, the English version is downloaded by default)

The second option is to download one of the released packages, which can be found in the [Releases section](https://gitlab.com/Taro94/realms/-/releases). Choose either "Realms-PL" or "Realms-EN" in the newest release (depending on your preferred localization) and install the module according to instructions included in the downloaded package's "readme.txt" file.

## Building from source

You can also build the module yourself if you wish. You can do it the easy way or the hard way.

### With nasher

[Nasher](https://github.com/squattingmonk/nasher.nim) is the build manager used by Realms and using it is the recommended way of building the module.

Requirements:

- nasher and its dependencies ([nwnsc](https://neverwintervault.org/project/nwnee/other/tool/nwnsc-nwn-enhanced-edition-script-compiler) and [neverwinter.nim](https://github.com/niv/neverwinter.nim)); note that neverwinter.nim is installed automatically when installing nasher via nimble

Note: if you are on Manjaro or any other ArchLinux distribution, you can install nasher and its dependencies from [AUR](http://aur.archlinux.org/packages/nasher).

Building:

1. Ensure nasher has been properly configured, the most likely options requiring configuration being:
   - `nasher config --set installDir <userDirPath>` to set the path to the NWN user directory
   - `nasher config --nssFlags:"-n <installDirPath>"` to set the path to the NWN install directory (if location is not Steam's default)
   - `nasher config --set nssCompiler <nwnscPath>` to set the path to nwnsc (if it wasn't added to PATH)
2. Get the source by downloading it or via `git clone https://gitlab.com/Taro94/realms.git`.
3. Navigate to the project directory in cmd or terminal and call `nasher install release` (assuming nasher's directory has been added to PATH).
4. Run `nasher install hakpak` to build and install the module's hakpak.
5. Run `nasher install tlk-XX`, replacing `XX` with either `en` or `pl` to build the English or Polish talk-table file.

### Without nasher

Building the module without nasher is possible, if cumbersome.

Note: by saving the module in the toolset, you lose the pre-packaged module.sq3 file containing pre-made NPC companions. Building the module with nasher is thus *highly* recommended and the only other option that won't break the pre-packaged database is using `nwn_erf` manually. In short, it's not worth it, just use nasher.

Requirements:

- [nwn_gff](https://github.com/niv/neverwinter.nim/releases)
- [nwn_tlk](https://github.com/niv/neverwinter.nim/releases)
- [nwn_erf](https://github.com/niv/neverwinter.nim/releases)
- [nwnsc](https://github.com/nwneetools/nwnsc/releases)
- Preferrably some script or tool to flatten a directory, because you don't want to do it by hand

Building:

1. Get the source by downloading it or via `git clone https://gitlab.com/Taro94/realms.git`.
2. Create a hakpak named "realms.hak" using `nwn_erf` containing all files found in "src/hak" and its subdirectories and put it in your NWN:EE user directory's "hak" subdirectory.
3. Convert `src/tlk/en/realms.tlk.json` or `src/tlk/pl/realms.tlk.json` (based on the language you want) to a `realms.tlk` file with `nwn_tlk` and put it in your NWN:EE user directory's "tlk" subdirectory.
4. Flatten the `src/mod` directory structure.
5. Compile all scripts (*.nss) with `nwnsc`.
6. Convert all the JSON files to GFF files with `nwn_gff`.
7. Pack all the module files (compiled scripts and GFF files from "src/mod/") into a "Realms.mod" file with `nwn_erf`.
8. Put the module file in your NWN:EE user directory's "module" subdirectory.

## Project structure

Project files are categorized into directories corresponding to different module files (mod, hak and tlk). Everything from `mod` goes into the module file, everything from `hak` into the hakpak file and *one* file from `tlk` is converted into a talk-table file (depending on language).

`core-libs` is not packed into any module file, as it only contains core NWN libraries for the purpose of VSC tooling (like intellisense). No changes should be made to files in this folder. Any modified core libraries should be moved elsewhere, within the `mod` folder (like Community Patch includes).

### Module

Contents of the module are further categorized (except the few files lying directly in `mod`):

- ai - Tony K's AI files, which the module uses,
- palettes - all the module's palettes
- community-patch - fixes from CPP made by Shadooow, which the module uses, as well as some extra fixes from other sources; this is where general gameplay fixes should be placed if any new ones are added,
- summons - directory with all the summon blueprints so they can be modified in the toolset when necessary (for example, their event scripts have been changed),
- base-systems - collection of general libraries that are not module-specific and can be used by scripts in `demiurge-framework` and in `realms` alike,
- demiurge-framework - all the module systems like world generation,
- realms - the module's actual contents like item blueprints or templates of encounters, towns, quests, etc

### Module events

The module's entry point, i.e. its core `OnModuleStart` event script is `evn_modstart` from `base-systems` (master event handler, see the script for details). It registers another script that will fire after it, `mod_dfstart` from `demiurge-framework` that contains Demiurge Framework's specific startup code. Finally, `mod_dfstart` registers `mod_start` from `realms` which contains content-specific startup code. Scripts for other events are registered in `mod_dfstart` and `mod_start`.

### File prefixes

The project follows a specific convention of prefixing file names, presented below. Every resource type has its own rules. Resources from imported systems (like Tony K's AI) generally don't follow these conventions.

#### Scripts

`inc_` - include files,
`exe_` - procedures, i.e. scripts that are to be called with `ExecuteScript`,
`clk_` - scripts that are to be used with targeting events, i.e. after a PC clicks on something,
`con_` - conditional scripts in conversations; if the script is specific to a conversation, the prefix will typically be followed with `XXX_`, where XXX identifies the conversation in some way,
`cnv_` - on action taken scripts in conversations; can by followed with `XXX_` like conditional ones,
`evn_` - master event handlers (see `inc_scriptevents` for details),
`cre_` - creature event scripts,
`are_` - area event scripts,
`tri_` - trigger event scripts,
`obj_` - placeable event scripts,
`mod_` - module event scripts,
`sho_` - store object event scripts,
`cmd_` - scripts fired when a specific chat command is used by a player,
`dbg_` - scripts fired when a specific chat command is used by a player in the module's debug mode,
`hnd_` - Demiurge Framework's handler scripts,
`rea_` - realm entity scripts,
`que_` - quest entity scripts,
`bio_` - biome entity scripts,
`twn_` - town entity scripts,
`enc_` - encounter entity scripts,
`spe_` - special area entity scripts,
`cha_` - champion entity scripts,
`bos_` - boss area entity scripts,
`fac_` - facility entity scripts,
`plo_` - plot entity scripts,
`sto_` - store entity scripts,
`it_` - item scripts (tag-based scripting),
`grp_` - scripts used in conjunction with the functions in `inc_groups` library as OnCreation events,

#### Items

All custom items so far have the `it_` prefix.

#### Creatures

`npc_` - NPC creatures that are to be encountered in towns,
`cre_` - other creatures.

#### Placeables

All custom placeables so far have the `obj_` prefix.

#### Waypoints

All custom waypoints so far have the `wp_` prefix.

#### Store objects

All custom store objects so far have the `sto_` prefix.

#### Triggers

All custom triggers so far have the `tri_` prefix.
