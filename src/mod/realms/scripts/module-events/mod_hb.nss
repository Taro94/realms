void main()
{
    if (GetGameDifficulty() >= GAME_DIFFICULTY_CORE_RULES)
        SetLocalInt(OBJECT_SELF, "X2_SWITCH_ENABLE_UMD_SCROLLS", TRUE);
    else
        DeleteLocalInt(OBJECT_SELF, "X2_SWITCH_ENABLE_UMD_SCROLLS");
}