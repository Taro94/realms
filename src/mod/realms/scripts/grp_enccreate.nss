#include "inc_arrays"
#include "inc_scriptevents"
#include "inc_groups"

// Script to be executed by every creature created in an encounter
void main()
{
    object encounter = GetCreatureGroupSpawnerObject();
    string encounterArray = "EncounterCreatures";

    AddObjectArrayElement(encounterArray, OBJECT_SELF, FALSE, encounter);
    SetLocalObject(OBJECT_SELF, "Encounter", encounter);
    AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_encdeath", OBJECT_SELF);
}