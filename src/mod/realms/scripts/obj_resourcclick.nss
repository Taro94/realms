void main()
{
    object PC = GetLastUsedBy();
    string resourceResRef = GetLocalString(OBJECT_SELF, "Resource");
    int resourceNum = GetLocalInt(OBJECT_SELF, "ResNum");
    object resources = CreateItemOnObject(resourceResRef, PC, resourceNum);
    SetPlotFlag(OBJECT_SELF, FALSE);
    DestroyObject(OBJECT_SELF);
}
