#include "inc_debug"
#include "inc_random"
#include "inc_lootconsts"
#include "inc_arrays"
#include "inc_common"
#include "inc_biomes"
#include "inc_tiles"
#include "inc_random"

// Functions for loot and store inventory generation

// Function generally called by every biome's GetBiomeLoot (until it turns out differences in loot tables between biomes are preferred);
// if nForStore is TRUE, no gold loot will be generated.
struct TreasureChestLoot GetLoot(string sSeedName, int nLevel, int nLootQuality, int nForStore=FALSE);

// Returns random spell scroll resref by innate level for a specific randomness seed
string GetRandomSpellScrollResRef(string sSeedName, int nInnateLevel);

// Function generally called by every biome's store's SpawnStoreInventory (until it turns out differences in loot tables between biomes are preferred);
object CreateStoreWithItems(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint, int nFirstRegion=FALSE);

// Function called by fac_thiefguild to spawn a thief guild store
object CreateThiefGuildStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);

// Function called by fac_temple to spawn a temple store
object CreateTempleStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);

// Function called by fac_mageguild to spawn a mage guild store
object CreateMageGuildStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);

// Function called by fac_armory to spawn an armory store
object CreateArmoryStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);

// Function that spawns random treasure in oInventory's inventory.
void SpawnRandomTreasureInInventory(string sSeedName, object oInventory, int nLootNumber);

int _GetIsSpellValidForPC(int spellId)
{
    switch (spellId)
    {
        case SPELL_EPIC_MUMMY_DUST:
        case SPELL_EPIC_HELLBALL:
        case SPELL_EPIC_MAGE_ARMOR:
        case SPELL_EPIC_RUIN:
        case SPELL_EPIC_DRAGON_KNIGHT:
        case 695: //epic warding, couldn't find the constant...
            return FALSE;
    }
    string bard = Get2DAString("spells", "Bard", spellId);
    string cleric = Get2DAString("spells", "Cleric", spellId);
    string druid = Get2DAString("spells", "Druid", spellId);
    string paladin = Get2DAString("spells", "Paladin", spellId);
    string ranger = Get2DAString("spells", "Ranger", spellId);
    string wizSorc = Get2DAString("spells", "Wiz_Sorc", spellId);
    return (bard != "" || cleric != "" || druid != "" || paladin != "" || ranger != "" || wizSorc != "");
}

string GetRandomSpellScrollResRef(string sSeedName, int nInnateLevel)
{
    if (nInnateLevel < 0 || nInnateLevel > 9)
    {
        LogWarning("Attempted to generate a scroll for invalid innate level: %n", nInnateLevel);
        return "";
    }

    string innateString = IntToString(nInnateLevel);
    string arrayName = "SCROLL_SPELLS_ARRAY";
    if (GetIntArraySize(arrayName) == 0)
    {
        CreateIntArray(arrayName);
        int spellId = 0;
        for (spellId = 0; spellId < 1000; spellId++)
        {
            string innate = Get2DAString("spells", "Innate", spellId);
            string masterSpell = Get2DAString("spells", "Master", spellId);
            if (innate != innateString)
                continue;
            string nameStringRef = Get2DAString("spells", "Name", spellId);
            int isMasterSpell = masterSpell == "";

            //Check for 50, 137 and 150 spells is due to deleted subspells of Resist Elements and Protection from Elements
            int isSpellValid =
                isMasterSpell
                && spellId != 50 && spellId != 137 && spellId != 150
                && GetIsStringInt(nameStringRef)
                && _GetIsSpellValidForPC(spellId);
            if (isSpellValid)
                AddIntArrayElement(arrayName, spellId);
        }
    }

    int arraySize = GetIntArraySize(arrayName);
    if (arraySize == 0)
    {
        LogWarning("No spells found for innate level %n", nInnateLevel);
        return "";
    }
    int selected = GetIntArrayElement(arrayName, RandomNext(arraySize, sSeedName));

    string resref = Get2DAString("des_crft_scroll", "Bard", selected);
    if (resref == "" || GetStringLeft(resref, 1) == "*")
        resref = Get2DAString("des_crft_scroll", "Cleric", selected);
    if (resref == "" || GetStringLeft(resref, 1) == "*")
        resref = Get2DAString("des_crft_scroll", "Druid", selected);
    if (resref == "" || GetStringLeft(resref, 1) == "*")
        resref = Get2DAString("des_crft_scroll", "Paladin", selected);
    if (resref == "" || GetStringLeft(resref, 1) == "*")
        resref = Get2DAString("des_crft_scroll", "Ranger", selected);
    if (resref == "" || GetStringLeft(resref, 1) == "*")
        resref = Get2DAString("des_crft_scroll", "Wiz_Sorc", selected);
    if (resref == "" || GetStringLeft(resref, 1) == "*")
        LogWarning("Selected non-existant spell with id = %n for scroll", selected);

    return resref;
}

struct TreasureChestLoot _GoldLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality)
{
    struct TreasureChestLoot result;
    LogInfo("Generating gold loot");
    switch (nEffectiveLevel)
    {
        case -1:
            result.gold = RandomNext(20, sSeedName) + 5; // ~15
            break;
        case 0:
            result.gold = RandomNext(81, sSeedName) + 25; // ~65
            break;
        case 1:
            result.gold = RandomNext(81, sSeedName) + 106; // ~146
            break;
        case 2:
            result.gold = RandomNext(91, sSeedName) + 255; // ~300
            break;
        case 3:
            result.gold = RandomNext(91, sSeedName) + 400; // ~445
            break;
        case 4:
            result.gold = RandomNext(201, sSeedName) + 600; // ~700
            break;
    }
    return result;
}

struct TreasureChestLoot _PoisonLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nEffectiveLevel < 2)
    {
        switch (RandomNext(3, sSeedName))
        {
            case 0:
                result.itemResRef = "x2_it_poison008"; //Centipede Venom, weak
                break;
            case 1:
                result.itemResRef = "x2_it_poison009"; //Giant Bee Venom, weak
                break;
            case 2:
                result.itemResRef = "x2_it_poison007"; //Spider Venom, weak
                break;
        }
    }
    else if (nEffectiveLevel < 5)
    {
        switch (RandomNext(3, sSeedName))
        {
            case 0:
                result.itemResRef = "x2_it_poison014"; //Centipede Venom, mild
                break;
            case 1:
                result.itemResRef = "x2_it_poison015"; //Giant Bee Venom, mild
                break;
            case 2:
                result.itemResRef = "x2_it_poison013"; //Spider Venom, mild
                break;
        }
    }
    if (result.itemResRef == "")
        LogWarning("Invalid poison resref (inc_loot, _PoisonLoot)");
    return result;
}

struct TreasureChestLoot _MiscLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nLootQuality == LOOT_QUALITY_USELESS)
    {
        switch (RandomNext(5, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_torch001"; //Torch
                break;
            case 1:
                result.itemResRef = "nw_it_msmlmisc24"; //Garlic
                break;
            case 2:
                result.itemResRef = "nw_it_msmlmisc23"; //Belladona
                break;
            case 3:
                result.itemResRef = "nw_it_contain001"; //Large Box
                break;
            case 4:
                result.itemResRef = "x2_it_lightgem07"; //White Light Gem
        }
    }
    else if (nLootQuality == LOOT_QUALITY_BAD)
    {
        switch (RandomNext(6, sSeedName))
        {
            case 0:
                result.itemResRef = "x1_wmgrenade002"; //Alchemist's fire
                break;
            case 1:
                result.itemResRef = "x1_wmgrenade004"; //Choking Powder
                break;
            case 2:
                result.itemResRef = "x1_wmgrenade001"; //Acid Flask
                break;
            case 3:
                result.itemResRef = "x1_wmgrenade003"; //Caltrops
                break;
            case 4:
                result.itemResRef = "x1_wmgrenade005"; //Holy Water
                break;
            case 5:
                result.itemResRef = "x1_wmgrenade006"; //Tanglefoot Bag
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_AVERAGE)
    {
        switch (RandomNext(7, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_picks001"; //Thieves' Tools +1
                break;
            case 1:
                result.itemResRef = "it_emptybottle"; //Magical Potion Bottle
                break;
            case 2:
                result.itemResRef = "it_bonewand"; //Bone Wand
                break;
            case 3:
                result.itemResRef = "it_blankscroll"; //Blank Scroll
                break;
            case 4:
                result.itemResRef = "x2_it_poison008"; //Centipede Venom, weak
                break;
            case 5:
                result.itemResRef = "x2_it_poison009"; //Giant Bee Venom, weak
                break;
            case 6:
                result.itemResRef = "x2_it_poison007"; //Spider Venom, weak
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_GOOD)
    {
        switch (RandomNext(5, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_picks002"; //Thieves' Tools +2
                break;
            case 1:
                result.itemResRef = "x0_it_mthnmisc05"; //Dust of Appearance
                break;
            case 2:
                result.itemResRef = "x2_it_poison014"; //Centipede Venom, mild
                break;
            case 3:
                result.itemResRef = "x2_it_poison015"; //Giant Bee Venom, mild
                break;
            case 4:
                result.itemResRef = "x2_it_poison013"; //Spider Venom, mild
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_GREAT)
    {
        switch (RandomNext(8, sSeedName))
        {
            case 0:
            case 1:
                result.itemResRef = "x2_it_picks001"; //Thieves' Tools +8
                break;
            case 2:
            case 3:
                result.itemResRef = "nw_it_contain003"; //Lesser Magic Bag
                break;
            case 4:
                result.itemResRef = "x2_it_acidbomb"; //Acid Bomb
                break;
            case 5:
                result.itemResRef = "x2_it_firebomb"; //Fire Bomb
                break;
            case 6:
            case 7:
                result.itemResRef = "it_soulstone"; //Soulstone
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_EXTRAORDINARY)
    {
        switch (RandomNext(7, sSeedName))
        {
            case 0:
                result.itemResRef = "x2_it_picks002"; //Thieves' Tools +12
                break;
            case 1:
                result.itemResRef = "nw_it_contain006"; //Bag of Holding
                break;
            case 2:
                result.itemResRef = "x2_it_acidbomb"; //Acid Bomb
                break;
            case 3:
                result.itemResRef = "x2_it_firebomb"; //Fire Bomb
                break;
            case 4:
                result.itemResRef = "x0_it_mthnmisc06"; //Dust of Disappearance
                break;
            case 5:
                result.itemResRef = "x0_it_mthnmisc11"; //Lens of Detection
            case 6:
                result.itemResRef = "it_soulstone"; //Soulstone
                break;
        }
    }

    LogInfo("Generating miscellaneous loot");
    return result;
}

struct TreasureChestLoot _MageGuildPotionLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nLootQuality == LOOT_QUALITY_USELESS)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }
    else if (nLootQuality == LOOT_QUALITY_BAD)
    {
        switch (RandomNext(2, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_mpotion013"; //Potion of Endurance
                break;
            case 1:
                result.itemResRef = "x2_it_mpotion001"; //Potion of Ironguts
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_AVERAGE)
    {
        switch (RandomNext(1, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_mpotion019"; //Potion of Lore
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_GOOD)
    {
        switch (RandomNext(2, sSeedName))
        {
            case 0:
                result.itemResRef = "x2_it_mpotion002"; //Potion of Death Armor
                break;
            case 1:
                result.itemResRef = "nw_it_mpotion005"; //Potion of Barkskin
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_GREAT)
    {
        switch (RandomNext(6, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_mpotion015"; //Potion of Bull's Strength
                break;
            case 1:
                result.itemResRef = "nw_it_mpotion014"; //Potion of Cat's Grace
                break;
            case 2:
                result.itemResRef = "nw_it_mpotion010"; //Potion of Eagle's Splendor
                break;
            case 3:
                result.itemResRef = "nw_it_mpotion013"; //Potion of Endurance
                break;
            case 4:
                result.itemResRef = "nw_it_mpotion017"; //Potion of Fox's Cunning
                break;
            case 5:
                result.itemResRef = "nw_it_mpotion018"; //Potion of Owl's Wisdom
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_EXTRAORDINARY)
    {
        switch (RandomNext(1, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_mpotion004"; //Potion of Speed
                break;
        }
    }

    return result;
}

struct TreasureChestLoot _PotionLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nLootQuality == LOOT_QUALITY_USELESS)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }
    else if (nLootQuality == LOOT_QUALITY_BAD)
    {
        switch (RandomNext(3, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_mpotion006"; //Potion of Antidote
                break;
            case 1:
                result.itemResRef = "nw_it_mpotion013"; //Potion of Endurance
                break;
            case 2:
                result.itemResRef = "x2_it_mpotion001"; //Potion of Ironguts
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_AVERAGE)
    {
        switch (RandomNext(5, sSeedName))
        {
            case 0:
            case 1:
                result.itemResRef = "nw_it_mpotion001"; //Potion of Cure Light Wounds
                break;
            case 2:
                result.itemResRef = "nw_it_mpotion009"; //Potion of Bless
                break;
            case 3:
                result.itemResRef = "nw_it_mpotion016"; //Potion of Aid
                break;
            case 4:
                result.itemResRef = "nw_it_mpotion019"; //Potion of Lore
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_GOOD)
    {
        switch (RandomNext(4, sSeedName))
        {
            case 0:
                result.itemResRef = "x2_it_mpotion002"; //Potion of Death Armor
                break;
            case 1:
                result.itemResRef = "nw_it_mpotion008"; //Potion of Invisibility
                break;
            case 2:
                result.itemResRef = "nw_it_mpotion011"; //Potion of Lesser Restoration
                break;
            case 3:
                result.itemResRef = "nw_it_mpotion005"; //Potion of Barkskin
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_GREAT)
    {
        switch (RandomNext(9, sSeedName))
        {
            case 0:
            case 1:
                result.itemResRef = "nw_it_mpotion020"; //Potion of Cure Moderate Wounds
                break;
            case 2:
                result.itemResRef = "nw_it_mpotion015"; //Potion of Bull's Strength
                break;
            case 3:
                result.itemResRef = "nw_it_mpotion014"; //Potion of Cat's Grace
                break;
            case 4:
                result.itemResRef = "nw_it_mpotion010"; //Potion of Eagle's Splendor
                break;
            case 5:
                result.itemResRef = "nw_it_mpotion013"; //Potion of Endurance
                break;
            case 6:
                result.itemResRef = "nw_it_mpotion017"; //Potion of Fox's Cunning
                break;
            case 7:
                result.itemResRef = "nw_it_mpotion018"; //Potion of Owl's Wisdom
                break;
            case 8:
                result.itemResRef = "nw_it_mpotion007"; //Potion of Clarity
                break;
        }
    }
    else if (nLootQuality == LOOT_QUALITY_EXTRAORDINARY)
    {
        switch (RandomNext(1, sSeedName))
        {
            case 0:
                result.itemResRef = "nw_it_mpotion004"; //Potion of Speed
                break;
        }
    }

    LogInfo("Generating potion loot");
    return result;
}

string _GetMinorTrapResRef(string sSeedName)
{
    switch (RandomNext(10, sSeedName))
    {
        case 0:
            return "nw_it_trap033"; //Minor Acid Splash
        case 1:
            return "nw_it_trap013"; //Minor Blob of Acid
        case 2:
            return "nw_it_trap021"; //Minor Electrical
        case 3:
            return "nw_it_trap017"; //Minor Fire
        case 4:
            return "nw_it_trap029"; //Minor Frost
        case 5:
            return "nw_it_trap025"; //Minor Gas
        case 6:
            return "nw_it_trap005"; //Minor Holy
        case 7:
            return "nw_it_trap041"; //Minor Negative
        case 8:
            return "nw_it_trap037"; //Minor Sonic
        case 9:
            return "nw_it_trap001"; //Minor Spike
        case 10:
            return "nw_it_trap009"; //Minor Tangle
    }
    return "";
}

string _GetAverageTrapResRef(string sSeedName)
{
    switch (RandomNext(10, sSeedName))
    {
        case 0:
            return "nw_it_trap034"; //Average Acid Splash
        case 1:
            return "nw_it_trap014"; //Average Blob of Acid
        case 2:
            return "nw_it_trap022"; //Average Electrical
        case 3:
            return "nw_it_trap018"; //Average Fire
        case 4:
            return "nw_it_trap030"; //Average Frost
        case 5:
            return "nw_it_trap026"; //Average Gas
        case 6:
            return "nw_it_trap006"; //Average Holy
        case 7:
            return "nw_it_trap042"; //Average Negative
        case 8:
            return "nw_it_trap038"; //Average Sonic
        case 9:
            return "nw_it_trap002"; //Average Spike
        case 10:
            return "nw_it_trap010"; //Average Tangle
    }
    return "";
}

string _GetStrongTrapResRef(string sSeedName)
{
    switch (RandomNext(10, sSeedName))
    {
        case 0:
            return "nw_it_trap035"; //Strong Acid Splash
        case 1:
            return "nw_it_trap015"; //Strong Blob of Acid
        case 2:
            return "nw_it_trap023"; //Strong Electrical
        case 3:
            return "nw_it_trap019"; //Strong Fire
        case 4:
            return "nw_it_trap031"; //Strong Frost
        case 5:
            return "nw_it_trap027"; //Strong Gas
        case 6:
            return "nw_it_trap007"; //Strong Holy
        case 7:
            return "nw_it_trap043"; //Strong Negative
        case 8:
            return "nw_it_trap039"; //Strong Sonic
        case 9:
            return "nw_it_trap003"; //Strong Spike
        case 10:
            return "nw_it_trap011"; //Strong Tangle
    }
    return "";
}

string _GetDeadlyTrapResRef(string sSeedName)
{
    switch (RandomNext(10, sSeedName))
    {
        case 0:
            return "nw_it_trap036"; //Deadly Acid Splash
        case 1:
            return "nw_it_trap016"; //Deadly Blob of Acid
        case 2:
            return "nw_it_trap024"; //Deadly Electrical
        case 3:
            return "nw_it_trap020"; //Deadly Fire
        case 4:
            return "nw_it_trap032"; //Deadly Frost
        case 5:
            return "nw_it_trap028"; //Deadly Gas
        case 6:
            return "nw_it_trap008"; //Deadly Holy
        case 7:
            return "nw_it_trap044"; //Deadly Negative
        case 8:
            return "nw_it_trap040"; //Deadly Sonic
        case 9:
            return "nw_it_trap004"; //Deadly Spike
        case 10:
            return "nw_it_trap012"; //Deadly Tangle
    }
    return "";
}

string _GetEpicTrapResRef(string sSeedName)
{
    switch (RandomNext(4, sSeedName))
    {
        case 0:
            return "x2_it_trap001"; //Epic Electrical
        case 1:
            return "x2_it_trap002"; //Epic Fire
        case 2:
            return "x2_it_trap003"; //Epic Frost
        case 3:
            return "x2_it_trap004"; //Epic Sonic
    }
    return "";
}

struct TreasureChestLoot _TrapLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nLootQuality == LOOT_QUALITY_USELESS || nLootQuality == LOOT_QUALITY_BAD)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }
    else if (nLootQuality == LOOT_QUALITY_AVERAGE || nLootQuality == LOOT_QUALITY_GOOD)
    {
        result.itemResRef = _GetMinorTrapResRef(sSeedName);
    }
    else if (nLootQuality == LOOT_QUALITY_GREAT || nLootQuality == LOOT_QUALITY_EXTRAORDINARY)
    {
        result.itemResRef = _GetAverageTrapResRef(sSeedName);
    }

    LogInfo("Generating trap loot");
    return result;
}

struct TreasureChestLoot _ScrollLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;

    int spellLevel = 0;

    switch (nEffectiveLevel)
    {
        case -1:
            if (nForStore)
                return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
            return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        case 0:
            spellLevel = 0;
            break;
        case 1:
            spellLevel = 1;
            break;
        case 2:
            spellLevel = 2;
            break;
        case 3:
        case 4:
            spellLevel = 3;
            break;
        case 5:
        case 6:
            spellLevel = 4;
            break;
        case 7:
        case 8:
            spellLevel = 5;
            break;
        case 9:
        case 10:
            spellLevel = 6;
        case 11:
            result.itemStackSize = 2;
            break;
        case 12:
        case 13:
            spellLevel = 7;
        case 14:
            result.itemStackSize = 2;
            break;
        case 15:
        case 16:
            spellLevel = 8;
        case 17:
            result.itemStackSize = 2;
            break;
        case 18:
        case 19:
            spellLevel = 9;
        case 20:
            result.itemStackSize = 2;
            break;
    }

    //Randomize some more, so that level 0 or 1 scrolls can still spawn at higher levels when the loot is "bad"
    if (nLootQuality == LOOT_QUALITY_USELESS && spellLevel != 0)
    {
        spellLevel = RandomNext(spellLevel + 1, sSeedName); //ex: for spellLevel 5, reroll between 0 and 5 (0-5 + 0)
    }
    else if (nLootQuality == LOOT_QUALITY_BAD && spellLevel > 1)
    {
        spellLevel = RandomNext(spellLevel, sSeedName) + 1; //ex: for spellLevel 5, reroll between 1 and 5 (0-4 + 1)
    }
    else if (nLootQuality == LOOT_QUALITY_AVERAGE && spellLevel > 3)
    {
        spellLevel = RandomNext(spellLevel - 1, sSeedName) + 2; //ex: for spellLevel 5, reroll between 2 and 5 (0-3 + 2)
    }
    else if (nLootQuality == LOOT_QUALITY_GOOD && spellLevel > 4)
    {
        spellLevel = RandomNext(spellLevel - 2, sSeedName) + 3; //ex: for spellLevel 5, reroll between 3 and 5 (0-2 + 3)
    }

    result.itemResRef = GetRandomSpellScrollResRef(sSeedName, spellLevel);
    LogInfo("Generating scroll loot");
    return result;
}

struct TreasureChestLoot _HealingKitLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;

    switch (nLootQuality)
    {
        case LOOT_QUALITY_USELESS:
            result.itemResRef = "nw_it_medkit001";
            break;
        case LOOT_QUALITY_BAD:
            result.itemResRef = "nw_it_medkit001";
            break;
        case LOOT_QUALITY_AVERAGE:
            result.itemResRef = "nw_it_medkit002";
            break;
        case LOOT_QUALITY_GOOD:
            result.itemResRef = "nw_it_medkit003";
            break;
        case LOOT_QUALITY_GREAT:
            result.itemResRef = "nw_it_medkit004";
            break;
        case LOOT_QUALITY_EXTRAORDINARY:
            result.itemResRef = "nw_it_medkit004";
            result.itemStackSize = 3;
            break;
    }

    LogInfo("Generating healing kit loot");
    return result;
}

struct TreasureChestLoot _AmmoAndThrown(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nEffectiveLevel < 1)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }
    else if (nEffectiveLevel == 1)
    {
        switch (RandomNext(6, sSeedName))
        {
            case 0:
                result.itemResRef = "it_arrow00a";
                result.itemStackSize = 99;
                break;
            case 1:
                result.itemResRef = "it_bolt00a";
                result.itemStackSize = 99;
                break;
            case 2:
                result.itemResRef = "it_bullet00a";
                result.itemStackSize = 99;
                break;
            case 3:
                result.itemResRef = "it_dart00a";
                result.itemStackSize = 50;
                break;
            case 4:
                result.itemResRef = "it_shuriken00a";
                result.itemStackSize = 50;
                break;
            case 5:
                result.itemResRef = "it_throwaxe00a";
                result.itemStackSize = 50;
                break;
        }
    }
    else if (nEffectiveLevel == 2)
    {
        switch (RandomNext(6, sSeedName))
        {
            case 0:
                result.itemResRef = "it_arrow01a";
                result.itemStackSize = 99;
                break;
            case 1:
                result.itemResRef = "it_bolt01a";
                result.itemStackSize = 99;
                break;
            case 2:
                result.itemResRef = "it_bullet01a";
                result.itemStackSize = 99;
                break;
            case 3:
                result.itemResRef = "it_dart01a";
                result.itemStackSize = 50;
                break;
            case 4:
                result.itemResRef = "it_shuriken01a";
                result.itemStackSize = 50;
                break;
            case 5:
                result.itemResRef = "it_throwaxe01a";
                result.itemStackSize = 50;
                break;
        }
    }
    else if (nEffectiveLevel == 3)
    {
        switch (RandomNext(6, sSeedName))
        {
            case 0:
                result.itemResRef = "it_arrow02a";
                result.itemStackSize = 99;
                break;
            case 1:
                result.itemResRef = "it_bolt02a";
                result.itemStackSize = 99;
                break;
            case 2:
                result.itemResRef = "it_bullet02a";
                result.itemStackSize = 99;
                break;
            case 3:
                result.itemResRef = "it_dart02a";
                result.itemStackSize = 50;
                break;
            case 4:
                result.itemResRef = "it_shuriken02a";
                result.itemStackSize = 50;
                break;
            case 5:
                result.itemResRef = "it_throwaxe02a";
                result.itemStackSize = 50;
                break;
        }
    }
    else if (nEffectiveLevel == 4)
    {
        switch (RandomNext(6, sSeedName))
        {
            case 0:
                result.itemResRef = "it_arrow03a";
                result.itemStackSize = 99;
                break;
            case 1:
                result.itemResRef = "it_bolt03a";
                result.itemStackSize = 99;
                break;
            case 2:
                result.itemResRef = "it_bullet03a";
                result.itemStackSize = 99;
                break;
            case 3:
                result.itemResRef = "it_dart03a";
                result.itemStackSize = 50;
                break;
            case 4:
                result.itemResRef = "it_shuriken03a";
                result.itemStackSize = 50;
                break;
            case 5:
                result.itemResRef = "it_throwaxe03a";
                result.itemStackSize = 50;
                break;
        }
    }
    LogInfo("Generating ammo/thrown loot");
    return result;
}

struct TreasureChestLoot _WeaponLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nEffectiveLevel < 1)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }

    switch (RandomNext(36, sSeedName))
    {
        case 0:
            result.itemResRef = "it_bastard00a";
            break;
        case 1:
            result.itemResRef = "it_battleaxe00a";
            break;
        case 2:
            result.itemResRef = "it_club00a";
            break;
        case 3:
            result.itemResRef = "it_dagger00a";
            break;
        case 4:
            result.itemResRef = "it_diremace00a";
            break;
        case 5:
            result.itemResRef = "it_doubleaxe00a";
            break;
        case 6:
            result.itemResRef = "it_dwarfaxe00a";
            break;
        case 7:
            result.itemResRef = "it_greataxe00a";
            break;
        case 8:
            result.itemResRef = "it_greatsw00a";
            break;
        case 9:
            result.itemResRef = "it_halberd00a";
            break;
        case 10:
            result.itemResRef = "it_handaxe00a";
            break;
        case 11:
            result.itemResRef = "it_hflail00a";
            break;
        case 12:
            result.itemResRef = "it_hxbow00a";
            break;
        case 13:
            result.itemResRef = "it_kama00a";
            break;
        case 14:
            result.itemResRef = "it_katana00a";
            break;
        case 15:
            result.itemResRef = "it_kukri00a";
            break;
        case 16:
            result.itemResRef = "it_lbow00a";
            break;
        case 17:
            result.itemResRef = "it_lflail00a";
            break;
        case 18:
            result.itemResRef = "it_lhammer00a";
            break;
        case 19:
            result.itemResRef = "it_longsw00a";
            break;
        case 20:
            result.itemResRef = "it_lxbow00a";
            break;
        case 21:
            result.itemResRef = "it_mace00a";
            break;
        case 22:
            result.itemResRef = "it_mornstar00a";
            break;
        case 23:
            result.itemResRef = "it_rapier00a";
            break;
        case 24:
            result.itemResRef = "it_sbow00a";
            break;
        case 25:
            result.itemResRef = "it_scimitar00a";
            break;
        case 26:
            result.itemResRef = "it_scythe00a";
            break;
        case 27:
            result.itemResRef = "it_shortsw00a";
            break;
        case 28:
            result.itemResRef = "it_sickle00a";
            break;
        case 29:
            result.itemResRef = "it_sling00a";
            break;
        case 30:
            result.itemResRef = "it_spear00a";
            break;
        case 31:
            result.itemResRef = "it_staff00a";
            break;
        case 32:
            result.itemResRef = "it_trident00a";
            break;
        case 33:
            result.itemResRef = "it_twosword00a";
            break;
        case 34:
            result.itemResRef = "it_whammer00a";
            break;
        case 35:
            result.itemResRef = "it_whip00a";
            break;
    }

    if (nEffectiveLevel > 1 && RandomNext(37, sSeedName) == 0)
        result.itemResRef = "it_mstaff01a";

    if (nEffectiveLevel == 2)
    {
        result.itemResRef = GetStringLeft(result.itemResRef, GetStringLength(result.itemResRef)-3) + "01a";
    }
    else if (nEffectiveLevel == 3)
    {
        result.itemResRef = GetStringLeft(result.itemResRef, GetStringLength(result.itemResRef)-3) + "02a";
    }
    if (nEffectiveLevel == 4)
    {
        result.itemResRef = GetStringLeft(result.itemResRef, GetStringLength(result.itemResRef)-3) + "03a";
    }

    LogInfo("Generating weapon loot");
    return result;
}

struct TreasureChestLoot _ArmorLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;

    if (nEffectiveLevel < 1)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }

    if (nEffectiveLevel == 1)
    {
        switch (RandomNext(7, sSeedName))
        {
            case 0:
                result.itemResRef = "it_armor01_00a";
                break;
            case 1:
                result.itemResRef = "it_armor02_00a";
                break;
            case 2:
                result.itemResRef = "it_armor04_00a";
                break;
            case 3:
                result.itemResRef = "it_armor06_00a";
                break;
            case 4:
                result.itemResRef = "it_armor08_00a";
                break;
            case 5:
            case 6:
                result.itemResRef = "it_sshield00a";
                break;
        }
    }
    else if (nEffectiveLevel == 2)
    {
        switch (RandomNext(12, sSeedName))
        {
            case 0:
                result.itemResRef = "it_armor00_01a";
                break;
            case 1:
                result.itemResRef = "it_armor01_01a";
                break;
            case 2:
                result.itemResRef = "it_armor02_01a";
                break;
            case 3:
                result.itemResRef = "it_armor04_01a";
                break;
            case 4:
                result.itemResRef = "it_armor05_01a";
                break;
            case 5:
                result.itemResRef = "it_armor08_01a";
                break;
            case 6:
                result.itemResRef = "it_armor09_01a";
                break;
            case 7:
            case 8:
            case 9:
                result.itemResRef = "it_lshield01a";
                break;
            case 10:
            case 11:
                result.itemResRef = "it_helmet01a";
                break;
        }
    }
    else if (nEffectiveLevel == 3)
    {
        switch (RandomNext(15, sSeedName))
        {
            case 0:
                result.itemResRef = "it_armor00_02a";

                break;
            case 1:
                result.itemResRef = "it_armor00_02b";
                break;
            case 2:
                result.itemResRef = "it_armor01_02a";
                break;
            case 3:
                result.itemResRef = "it_armor02_02a";
                break;
            case 4:
                result.itemResRef = "it_armor04_02a";
                break;
            case 5:
                result.itemResRef = "it_armor06_02a";
                break;
            case 6:
                result.itemResRef = "it_armor07_02a";
                break;
            case 7:
                result.itemResRef = "it_armor10_02a";
                break;
            case 8:
                result.itemResRef = "it_armor11_02a";
                break;
            case 9:
            case 10:
            case 11:
                result.itemResRef = "it_tshield02a";
                break;
            case 12:
            case 13:
            case 14:
                result.itemResRef = "it_helmet02a";
                break;
        }
    }
    else if (nEffectiveLevel == 4)
    {
        switch (RandomNext(13, sSeedName))
        {
            case 0:
                result.itemResRef = "it_armor00_03a";
                break;
            case 1:
                result.itemResRef = "it_armor01_03b";
                break;
            case 2:
                result.itemResRef = "it_armor02_03a";
                break;
            case 3:
                result.itemResRef = "it_armor04_03a";
                break;
            case 4:
                result.itemResRef = "it_armor05_03a";
                break;
            case 5:
                result.itemResRef = "it_armor08_03a";
                break;
            case 6:
                result.itemResRef = "it_armor10_03a";
                break;
            case 7:
                result.itemResRef = "it_armor11_03a";
                break;
            case 8:
                result.itemResRef = "it_armor12_03a";
                break;
            case 9:
            case 10:
            case 11:
            case 12:
                result.itemResRef = "it_helmet03a";
                break;
        }
    }
    return result;
}

struct TreasureChestLoot _AccessoryLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;

    if (nEffectiveLevel < 2)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }
    else if (nEffectiveLevel == 2)
    {
        switch (RandomNext(14, sSeedName))
        {
            case 0:
                result.itemResRef = "it_amulet01a";
                break;
            case 1:
                result.itemResRef = "it_amulet01b";
                break;
            case 2:
                result.itemResRef = "it_amulet01c";
                break;
            case 3:
            case 4:
                result.itemResRef = "it_belt01a";
                break;
            case 5:
                result.itemResRef = "it_boots01a";
                break;
            case 6:
                result.itemResRef = "it_boots01b";
                break;
            case 7:
            case 8:
                result.itemResRef = "it_cloak01a";
                break;
            case 9:
                result.itemResRef = "it_gloves01a";
                break;
            case 10:
                result.itemResRef = "it_gloves01b";
                break;
            case 11:
                result.itemResRef = "it_gloves01c";
                break;
            case 12:
                result.itemResRef = "it_ring01a";
                break;
            case 13:
                result.itemResRef = "it_ring01b";
                break;
        }
    }
    else if (nEffectiveLevel == 3)
    {
        switch (RandomNext(13, sSeedName))
        {
            case 0:
            case 1:
                result.itemResRef = "it_amulet02a";
                break;
            case 2:
                result.itemResRef = "it_belt02a";
                break;
            case 3:
                result.itemResRef = "it_boots02a";
                break;
            case 4:
                result.itemResRef = "it_boots02b";
                break;
            case 5:
            case 6:
                result.itemResRef = "it_cloak02a";
                break;
            case 7:
                result.itemResRef = "it_bracers02a";
                break;
            case 8:
                result.itemResRef = "it_gloves02a";
                break;
            case 9:
                result.itemResRef = "it_gloves02b";
                break;
            case 10:
                result.itemResRef = "it_ring02a";
                break;
            case 11:
                result.itemResRef = "it_ring02b";
                break;
            case 12:
                result.itemResRef = "it_ring02c";
                break;
        }
    }
    else if (nEffectiveLevel == 4)
    {
        switch (RandomNext(32, sSeedName))
        {
            case 0:
                result.itemResRef = "it_amulet03a";
                break;
            case 1:
                result.itemResRef = "it_amulet03b";
                break;
            case 2:
                result.itemResRef = "it_amulet03c";
                break;
            case 3:
                result.itemResRef = "it_amulet03d";
                break;
            case 4:
                result.itemResRef = "it_amulet03e";
                break;
            case 5:
                result.itemResRef = "it_amulet03f";
                break;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                result.itemResRef = "it_belt03a";
                break;
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                result.itemResRef = "it_boots03a";
                break;
            case 16:
            case 17:
                result.itemResRef = "it_cloak03a";
                break;
            case 18:
            case 19:
                result.itemResRef = "it_cloak03b";
                break;
            case 20:
                result.itemResRef = "it_bracers03a";
                break;
            case 21:
                result.itemResRef = "it_gloves03a";
                break;
            case 22:
                result.itemResRef = "it_gloves03b";
                break;
            case 23:
                result.itemResRef = "it_gloves03c";
                break;
            case 24:
                result.itemResRef = "it_gloves03d";
                break;
            case 25:
                result.itemResRef = "it_gloves03e";
                break;
            case 26:
                result.itemResRef = "it_gloves03f";
                break;
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
                result.itemResRef = "it_ring03a";
                break;
        }
    }
    return result;
}

struct TreasureChestLoot _OtherEqLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    if (nEffectiveLevel < 2 || RandomNext(3, sSeedName) == 0)
        result = _ArmorLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality, nForStore);
    else
        result = _AccessoryLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality, nForStore);

    LogInfo("Generating other eq loot");
    return result;
}

struct TreasureChestLoot _WandLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nEffectiveLevel < 2)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }
    else if (nEffectiveLevel == 2)
    {
        result.itemResRef = "it_wand01a";
    }
    else
    {
        result.itemResRef = "it_wand02a";
    }
    LogInfo("Generating wand loot");
    return result;
}

struct TreasureChestLoot _RodLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    if (nEffectiveLevel < 1)
    {
        if (nForStore)
            return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
    }
    else
    {
        switch (RandomNext(3, sSeedName))
        {
            case 0:
                result.itemResRef = "it_rod01a";
                break;
            case 1:
                result.itemResRef = "it_rod01b";
                break;
            case 2:
                result.itemResRef = "it_rod01c";
                break;
        }
    }
    LogInfo("Generating rod loot");
    return result;
}


struct TreasureChestLoot _RecipeLoot(string sSeedName, int nEffectiveLevel, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    struct TreasureChestLoot result;
    result.itemStackSize = 1;
    int recipeType = RandomNext(3, sSeedName);
    if (recipeType == 0)
    {
        //Traps
        if (nEffectiveLevel < 2)
        {
            if (nForStore)
                return _MiscLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
            return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        }
        switch (RandomNext(3, sSeedName))
        {
            case 0:
                result.itemResRef = "it_rectrap01c";
                break;
            case 1:
                result.itemResRef = "it_rectrap01b";
                break;
            case 2:
                result.itemResRef = "it_rectrap01c";
                break;
        }
    }
    else if (recipeType == 1)
    {
        //Armor
        if (nEffectiveLevel < 2)
        {
            return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        }
        else if (nEffectiveLevel == 1)
        {
            result.itemResRef = "it_recarm05_01a";
        }
        else if (nEffectiveLevel == 2)
        {
            result.itemResRef = "it_recarm10_02a";
        }
        else if (nEffectiveLevel == 3)
        {
            result.itemResRef = "it_recarm02_03a";
        }
    }
    else if (recipeType == 2)
    {
        //Weapons
        if (nEffectiveLevel < 2)
        {
            return _GoldLoot(sSeedName, nEffectiveLevel, nLevel, nLootQuality);
        }
        else if (nEffectiveLevel == 2)
        {
            switch (RandomNext(4, sSeedName))
            {
                case 0:
                    result.itemResRef = "it_recarrow01a";
                    break;
                case 1:
                    result.itemResRef = "it_recdart01a";
                    break;
                case 2:
                    result.itemResRef = "it_reckama01a";
                    break;
                case 3:
                    result.itemResRef = "it_rectrident01a";
                    break;
            }
        }
        else if (nEffectiveLevel == 3)
        {
            switch (RandomNext(4, sSeedName))
            {
                case 0:
                    result.itemResRef = "it_recbolt02a";
                    break;
                case 1:
                    result.itemResRef = "it_recdagger02a";
                    break;
                case 2:
                    result.itemResRef = "it_recshurike02a";
                    break;
                case 3:
                    result.itemResRef = "it_recspear02a";
                    break;
            }
        }
        else if (nEffectiveLevel == 4)
        {
            switch (RandomNext(4, sSeedName))
            {
                case 0:
                    result.itemResRef = "it_recbullet03a";
                    break;
                case 1:
                    result.itemResRef = "it_reclongsw03a";
                    break;
                case 2:
                    result.itemResRef = "it_recmornsta03a";
                    break;
                case 3:
                    result.itemResRef = "it_recthrowax03a";
                    break;
            }
        }
    }
    LogInfo("Generating recipe loot");
    return result;
}

struct TreasureChestLoot GetLoot(string sSeedName, int nLevel, int nLootQuality, int nForStore=FALSE)
{
    int effectiveLevel = nLevel - 3 + nLootQuality;

    if (nLootQuality == LOOT_QUALITY_USELESS && nLevel == 1)
    {
        switch (RandomNext(4, sSeedName))
        {
            case 0:
                return _PotionLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
            case 1:
                return _HealingKitLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
            case 2:
                return _MiscLoot(sSeedName, effectiveLevel, nLevel, nLootQuality);
            case 3:
                return _GoldLoot(sSeedName, effectiveLevel, nLevel, nLootQuality);
            default:
                LogWarning("Unexpected switch case (inc_loot, GetLoot, [1])");
                return _GoldLoot(sSeedName, effectiveLevel, nLevel, nLootQuality);
        }
    }
    else
    {
        int lootType = RandomNext(100, sSeedName);
        if (lootType < 3) //3% chance for rod
            return _RodLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 6) //3% chance for wand
            return _WandLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 12) //6% chance for scroll
            return _ScrollLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 24) //12% chance for potion
            return _PotionLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 30) //6% chance for trap
            return _TrapLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 42) //12% chance for healing kit
            return _HealingKitLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 48) //6% chance for recipe
            return _RecipeLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 54) //6% chance for miscellaneous
            return _MiscLoot(sSeedName, effectiveLevel, nLevel, nLootQuality);
        else if (lootType < 60) //6% chance for gold
            return _GoldLoot(sSeedName, effectiveLevel, nLevel, nLootQuality);
        else if (lootType < 84) //24% chance for weapon
            return _WeaponLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else if (lootType < 92) //8% chance for equipment piece
            return _OtherEqLoot(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
        else //8% chance for ammo or a throwing weapon
            return _AmmoAndThrown(sSeedName, effectiveLevel, nLevel, nLootQuality, nForStore);
    }

    LogWarning("Unexpected switch case (inc_loot, GetLoot, [2])");
    return _GoldLoot(sSeedName, effectiveLevel, nLevel, nLootQuality);
}

string _GetHealingPotionResRef(int nEffectiveLevel)
{
    switch (nEffectiveLevel)
    {
        case -1:
        case 0:
        case 1:
            return "nw_it_mpotion001"; //Light
        case 2:
            return "nw_it_mpotion020"; //Moderate
        case 3:
        case 4:
            return "nw_it_mpotion002"; //Serious
        case 5:
        case 6:
            return "nw_it_mpotion003"; //Critical
        default:
            return "nw_it_mpotion012"; //Heal
    }
    return "";
}

int _RandomizeQuality(string sSeedName)
{
    int qualityNumber = RandomNext(100, sSeedName);
    int quality;
    if (qualityNumber < 5)
        quality = LOOT_QUALITY_USELESS;
    else if (qualityNumber < 15)
        quality = LOOT_QUALITY_BAD;
    else if (qualityNumber < 50)
        quality = LOOT_QUALITY_AVERAGE;
    else if (qualityNumber < 85)
        quality = LOOT_QUALITY_GOOD;
    else if (qualityNumber < 95)
        quality = LOOT_QUALITY_GREAT;
    else
        quality = LOOT_QUALITY_EXTRAORDINARY;
    return quality;
}

object CreateStoreWithItems(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint, int nFirstRegion=FALSE)
{
    string resref = "sto_empty";
    object store = CreateObject(OBJECT_TYPE_STORE, resref, GetLocation(oDestinationWaypoint));

    //Healing potions - about 15 in total
    int potionsNum = RandomNext(7, sSeedName) + 12;
    int i;
    for (i = 0; i < potionsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        string resref = _GetHealingPotionResRef(effectiveLevel);
        CreateItemOnObject(resref, store, 1);
    }

    //Torches - anywhere from 0 to 2
    int torchesNum = RandomNext(3, sSeedName);
    for (i = 0; i < torchesNum; i++)
    {
        CreateItemOnObject("nw_it_torch001", store, 1);
    }

    //Healing kits - like potions, approximately 15
    int kitsNum = RandomNext(7, sSeedName) + 12;
    for (i = 0; i < kitsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        struct TreasureChestLoot kitTreasure = _HealingKitLoot("", 0, 0, quality);
        CreateItemOnObject(kitTreasure.itemResRef, store, 1);
    }

    //A few basic armors (and a shield) - anywhere from 2 to 6
    int armorsNum = RandomNext(5, sSeedName) + 2;
    for (i = 0; i < armorsNum; i++)
    {
        string resref = "";
        switch (RandomNext(6, sSeedName))
        {
            case 0:
                resref = "it_armor01_00a";
                break;
            case 1:
                resref = "it_armor02_00a";
                break;
            case 2:
                resref = "it_armor04_00a";
                break;
            case 3:
                resref = "it_armor06_00a";
                break;
            case 4:
                resref = "it_armor08_00a";
                break;
            case 5:
                resref = "it_sshield00a";
                break;
        }
        CreateItemOnObject(resref, store, 1);
    }

    //Basic ammunition
    for (i = 0; i < RandomNext(2, sSeedName)+1; i++)
        CreateItemOnObject("it_arrow00a", store, 99);
    for (i = 0; i < RandomNext(2, sSeedName)+1; i++)
        CreateItemOnObject("it_bolt00a", store, 99);
    for (i = 0; i < RandomNext(2, sSeedName)+1; i++)
        CreateItemOnObject("it_bullet00a", store, 99);
    for (i = 0; i < RandomNext(2, sSeedName)+1; i++)
        CreateItemOnObject("it_dart00a", store, 50);
    for (i = 0; i < RandomNext(2, sSeedName)+1; i++)
        CreateItemOnObject("it_shuriken00a", store, 50);
    for (i = 0; i < RandomNext(2, sSeedName)+1; i++)
        CreateItemOnObject("it_throwaxe00a", store, 50);

    //Basic crafting components
    int componentsNum = RandomNext(8) + 3;
    for (i = 0; i < componentsNum; i++)
    {
        string resref = "";
        switch (RandomNext(3, sSeedName))
        {
            case 0:
                resref = "it_emptybottle";
                break;
            case 1:
                resref = "it_bonewand";
                break;
            case 2:
                resref = "it_blankscroll";
                break;
        }
        CreateItemOnObject(resref, store, 1);
    }

    //If this is the first region, add basic weapons of every type
    if (nFirstRegion)
    {
        CreateItemOnObject("it_bastard00a", store);
        CreateItemOnObject("it_battleaxe00a", store);
        CreateItemOnObject("it_club00a", store);
        CreateItemOnObject("it_dagger00a", store);
        CreateItemOnObject("it_diremace00a", store);
        CreateItemOnObject("it_doubleaxe00a", store);
        CreateItemOnObject("it_dwarfaxe00a", store);
        CreateItemOnObject("it_greataxe00a", store);
        CreateItemOnObject("it_greatsw00a", store);
        CreateItemOnObject("it_halberd00a", store);
        CreateItemOnObject("it_handaxe00a", store);
        CreateItemOnObject("it_hflail00a", store);
        CreateItemOnObject("it_hxbow00a", store);
        CreateItemOnObject("it_kama00a", store);
        CreateItemOnObject("it_katana00a", store);
        CreateItemOnObject("it_kukri00a", store);
        CreateItemOnObject("it_lbow00a", store);
        CreateItemOnObject("it_lflail00a", store);
        CreateItemOnObject("it_lhammer00a", store);
        CreateItemOnObject("it_longsw00a", store);
        CreateItemOnObject("it_lxbow00a", store);
        CreateItemOnObject("it_mace00a", store);
        CreateItemOnObject("it_mornstar00a", store);
        CreateItemOnObject("it_rapier00a", store);
        CreateItemOnObject("it_sbow00a", store);
        CreateItemOnObject("it_scimitar00a", store);
        CreateItemOnObject("it_scythe00a", store);
        CreateItemOnObject("it_shortsw00a", store);
        CreateItemOnObject("it_sickle00a", store);
        CreateItemOnObject("it_sling00a", store);
        CreateItemOnObject("it_spear00a", store);
        CreateItemOnObject("it_staff00a", store);
        CreateItemOnObject("it_trident00a", store);
        CreateItemOnObject("it_twosword00a", store);
        CreateItemOnObject("it_whammer00a", store);
        CreateItemOnObject("it_whip00a", store);
    }


    //Other stuff can be generated like regular loot
    int anythingNum = 51 + RandomNext(10, sSeedName);
    for (i = 0; i < anythingNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        struct TreasureChestLoot item = GetLoot(sSeedName, nBiomeStartingLevel, quality, TRUE);
        int j;
        if (item.itemStackSize >= 50)
            CreateItemOnObject(item.itemResRef, store, item.itemStackSize);
        else for (j = 0; j < item.itemStackSize; j++)
                CreateItemOnObject(item.itemResRef, store, 1);
    }

    SetStoreGold(store, 1000);
    return store;
}

object CreateThiefGuildStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    object store = CreateObject(OBJECT_TYPE_STORE, "sto_thiefguild", GetLocation(oDestinationWaypoint));

    //Thieves' tools
    int toolsNum = 3 + RandomNext(3, sSeedName);
    int i;
    for (i = 0; i < toolsNum; i++)
    {
        string resref;
        switch (RandomNext(3))
        {
            case 0:
                resref = "nw_it_picks001";
                break;
            case 1:
                resref = "nw_it_picks002";
                break;
            case 2:
                resref = "x2_it_picks001";
                break;
        }
        CreateItemOnObject(resref, store, 1);
    }

    //Traps
    int trapsNum = RandomNext(5, sSeedName) + 2;
    for (i = 0; i < trapsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        if (quality <= LOOT_QUALITY_BAD)
            quality = LOOT_QUALITY_AVERAGE;
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        struct TreasureChestLoot trapTreasure = _TrapLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(trapTreasure.itemResRef, store, 1);
    }

    //Dusts of appearance and disappearance
    int appDustsNum = RandomNext(5) - 2;
    for (i = 0; i < appDustsNum; i++)
    {
        CreateItemOnObject("x0_it_mthnmisc05", store);
    }
    int disDustsNum = RandomNext(5) - 3;
    for (i = 0; i < disDustsNum; i++)
    {
        CreateItemOnObject("x0_it_mthnmisc06", store);
    }

    //Poisons
    int poisonsNum = RandomNext(5, sSeedName) + 2;
    for (i = 0; i < poisonsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        struct TreasureChestLoot poisonTreasure = _PoisonLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality);
        CreateItemOnObject(poisonTreasure.itemResRef, store, 1);
    }

    //Rogue potions
    int potionsNum = RandomNext(4, sSeedName) + 1;
    for (i = 0; i < potionsNum; i++)
    {
        string resref;
        switch (RandomNext(3))
        {
            case 0:
                resref = "nw_it_mpotion008";
                break;
            case 1:
                resref = "nw_it_mpotion014";
                break;
            case 2:
                resref = "nw_it_mpotion017";
                break;
        }
        if (_RandomizeQuality(sSeedName) == LOOT_QUALITY_EXTRAORDINARY)
            resref = "nw_it_mpotion004";
        CreateItemOnObject(resref, store, 1);
    }

    return store;
}

object CreateTempleStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    object store = CreateObject(OBJECT_TYPE_STORE, "sto_emptysell", GetLocation(oDestinationWaypoint));

    //Soulstones
    int stonesNum = RandomNext(3, sSeedName) + 1;
    int i;
    for (i = 0; i < stonesNum; i++)
    {
        CreateItemOnObject("it_soulstone", store);
    }

    //Healing potions
    int potionsNum = RandomNext(5, sSeedName) + 8;
    for (i = 0; i < potionsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        string resref = _GetHealingPotionResRef(effectiveLevel);
        CreateItemOnObject(resref, store, 1);
    }

    //Other potions
    int otherPotsNum = RandomNext(4, sSeedName) + 1;
    for (i = 0; i < otherPotsNum; i++)
    {
        string resref;
        switch (RandomNext(10, sSeedName))
        {
            case 0:
            case 1:
            case 2:
                resref = "nw_it_mpotion006";
                break;
            case 3:
            case 4:
                resref = "nw_it_mpotion009";
                break;
            case 5:
            case 6:
                resref = "nw_it_mpotion016";
                break;
            case 7:
            case 8:
                resref = "nw_it_mpotion011";
                break;
            case 9:
                resref = "nw_it_mpotion007";
                break;
        }
        CreateItemOnObject(resref, store, 1);
    }

    return store;
}

object CreateMageGuildStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    object store = CreateObject(OBJECT_TYPE_STORE, "sto_emptysell", GetLocation(oDestinationWaypoint));
    int i;

    //Magic crafting items
    int craftNum = RandomNext(5, sSeedName) + 6;
    for (i = 0; i < craftNum; i++)
    {
        string resref;
        switch (RandomNext(3, sSeedName))
        {
            case 0:
                resref = "it_emptybottle";
                break;
            case 1:
                resref = "it_bonewand";
                break;
            case 2:
                resref = "it_blankscroll";
                break;
        }
        CreateItemOnObject(resref, store, 1);
    }

    //Magic scrolls
    int scrollsNum = RandomNext(10, sSeedName) + 5;
    for (i = 0; i < scrollsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        struct TreasureChestLoot scroll = _ScrollLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(scroll.itemResRef, store, 1);
    }

    //Magical accessories
    int accessoryNum = RandomNext(3, sSeedName) + 2;
    if (nBiomeStartingLevel == 1)
        accessoryNum -= 1;
    for (i = 0; i < accessoryNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        if (effectiveLevel < 2)
            effectiveLevel = 2;
        struct TreasureChestLoot item = _AccessoryLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(item.itemResRef, store, 1);
    }

    //Rods and wands
    int wandsNum = RandomNext(3, sSeedName) + 2;
    for (i = 0; i < wandsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        struct TreasureChestLoot item;
        if (effectiveLevel < 1)
            effectiveLevel = 1;
        if (effectiveLevel < 2 || RandomNext(3, sSeedName) == 0)
            item = _RodLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        else
            item = _WandLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(item.itemResRef, store, 1);
    }

    //Potions
    int potsNum = RandomNext(4, sSeedName) + 3;
    for (i = 0; i < potsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        if (quality == LOOT_QUALITY_USELESS)
            quality = LOOT_QUALITY_BAD;
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        struct TreasureChestLoot item = _MageGuildPotionLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(item.itemResRef, store, 1);
    }

    return store;
}

object CreateArmoryStore(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    object store = CreateObject(OBJECT_TYPE_STORE, "sto_emptysell", GetLocation(oDestinationWaypoint));
    int i;

    //Weapons
    int weaponsNum = RandomNext(5, sSeedName) + 23;
    for (i = 0; i < weaponsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        if (effectiveLevel < 1)
            effectiveLevel = 1;
        struct TreasureChestLoot item = _WeaponLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(item.itemResRef, store, item.itemStackSize);
    }

    //Armors
    int armorsNum = RandomNext(5, sSeedName) + 5;
    for (i = 0; i < armorsNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        if (effectiveLevel < 1)
            effectiveLevel = 1;
        struct TreasureChestLoot item = _ArmorLoot(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(item.itemResRef, store, 1);
    }

    //Ammo and thrown
    int ammoNum = RandomNext(5, sSeedName) + 3;
    for (i = 0; i < ammoNum; i++)
    {
        int quality = _RandomizeQuality(sSeedName);
        int effectiveLevel = nBiomeStartingLevel - 3 + quality;
        if (effectiveLevel < 1)
            effectiveLevel = 1;
        struct TreasureChestLoot item = _AmmoAndThrown(sSeedName, effectiveLevel, nBiomeStartingLevel, quality, TRUE);
        CreateItemOnObject(item.itemResRef, store, item.itemStackSize);
    }

    return store;
}

void SpawnRandomTreasureInInventory(string sSeedName, object oInventory, int nLootNumber)
{
    object area = GetArea(oInventory);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    string biomeScript = GetBiomeScript(biome);
    int lvl = GetRegionStartingLevel(biome);

    int inventoryGold = 0;
    int totalQuality = 0; //it will be anywhere from 2 to 24, most often around 10-11.
    int i;
    for (i = 0; i < nLootNumber; i++)
    {
        int qualityNumber = RandomNext(100, sSeedName);
        int quality;
        if (qualityNumber < 5)
            quality = LOOT_QUALITY_USELESS;
        else if (qualityNumber < 15)
            quality = LOOT_QUALITY_BAD;
        else if (qualityNumber < 50)
            quality = LOOT_QUALITY_AVERAGE;
        else if (qualityNumber < 85)
            quality = LOOT_QUALITY_GOOD;
        else if (qualityNumber < 95)
            quality = LOOT_QUALITY_GREAT;
        else
            quality = LOOT_QUALITY_EXTRAORDINARY;
        struct TreasureChestLoot loot = GetBiomeLoot(biomeScript, sSeedName, lvl, quality);
        inventoryGold += loot.gold;
        object item = CreateItemOnObject(loot.itemResRef, oInventory, loot.itemStackSize);
        if (!GetIsObjectValid(item) && loot.gold == 0)
        {
            LogWarning("No item spawned in inventory, resRef: " + loot.itemResRef);
        }
        SetStolenFlag(item, TRUE);
        totalQuality += quality;
    }
    CreateItemOnObject("nw_it_gold001", oInventory, inventoryGold);
}