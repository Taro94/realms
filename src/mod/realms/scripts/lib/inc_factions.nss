#include "inc_common"

// Library containing various faction functionality

const int FACTION_HOSTILE = 1;
const int FACTION_COMMONER = 2;
const int FACTION_DEFENDER = 3;
const int FACTION_MERCHANT = 4;
const int FACTION_COMPANIONS = 5;
const int FACTION_REST_ENCOUNTER = 6;
const int FACTION_UNDEAD = 7;
const int FACTION_BANDITS = 8;
const int FACTION_TEMPORARY_NEUTRAL = 9;
const int FACTION_WILDLIFE_HOSTILE = 10;
const int FACTION_WILDLIFE_PEACEFUL = 11;
const int FACTION_ALLY = 12;

// Sets the creature's faction to a faction using one of the FACTION_* constants.
void SetFaction(object oCreature, int nFaction);

// Returns a FACTION_* constant based on oCreature's current faction. 
// Returns 0 if the creature belongs to a PC faction (party) and on error.
int GetFaction(object oCreature);

string _FactionConstantToHolderResRef(int nFaction)
{
    switch (nFaction)
    {
        case FACTION_HOSTILE:
            return "cre_fachostile";
        case FACTION_COMMONER:
            return "cre_faccommoner";
        case FACTION_DEFENDER:
            return "cre_facdefender";
        case FACTION_MERCHANT:
            return "cre_facmerch";
        case FACTION_COMPANIONS:
            return "cre_facneutral";
        case FACTION_REST_ENCOUNTER:
            return "cre_facrestenc";
        case FACTION_UNDEAD:
            return "cre_facundead";
        case FACTION_BANDITS:
            return "cre_facbandits";
        case FACTION_TEMPORARY_NEUTRAL:
            return "cre_facbanditsn";
        case FACTION_WILDLIFE_HOSTILE:
            return "cre_facwildhost";
        case FACTION_ALLY:
            return "cre_facally";
    }
    return "";
}

object _GetFactionHolder(int nFaction)
{
    string resref = _FactionConstantToHolderResRef(nFaction);
    string varName = "FACTION_HOLDER_"+resref;
    object mod = GetModule();
    object facHolder = GetLocalObject(mod, varName);
    if (!GetIsObjectValid(facHolder))
    {
        facHolder = CreateObject(OBJECT_TYPE_CREATURE, resref, Limbo());
        SetLocalObject(mod, varName, facHolder);
    }
    return facHolder;
}

void SetFaction(object oCreature, int nFaction)
{
    object facHolder = _GetFactionHolder(nFaction);
    ChangeFaction(oCreature, facHolder);
}

int GetFaction(object oCreature)
{
    int i = 1;
    while (TRUE)
    {
        object facHolder = _GetFactionHolder(i++);
        if (!GetIsObjectValid(facHolder))
            return 0;
        if (GetFactionEqual(oCreature, facHolder))
            return i;
    }
    return 0;
}