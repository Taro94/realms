#include "inc_random"
#include "inc_language"

string RandomGoblinName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(50, sSeedName))
    {
        case 0: name = "Glar"; break;
        case 1: name = "Vobuld"; break;
        case 2: name = "Cruk"; break;
        case 3: name = "Crig"; break;
        case 4: name = "Fruts"; break;
        case 5: name = "Pyug"; break;
        case 6: name = "Chug"; break;
        case 7: name = "Clyr"; break;
        case 8: name = "Jiolb"; break;
        case 9: name = "Zieq"; break;
        case 10: name = "Aar"; break;
        case 11: name = "Ykx"; break;
        case 12: name = "Krird"; break;
        case 13: name = "Dynk"; break;
        case 14: name = "Glagrulk"; break;
        case 15: name = "Gnok"; break;
        case 16: name = "Egz"; break;
        case 17: name = "Aabhaags"; break;
        case 18: name = "Triebs"; break;
        case 19: name = "Wiogdurx"; break;
        case 20: name = "Pruzikx"; break;
        case 21: name = "Buics"; break;
        case 22: name = "Dreesb"; break;
        case 23: name = "Jirdur"; break;
        case 24: name = "Briank"; break;
        case 25: name = "Kort"; break;
        case 26: name = "Wec"; break;
        case 27: name = "Cakt"; break;
        case 28: name = "Hatez"; break;
        case 29: name = "Del"; break;
        case 30: name = "Tiakz"; break;
        case 31: name = "Riebs"; break;
        case 32: name = "Rytuikt"; break;
        case 33: name = "Vot"; break;
        case 34: name = "Gnaneank"; break;
        case 35: name = "Gnert"; break;
        case 36: name = "Traaddonk"; break;
        case 37: name = "Pior"; break;
        case 38: name = "Ubkekx"; break;
        case 39: name = "Ciezbosb"; break;
        case 40: name = "Stenik"; break;
        case 41: name = "Pliazging"; break;
        case 42: name = "Gleardunk"; break;
        case 43: name = "Blizard"; break;
        case 44: name = "Aabzer"; break;
        case 45: name = "Stiokz"; break;
        case 46: name = "Bumez"; break;
        case 47: name = "Vek"; break;
        case 48: name = "Vaal"; break;
        case 49: name = "Criobs"; break;
    }
    AssertStringNotEqual("", name, "(inc_names, RandomGoblinName, name)");
    return name;
}

string RandomOrcName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(50, sSeedName))
    {
        case 0: name = "Vambag"; break;
        case 1: name = "Ikgnath"; break;
        case 2: name = "Snugug"; break;
        case 3: name = "Xugarf"; break;
        case 4: name = "Malthu"; break;
        case 5: name = "Garekk"; break;
        case 6: name = "Burguk"; break;
        case 7: name = "Knaraugh"; break;
        case 8: name = "Krognak"; break;
        case 9: name = "Zarfu"; break;
        case 10: name = "Cubub"; break;
        case 11: name = "Bogugh"; break;
        case 12: name = "Xruul"; break;
        case 13: name = "Zurgug"; break;
        case 14: name = "Jreghug"; break;
        case 15: name = "Woghuglat"; break;
        case 16: name = "Ertguth"; break;
        case 17: name = "Baghig"; break;
        case 18: name = "Xolag"; break;
        case 19: name = "Knuguk"; break;
        case 20: name = "Urgran"; break;
        case 21: name = "Shura"; break;
        case 22: name = "Kurmbag"; break;
        case 23: name = "Yargol"; break;
        case 24: name = "Orpigig"; break;
        case 25: name = "Valthurg"; break;
        case 26: name = "Xujarek"; break;
        case 27: name = "Mul"; break;
        case 28: name = "Gnoth"; break;
        case 29: name = "Sugarod"; break;
        case 30: name = "Buomaugh"; break;
        case 31: name = "Peghed"; break;
        case 32: name = "Khagra"; break;
        case 33: name = "Azhug"; break;
        case 34: name = "Ekganit"; break;
        case 35: name = "Dudagog"; break;
        case 36: name = "Xugor"; break;
        case 37: name = "Ymafubag"; break;
        case 38: name = "Marfu"; break;
        case 39: name = "Spathu"; break;
        case 40: name = "Ergoth"; break;
        case 41: name = "Winug"; break;
        case 42: name = "Fargigoth"; break;
        case 43: name = "Ughash"; break;
        case 44: name = "Frikug"; break;
        case 45: name = "Durbul"; break;
        case 46: name = "Aghed"; break;
        case 47: name = "Xorakk"; break;
        case 48: name = "Umragig"; break;
        case 49: name = "Ghazat"; break;
    }
    return name;
}

string RandomSpiderName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(60, sSeedName))
    {
        case 0: name = "Qhanqaq"; break;
        case 1: name = "Chalzeeb"; break;
        case 2: name = "Xalzob"; break;
        case 3: name = "Zhicuvix"; break;
        case 4: name = "Aqtiq"; break;
        case 5: name = "Xiqab"; break;
        case 6: name = "Zechukzis"; break;
        case 7: name = "Intit"; break;
        case 8: name = "Qikkeer"; break;
        case 9: name = "Rheekzut"; break;
        case 10: name = "Rhark'os"; break;
        case 11: name = "Qinrus"; break;
        case 12: name = "Karkus"; break;
        case 13: name = "Cherkab"; break;
        case 14: name = "Chexuteb"; break;
        case 15: name = "Chocaras"; break;
        case 16: name = "Cheqtox"; break;
        case 17: name = "Szisso"; break;
        case 18: name = "Nenchazib"; break;
        case 19: name = "Srark'er"; break;
        case 20: name = "Khakith"; break;
        case 21: name = "Sziri"; break;
        case 22: name = "Zieq'shith"; break;
        case 23: name = "Ailnithaish"; break;
        case 24: name = "Nivor"; break;
        case 25: name = "Zik'sar"; break;
        case 26: name = "Rhaqos"; break;
        case 27: name = "Kleicires"; break;
        case 28: name = "Shizush"; break;
        case 29: name = "Qora"; break;
        case 30: name = "Shrairo"; break;
        case 31: name = "Shrik'thu"; break;
        case 32: name = "Nithur"; break;
        case 33: name = "Rhethar"; break;
        case 34: name = "Havi"; break;
        case 35: name = "Yitholleer"; break;
        case 36: name = "Qothishia"; break;
        case 37: name = "Yail'she"; break;
        case 38: name = "Hirhissosh"; break;
        case 39: name = "Lhavoqeis"; break;
        case 40: name = "Ichiza"; break;
        case 41: name = "Szeqor"; break;
        case 42: name = "Zan'qad"; break;
        case 43: name = "Laicar"; break;
        case 44: name = "Cat'acho"; break;
        case 45: name = "Nirah"; break;
        case 46: name = "Lok'sih"; break;
        case 47: name = "Khizu"; break;
        case 48: name = "Chaiqrochi"; break;
        case 49: name = "Sollazih"; break;
        case 50: name = "Zeerk'ex"; break;
        case 51: name = "Srorkarrob"; break;
        case 52: name = "Ziakus"; break;
        case 53: name = "Zouser"; break;
        case 54: name = "Khouqzub"; break;
        case 55: name = "Qrizas"; break;
        case 56: name = "Szes'tuq"; break;
        case 57: name = "Szilraq"; break;
        case 58: name = "Zhanros"; break;
        case 59: name = "Okroted"; break;
    }
    return name;
}

string RandomBadgerName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(60, sSeedName))
    {
        case 0: name = "Trufflebundler"; break;
        case 1: name = "Maizetaker"; break;
        case 2: name = "Keesha"; break;
        case 3: name = "Turnipstasher"; break;
        case 4: name = "Bulbguard"; break;
        case 5: name = "Spudwasher"; break;
        case 6: name = "Salepguard"; break;
        case 7: name = "Sticks"; break;
        case 8: name = "Saplinglover"; break;
        case 9: name = "Squashgrower"; break;
        case 10: name = "Stalkhogger"; break;
        case 11: name = "Makeba"; break;
        case 12: name = "Pecanhunter"; break;
        case 13: name = "Cressholder"; break;
        case 14: name = "Sproutburrower"; break;
        case 15: name = "Raisin"; break;
        case 16: name = "Sproutstacker"; break;
        case 17: name = "Chewy"; break;
        case 18: name = "Acorngroomer"; break;
        case 19: name = "Bloomsnooper"; break;
        case 20: name = "Yamstealer"; break;
        case 21: name = "Applebundler"; break;
        case 22: name = "Yamplanter"; break;
        case 23: name = "Plumpicker"; break;
        case 24: name = "Barleydiner"; break;
        case 25: name = "Rootmasher"; break;
        case 26: name = "Bluebell"; break;
        case 27: name = "Acornfeeder"; break;
        case 28: name = "Figrobber"; break;
        case 29: name = "Prunestacker"; break;
        case 30: name = "Bloomsavorer"; break;
        case 31: name = "Rufus"; break;
        case 32: name = "Thor"; break;
        case 33: name = "Fruitreacher"; break;
        case 34: name = "Cresschopper"; break;
        case 35: name = "Grizzly"; break;
        case 36: name = "Fruitgatherer"; break;
        case 37: name = "Sproutrester"; break;
        case 38: name = "Walnutdigger"; break;
        case 39: name = "Pecanburper"; break;
        case 40: name = "Carrotchopper"; break;
        case 41: name = "Thembi"; break;
        case 42: name = "Astro"; break;
        case 43: name = "Amigo"; break;
        case 44: name = "Baloo"; break;
        case 45: name = "Pam"; break;
        case 46: name = "Taj"; break;
        case 47: name = "Hazel"; break;
        case 48: name = "Ascaris"; break;
        case 49: name = "Zara"; break;
        case 50: name = "Gollum"; break;
        case 51: name = "Cairo"; break;
        case 52: name = "Isily"; break;
        case 53: name = "Bitsy"; break;
        case 54: name = "Moby"; break;
        case 55: name = "Nacho"; break;
        case 56: name = "Baloo"; break;
        case 57: name = "Clawsborne"; break;
        case 58: name = "Mocha"; break;
        case 59: name = "Zuzu"; break;
    }
    return name;
}

string RandomBeetleName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(60, sSeedName))
    {
        case 0: name = "Fici"; break;
        case 1: name = "Fissab"; break;
        case 2: name = "Bell"; break;
        case 3: name = "Beetfo"; break;
        case 4: name = "Fila"; break;
        case 5: name = "Gook"; break;
        case 6: name = "Alta"; break;
        case 7: name = "Akhenaten"; break;
        case 8: name = "Idophor"; break;
        case 9: name = "Nuzzle"; break;
        case 10: name = "Typchray"; break;
        case 11: name = "Xuzzayua"; break;
        case 12: name = "Alakyrr"; break;
        case 13: name = "Ozzy"; break;
        case 14: name = "Elania"; break;
        case 15: name = "Zechchenoa"; break;
        case 16: name = "Argyrodes"; break;
        case 17: name = "Nefertari"; break;
        case 18: name = "Glowstick"; break;
        case 19: name = "Floof"; break;
        case 20: name = "Jettesue"; break;
        case 21: name = "Skrill"; break;
        case 22: name = "Slasgh"; break;
        case 23: name = "Twist"; break;
        case 24: name = "Kloozuck"; break;
        case 25: name = "Pipsqueak"; break;
        case 26: name = "Slerdach"; break;
        case 27: name = "Strafe"; break;
        case 28: name = "Lorn"; break;
        case 29: name = "Ghulftumb"; break;
        case 30: name = "Lumos"; break;
        case 31: name = "Splat"; break;
        case 32: name = "Beetlejuice"; break;
        case 33: name = "Scissa"; break;
        case 34: name = "Sipuncula"; break;
        case 35: name = "Beebop"; break;
        case 36: name = "Crackly"; break;
        case 37: name = "Kaia"; break;
        case 38: name = "Hive"; break;
        case 39: name = "Twitch"; break;
        case 40: name = "Folt"; break;
        case 41: name = "Vavb"; break;
        case 42: name = "Bitsy"; break;
        case 43: name = "Libnrak"; break;
        case 44: name = "Ijushir"; break;
        case 45: name = "Meleth"; break;
        case 46: name = "Slerdach"; break;
        case 47: name = "Haemosu"; break;
        case 48: name = "Crawly"; break;
        case 49: name = "Torch"; break;
        case 50: name = "Acrisius"; break;
        case 51: name = "Buzzboss"; break;
        case 52: name = "Ungoliant"; break;
        case 53: name = "Tuvok"; break;
        case 54: name = "Alexei"; break;
        case 55: name = "Ujarak"; break;
        case 56: name = "Wash"; break;
        case 57: name = "Skreech"; break;
        case 58: name = "Facehugger"; break;
        case 59: name = "Legs"; break;
    }
    return name;
}

string RandomRatName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(60, sSeedName))
    {
        case 0: name = "Velka"; break;
        case 1: name = "Crosier"; break;
        case 2: name = "Yuuhi"; break;
        case 3: name = "Onezumi"; break;
        case 4: name = "Housemaid"; break;
        case 5: name = "Tamotsu"; break;
        case 6: name = "Coop"; break;
        case 7: name = "Sohma"; break;
        case 8: name = "Kreacher"; break;
        case 9: name = "Rodent"; break;
        case 10: name = "Dukemon"; break;
        case 11: name = "Charmin"; break;
        case 12: name = "Pakupaku"; break;
        case 13: name = "Creevey"; break;
        case 14: name = "Rile"; break;
        case 15: name = "Grumpig"; break;
        case 16: name = "Loman"; break;
        case 17: name = "Fatso"; break;
        case 18: name = "Steada"; break;
        case 19: name = "Fudge"; break;
        case 20: name = "Soron"; break;
        case 21: name = "Saybrook"; break;
        case 22: name = "Cobra"; break;
        case 23: name = "Mangefur"; break;
        case 24: name = "Bolo"; break;
        case 25: name = "Gringe"; break;
        case 26: name = "Roadkill"; break;
        case 27: name = "Gart"; break;
        case 28: name = "Buckbeak"; break;
        case 29: name = "Dissonance"; break;
        case 30: name = "Corneli"; break;
        case 31: name = "Mittenwald"; break;
        case 32: name = "Lassie"; break;
        case 33: name = "Daee"; break;
        case 34: name = "Twitchtip"; break;
        case 35: name = "Nuncio"; break;
        case 36: name = "Karlik"; break;
        case 37: name = "Hangleton"; break;
        case 38: name = "Fink"; break;
        case 39: name = "Baeth"; break;
        case 40: name = "Niranye"; break;
        case 41: name = "Josep"; break;
        case 42: name = "Faison"; break;
        case 43: name = "Bisig"; break;
        case 44: name = "Wizardry"; break;
        case 45: name = "Baku"; break;
        case 46: name = "Esmerelle"; break;
        case 47: name = "Synth"; break;
        case 48: name = "Bailing"; break;
        case 49: name = "Skelton"; break;
        case 50: name = "Cyrano"; break;
        case 51: name = "Zobelle"; break;
        case 52: name = "Ruma"; break;
        case 53: name = "Thumper"; break;
        case 54: name = "Blackwater"; break;
        case 55: name = "Luxa"; break;
        case 56: name = "Tsuri"; break;
        case 57: name = "Hatori"; break;
        case 58: name = "Ferga"; break;
        case 59: name = "Dogwood"; break;
    }
    return name;
}

string RandomFemaleHumanName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(65, sSeedName))
    {
        case 0: name = "Onnor"; break;
        case 1: name = "Lora"; break;
        case 2: name = "Jinn"; break;
        case 3: name = "Bearnas"; break;
        case 4: name = "Beti"; break;
        case 5: name = "Dwysil"; break;
        case 6: name = "Elliw"; break;
        case 7: name = "Eurolwyn"; break;
        case 8: name = "Anwen"; break;
        case 9: name = "Cerys"; break;
        case 10: name = "Selene"; break;
        case 11: name = "Teresa"; break;
        case 12: name = "Altea"; break;
        case 13: name = "Sandra"; break;
        case 14: name = "Jasmin"; break;
        case 15: name = "Eiriana"; break;
        case 16: name = "Rhona"; break;
        case 17: name = "Cerys"; break;
        case 18: name = "Gwyneth"; break;
        case 19: name = "Ifanna"; break;
        case 20: name = "Azeline"; break;
        case 21: name = "Yoanie"; break;
        case 22: name = "Lila"; break;
        case 23: name = "Briny"; break;
        case 24: name = "Ana"; break;
        case 25: name = "Cristeena"; break;
        case 26: name = "Fingola"; break;
        case 27: name = "Mureal"; break;
        case 28: name = "Malane"; break;
        case 29: name = "Julia"; break;
        case 30: name = "Joannia"; break;
        case 31: name = "Cristen"; break;
        case 32: name = "Feena"; break;
        case 33: name = "Lucy"; break;
        case 34: name = "Vorana"; break;
        case 35: name = "Cossot"; break;
        case 36: name = "Lilee"; break;
        case 37: name = "Brede"; break;
        case 38: name = "Alice"; break;
        case 39: name = "Aelid"; break;
        case 40: name = "Breata"; break;
        case 41: name = "Cara"; break;
        case 42: name = "Cossot"; break;
        case 43: name = "Breesha"; break;
        case 44: name = "Lula"; break;
        case 45: name = "Aurick"; break;
        case 46: name = "Breda"; break;
        case 47: name = "Nessa"; break;
        case 48: name = "Inot"; break;
        case 49: name = "Agnes"; break;
        case 50: name = "Essa"; break;
        case 51: name = "Meave"; break;
        case 52: name = "Feena"; break;
        case 53: name = "Mawde"; break;
        case 54: name = "Mariot"; break;
        case 55: name = "Kateryn"; break;
        case 56: name = "Caly"; break;
        case 57: name = "Reina"; break;
        case 58: name = "Emell"; break;
        case 59: name = "Mona"; break;
        case 60: name = "Moira"; break;
        case 61: name = "Tosha"; break;
        case 62: name = "Kathleen"; break;
        case 63: name = "Joney"; break;
        case 64: name = "Lilee"; break;
    }
    AssertStringNotEqual("", name, "(inc_names, RandomFemaleHumanName, name)");
    return name;
}

string RandomMaleHumanName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(75, sSeedName))
    {
        case 0: name =  "Lewys"; break;
        case 1: name =  "Gwyn"; break;
        case 2: name =  "Gawain"; break;
        case 3: name =  "Vaddon"; break;
        case 4: name =  "Edwyn"; break;
        case 5: name =  "Gareth"; break;
        case 6: name =  "Owain"; break;
        case 7: name =  "Emrys"; break;
        case 8: name =  "Glanmor"; break;
        case 9: name =  "Medwyn"; break;
        case 10: name = "Bevin"; break;
        case 11: name = "Robat"; break;
        case 12: name = "Teilo"; break;
        case 13: name = "Arthur"; break;
        case 14: name = "Cranog"; break;
        case 15: name = "Alwyn"; break;
        case 16: name = "Maden"; break;
        case 17: name = "Scott"; break;
        case 18: name = "Morcan"; break;
        case 19: name = "Merwyn"; break;
        case 20: name = "Sulwyn"; break;
        case 21: name = "Pedr"; break;
        case 22: name = "Morys"; break;
        case 23: name = "Derec"; break;
        case 24: name = "Amhar"; break;
        case 25: name = "Bryn"; break;
        case 26: name = "Selwyn"; break;
        case 27: name = "Gwydion"; break;
        case 28: name = "Medyr"; break;
        case 29: name = "Anwyl"; break;
        case 30: name = "Emlyn"; break;
        case 31: name = "Tristyn"; break;
        case 32: name = "Aled"; break;
        case 33: name = "Derwen"; break;
        case 34: name = "Aron"; break;
        case 35: name = "Artur"; break;
        case 36: name = "Medyr"; break;
        case 37: name = "Ivar"; break;
        case 38: name = "James"; break;
        case 39: name = "Otnel"; break;
        case 40: name = "Geoffrey"; break;
        case 41: name = "Jac"; break;
        case 42: name = "Eurion"; break;
        case 43: name = "Delwin"; break;
        case 44: name = "Trefor"; break;
        case 45: name = "Anwill"; break;
        case 46: name = "Guto"; break;
        case 47: name = "Bryn"; break;
        case 48: name = "Saith"; break;
        case 49: name = "Olave"; break;
        case 50: name = "Martyn"; break;
        case 51: name = "Gorman"; break;
        case 52: name = "Ranlyn"; break;
        case 53: name = "Dufgal"; break;
        case 54: name = "Gilbert"; break;
        case 55: name = "Flan"; break;
        case 56: name = "Perick"; break;
        case 57: name = "Sigurd"; break;
        case 58: name = "Godred"; break;
        case 59: name = "Ector"; break;
        case 60: name = "Donn"; break;
        case 61: name = "Andrew"; break;
        case 62: name = "Mold"; break;
        case 63: name = "Hugen"; break;
        case 64: name = "Edward"; break;
        case 65: name = "Fergus"; break;
        case 66: name = "Rumund"; break;
        case 67: name = "Laurence"; break;
        case 68: name = "Eamon"; break;
        case 69: name = "Roger"; break;
        case 70: name = "William"; break;
        case 71: name = "Christopher"; break;
        case 72: name = "Geffry"; break;
        case 73: name = "William"; break;
        case 74: name = "Dolen"; break;
    }
    AssertStringNotEqual("", name, "(inc_names, RandomMaleHumanName, name)");
    return name;
}

string RandomHumanSurname(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(144, sSeedName))
    {
        case 0: name =  "Gough"; break;
        case 1: name =  "Pierce"; break;
        case 2: name =  "Evans"; break;
        case 3: name =  "Moyle"; break;
        case 4: name =  "Owen"; break;
        case 5: name =  "Gettings"; break;
        case 6: name =  "Ellis"; break;
        case 7: name =  "Prosser"; break;
        case 8: name =  "Nevitt"; break;
        case 9: name =  "Penry"; break;
        case 10: name = "Brodrick"; break;
        case 11: name = "Harris"; break;
        case 12: name = "Morris"; break;
        case 13: name = "Griffin"; break;
        case 14: name = "Pritchett"; break;
        case 15: name = "David"; break;
        case 16: name = "Helia"; break;
        case 17: name = "Moon"; break;
        case 18: name = "Wynn"; break;
        case 19: name = "Badder"; break;
        case 20: name = "Pride"; break;
        case 21: name = "Moor"; break;
        case 22: name = "Kendrick"; break;
        case 23: name = "Bellis"; break;
        case 24: name = "Trevor"; break;
        case 25: name = "Owen"; break;
        case 26: name = "Price"; break;
        case 27: name = "Powe"; break;
        case 28: name = "Griffin"; break;
        case 29: name = "Bellis"; break;
        case 30: name = "Floyd"; break;
        case 31: name = "Gaynor"; break;
        case 32: name = "Gower"; break;
        case 33: name = "Hewitt"; break;
        case 34: name = "Harley"; break;
        case 35: name = "Edris"; break;
        case 36: name = "Craddock"; break;
        case 37: name = "David"; break;
        case 38: name = "MacGell"; break;
        case 39: name = "Hyk"; break;
        case 40: name = "MacInesh"; break;
        case 41: name = "Morgan"; break;
        case 42: name = "Rhees"; break;
        case 43: name = "Binion"; break;
        case 44: name = "Probert"; break;
        case 45: name = "Voils"; break;
        case 46: name = "Maddocks"; break;
        case 47: name = "Glines"; break;
        case 48: name = "Nest"; break;
        case 49: name = "Gragh"; break;
        case 50: name = "Macrobin"; break;
        case 51: name = "Walsh"; break;
        case 52: name = "Butler"; break;
        case 53: name = "Daniell"; break;
        case 54: name = "Lyst"; break;
        case 55: name = "Scott"; break;
        case 56: name = "Caterall"; break;
        case 57: name = "Macdik"; break;
        case 58: name = "Calcote"; break;
        case 59: name = "Wode"; break;
        case 60: name = "Browne"; break;
        case 61: name = "Abelson"; break;
        case 62: name = "Heresson"; break;
        case 63: name = "Kerdar"; break;
        case 64: name = "Gall"; break;
        case 65: name = "Closse"; break;
        case 66: name = "Cristall"; break;
        case 67: name = "Hubart"; break;
        case 68: name = "Gragh"; break;
        case 69: name = "Ince"; break;
        case 70: name = "Vause"; break;
        case 71: name = "Smyth"; break;
        case 72: name = "Maderel"; break;
        case 73: name = "Gale"; break;
        case 74: name = "Mychel"; break;
        case 75: name = "Gall"; break;
        case 76: name = "Cristall"; break;
        case 77: name = "Norres"; break;
        case 78: name = "Coke"; break;
        case 79: name = "Siosal"; break;
        case 80: name = "Nest"; break;
        case 81: name = "Glines"; break;
        case 82: name = "Penrose"; break;
        case 83: name = "Prosser"; break;
        case 84: name = "Dawes"; break;
        case 85: name = "Martes"; break;
        case 86: name = "Anzivino"; break;
        case 87: name = "Manganelli"; break;
        case 88: name = "Sturniolo"; break;
        case 89: name = "Millet"; break;
        case 90: name = "Skywaker"; break;
        case 91: name = "Moses"; break;
        case 92: name = "Bellis"; break;
        case 93: name = "Caleb"; break;
        case 94: name = "Eynon"; break;
        case 95: name = "Mower"; break;
        case 96: name = "Garel"; break;
        case 97: name = "Galand"; break;
        case 98: name = "Konan"; break;
        case 99: name = "Abalain"; break;
        case 100: name = "Smith"; break;
        case 101: name = "Perrot"; break;
        case 102: name = "Carre"; break;
        case 103: name = "Lake"; break;
        case 104: name = "Sammesbury"; break;
        case 105: name = "Breen"; break;
        case 106: name = "Clerke"; break;
        case 107: name = "Piper"; break;
        case 108: name = "Portok"; break;
        case 109: name = "Orme"; break;
        case 110: name = "Breden"; break;
        case 111: name = "Avelson"; break;
        case 112: name = "Wilson"; break;
        case 113: name = "Rede"; break;
        case 114: name = "Dudziak"; break;
        case 115: name = "Skylycorn"; break;
        case 116: name = "Walsh"; break;
        case 117: name = "Pulley"; break;
        case 118: name = "Cowper"; break;
        case 119: name = "Coland"; break;
        case 120: name = "Bowge"; break;
        case 121: name = "Barrett"; break;
        case 122: name = "Parre"; break;
        case 123: name = "Hymyn"; break;
        case 124: name = "Breen"; break;
        case 125: name = "Byrch"; break;
        case 126: name = "Atkyn"; break;
        case 127: name = "Owle"; break;
        case 128: name = "Homlyn"; break;
        case 129: name = "Carre"; break;
        case 130: name = "Baggins"; break;
        case 131: name = "Rushton"; break;
        case 132: name = "Preston"; break;
        case 133: name = "Portok"; break;
        case 134: name = "MacIssak"; break;
        case 135: name = "Goldesmyth"; break;
        case 136: name = "Mason"; break;
        case 137: name = "Breden"; break;
        case 138: name = "Cristall"; break;
        case 139: name = "Holt"; break;
        case 140: name = "Spakeman"; break;
        case 141: name = "Vessy"; break;
        case 142: name = "Avelson"; break;
        case 143: name = "Ireshman"; break;
    }
    AssertStringNotEqual("", name, "(inc_names, RandomHumanSurname, name)");
    return name;
}

string RandomMaleHalflingName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(75, sSeedName))
    {
        case 0: name =  "Danzu"; break;
        case 1: name =  "Marry"; break;
        case 2: name =  "Jozin"; break;
        case 3: name =  "Norwan"; break;
        case 4: name =  "Vinzu"; break;
        case 5: name =  "Tarbin"; break;
        case 6: name =  "Horhace"; break;
        case 7: name =  "Quinton"; break;
        case 8: name =  "Xoeon"; break;
        case 9: name =  "Elvias"; break;
        case 10: name = "Yenfire"; break;
        case 11: name = "Erhorn"; break;
        case 12: name = "Quinmin"; break;
        case 13: name = "Dangin"; break;
        case 14: name = "Elric"; break;
        case 15: name = "Xanyas"; break;
        case 16: name = "Quinumo"; break;
        case 17: name = "Quinwan"; break;
        case 18: name = "Valwrick"; break;
        case 19: name = "Valnad"; break;
        case 20: name = "Pimumo"; break;
        case 21: name = "Marmin"; break;
        case 22: name = "Belgin"; break;
        case 23: name = "Corwan"; break;
        case 24: name = "Faltran"; break;
        case 25: name = "Tebin"; break;
        case 26: name = "Bellan"; break;
        case 27: name = "Vinmin"; break;
        case 28: name = "Conder"; break;
        case 29: name = "Uritran"; break;
        case 30: name = "Janfire"; break;
        case 31: name = "Vinhorn"; break;
        case 32: name = "Sharkin"; break;
        case 33: name = "Merras"; break;
        case 34: name = "Connan"; break;
        case 35: name = "Barumo"; break;
        case 36: name = "Wilner"; break;
        case 37: name = "Corkas"; break;
        case 38: name = "Laamin"; break;
        case 39: name = "Horbin"; break;
        case 40: name = "Corner"; break;
        case 41: name = "Irawrick"; break;
        case 42: name = "Halzin"; break;
        case 43: name = "Barder"; break;
        case 44: name = "Laace"; break;
        case 45: name = "Finhace"; break;
        case 46: name = "Garumo"; break;
        case 47: name = "Aryas"; break;
        case 48: name = "Zendal"; break;
        case 49: name = "Urikin"; break;
        case 50: name = "Garton"; break;
        case 51: name = "Yardak"; break;
        case 52: name = "Osnan"; break;
        case 53: name = "Pimlan"; break;
        case 54: name = "Nener"; break;
        case 55: name = "Wilzor"; break;
        case 56: name = "Vinemin"; break;
        case 57: name = "Coremin"; break;
        case 58: name = "Vinpos"; break;
        case 59: name = "Elzor"; break;
        case 60: name = "Uriry"; break;
        case 61: name = "Merner"; break;
        case 62: name = "Ulyver"; break;
        case 63: name = "Goster"; break;
        case 64: name = "Quokas"; break;
        case 65: name = "Jolos"; break;
        case 66: name = "Nekin"; break;
        case 67: name = "Neyver"; break;
        case 68: name = "Tom"; break;
        case 69: name = "Flynemin"; break;
        case 70: name = "Perry"; break;
        case 71: name = "Mip"; break;
        case 72: name = "Sharner"; break;
        case 73: name = "Finner"; break;
        case 74: name = "Elkin"; break;
    }
    AssertStringNotEqual("", name, "(inc_names, RandomMaleHalflingName, name)");
    return name;
}

string RandomFemaleHalflingName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(75, sSeedName))
    {
        case 0: name =  "Riara"; break;
        case 1: name =  "Qiri"; break;
        case 2: name =  "Belyra"; break;
        case 3: name =  "Zengwen"; break;
        case 4: name =  "Odiree"; break;
        case 5: name =  "Jayelle"; break;
        case 6: name =  "Gelna"; break;
        case 7: name =  "Belris"; break;
        case 8: name =  "Ankis"; break;
        case 9: name =  "Nena"; break;
        case 10: name = "Nana"; break;
        case 11: name = "Granys"; break;
        case 12: name = "Rosnys"; break;
        case 13: name = "Paekis"; break;
        case 14: name = "Wibyn"; break;
        case 15: name = "Fenwyse"; break;
        case 16: name = "Eraora"; break;
        case 17: name = "Belri"; break;
        case 18: name = "Hiljen"; break;
        case 19: name = "Froora"; break;
        case 20: name = "Fenula"; break;
        case 21: name = "Oramita"; break;
        case 22: name = "Witina"; break;
        case 23: name = "Gelzana"; break;
        case 24: name = "Gragrace"; break;
        case 25: name = "Odisys"; break;
        case 26: name = "Calkis"; break;
        case 27: name = "Uvigrace"; break;
        case 28: name = "Odiyola"; break;
        case 29: name = "Nedeni"; break;
        case 30: name = "Zenmia"; break;
        case 31: name = "Safice"; break;
        case 32: name = "Xifice"; break;
        case 33: name = "Eilie"; break;
        case 34: name = "Hilvira"; break;
        case 35: name = "Malrana"; break;
        case 36: name = "Trynri"; break;
        case 37: name = "Saula"; break;
        case 38: name = "Rosnys"; break;
        case 39: name = "Zefmita"; break;
        case 40: name = "Maleni"; break;
        case 41: name = "Kelbrix"; break;
        case 42: name = "Verla"; break;
        case 43: name = "Lidbyn"; break;
        case 44: name = "Yori"; break;
        case 45: name = "Paesys"; break;
        case 46: name = "Erabrix"; break;
        case 47: name = "Xanlie"; break;
        case 48: name = "Hilrana"; break;
        case 49: name = "Wicey"; break;
        case 50: name = "Lidcey"; break;
        case 51: name = "Rosvira"; break;
        case 52: name = "Rose"; break;
        case 53: name = "Wini"; break;
        case 54: name = "Hilora"; break;
        case 55: name = "Yodrey"; break;
        case 56: name = "Andove"; break;
        case 57: name = "Eomia"; break;
        case 58: name = "Darcey"; break;
        case 59: name = "Orani"; break;
        case 60: name = "Nefice"; break;
        case 61: name = "Paeyola"; break;
        case 62: name = "Malri"; break;
        case 63: name = "Gralile"; break;
        case 64: name = "Calhaly"; break;
        case 65: name = "Pruyola"; break;
        case 66: name = "Mareni"; break;
        case 67: name = "Arielle"; break;
        case 68: name = "Hayola"; break;
        case 69: name = "Isawyse"; break;
        case 70: name = "Fayphina"; break;
        case 71: name = "Diayola"; break;
        case 72: name = "Frolie"; break;
        case 73: name = "Ariyola"; break;
        case 74: name = "Ariora"; break;
    }
    AssertStringNotEqual("", name, "(inc_names, RandomFemaleHalflingName, name)");
    return name;
}

string RandomHalflingSurname(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(144, sSeedName))
    {
        case 0: name =  "Hogflower"; break;
        case 1: name =  "Underdew"; break;
        case 2: name =  "Lightbelly"; break;
        case 3: name =  "Sunace"; break;
        case 4: name =  "Riverearth"; break;
        case 5: name =  "Proudsurge"; break;
        case 6: name =  "Grassdancer"; break;
        case 7: name =  "Humblesong"; break;
        case 8: name =  "Silentstep"; break;
        case 9: name =  "Mosswoods"; break;
        case 10: name = "Clearwoods"; break;
        case 11: name = "Summerpot"; break;
        case 12: name = "Bramblewillow"; break;
        case 13: name = "Tallspirit"; break;
        case 14: name = "Whisperwhistle"; break;
        case 15: name = "Cindertopple"; break;
        case 16: name = "Fastrabbit"; break;
        case 17: name = "Stonedancer"; break;
        case 18: name = "Autumnstep"; break;
        case 19: name = "Forecrest"; break;
        case 20: name = "Brightfeet"; break;
        case 21: name = "Flintspell"; break;
        case 22: name = "Ravenbeam"; break;
        case 23: name = "Nimblefound"; break;
        case 24: name = "Underman"; break;
        case 25: name = "Greatmoon"; break;
        case 26: name = "Springfoot"; break;
        case 27: name = "Tossmouse"; break;
        case 28: name = "Whisperbridge"; break;
        case 29: name = "Warmglide"; break;
        case 30: name = "Cherrybraid"; break;
        case 31: name = "Thistlebrush"; break;
        case 32: name = "Cinderbeam"; break;
        case 33: name = "Hillbrace"; break;
        case 34: name = "Glenhollow"; break;
        case 35: name = "Bigspirit"; break;
        case 36: name = "Keenbloom"; break;
        case 37: name = "Mossvale"; break;
        case 38: name = "Fatfeet"; break;
        case 39: name = "Flintmane"; break;
        case 40: name = "Stillbelly"; break;
        case 41: name = "Fogcreek"; break;
        case 42: name = "Wildwood"; break;
        case 43: name = "Greenfound"; break;
        case 44: name = "Heartpot"; break;
        case 45: name = "Tossleaf"; break;
        case 46: name = "Silverstep"; break;
        case 47: name = "Lunarbelly"; break;
        case 48: name = "Marblehand"; break;
        case 49: name = "Cinderwoods"; break;
        case 50: name = "Smoothwhistle"; break;
        case 51: name = "Riverbrush"; break;
        case 52: name = "Laughingstep"; break;
        case 53: name = "Reedfinger"; break;
        case 54: name = "Thornspark"; break;
        case 55: name = "Rosesky"; break;
        case 56: name = "Strongleaf"; break;
        case 57: name = "Whisperwater"; break;
        case 58: name = "Twilightwillow"; break;
        case 59: name = "Grandbough"; break;
        case 60: name = "Lighthands"; break;
        case 61: name = "Dustpot"; break;
        case 62: name = "Dusthare"; break;
        case 63: name = "Hogvale"; break;
        case 64: name = "Stillbottle"; break;
        case 65: name = "Ambermoon"; break;
        case 66: name = "Wisewind"; break;
        case 67: name = "Keenbrace"; break;
        case 68: name = "Goldhands"; break;
        case 69: name = "Boulderace"; break;
        case 70: name = "Rosedew"; break;
        case 71: name = "Commondancer"; break;
        case 72: name = "Fasteyes"; break;
        case 73: name = "Quickfoot"; break;
        case 74: name = "Shadowfellow"; break;
        case 75: name = "Freespell"; break;
        case 76: name = "Nightmeadow"; break;
        case 77: name = "Glenfingers"; break;
        case 78: name = "Summerbrand"; break;
        case 79: name = "Smoothflower"; break;
        case 80: name = "Lonetopple"; break;
        case 81: name = "Humblemantle"; break;
        case 82: name = "Glengather"; break;
        case 83: name = "Ravenbrook"; break;
        case 84: name = "Teabeam"; break;
        case 85: name = "Dustlade"; break;
        case 86: name = "Mistsong"; break;
        case 87: name = "Elderspell"; break;
        case 88: name = "Bronzebarrel"; break;
        case 89: name = "Littlehands"; break;
        case 90: name = "Highcobble"; break;
        case 91: name = "Cloudfinger"; break;
        case 92: name = "Stillwoods"; break;
        case 93: name = "Glowberry"; break;
        case 94: name = "Mildfoot"; break;
        case 95: name = "Tenmane"; break;
        case 96: name = "Fernbrace"; break;
        case 97: name = "Stillflow"; break;
        case 98: name = "Underleaf"; break;
        case 99: name = "Ravenfellow"; break;
        case 100: name = "Greatbelly"; break;
        case 101: name = "Grasswind"; break;
        case 102: name = "Clearearth"; break;
        case 103: name = "Reedfellow"; break;
        case 104: name = "Strongfinger"; break;
        case 105: name = "Eldermane"; break;
        case 106: name = "Earthvale"; break;
        case 107: name = "Greentop"; break;
        case 108: name = "Cloudmoon"; break;
        case 109: name = "Rumblemouse"; break;
        case 110: name = "Barleyace"; break;
        case 111: name = "Wisedew"; break;
        case 112: name = "Bronzeflow"; break;
        case 113: name = "Truebarrel"; break;
        case 114: name = "Moonshine"; break;
        case 115: name = "Applewillow"; break;
        case 116: name = "Eldercreek"; break;
        case 117: name = "Dustfellow"; break;
        case 118: name = "Proudleaf"; break;
        case 119: name = "Fogbrace"; break;
        case 120: name = "Stoneshadow"; break;
        case 121: name = "Hightop"; break;
        case 122: name = "Cherrycloak"; break;
        case 123: name = "Mildearth"; break;
        case 124: name = "Rumblemantle"; break;
        case 125: name = "Freemeadow"; break;
        case 126: name = "Thornrabbit"; break;
        case 127: name = "Thornhill"; break;
        case 128: name = "Riverfeet"; break;
        case 129: name = "Havenhand"; break;
        case 130: name = "Brightspell"; break;
        case 131: name = "Flintsurge"; break;
        case 132: name = "Whispercloak"; break;
        case 133: name = "Nightbraid"; break;
        case 134: name = "Keenshine"; break;
        case 135: name = "Summerstep"; break;
        case 136: name = "Grasspot"; break;
        case 137: name = "Smoothseeker"; break;
        case 138: name = "Longtop"; break;
        case 139: name = "Undersong"; break;
        case 140: name = "Greenwater"; break;
        case 141: name = "Rosefoot"; break;
        case 142: name = "Fastbottle"; break;
        case 143: name = "Lunarflower"; break;
    }
    AssertStringNotEqual("", name, "(inc_names, RandomHalflingSurname, name)");
    return name;
}

struct LingualFormData
{
    int plural;
    int female;
    int femaleWhenPlural;
    int special;
    int genderless;
};

string _GetPolishTavernNameAdjective(string sMaleSingular, string sGenderlessSingular, string sFemaleSingular, string sMalePlural, string sSpecialMaleSingular, string sSpecialFemaleSingular, string sSpecialPlural, struct LingualFormData form)
{
    if (!form.plural && !form.special)
    {
        if (form.genderless)
            return sGenderlessSingular;
        if (form.female)
            return sFemaleSingular;
        return sMaleSingular;
    }
    if (!form.plural)
    {
        if (form.female)
            return "Pod " + sSpecialFemaleSingular;
        return "Pod " + sSpecialMaleSingular;
    }
    if ((form.female || form.femaleWhenPlural || form.genderless) && !form.special)
    {
        return sGenderlessSingular;
    }
    if (!form.special)
    {
        return sMalePlural;
    }
    return "Pod " + sSpecialPlural;
}

string _GetEnglishTavernNameAdjective(string sWord, struct LingualFormData form)
{
    if (form.special)
        return "Ye " + sWord;
    return "The " + sWord;
}

string _GetEnglishTavernNameMain(string sSingular, string sPlural, struct LingualFormData form)
{
    if (form.plural)
        return sPlural;
    return sSingular;
}

string _GetPolishTavernNameMain(string sSingular, string sPlural, string sSpecialSingular, string sSpecialPlural, struct LingualFormData form)
{
    if (form.plural && form.special)
        return sSpecialPlural;
    if (form.plural)
        return sPlural;
    if (form.special)
        return sSpecialSingular;
    return sSingular;
}

string RandomTavernName(string sSeedName="default")
{
    struct LingualFormData form;
    if (RandomNext(5) == 0)
        form.special = TRUE;
    if (RandomNext(3) == 0)
        form.plural = TRUE;
    string part1, part2;

    switch (RandomNext(66, sSeedName))
    {
        case 0:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Spruce", "Spruces", form), 
                _GetPolishTavernNameMain("Wierzba", "Wierzby", "Wierzb�", "Wierzbami", form));
                break;
        case 1:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Cauldron", "Cauldrons", form), 
                _GetPolishTavernNameMain("Kocio�", "Kot�y", "Kot�em", "Kot�ami", form));
                break;
        case 2:
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Lumberjack", "Lumberjacks", form), 
                _GetPolishTavernNameMain("Drwal", "Drwale", "Drwalem", "Drwalami", form));
                break;
        case 3:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Jackal", "Jackals", form), 
                _GetPolishTavernNameMain("Szakal", "Szakale", "Szakalem", "Szakalami", form));
                break;
        case 4:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Lion", "Lions", form), 
                _GetPolishTavernNameMain("Lew", "Lwy", "Lwem", "Lwami", form));
                break;
        case 5:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Dwarf", "Dwarves", form), 
                _GetPolishTavernNameMain("Karze�", "Kar�y", "Kar�em", "Kar�ami", form));
                break;
        case 6:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Mug", "Mugs", form), 
                _GetPolishTavernNameMain("Kufel", "Kufle", "Kuflem", "Kuflami", form));
                break;
        case 7:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Dragon", "Dragons", form), 
                _GetPolishTavernNameMain("Smok", "Smoki", "Smokiem", "Smokami", form));
                break;
        case 8:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Bottle", "Bottles", form), 
                _GetPolishTavernNameMain("Butelka", "Butelki", "Butelk�", "Butelkami", form));
                break;
        case 9:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Octopus", "Octopi", form), 
                _GetPolishTavernNameMain("O�miorniczka", "O�miorniczki", "O�miorniczk�", "O�miorniczkami", form));
                break;
        case 10:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Horseshoe", "Horseshoes", form), 
                _GetPolishTavernNameMain("Podkowa", "Podkowy", "Podkow�", "Podkowami", form));
                break;
        case 11:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Boot", "Boots", form), 
                _GetPolishTavernNameMain("But", "Buty", "Butem", "Butami", form));
                break;
        case 12:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Parrot", "Parrots", form), 
                _GetPolishTavernNameMain("Papuga", "Papugi", "Papug�", "Papugami", form));
                break;
        case 13:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Virgin", "Virgins", form), 
                _GetPolishTavernNameMain("Dziewica", "Dziewice", "Dziewic�", "Dziewicami", form));
                break;
        case 14:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Lamb", "Lamb", form), 
                _GetPolishTavernNameMain("Baranek", "Baranki", "Barankiem", "Barankami", form));
                break;
        case 15:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Twin", "Twins", form), 
                _GetPolishTavernNameMain("Bli�niak", "Bli�ni�ta", "Bli�niakiem", "Bli�ni�tami", form));
                break;
        case 16:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Jug", "Jugs", form), 
                _GetPolishTavernNameMain("Dzban", "Dzbany", "Dzbanem", "Dzbanami", form));
                break;
        case 17:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Pony", "Ponies", form), 
                _GetPolishTavernNameMain("Kucyk", "Kucyki", "Kucykiem", "Kucykami", form));
                break;
        case 18:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Rose", "Roses", form), 
                _GetPolishTavernNameMain("R�a", "R�e", "R�", "R�ami", form));
                break;
        case 19:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Daisy", "Daisies", form), 
                _GetPolishTavernNameMain("Stokrotka", "Stokrotki", "Stokrotk�", "Stokrotkami", form));
                break;
        case 20:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Orchid", "Orchids", form), 
                _GetPolishTavernNameMain("Orchidea", "Orchidee", "Orchide�", "Orchideami", form));
                break;
        case 21:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Magnolia", "Magnolias", form), 
                _GetPolishTavernNameMain("Magnolia", "Magnolie", "Magnoli�", "Magnoliami", form));
                break;
        case 22:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Cat", "Cats", form), 
                _GetPolishTavernNameMain("Kot", "Koty", "Kotem", "Kotami", form));
                break;
        case 23:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Goat", "Goats", form), 
                _GetPolishTavernNameMain("Koza", "Kozy", "Koz�", "Kozami", form));
                break;
        case 24:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Wolf", "Wolves", form), 
                _GetPolishTavernNameMain("Wilk", "Wilki", "Wilkiem", "Wilkami", form));
                break;
        case 25:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Eagle", "Eagles", form), 
                _GetPolishTavernNameMain("Orze�", "Or�y", "Or�em", "Or�ami", form));
                break;
        case 26:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Raven", "Ravens", form), 
                _GetPolishTavernNameMain("Kruk", "Kruki", "Krukiem", "Krukami", form));
                break;
        case 27:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Chaise", "Chaises", form), 
                _GetPolishTavernNameMain("Bryczka", "Bryczki", "Bryczk�", "Bryczkami", form));
                break;
        case 28:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Wife", "Wives", form), 
                _GetPolishTavernNameMain("�ona", "�ony", "�on�", "�onami", form));
                break;
        case 29:
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Knight", "Knights", form), 
                _GetPolishTavernNameMain("Rycerz", "Rycerze", "Rycerzem", "Rycerzami", form));
                break;
        case 30:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Trail", "Trails", form), 
                _GetPolishTavernNameMain("Szlak", "Szlaki", "Szlakiem", "Szlakami", form));
                break;
        case 31:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Lady", "Ladies", form), 
                _GetPolishTavernNameMain("Dama", "Damy", "Dam�", "Damami", form));
                break;
        case 32:
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Wizard", "Wizards", form), 
                _GetPolishTavernNameMain("Czarodziej", "Czarodzieje", "Czarodziejem", "Czarodziejami", form));
                break;
        case 33:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Hag", "Hags", form), 
                _GetPolishTavernNameMain("Wied�ma", "Wied�my", "Wied�m�", "Wied�mami", form));
                break;
        case 34:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Basilisk", "Basilisks", form), 
                _GetPolishTavernNameMain("Bazyliszek", "Bazyliszki", "Bazyliszkiem", "Bazyliszkami", form));
                break;
        case 35:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Harpy", "Harpies", form), 
                _GetPolishTavernNameMain("Harpia", "Harpie", "Harpi�", "Harpiami", form));
                break;
        case 36:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Nymph", "Nymphs", form), 
                _GetPolishTavernNameMain("Nimfa", "Nimfy", "Nimf�", "Nimfami", form));
                break;
        case 37:
            form.genderless = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Tree", "Trees", form), 
                _GetPolishTavernNameMain("Drzewo", "Drzewa", "Drzewem", "Drzewami", form));
                break;
        case 38:
            form.genderless = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Eye", "Eyes", form), 
                _GetPolishTavernNameMain("Oczko", "Oczka", "Oczkiem", "Oczkami", form));
                break;
        case 39:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Peacock", "Peacocks", form), 
                _GetPolishTavernNameMain("Paw", "Pawie", "Pawiem", "Pawiami", form));
                break;
        case 40:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Crown", "Crowns", form), 
                _GetPolishTavernNameMain("Korona", "Korony", "Koron�", "Koronami", form));
                break;
        case 41:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Dress", "Dresses", form), 
                _GetPolishTavernNameMain("Suknia", "Suknie", "Sukni�", "Sukniami", form));
                break;
        case 42:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Waitress", "Waitresses", form), 
                _GetPolishTavernNameMain("Kelnerka", "Kelnerki", "Kelnerk�", "Kelnerkami", form));
                break;
        case 43:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Dancer", "Dancers", form), 
                _GetPolishTavernNameMain("Tancerka", "Tancerki", "Tancerk�", "Tancerkami", form));
                break;
        case 44:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Shoe", "Shoes", form), 
                _GetPolishTavernNameMain("Bucik", "Buciki", "Bucikiem", "Bucikami", form));
                break;
        case 45:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Woman", "Women", form), 
                _GetPolishTavernNameMain("Baba", "Baby", "Bab�", "Babami", form));
                break;
        case 46:
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Cleric", "Clerics", form), 
                _GetPolishTavernNameMain("Kap�an", "Kap�ani", "Kap�anem", "Kap�anami", form));
                break;
        case 47:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Goblin", "Goblins", form), 
                _GetPolishTavernNameMain("Goblin", "Gobliny", "Goblinem", "Goblinami", form));
                break;
        case 48:
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("King", "Kings", form), 
                _GetPolishTavernNameMain("Kr�l", "Kr�lowie", "Kr�lem", "Kr�lami", form));
                break;
        case 49:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Countess", "Countesses", form), 
                _GetPolishTavernNameMain("Hrabina", "Hrabiny", "Hrabin�", "Hrabinami", form));
                break;
        case 50:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Pigeon", "Pigeons", form), 
                _GetPolishTavernNameMain("Go��b", "Go��bie", "Go��biem", "Go��biami", form));
                break;
        case 51:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Sword", "Swords", form), 
                _GetPolishTavernNameMain("Miecz", "Miecze", "Mieczem", "Mieczami", form));
                break;
        case 52:
            form.genderless = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Blade", "Blades", form), 
                _GetPolishTavernNameMain("Ostrze", "Ostrza", "Ostrzem", "Ostrzami", form));
                break;
        case 53:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Dagger", "Daggers", form), 
                _GetPolishTavernNameMain("Sztylet", "Sztylety", "Sztyletem", "Sztyletami", form));
                break;
        case 54:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Axe", "Axes", form), 
                _GetPolishTavernNameMain("Top�r", "Topory", "Toporem", "Toporami", form));
                break;
        case 55:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Shield", "Shields", form), 
                _GetPolishTavernNameMain("Tarcza", "Tarcze", "Tarcz�", "Tarczami", form));
                break;
        case 56:
            form.female = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Song", "Songs", form), 
                _GetPolishTavernNameMain("Pie��", "Pie�ni", "Pie�ni�", "Pie�niami", form));
                break;
        case 57:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Gryphon", "Gryphons", form), 
                _GetPolishTavernNameMain("Gryf", "Gryfy", "Gryfem", "Gryfami", form));
                break;
        case 58:
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Orc", "Orcs", form), 
                _GetPolishTavernNameMain("Ork", "Orkowie", "Orkiem", "Orkami", form));
                break;
        case 59:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Elf", "Elves", form), 
                _GetPolishTavernNameMain("Elf", "Elfy", "Elfem", "Elfami", form));
                break;
        case 60:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Unicorn", "Unicorns", form), 
                _GetPolishTavernNameMain("Jednoro�ec", "Jednoro�ce", "Jednoro�cem", "Jednoro�cami", form));
                break;
        case 61:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Pegasus", "Pegasi", form), 
                _GetPolishTavernNameMain("Pegaz", "Pegazy", "Pegazem", "Pegazami", form));
                break;
        case 62:
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Monk", "Monks", form), 
                _GetPolishTavernNameMain("Mnich", "Mnisi", "Mnichem", "Mnichami", form));
                break;
        case 63:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Serpent", "Serpents", form), 
                _GetPolishTavernNameMain("W��", "W�e", "W�em", "W�ami", form));
                break;
        case 64:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Steed", "Steeds", form), 
                _GetPolishTavernNameMain("Rumak", "Rumaki", "Rumakiem", "Rumakami", form));
                break;
        case 65:
            form.femaleWhenPlural = TRUE;
            part2 = GetLocalizedString(
                _GetEnglishTavernNameMain("Cup", "Cups", form), 
                _GetPolishTavernNameMain("Kielich", "Kielichy", "Kielichem", "Kielichami", form));
                break;
    }

    switch (RandomNext(45, sSeedName))
    {
        case 0:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Joyful", form), 
                _GetPolishTavernNameAdjective("Radosny", "Radosne", "Radosna", "Rado�ni", "Radosnym", "Radosn�", "Radosnymi", form));
            break;
        case 1:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Golden", form), 
                _GetPolishTavernNameAdjective("Z�oty", "Z�ote", "Z�ota", "Z�oci", "Z�otym", "Z�ot�", "Z�otymi", form));
            break;
        case 2:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Sharp", form), 
                _GetPolishTavernNameAdjective("Ostry", "Ostre", "Ostra", "Ostrzy", "Ostrym", "Ostr�", "Ostrymi", form));
            break;
        case 3:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Infuriating", form), 
                _GetPolishTavernNameAdjective("Niezno�ny", "Niezno�ne", "Niezno�na", "Niezno�ni", "Niezno�nym", "Niezno�n�", "Niezno�nymi", form));
            break;
        case 4:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Fat", form), 
                _GetPolishTavernNameAdjective("Gruby", "Grube", "Gruba", "Grubi", "Grubym", "Grub�", "Grubymi", form));
            break;
        case 5:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Drunk", form), 
                _GetPolishTavernNameAdjective("Pijany", "Pijane", "Pijana", "Pijani", "Pijanym", "Pijan�", "Pijanymi", form));
            break;
        case 6:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Sad", form), 
                _GetPolishTavernNameAdjective("Smutny", "Smutne", "Smutna", "Smutni", "Smutnym", "Smutn�", "Smutnymi", form));
            break;
        case 7:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("White", form), 
                _GetPolishTavernNameAdjective("Bia�y", "Bia�e", "Bia�a", "Biali", "Bia�ym", "Bia��", "Bia�ymi", form));
            break;
        case 8:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Alcoholic", form), 
                _GetPolishTavernNameAdjective("Piwny", "Piwne", "Piwna", "Piwni", "Piwnym", "Piwn�", "Piwnymi", form));
            break;
        case 9:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Horned", form), 
                _GetPolishTavernNameAdjective("Rogaty", "Rogate", "Rogata", "Rogaci", "Rogatym", "Rogat�", "Rogatymi", form));
            break;
        case 10:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Mad", form), 
                _GetPolishTavernNameAdjective("Szalony", "Szalone", "Szalona", "Szaleni", "Szalonym", "Szalon�", "Szalonymi", form));
            break;
        case 11:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Crazy", form), 
                _GetPolishTavernNameAdjective("Zwariowany", "Zwariowane", "Zwariowana", "Zwariowani", "Zwariowanym", "Zwariowan�", "Zwariowanymi", form));
            break;
        case 12:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Frisky", form), 
                _GetPolishTavernNameAdjective("Rozbrykany", "Rozbrykane", "Rozbrykana", "Rozbrykani", "Rozbrykanym", "Rozbrykan�", "Rozbrykanymi", form));
            break;
        case 13:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Dancing", form), 
                _GetPolishTavernNameAdjective("Rozta�czony", "Rozta�czone", "Rozta�czona", "Rozta�czeni", "Rozta�czonym", "Rozta�czon�", "Rozta�czonymi", form));
            break;
        case 14:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Happy", form), 
                _GetPolishTavernNameAdjective("Weso�y", "Weso�e", "Weso�a", "Weseli", "Weso�ym", "Weso��", "Weso�ymi", form));
            break;
        case 15:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Laughing", form), 
                _GetPolishTavernNameAdjective("Roze�miany", "Roze�miane", "Roze�miana", "Roze�miani", "Roze�mianym", "Roze�mian�", "Roze�mianymi", form));
            break;
        case 16:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Kind", form), 
                _GetPolishTavernNameAdjective("Mi�y", "Mi�e", "Mi�a", "Mili", "Mi�ym", "Mi��", "Mi�ymi", form));
            break;
        case 17:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Raging", form), 
                _GetPolishTavernNameAdjective("W�ciek�y", "W�ciek�e", "W�ciek�a", "W�ciekli", "W�ciek�ym", "W�ciek��", "W�ciek�ymi", form));
            break;
        case 18:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Silent", form), 
                _GetPolishTavernNameAdjective("Cichy", "Ciche", "Cicha", "Cisi", "Cichym", "Cich�", "Cichymi", form));
            break;
        case 19:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Offended", form), 
                _GetPolishTavernNameAdjective("Obra�ony", "Obra�one", "Obra�ona", "Obra�eni", "Obra�onym", "Obra�on�", "Obra�onymi", form));
            break;
        case 20:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Smart", form), 
                _GetPolishTavernNameAdjective("Bystry", "Bystre", "Bystra", "Bystrzy", "Bystrym", "Bystr�", "Bystrymi", form));
            break;
        case 21:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Red", form), 
                _GetPolishTavernNameAdjective("Czerwony", "Czerwone", "Czerwona", "Czerwoni", "Czerwonym", "Czerwon�", "Czerwonymi", form));
            break;
        case 22:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Black", form), 
                _GetPolishTavernNameAdjective("Czarny", "Czarne", "Czarna", "Czarni", "Czarnym", "Czarn�", "Czarnymi", form));
            break;
        case 23:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Blue", form), 
                _GetPolishTavernNameAdjective("B��kitny", "B��kitne", "B��kitna", "B��kitni", "B��kitnym", "B��kitn�", "B��kitnymi", form));
            break;
        case 24:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Bloody", form), 
                _GetPolishTavernNameAdjective("Krwawy", "Krwawe", "Krwawa", "Krwawi", "Krwawym", "Krwaw�", "Krwawymi", form));
            break;
        case 25:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Ruby", form), 
                _GetPolishTavernNameAdjective("Rubinowy", "Rubinowe", "Rubinowa", "Rubinowi", "Rubinowym", "Rubinow�", "Rubinowymi", form));
            break;
        case 26:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Sapphire", form), 
                _GetPolishTavernNameAdjective("Szafirowy", "Szafirowe", "Szafirowa", "Szafirowi", "Szafirowym", "Szafirow�", "Szafirowymi", form));
            break;
        case 27:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Emerald", form), 
                _GetPolishTavernNameAdjective("Szmaragdowy", "Szmaragdowe", "Szmaragdowa", "Szmaragdowi", "Szmaragdowym", "Szmaragdow�", "Szmaragdowymi", form));
            break;
        case 28:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Old", form), 
                _GetPolishTavernNameAdjective("Stary", "Stare", "Stara", "Starzy", "Starym", "Star�", "Starymi", form));
            break;
        case 29:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Royal", form), 
                _GetPolishTavernNameAdjective("Kr�lewski", "Kr�lewskie", "Kr�lewska", "Kr�lewscy", "Kr�lewskim", "Kr�lewsk�", "Kr�lewskimi", form));
            break;
        case 30:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Silver", form), 
                _GetPolishTavernNameAdjective("Srebrny", "Srebrne", "Srebrna", "Srebrni", "Srebrnym", "Srebrn�", "Srebrnymi", form));
            break;
        case 31:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Iron", form), 
                _GetPolishTavernNameAdjective("�elazny", "�elazne", "�elazna", "�ela�ni", "�elaznym", "�elazn�", "�elaznymi", form));
            break;
        case 32:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Heroic", form), 
                _GetPolishTavernNameAdjective("Waleczny", "Waleczne", "Waleczna", "Waleczni", "Walecznym", "Waleczn�", "Walecznymi", form));
            break;
        case 33:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Brave", form), 
                _GetPolishTavernNameAdjective("Odwa�ny", "Odwa�ne", "Odwa�na", "Odwa�ni", "Odwa�nym", "Odwa�n�", "Odwa�nymi", form));
            break;
        case 34:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Courageous", form), 
                _GetPolishTavernNameAdjective("Dzielny", "Dzielne", "Dzielna", "Dzielni", "Dzielnym", "Dzieln�", "Dzielnymi", form));
            break;
        case 35:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Loyal", form), 
                _GetPolishTavernNameAdjective("Lojalny", "Lojalne", "Lojalna", "Lojalni", "Lojalnym", "Lojaln�", "Lojalnymi", form));
            break;
        case 36:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Wise", form), 
                _GetPolishTavernNameAdjective("M�dry", "M�dre", "M�dra", "M�drzy", "M�drym", "M�dr�", "M�drymi", form));
            break;
        case 37:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Pious", form), 
                _GetPolishTavernNameAdjective("Pobo�ny", "Pobo�ne", "Pobo�na", "Pobo�ni", "Pobo�nym", "Pobo�n�", "Pobo�nymi", form));
            break;
        case 38:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Bored", form), 
                _GetPolishTavernNameAdjective("Znudzony", "Znudzone", "Znudzona", "Znudzeni", "Znudzonym", "Znudzon�", "Znudzonymi", form));
            break;
        case 39:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Cherry", form), 
                _GetPolishTavernNameAdjective("Wi�niowy", "Wi�niowe", "Wi�niowa", "Wi�niowi", "Wi�niowym", "Wi�niow�", "Wi�niowymi", form));
            break;
        case 40:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Apple", form), 
                _GetPolishTavernNameAdjective("Jab�kowy", "Jab�kowe", "Jab�kowa", "Jab�kowi", "Jab�kowym", "Jab�kow�", "Jab�kowymi", form));
            break;
        case 41:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Pear", form), 
                _GetPolishTavernNameAdjective("Gruszkowy", "Gruszkowe", "Gruszkowa", "Gruszkowi", "Gruszkowym", "Gruszkow�", "Gruszkowymi", form));
            break;
        case 42:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Moon", form), 
                _GetPolishTavernNameAdjective("Ksi�ycowy", "Ksi�ycowe", "Ksi�ycowa", "Ksi�ycowi", "Ksi�ycowym", "Ksi�ycow�", "Ksi�ycowymi", form));
            break;
        case 43:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Venomous", form), 
                _GetPolishTavernNameAdjective("Jadowity", "Jadowite", "Jadowita", "Jadowici", "Jadowitym", "Jadowit�", "Jadowitymi", form));
            break;
        case 44:
            part1 = GetLocalizedString(
                _GetEnglishTavernNameAdjective("Poisonous", form), 
                _GetPolishTavernNameAdjective("Truj�cy", "Truj�ce", "Truj�ca", "Truj�cy", "Truj�cym", "Truj�c�", "Truj�cymi", form));
            break;
    }

    return "\"" + part1 + " " + part2 + "\"";
}

string RandomHumanTownName(string sSeedName="default")
{
    string name = "";
    switch (RandomNext(100, sSeedName))
    {
        case 0: name =  "Tottenham"; break;
        case 1: name =  "Islesbury"; break;
        case 2: name =  "Macclesfield"; break;
        case 3: name =  "Farwater"; break;
        case 4: name =  "Caelkirk"; break;
        case 5: name =  "Barnemouth"; break;
        case 6: name =  "Alcombey"; break;
        case 7: name =  "Guthram"; break;
        case 8: name =  "Ballymena"; break;
        case 9: name =  "Irragin"; break;
        case 10: name =  "Favorsham"; break;
        case 11: name =  "Longdale"; break;
        case 12: name =  "Dry Gulch"; break;
        case 13: name =  "Stathford"; break;
        case 14: name =  "Warcester"; break;
        case 15: name =  "Seameet"; break;
        case 16: name =  "Davenport"; break;
        case 17: name =  "Ely"; break;
        case 18: name =  "Westray"; break;
        case 19: name =  "Hillford"; break;
        case 20: name =  "Beckinsale"; break;
        case 21: name =  "Merton"; break;
        case 22: name =  "Southwold"; break;
        case 23: name =  "Murrayfield"; break;
        case 24: name =  "Dundee"; break;
        case 25: name =  "Chesterfield"; break;
        case 26: name =  "New Cresthill"; break;
        case 27: name =  "Thralkeld"; break;
        case 28: name =  "Troutberk"; break;
        case 29: name =  "Swindon"; break;
        case 30: name =  "Rotherhithe"; break;
        case 31: name =  "Easthaven"; break;
        case 32: name =  "Barnemouth"; break;
        case 33: name =  "Blencalgo"; break;
        case 34: name =  "Wolfpine"; break;
        case 35: name =  "Swindlincote"; break;
        case 36: name =  "Ramshorn"; break;
        case 37: name =  "Bredon"; break;
        case 38: name =  "Limesvilles"; break;
        case 39: name =  "Bleakburn"; break;
        case 40: name =  "Emelle"; break;
        case 41: name =  "Gillamoor"; break;
        case 42: name =  "Bexley"; break;
        case 43: name =  "Tardide"; break;
        case 44: name =  "Travercraig"; break;
        case 45: name =  "Deathfall"; break;
        case 46: name =  "Wheldrake"; break;
        case 47: name =  "Braedon"; break;
        case 48: name =  "Swanford"; break;
        case 49: name =  "Tergaron"; break;
        case 50: name =  "Lindow"; break;
        case 51: name =  "Colkirk"; break;
        case 52: name =  "Quan Ma"; break;
        case 53: name =  "Snowmelt"; break;
        case 54: name =  "Langdale"; break;
        case 55: name =  "Conriston"; break;
        case 56: name =  "Harthwaite"; break;
        case 57: name =  "Paendley"; break;
        case 58: name =  "Addersfield"; break;
        case 59: name =  "Arkaley"; break;
        case 60: name =  "Alnwick"; break;
        case 61: name =  "Pendle"; break;
        case 62: name =  "Erast"; break;
        case 63: name =  "Gilramore"; break;
        case 64: name =  "Falcon Haven"; break;
        case 65: name =  "Barcombe"; break;
        case 66: name =  "Ilragorn"; break;
        case 67: name =  "Runswick"; break;
        case 68: name =  "Caerdydd"; break;
        case 69: name =  "Ecrin"; break;
        case 70: name =  "Kald"; break;
        case 71: name =  "Draycott"; break;
        case 72: name =  "Llyne"; break;
        case 73: name =  "Halivaara"; break;
        case 74: name =  "Ozryn"; break;
        case 75: name =  "Bracklewhyte"; break;
        case 76: name =  "Venzor"; break;
        case 77: name =  "Warrington"; break;
        case 78: name =  "Bamburgh"; break;
        case 79: name =  "Haerndean"; break;
        case 80: name =  "Ormskirk"; break;
        case 81: name =  "Dangarnon"; break;
        case 82: name =  "Abingdon"; break;
        case 83: name =  "Farncombe"; break;
        case 84: name =  "Paentmarwy"; break;
        case 85: name =  "Lakeshore"; break;
        case 86: name =  "Holsworthy"; break;
        case 87: name =  "Orilon"; break;
        case 88: name =  "Willesden"; break;
        case 89: name =  "Gramsby"; break;
        case 90: name =  "Auchendinny"; break;
        case 91: name =  "Sharnwick"; break;
        case 92: name =  "Tylwaerdreath"; break;
        case 93: name =  "Dungannon"; break;
        case 94: name =  "Nancledra"; break;
        case 95: name =  "Caelfall"; break;
        case 96: name =  "Urmkirkey"; break;
        case 97: name =  "Black Hallows"; break;
        case 98: name =  "Bellmoral"; break;
        case 99: name =  "Wintervale"; break;
    }
    return name;
}