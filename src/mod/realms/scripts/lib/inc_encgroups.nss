#include "inc_encounters"
#include "inc_groups"
#include "inc_arrays"

void _VoidSpawnCreatureGroup(string sGroup2da, int nLevel, location lLocation, string sSeedName)
{
    SpawnCreatureGroup(sGroup2da, nLevel, lLocation, 6.0, FALSE, sSeedName, FALSE, "grp_enccreate");
}

// Wrapper for SpawnCreatureGroup from inc_groups in context of encounters.
void SpawnEncounterGroup(object oEncounter, string sSeedName, string sGroup2da)
{
    int level = GetEncounterLevel(oEncounter);
    string encounterArray = "EncounterCreatures";
    location loc = GetLocationWithinEncounterBounds(oEncounter, 6.0, 6.0);
    CreateObjectArray(encounterArray, 0, oEncounter);
    AssignCommand(oEncounter, _VoidSpawnCreatureGroup(sGroup2da, level, loc, sSeedName));
}