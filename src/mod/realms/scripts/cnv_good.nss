#include "inc_common"

void main()
{
    string param = GetScriptParam("Value");
    int shift = StringToInt(param);
    AdjustAligmentOfAllPCs(ALIGNMENT_GOOD, shift);
}
