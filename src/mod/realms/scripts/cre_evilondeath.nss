#include "inc_common"

void main()
{
    object killer = GetLastKiller();
    if (GetIsPC(GetPCMaster(killer)))
    {
        AdjustAligmentOfAllPCs(ALIGNMENT_EVIL, 10);
    }
}