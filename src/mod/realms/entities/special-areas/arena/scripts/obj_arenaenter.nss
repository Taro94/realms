#include "inc_tiles"
#include "inc_horses"

void main()
{
    object tile = GetTile(OBJECT_SELF);
    object wp = GetTileWaypoint(tile, "wp_arenaentrance");
    object PC = GetEnteringObject();

    AssignCommand(PC, JumpToObjectRespectingHorses(wp));
}
