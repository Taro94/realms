#include "inc_convoptions"
#include "inc_arrays"
#include "inc_companions"
#include "inc_language"

void main()
{
    object PC = GetPCSpeaker();

    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    //First option is the speaking PC
    string option = GetLocalizedString("I will fight.", "Ja b�d� walczy�.");
    string value = ObjectToString(PC);
    AddStringArrayElement(optionsArray, option);
    AddStringArrayElement(valuesArray, value);

    //Other options are PC's henchmen
    int i = 1;
    object companion = GetHenchman(PC, i);
    while (GetIsObjectValid(companion))
    {
        if (GetIsCreatureCompanion(companion))
        {
            option = GetName(companion) + ".";
            value = ObjectToString(companion);
            AddStringArrayElement(optionsArray, option);
            AddStringArrayElement(valuesArray, value);
        }
        companion = GetHenchman(PC, ++i);
    }

    LoadConversationOptions(optionsArray, valuesArray);
}
