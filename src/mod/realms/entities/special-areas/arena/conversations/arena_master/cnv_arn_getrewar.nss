#include "inc_quests"
#include "inc_specialare"

//Get reward from the arena master and set variable denoting it
void main()
{
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    SetLocalInt(encounter, "PostReward", TRUE);
    
    object PC = GetPCSpeaker();
    int reward = GetLocalInt(encounter, "REWARD");
    GiveGoldToCreature(PC, reward);

    //If there is a quest to bring the figurine, give the figurine
    object quest = GetQuestOfSpecialArea(encounter);
    if (GetQuestScript(quest) == "que_arenachamp")
    {
        object figurine = CreateItemOnObject("it_arenatrophy", PC);
        SetLocalObject(figurine, "Quest", quest);
        SetLocalObject(quest, "Figurine", figurine);
    }
}
