#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_towns"
#include "inc_scriptevents"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_arenachamp";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object area = GetQuestSpecialArea(quest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object area = GetQuestSpecialArea(quest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    result.gold = 150 + RandomNext(101, sSeedName);
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object area = GetQuestSpecialArea(oQuest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Mistrz Areny";
        result.journalInitial = questgiverName+" z "+townName+" pragnie pozyska� do swojej kolekcji trofeum, kt�re mog� wygra� podejmuj�c si� walk na arenie. Za statuetk� otrzymam "+IntToString(questReward.gold)+" sztuk z�ota, mog� te� zatrzyma� dla siebie nagrod� pieni�n� z areny.";
        result.journalSuccess = "Mam statuetk�. Nadszed� czas wr�ci� do "+townName+", gdzie "+questgiverName+" wr�czy mi za ni� "+IntToString(questReward.gold)+" sztuk z�ota nagrody.";
        if (isMale)
            result.journalFailure = "Nie uda�o mi si� zwyci�y� na arenie. "+questgiverName+" z "+townName+" b�dzie musia� zrezygnowa� z tego trofeum w swej kolekcji.";
        else
            result.journalFailure = "Nie uda�o mi si� zwyci�y� na arenie. "+questgiverName+" z "+townName+" b�dzie musia�a zrezygnowa� z tego trofeum w swej kolekcji.";
        if (isMale)
            result.journalSuccessFinished = questgiverName+" jest zadowolony z figurki, a do mojej kieszeni trafi�o wi�cej z�ota ni� tylko nagroda z areny. To by� dobry dzie�.";
        else
            result.journalSuccessFinished = questgiverName+" jest zadowolona z figurki, a do mojej kieszeni trafi�o wi�cej z�ota ni� tylko nagroda z areny. To by� dobry dzie�.";
        result.journalAbandoned = "Zap�ata nie by�a godziwa, a je�eli kiedy� najdzie mnie ochota na branie udzia�u w walkach na arenie, to trofeum zachowam dla siebie.";
        result.npcIntroLine = "Z niebios mi spadasz. Ju� wiem, jak mog� zdoby� t� statuetk�!";
        result.npcContentLine = "Zaczn� od pocz�tku. Moj� pasj� jest kolekcjonowanie rzadkich przedmiot�w, cho� nie zawsze warto�ciowych. Od wielu miesi�cy pragn� doda� do swojej kolekcji statuetk�, kt�r� mo�na wygra� bior�c udzia� w walkach na pobliskiej arenie za miastem. Ja nie nadaj� si� do walki, ale co innego ty!";
        result.npcRewardLine = "Je�eli podejmiesz wyzwanie, zwyci�ysz i przyniesiesz mi statuetk�, wr�cz� ci w zamian "+IntToString(questReward.gold)+" sztuk z�ota. Mo�e nie wygl�da to na wiele, ale poza statuetk� za wygran� otrzymasz r�wnie� poka�n� nagrod� pieni�n�, kt�r� mo�esz zatrzyma�. Co powiesz?";
        result.npcBeforeCompletionLine = "Gdzie moja statuetka?";
        result.playerReportingSuccess = "Oto ona. Niech stanowi ozdob� twej kolekcji.";
        result.playerReportingFailure = "Nie mam jej. Nie uda�o mi si� uko�czy� wyzwania.";
        result.npcReceivedSuccess = "O tak! Jest wspania�a! We� to z�oto. To dla mnie niewielka cena za tak rzadki przedmiot!";
        if (isMale)
            result.npcReceivedFailure = "A niech to! Minie sporo czasu, zanim arena zaakceptuje wyzwanie nast�pnych �mia�k�w. Jestem tob� rozczarowany.";
        else
            result.npcReceivedFailure = "A niech to! Minie sporo czasu, zanim arena zaakceptuje wyzwanie nast�pnych �mia�k�w. Jestem tob� rozczarowana.";
        result.npcAfterSuccess = "Musz� pomy�le� nad dobrym miejscem dla tego eksponatu! Mo�e na kredensie? Nie, tam nie...";
        result.npcAfterFailure = "Skoro nie uda�o si� tej statuetki wygra�, mo�e b�dzie trzeba j� ukra��. Tylko �artuj�! Ale z drugiej strony...";
        result.npcAfterRefusal = "Na arenie mo�na du�o zarobi�, ale jak nie chcesz, to twoja sprawa!";
    }
    else
    {
        result.journalName = "Arena Champion";
        result.journalInitial = questgiverName+" of "+townName+" wants to add to their collection a trophy that will be awarded to the hero who survives in a local arena. I can trade that figurine for "+IntToString(questReward.gold)+" gold pieces in addition to keeping the monetary reward from the arena.";
        result.journalSuccess = "I have the figurine. It is time for me to return to "+townName+", where "+questgiverName+" will reward me with "+IntToString(questReward.gold)+" gold.";
        result.journalFailure = "I have lost the arena challenge. "+questgiverName+" of "+townName+" will have no way but to give up on the trophy.";
        result.journalSuccessFinished = questgiverName+" is happy with the trophy and my pockets are now heavier than if I were to keep it. This was a good day.";
        result.journalAbandoned = "The payment was not good enough and if I ever decide to fight on the arena, I shall keep the trophy for myself.";
        result.npcIntroLine = "Heavens have sent you to me. Now I know how I can lay my hands on this figurine!";
        result.npcContentLine = "Let me begin from the start. My passion is collecting rare items, though not necessarily valuable. For many months now I've been trying to acquire a figurine that can be won by fighting in a local arena outside of town. I'm not the type fit for battle, unlike you!";
        result.npcRewardLine = "If you take the challenge, win and bring me that trophy, I'll give you "+IntToString(questReward.gold)+" gold coins. This may not look much, but besides the trophy you will be rewarded with a hefty sum of gold which you'll be free to keep. What say you?";
        result.npcBeforeCompletionLine = "Where's my figurine?";
        result.playerReportingSuccess = "Here it is. May it become the cornerstone of your collection.";
        result.playerReportingFailure = "I don't have it. I failed the challenge.";
        result.npcReceivedSuccess = "Oh yes! It's marvelous! Take this gold. It's a small price to pay for such a rare item!";
        result.npcReceivedFailure = "To hells! It will be a long time before the arena accepts new participants. I must say I'm disappointed.";
        result.npcAfterSuccess = "I need to think of a good spot to put it in. Maybe on top of the wardrobe? No, not there...";
        result.npcAfterFailure = "If the figurine could not be won, maybe it can be stolen. I'm just kidding! Or am I...";
        result.npcAfterRefusal = "The arena offers its champions good coin, but if you don't want it, fine, that's your call.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    object tile = GetTile(questGiver);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
        switch (nIndex)
        {
            case 0:
                return isMale 
                    ? "If you ask me, "+questgiverName+" is crazy. He's obsessed with that 'collection' of his. Hasn't he asked you to bring him some pointless junk yet?"
                    : "If you ask me, "+questgiverName+" is crazy. She's obsessed with that 'collection' of hers. Hasn't she asked you to bring her some pointless junk yet?";
        }
    if (nLanguage == LANGUAGE_POLISH)
        switch (nIndex)
        {
            case 0:
                return isMale 
                    ? "Je�li kto� mia�by mnie pyta� o zdanie, "+questgiverName+" jest walni�ty. Ma bzika na punkcie tej swojej ca�ej 'kolekcji'. Nie prosi� ci� jeszcze o znalezienie jakich� bezwarto�ciowych �mieci?"
                    : "Je�li kto� mia�by mnie pyta� o zdanie, "+questgiverName+" jest walni�ta. Ma bzika na punkcie tej swojej ca�ej 'kolekcji'. Nie prosi�a ci� jeszcze o znalezienie jakich� bezwarto�ciowych �mieci?";
        }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{
    object quest = GetLocalObject(oQuest, "Figurine");
    DestroyObject(quest);
}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{
    object quest = GetLocalObject(oQuest, "Figurine");
    DestroyObject(quest);
}

// Function that may perform custom logic when the quest is generated.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed.
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}