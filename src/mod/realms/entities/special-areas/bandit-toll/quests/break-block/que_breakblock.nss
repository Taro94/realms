#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_towns"
#include "inc_scriptevents"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_breakblock";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object area = GetQuestSpecialArea(quest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object area = GetQuestSpecialArea(quest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    result.gold = 450 + RandomNext(201, sSeedName);
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object area = GetQuestSpecialArea(oQuest);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Bandyckie myto";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" zleci� mi pozbycie si� bandyt�w, kt�rzy zabarykadowali si� na szlaku i ��daj� haraczu za przej�cie od ka�dego podr�nika. Mam za to otrzyma� "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = questgiverName+" z "+townName+" zleci�a mi pozbycie si� bandyt�w, kt�rzy zabarykadowali si� na szlaku i ��daj� haraczu za przej�cie od ka�dego podr�nika. Mam za to otrzyma� "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = "Blokada bandyt�w zniszczona, a kupcy oraz inni podr�ni mog� na nowo czu� si� bezpiecznie. W "+townName+" "+questgiverName+" czeka na dobre wie�ci z moj� nagrod�, "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = "Jedyne, co uda�o mi si� osi�gn��, to rozw�cieczenie bandyt�w. Podr�uj�cy po regionie s� teraz w jeszcze wi�kszym niebezpiecze�stwie. "+questgiverName+" z "+townName+" powinien si� o tym dowiedzie�.";
        else
            result.journalFailure = "Jedyne, co uda�o mi si� osi�gn��, to rozw�cieczenie bandyt�w. Podr�uj�cy po regionie s� teraz w jeszcze wi�kszym niebezpiecze�stwie. "+questgiverName+" z "+townName+" powinna si� o tym dowiedzie�.";
        result.journalSuccessFinished = "�otry nie stanowi� ju� zagro�enia, a mieszka�cy "+townName+" mog� podr�owa� po okolicy bezpiecznie.";
        result.journalAbandoned = "Wol� zap�aci� bandytom myto zamiast ich prowokowa�. Moja podr� jest wystarczaj�co niebezpieczna bez szukania k�opot�w.";
        result.npcIntroLine = "Czy chcesz pom�c podr�uj�cym z i do "+townName+", a przy okazji dobrze zarobi�?";
        result.npcContentLine = "Bandyci za�o�yli obozowisko i prowizoryczn� barykad� na samym �rodku szlaku, kt�rym cz�sto w�druj� kupcy i inni podr�ni. Domagaj� si� z�ota za przej�cie, a kto nie zap�aci, ten musi zawr�ci� albo straci g�ow�. Wi�kszo�� decyduje si� p�aci�, lecz moim zdaniem lepiej zap�aci� troch� wi�cej i pozby� si� problemu raz na zawsze.";
        result.npcRewardLine = "Oferuj� ci "+IntToString(questReward.gold)+" sztuk z�ota, wystarczy, �e pozb�dziesz si� bandyt�w. Nie obchodzi mnie, czy zgin�, czy zwiej� gdzie pieprz ro�nie - ma ich tu nie by�!";
        result.npcBeforeCompletionLine = "Tak? Jakie� nowe wie�ci w sprawie blokady bandyt�w?";
        result.playerReportingSuccess = "Wasz problem zosta� rozwi�zany. Podr�ni na miejscu zastan� jedynie pozosta�o�ci w postaci palisad i namiot�w.";
        result.playerReportingFailure = "�ajdaki mnie pokona�y. Obawiam si�, �e teraz mog� jeszcze agresywniej obchodzi� si� z podr�nymi.";
        if (isMale)
            result.npcReceivedSuccess = "Wybornie! Wiedzia�em, �e lepiej spo�ytkuj� pieni�dze na kogo� takiego, jak ty, ni� na haracze dla tych szumowin! Oto obiecane z�oto.";
        else
            result.npcReceivedSuccess = "Wybornie! Wiedzia�am, �e lepiej spo�ytkuj� pieni�dze na kogo� takiego, jak ty, ni� na haracze dla tych szumowin! Oto obiecane z�oto.";
        result.npcReceivedFailure = "A niech to! Mo�e lepiej by�o te pieni�dze odda� bandytom...";
        result.npcAfterSuccess = "Drogi s� zn�w bezpieczne. To powinno o�ywi� handel z s�siednimi wioskami.";
        result.npcAfterFailure = "Spo�eczno�� "+townName+" musi uzbiera� tyle z�ota, ile to mo�liwe. Mo�e to udobrucha tych �ajdak�w.";
        result.npcAfterRefusal = "Trudno. W ko�cu zjawi si� kto� ch�tny.";
    }
    else
    {
        result.journalName = "Bandit toll";
        result.journalInitial = questgiverName+" of "+townName+" ordered me to get rid of the bandits who have created a crude barricade on the road nearby and demand payment from every traveller. I am to receive "+IntToString(questReward.gold)+" for the task.";
        result.journalSuccess = "The bandit blockade has been broken, merchants and other travellers can feel safe again. "+questgiverName+" awaits the good news in "+townName+" and is probably eager to hand me "+IntToString(questReward.gold)+" gold pieces of reward.";
        result.journalFailure = "I have only made things worse by angering the bandits. Everyone traversing the region will be in an even greater danger now. "+questgiverName+" of "+townName+" should be informed of this.";
        result.journalSuccessFinished = "These thugs pose no threat to anyone anymore and the people of "+townName+" can travel safely.";
        result.journalAbandoned = "I'd rather pay the bandits their toll than provoke them. My journey is dangerous enough without looking for extra trouble.";
        result.npcIntroLine = "Would you like to help the people travelling to and from "+townName+" and earn some coin at the same time?";
        result.npcContentLine = "Bandits have made a camp and a crude barricade in the middle of a bustling road frequently traversed by merchants and other travellers. They demand payment for passage and whoever refuses has to turn back or lose their head. Most decide to pay, but I personally think it is better to pay more and get rid of the problem once and for all.";
        result.npcRewardLine = "My offer is "+IntToString(questReward.gold)+" pieces of gold, all you have to do is get rid of the bandits. I don't care if they die or run with their tails behind their backs - I want them gone!";
        result.npcBeforeCompletionLine = "Yes? Any news regarding the bandit blockade?";
        result.playerReportingSuccess = "Your problem is solved now. Travellers will only find abandoned palisades and tents in place of the blockade.";
        result.playerReportingFailure = "The bastards have gotten the better of me. I'm afraid they may become even more aggressive towards travellers now.";
        result.npcReceivedSuccess = "Splendid! I knew that it was better to pay the money to someone like you rather than spend in on tolls for these scum! Here is your promised gold.";
        result.npcReceivedFailure = "Curses! Maybe it would have been better to give this gold to bandits after all...";
        result.npcAfterSuccess = "The roads are safe once more. This should revive the trade with neighboring villages.";
        result.npcAfterFailure = "The people of "+townName+" must now collect as much gold as possible. Maybe that can help us avoid the wrath of these bastards.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    object tile = GetTile(questGiver);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
        switch (nIndex)
        {
            case 0:
                return questgiverName+" is planning to hire someone to get rid of the bandits who have made a barricade on the road near "+townName+". I hope this won't anger them even more.";
        }
    if (nLanguage == LANGUAGE_POLISH)
        switch (nIndex)
        {
            case 0:
                return questgiverName+" planuje naj�� kogo�, kto pozb�dzie si� blokady bandyt�w, kt�rzy ufortyfikowali si� na drodze nieopodal "+townName+". Mam nadziej�, �e nie rozw�cieczy ich to jeszcze bardziej.";
        }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed.
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{
    object area = GetQuestSpecialArea(oQuest);
    SetLocalObject(area, "QUEST", oQuest);

    //Set intimidate DC for completing the quest without a fight
    SetLocalInt(oQuest, "INTIMIDATE_DC", 15);

    //OnDeath script for bandits to complete the quest
    object bandit = GetFirstObjectInArea(area);
    while (GetIsObjectValid(bandit))
    {
        if (GetTag(bandit) == "cre_bandtoll")
            AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_brkblkdeath", bandit);
        bandit = GetNextObjectInArea(area);
    }
}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{
    object area = GetQuestSpecialArea(oQuest);
    if (GetArea(oRespawningPC) == area && GetQuestState(oQuest) == QUEST_STATE_LINGERING)
    {
        UpdateQuestState(oQuest, QUEST_STATE_FAILED);
    }
}