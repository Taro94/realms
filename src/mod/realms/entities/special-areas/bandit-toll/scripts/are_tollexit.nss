// When the last PC leaves the bandit toll area, reset the entrance trigger
void main()
{
    object exiting = GetExitingObject();
    if (!GetIsPC(exiting))
        return;

    object PC = GetFirstObjectInArea(OBJECT_SELF);
    while (GetIsObjectValid(PC))
    {
        if (GetIsPC(PC) && PC != exiting)
            return;

        PC = GetNextObjectInArea(OBJECT_SELF);
    }

    DeleteLocalObject(OBJECT_SELF, "EnterTrigger");
    DeleteLocalInt(OBJECT_SELF, "WARNED");
}