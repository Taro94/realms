#include "inc_partyskills"
#include "inc_factions"
#include "hench_i0_ai"

void main()
{
    //Party pickpocket check on every attempt to take something, bandits turn hostile on fail
    int dc = GetLocalInt(GetArea(OBJECT_SELF), "WAGONSTEAL_DC");
    object PC = GetLastDisturbed();
    int type = GetInventoryDisturbType();
    if (type == INVENTORY_DISTURB_TYPE_ADDED || GetIsPartySkillSuccessful(PC, SKILL_PICK_POCKET, dc))
        return;

    int i = 1;
    string tag = "cre_bandtoll";
    object bandit = GetNearestObjectByTag(tag, OBJECT_SELF, i++);
    while (GetIsObjectValid(bandit))
    {
        if (!GetLocalInt(bandit, "STARTED_ATTACKING"))
        {
            SetLocalInt(bandit, "STARTED_ATTACKING", TRUE);
            SetFaction(bandit, FACTION_BANDITS);
            AssignCommand(bandit, HenchDetermineCombatRound());
        }
        bandit = GetNearestObjectByTag(tag, OBJECT_SELF, i++);
    }
}