#include "inc_partyskills"
#include "inc_reputation"

int StartingConditional()
{
    //Return TRUE if party intimidate check succeeds
    object specialArea = GetArea(OBJECT_SELF);
    object quest = GetLocalObject(specialArea, "QUEST");
    int dc = GetLocalInt(quest, "INTIMIDATE_DC") + GetReputationDCModifier();
    return GetIsPartySkillSuccessful(GetPCSpeaker(), SKILL_INTIMIDATE, dc);
}
