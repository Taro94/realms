int StartingConditional()
{
    //Return TRUE if the party can pass (paid or intimidated successfully)
    return GetLocalInt(GetArea(OBJECT_SELF), "CAN_PASS");
}
