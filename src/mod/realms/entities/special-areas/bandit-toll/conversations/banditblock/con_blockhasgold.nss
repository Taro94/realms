#include "inc_tokens"

int StartingConditional()
{
    //Set token 1001 to price to pay for passage and return TRUE if the PC has such gold
    int toll = GetLocalInt(GetArea(OBJECT_SELF), "TOLL");
    SetCustomTokenEx(1001, IntToString(toll));
    return GetGold(GetPCSpeaker()) >= toll;
}
