#include "inc_tokens"

int StartingConditional()
{
    //Set token 1001 to the price to pay for passage and return TRUE
    int toll = GetLocalInt(GetArea(OBJECT_SELF), "TOLL");
    SetCustomTokenEx(1001, IntToString(toll));
    return TRUE;
}
