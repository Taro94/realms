#include "inc_factions"
#include "inc_quests"

void _Leave()
{
    if (GetLocalInt(OBJECT_SELF, "STARTED_LEAVING"))
        return;

    object specialArea = GetArea(OBJECT_SELF);
    object quest = GetLocalObject(specialArea, "QUEST");

    object PC = GetPCSpeaker();
    int i = 1;
    string tag = "cre_bandtoll";
    object bandit = GetNearestObjectByTag(tag, PC, i++);
    while (GetIsObjectValid(bandit))
    {
        SetLocalInt(bandit, "STARTED_LEAVING", TRUE);
        SetFaction(bandit, FACTION_TEMPORARY_NEUTRAL);
        SetPlotFlag(bandit, TRUE);
        AssignCommand(bandit, ActionMoveAwayFromObject(PC, TRUE));
        DestroyObject(bandit, 3.0f);
        bandit = GetNearestObjectByTag(tag, PC, i++);
    }
}

void main()
{
    //Mark the bandits as ready to leave in a few seconds (and turn them plot so they can't be attacked anymore and won't attack you)
    SetLocalInt(OBJECT_SELF, "READY_TO_LEAVE", TRUE);
    object PC = GetPCSpeaker();
    int i = 1;
    string tag = "cre_bandtoll";
    object bandit = GetNearestObjectByTag(tag, PC, i++);
    while (GetIsObjectValid(bandit))
    {
        SetPlotFlag(bandit, TRUE);
        bandit = GetNearestObjectByTag(tag, PC, i++);
    }
    DelayCommand(6.0f, _Leave());
}
