#include "inc_partyskills"
#include "inc_reputation"

int StartingConditional()
{
    //Return TRUE if party intimidate check succeeds
    int dc = GetLocalInt(GetArea(OBJECT_SELF), "INTIMIDATE_DC") + GetReputationDCModifier();
    return GetIsPartySkillSuccessful(GetPCSpeaker(), SKILL_INTIMIDATE, dc);
}
