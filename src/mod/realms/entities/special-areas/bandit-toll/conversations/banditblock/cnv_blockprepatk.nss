#include "inc_factions"
#include "hench_i0_ai"

void _Attack()
{
    if (GetLocalInt(OBJECT_SELF, "STARTED_ATTACKING"))
        return;

    object PC = GetPCSpeaker();
    int i = 1;
    string tag = "cre_bandtoll";
    object bandit = GetNearestObjectByTag(tag, PC, i++);
    while (GetIsObjectValid(bandit))
    {
        SetLocalInt(bandit, "STARTED_ATTACKING", TRUE);
        SetFaction(bandit, FACTION_BANDITS);
        AssignCommand(bandit, HenchDetermineCombatRound());
        bandit = GetNearestObjectByTag(tag, PC, i++);
    }
}

void main()
{
    //Make the caller attack the PC in 6 seconds (unless the conversation is cancelled earlier)
    SetLocalInt(OBJECT_SELF, "READY_TO_ATTACK", TRUE);
    DelayCommand(6.0f, _Attack());
}
