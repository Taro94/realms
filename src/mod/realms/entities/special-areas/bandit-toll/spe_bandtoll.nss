#include "inc_debug"
#include "inc_random"
#include "inc_language"
#include "hnd_specialare"
#include "inc_biomes"
#include "inc_tiles"
#include "inc_scriptevents"
#include "inc_arrays"
#include "inc_factions"
#include "x0_i0_spawncond"
#include "inc_loot"

/////////////////////////
// ------------------- //
// Special area script //
// ------------------- //
/////////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "spe_bandtoll";

// Number of facility area templates (each in 4, 11 or 15 variants based on deadendAreas and passThroughAreas values)
int areasNumber = 1;

// Boolean value determining whether this special area may appear as a deadend area (an area with only a single entrance/exit).
// Either this or passThroughAreas needs to be TRUE (or both).
// If this is TRUE, area templates need to include variants with single exits (i.e. _01, _02, _04, _08).
int deadendAreas = FALSE;

// Boolean value determining whether this special area may appear as a pass-through area (an area with with multiple entrances/exits).
// Either this or deadendAreas needs to be TRUE (or both).
// If this is TRUE, area templates need to include variants with multiple exits (i.e. _03, _05, _06, _07, _09, _10, _11, _12, _13, _14, _15).
int passThroughAreas = TRUE;

// Minimum PC level this special area supports
int minPlayerLevel = 1;

// Maximum PC level this special area supports
int maxPlayerLevel = 40;

// Number of rumors regarding this special area (can be 0)
int rumorsNumber = 1;

// Number of available regular (non-specialty) quests for this special area
int regularQuestsNumber = 2;

// Number of available specialty quests for this special area (can be 0)
int specialtyQuestsNumber = 0;

// Set this to TRUE if you want quests for this special area only to generate if no preferred ones are available
int lowQuestGenerationPriority = FALSE;



//////////////////////
// Script functions //
//////////////////////


// Function that should return ResRef of an available area to spawn for nIndex in range [0, areasNumber-1] and provided exits flag (i.e. 15, 14, 13, etc.).
// You can use oBiome to use different variants based on biome (only needed if the special area is registered in multiple biomes).
// Only those exit flags that are determined by passThroughAreas and deadendAreas values need to be considered.
string OnGetSpecialAreaResRef(int nIndex, object oBiome, int nExitsFlag)
{
    string result = "toll_";

    string biomeScript = GetBiomeScript(oBiome);
    if (biomeScript == "bio_rural")
    {
        result += "rural_";
    }

    string exitsFlag = IntToString(nExitsFlag);
    if (nExitsFlag < 10)
        exitsFlag = "0" + exitsFlag;
    result += exitsFlag;

    return result;
}

// Function that is executed when the special area is spawned. Use it to spawn creatures and objects in the special area,
// create sub-areas and perform all other actions. You can use nBiomeStartingLevel and oBiome to base your special area's contents
// on factors such as expected PC level, townfolk's dominant race (to spawn appropriate NPCs), etc.
// You can also access other biome properties via functions executed on oBiome.
// Remember to call AddAreaToTile(oSpecialArea, newArea) from inc_tiles after creating any new sub-areas of oSpecialArea
// and note that any areas created this way will not have rest encounters turned on by default, but oSpecialArea will (see inc_resting for details).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed.
void OnInitializeSpecialArea(string sSeedName, object oBiome, object oSpecialArea, int nBiomeStartingLevel)
{
    //Encounter variables
    SetLocalInt(oSpecialArea, "TOLL", 350);
    SetLocalInt(oSpecialArea, "INTIMIDATE_DC", 11);
    SetLocalInt(oSpecialArea, "WAGONSTEAL_DC", 11);
    SetLocalInt(oSpecialArea, "WAGONHIDE_DC", 11);

    //Area OnExit
    AddEventScript(SUBEVENT_AREA_ON_EXIT, "are_tollexit", oSpecialArea);

    //Spawn bandit loot in the wagon
    object wagon = GetNearestObjectByTag("obj_banditwagon", GetFirstObjectInArea(oSpecialArea));
    SetEventScript(wagon, EVENT_SCRIPT_PLACEABLE_ON_INVENTORYDISTURBED, "obj_bwagdisturb"); //Note that obj_bwagonopen has already been foolishly set in the toolset on the wagon
    SpawnRandomTreasureInInventory(sSeedName, wagon, 3);

    //Spawn bandits
    CreateStringArray("ResRefs");
    AddStringArrayElement("ResRefs", "cre_bandita");
    AddStringArrayElement("ResRefs", "cre_banditb");

    object wp = GetFirstObjectInArea(oSpecialArea);
    while (GetIsObjectValid(wp))
    {
        string tag = GetTag(wp);
        if (GetObjectType(wp) == OBJECT_TYPE_WAYPOINT && (tag == "wp_bandit" || tag == "wp_banditstatic") && !GetLocalInt(wp, "SPAWNED_BANDIT"))
        {
            string resref = GetStringArrayElement("ResRefs", RandomNext(GetStringArraySize("ResRefs"), sSeedName));
            object bandit = CreateCreature(resref, GetLocation(wp), FALSE, "cre_bandtoll");
            SetFaction(bandit, FACTION_TEMPORARY_NEUTRAL);

            //Random animations
            AssignCommand(bandit, SetSpawnInCondition(NW_FLAG_IMMOBILE_AMBIENT_ANIMATIONS));
            if (tag == "wp_bandit")
                AssignCommand(bandit, SetSpawnInCondition(NW_FLAG_AMBIENT_ANIMATIONS));
            
            //Conversation
            SetLocalString(bandit, "Conversation", "banditblock");
            AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", bandit);

            //On perception warning
            AddEventScript(SUBEVENT_CREATURE_ON_PERCEPTION, "cre_tollwarn", bandit);

            SetLocalInt(wp, "SPAWNED_BANDIT", TRUE);
        }
        wp = GetNextObjectInArea(oSpecialArea);
    }
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on the special area instance
string OnGetSpecialAreaRumor(object oSpecialArea, int nIndex, int nLanguage)
{
    if (nLanguage == LANGUAGE_ENGLISH)
        switch (nIndex)
        {
            case 0:
                return "The bandits have blocked a road outside of town and demand payment from anyone passing through! I hear those who refuse meet a terrible end.";
        }
    if (nLanguage == LANGUAGE_POLISH)
        switch (nIndex)
        {
            case 0:
                return "Bandyci zablokowali drog� za osad� i domagaj� si� haraczu od ka�dego, kto pr�buje przej��! Podobno tych, kt�rzy odmawiaj�, spotyka straszny koniec.";
        }
    return "";
}

// Function that should return a random name for a special area, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomSpecialAreaName(string sSeedName, int nLanguage)
{
    if (nLanguage == LANGUAGE_POLISH)
    {
        switch (RandomNext(1))
        {
            case 0:
                return "Blokada bandyt�w";
        }
    }
    else
    {
        switch (RandomNext(1))
        {
            case 0:
                return "Bandit blockade";
        }
    }
    return "";
}

// Function that should return available regular quest script names for nIndex in range [0, regularQuestsNumber-1]
string OnGetSpecialAreaRegularQuestScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "que_breakblock";
        case 1:
            return "que_stolenlett";
    }
    return "";
}

// Function that should return available specialty quest script names for nIndex in range [0, specialtyQuestsNumber-1];
// A specialty quest is one that may either not be doable by the player(s) because it requires a certain skill or attributes
// (for example, a quest to pick pocket something from an NPC), or is inherently evil (for example murder)
// and/or bad for reputation, so that good-aligned PCs or anyone caring about their reputation would refuse it;
// This function never runs if specialtyQuestsNumber is 0, so it does not matter what it returns in that case (it still needs to exist and compile)
string OnGetSpecialAreaSpecialtyQuestScript(int nIndex)
{
    return "";
}