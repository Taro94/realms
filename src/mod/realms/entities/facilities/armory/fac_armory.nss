#include "inc_debug"
#include "inc_random"
#include "inc_language"
#include "hnd_facilities"
#include "inc_towns"
#include "inc_loot"
#include "inc_biomes"
#include "inc_arrays"
#include "inc_scriptevents"
#include "inc_areas"

//////////////////////
// ---------------- //
// Facility script  //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "fac_armory";

// Number of facility area templates (facility area should have its exit to the south, each area should potentially have variants for different towns/biomes, but these still count as 1)
int areasNumber = 1;

// Minimum PC level this facility supports
int minPlayerLevel = 1;

// Maximum PC level this facility supports
int maxPlayerLevel = 40;

// Number of rumors regarding this facility (can be 0)
int rumorsNumber = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return ResRef of a facility area to spawn for nIndex in range [0, areasNumber-1] and town object provided;
// For example, if areasNumber is 1, you may still return two different areas (with different tilesets) for a human town and for an elven town.
// You can also base your result on the biome (by using the GetBiomeOfTown() function).
string OnGetFacilityAreaResRef(int nIndex, object oTown)
{
    object biome = GetBiomeOfTown(oTown);
    string biomeScript = GetBiomeScript(biome);
    string template = "";

    //Select variant based on biome
    if (biomeScript == "bio_rural")
        template = "fac_armor_rural";

    return template;
}

// Function that is executed when the facility is spawned. Use it to spawn creatures and objects in the facility area,
// create sub-areas and perform all other actions. You can use nBiomeStartingLevel and oTown to base your facility's contents
// on factors such as expected PC level, townfolk's dominant race (to spawn appropriate NPCs), etc.
// You can also access other town and biome properties via functions executed on oTown.
// Remember to call AddAreaToTile(oTown, newArea) from inc_tiles after creating any new sub-areas of oFacility.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnInitializeFacility(string sSeedName, object oTown, object oFacility, int nBiomeStartingLevel)
{
    string name = GetLocalString(oTown, "Name") + " - " + GetLocalizedString("Armory", "Zbrojownia");
    object interiorArea = CopyAreaWithEventScripts("fac_armor_inter", "", name);
    SetLocalInt(interiorArea, "DRK_DARK_AREA", FALSE);
    AddAreaToTile(oTown, interiorArea);

    //Spawn store
    object wp = GetTileWaypoint(oTown, "wp_armory");
    object store = CreateArmoryStore(sSeedName, nBiomeStartingLevel, wp);

    //Spawn shopkeeper
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num));
    object merchant = CreateCreature(resref, GetLocation(wp));
    SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(merchant));
    SetPlotFlag(merchant, TRUE);
    SetLocalString(merchant, "Conversation", "armory");
    ClearCreatureEventScriptFromVariable(merchant, SUBEVENT_CREATURE_ON_CONVERSATION);
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", merchant);
    int language = GetServerLanguage();
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(merchant), GetGender(merchant), language, sSeedName);
    string lastName = GetRandomCommonerLastName(townScript, GetRacialType(merchant), GetGender(merchant), language, sSeedName);
    SetName(merchant, firstName + " " + lastName);
    SetLocalObject(merchant, "Store", store);
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a facility instance
string OnGetFacilityRumor(object oFacility, int nIndex, int nLanguage)
{
    if (nLanguage == LANGUAGE_ENGLISH)
        switch (nIndex)
        {
            case 0:
                return "There is a large armory on the outskirts where lots of amazing armors and weapons are crafted. It's the pride of our community!";
        }
    if (nLanguage == LANGUAGE_POLISH)
        switch (nIndex)
        {
            case 0:
                return "Na obrze�ach wioski jest zbrojownia, gdzie wytwarzany jest wy�mienity rynsztunek. Jest dum� naszej spo�eczno�ci.";
        }
    return "";
}

// Function that should return a random name for a facility, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomFacilityName(string sSeedName, int nLanguage)
{
    if (nLanguage == LANGUAGE_POLISH)
    {
        switch (RandomNext(1))
        {
            case 0:
                return "Obrze�a";
        }
    }
    else
    {
        switch (RandomNext(1))
        {
            case 0:
                return "Outskirts";
        }
    }
    return "";
}