#include "inc_tiles"
#include "inc_horses"

void main()
{
    object user = GetClickingObject();
    object tile = GetTile(OBJECT_SELF);
    object wp = GetTileWaypoint(tile, "wp_armoryexit");
    AssignCommand(user, JumpToObjectRespectingHorses(wp));
}
