#include "inc_tiles"

void main()
{
    string wpTag = GetLocalString(OBJECT_SELF, "WpTag");
    object wp = GetAreaWaypoint(GetArea(OBJECT_SELF), wpTag);
   
    SetFacing(GetFacing(wp));
    ClearAllActions();
    ActionPlayAnimation(ANIMATION_LOOPING_WORSHIP, 1.0, 10.0f);
}