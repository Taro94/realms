#include "inc_tmpservice"
#include "inc_plots"
#include "inc_realms"

int StartingConditional()
{
    string bless = GetScriptParam("Blessing");
    object realm = GetRealm();
    object plot = GetRealmPlot(realm);
    int biome = GetPlotCurrentBiomeIndex(plot);
    int price = GetBlessingPrice(bless, biome);
    object PC = GetPCSpeaker();

    return GetGold(PC) >= price;
}
