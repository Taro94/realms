#include "inc_tmpservice"
#include "inc_plots"
#include "inc_realms"

void main()
{
    string bless = GetScriptParam("Blessing");
    object realm = GetRealm();
    object plot = GetRealmPlot(realm);
    int biome = GetPlotCurrentBiomeIndex(plot);
    int price = GetBlessingPrice(bless, biome);
    object PC = GetPCSpeaker();

    if (GetGold(PC) < price)
        return;

    TakeGoldFromCreature(price, PC, TRUE);

    if (bless == "Heal")
        TempleHeal();
    else
        TempleBless(bless);
}
