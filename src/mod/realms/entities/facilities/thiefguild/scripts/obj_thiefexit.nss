#include "inc_tiles"
#include "inc_horses"

void main()
{
    object oUser = GetLastUsedBy();
    
    object tile = GetTile(OBJECT_SELF);
    object wp = GetTileWaypoint(tile, "wp_thiefentrance");
    AssignCommand(oUser, JumpToObjectRespectingHorses(wp));
}
