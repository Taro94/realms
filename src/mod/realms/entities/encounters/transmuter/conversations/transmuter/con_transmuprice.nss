#include "inc_tokens"
#include "inc_debug"

int StartingConditional()
{
    //Set CUSTOM1001 to the transmutation price and return TRUE
    string price = IntToString(GetLocalInt(OBJECT_SELF, "PRICE"));
    SetCustomTokenEx(1001, price);
    SetCustomTokenEx(1002, price); //1002 is used in one node because for some bugged reason 1001 is overwritten by a response node under it, although it doesn't happen anywhere else...
    return TRUE;
}
