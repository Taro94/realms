#include "inc_tokens"
#include "inc_convabort"

int StartingConditional()
{
    //Set CUSTOM1001 token to the transmuter's name and return TRUE
    MarkConversationStart("cnv_transabort");
    SetCustomTokenEx(1001, GetName(OBJECT_SELF));
    return TRUE;
}
