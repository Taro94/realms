void main()
{
    //Make the PC pay for the package and get it from NPC
    int gold = GetLocalInt(OBJECT_SELF, "PRICE");
    object PC = GetPCSpeaker();
    TakeGoldFromCreature(gold, PC, TRUE);

    object package = GetLocalObject(OBJECT_SELF, "Package");
    object quest = GetLocalObject(OBJECT_SELF, "Quest");

    //Create a copy, because ActionGiveItem is too unreliable
    object newPackage = CopyItem(package, GetPCSpeaker(), TRUE);
    SetLocalObject(quest, "Package", newPackage);
    SetLocalObject(OBJECT_SELF, "Package", newPackage);
    SetLocalObject(package, "Quest", quest);
    DestroyObject(package);
}
