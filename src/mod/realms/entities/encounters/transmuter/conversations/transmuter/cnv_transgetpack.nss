void main()
{
    //Give the transmuter's package to the PC
    object package = GetLocalObject(OBJECT_SELF, "Package");
    object quest = GetLocalObject(OBJECT_SELF, "Quest");

    //Create a copy, because ActionGiveItem is too unreliable
    object newPackage = CopyItem(package, GetPCSpeaker(), TRUE);
    SetLocalObject(quest, "Package", newPackage);
    SetLocalObject(OBJECT_SELF, "Package", newPackage);
    SetLocalObject(package, "Quest", quest);
    DestroyObject(package);
}
