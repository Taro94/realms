#include "x2_i0_spells"

// Return TRUE if there are exactly two transmutable weapons in the chest.
int GetIsChestInventoryValid(object oChest);

int GetIsChestInventoryValid(object oChest)
{
    object item = GetFirstItemInInventory(oChest);
    int count = 0;
    while (GetIsObjectValid(item))
    {
        int baseType = GetBaseItemType(item);
        int validItem = FALSE;
        if ((GetMeleeWeapon(item) || GetWeaponRanged(item)) && baseType != BASE_ITEM_THROWINGAXE && baseType != BASE_ITEM_DART && baseType != BASE_ITEM_SHURIKEN)
            validItem = TRUE;
        
        count++;
        if (!validItem || count > 2)
            return FALSE;

        item = GetNextItemInInventory(oChest);
    }
    if (count == 2)
        return TRUE;
        
    return FALSE;
}