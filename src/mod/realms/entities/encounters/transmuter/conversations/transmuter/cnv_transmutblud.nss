#include "inc_convoptions"
#include "inc_language"

void main()
{
    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    //Add bludgeoning weapons
    AddStringArrayElement(optionsArray, GetLocalizedString("Club.", "Maczuga."));
    AddStringArrayElement(valuesArray, "club");

    AddStringArrayElement(optionsArray, GetLocalizedString("Light flail.", "Lekki korbacz."));
    AddStringArrayElement(valuesArray, "lflail");

    AddStringArrayElement(optionsArray, GetLocalizedString("Heavy flail.", "Ci�ki korbacz."));
    AddStringArrayElement(valuesArray, "hflail");

    AddStringArrayElement(optionsArray, GetLocalizedString("Light hammer", "Lekki m�ot"));
    AddStringArrayElement(valuesArray, "lhammer");

    AddStringArrayElement(optionsArray, GetLocalizedString("Warhammer.", "M�ot bojowy."));
    AddStringArrayElement(valuesArray, "whammer");

    AddStringArrayElement(optionsArray, GetLocalizedString("Mace.", "Bu�awa."));
    AddStringArrayElement(valuesArray, "mace");

    AddStringArrayElement(optionsArray, GetLocalizedString("Morningstar.", "Morgensztern."));
    AddStringArrayElement(valuesArray, "mornstar");

    AddStringArrayElement(optionsArray, GetLocalizedString("Dire mace.", "Wielka bu�awa."));
    AddStringArrayElement(valuesArray, "diremace");

    AddStringArrayElement(optionsArray, GetLocalizedString("Quarterstaff.", "Laska."));
    AddStringArrayElement(valuesArray, "staff");

    LoadConversationOptions(optionsArray, valuesArray);
}
