#include "inc_language"
#include "inc_chestcheck"

string GetTransmutedItemResRef(object oItem1, object oItem2, string itemType)
{
    string resref1 = GetResRef(oItem1);
    string suffix1 = GetStringRight(resref1, 3);
    string tier1 = GetStringLeft(suffix1, 2);
    int numTier1 = StringToInt(tier1);

    string resref2 = GetResRef(oItem2);
    string suffix2 = GetStringRight(resref2, 3);
    string tier2 = GetStringLeft(suffix2, 2);
    int numTier2 = StringToInt(tier2);

    string outputTier = numTier1 < numTier2 ? tier1 : tier2;
    string result = "it_" + itemType + outputTier + "a";
    
    return result;
}

void main()
{
    //If PC does not have gold for transmutation, return.
    int gold = GetLocalInt(OBJECT_SELF, "PRICE");
    object PC = GetPCSpeaker();
    if (GetGold(PC) < gold)
        return;

    //If there are no longer compatible items in the chest, make the transmuter say something and return.
    object chest = GetLocalObject(OBJECT_SELF, "Chest");
    int itemsNotSuitable = FALSE;
    if (!GetIsChestInventoryValid(chest))
        itemsNotSuitable = TRUE;
    
    //Same if items are too powerful for this transmuter.
    object item1 = GetFirstItemInInventory(chest);
    object item2 = GetNextItemInInventory(chest);
    int tier1 = StringToInt(GetStringLeft(GetStringRight(GetResRef(item1), 3), 2));
    int tier2 = StringToInt(GetStringLeft(GetStringRight(GetResRef(item2), 3), 2));
    int maxTier = GetLocalInt(OBJECT_SELF, "MAXTIER");
    if (tier1 > maxTier || tier2 > maxTier)
        itemsNotSuitable = TRUE;

    if (itemsNotSuitable)
    {
        string msg = GetLocalizedString("I'm sorry, but the contents of the chest have changed.", "Wybacz, ale zawarto�� skrzyni uleg�a zmianie.");
        SpeakString(msg);
        return;
    }

    //Otherwise pay and do the magic.
    TakeGoldFromCreature(gold, PC, TRUE);
    ActionCastFakeSpellAtObject(SPELL_DISPEL_MAGIC, chest);
    string itemType = GetLocalString(OBJECT_SELF, "ItemType");
    string resref = GetTransmutedItemResRef(item1, item2, itemType);
    object newItem = CreateItemOnObject(resref, chest);
    SetIdentified(newItem, TRUE);
    DestroyObject(item1);
    DestroyObject(item2);

    //Make the NPC eager to give the PC a quest package after they've used their services
    DeleteLocalInt(OBJECT_SELF, "HESISTANT");
}
