#include "inc_partyskills"
#include "inc_reputation"
#include "inc_quests"
#include "inc_tokens"

int StartingConditional()
{
    //Set token 1001 to questgiver's name and return TRUE if party Persuade check is successful
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object questgiver = GetQuestGiver(quest);
    SetCustomTokenEx(1001, GetName(questgiver));

    int dc = GetLocalInt(OBJECT_SELF, "PERSUADE_DC") + GetReputationDCModifier();
    return GetIsPartySkillSuccessful(GetPCSpeaker(), SKILL_PERSUADE, dc);
}
