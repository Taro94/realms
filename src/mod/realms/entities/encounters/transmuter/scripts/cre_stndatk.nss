#include "inc_common"
#include "hench_i0_ai"

void main()
{
    if (!GetLocalInt(OBJECT_SELF, "Sitting"))
        return;
    
    DeleteLocalInt(OBJECT_SELF, "Sitting");
    ClearAllActions();
    HenchDetermineCombatRound();
}