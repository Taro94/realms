#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_common"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_delivertran";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    result.gold = 400 + RandomNext(201, sSeedName);
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    string firstName = GetLocalString(encounter, "TransmuterFirstName");
    string lastName = GetLocalString(encounter, "TransmuterLastName");

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Paczka alchemika";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" wynaj�� mnie do odebrania i dostarczenia mu pakunku od alchemika o imieniu "+firstName+" "+lastName+". Nagrod� za to nieskomplikowane zadanie b�dzie "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = questgiverName+" z "+townName+" wynaj�a mnie do odebrania i dostarczenia jej pakunku od alchemika o imieniu "+firstName+" "+lastName+". Nagrod� za to nieskomplikowane zadanie b�dzie "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = "Mam paczk�. Teraz musz� jedynie dostarczy� j� bezpiecznie do "+townName+", gdzie "+questgiverName+" wymieni j� na "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = "Pakunek od alchemika zosta� bezpowrotnie utracony. "+questgiverName+" z "+townName+" prawdopodobnie nie b�dzie uradowany t� wie�ci�.";
        else
            result.journalFailure = "Pakunek od alchemika zosta� bezpowrotnie utracony. "+questgiverName+" z "+townName+" prawdopodobnie nie b�dzie uradowana t� wie�ci�.";
        result.journalSuccessFinished = "Paczka zosta�a bezpiecznie dostarczona do zleceniodawcy. Zadanie zako�czone.";
        result.journalAbandoned = questgiverName+" szuka kogo�, kto b�dzie robi� za kuriera - niech szuka dalej, bo ja si� tego nie podejmuj�.";
        result.npcIntroLine = "Witaj! Potrzebuj� kogo�, kto bezpiecznie odbierze dla mnie pewn� przesy�k� - mo�e to ciebie szukam?";
        if (isMale)
            result.npcContentLine = "Alchemik znany jako "+firstName+" "+lastName+" ma dla mnie przesy�k� z zakupionymi przeze mnie towarami. Zgodzi�em si� odwiedzi� go w obozie gdy b�dzie przechodzi� przez region, ale po prawdzie troch� si� obawiam wychodzi� za miasto.";
        else
            result.npcContentLine = "Alchemik znany jako "+firstName+" "+lastName+" ma dla mnie przesy�k� z zakupionymi przeze mnie towarami. Zgodzi�am si� odwiedzi� go w obozie gdy b�dzie przechodzi� przez region, ale po prawdzie troch� si� obawiam wychodzi� za miasto.";
        result.npcRewardLine = "Je�li udasz si� tam za mnie, odbierzesz pakunek i dostarczysz go mi, wynagrodz� ci twoje wysi�ki "+IntToString(questReward.gold)+" sztukami z�ota. Mamy umow�?";
        result.npcBeforeCompletionLine = "Ju� wracasz? Czy "+firstName+" zgodzi� si� przekaza� ci moj� paczk�?";
        result.playerReportingSuccess = "Tak. Oto twoja w�asno��.";
        result.playerReportingFailure = "Nie uda�o si�. Przesy�ka zosta�a utracona.";
        result.npcReceivedSuccess = "�wietnie! Prosz�, oto twoja zap�ata, co do monety.";
        if (isMale)
            result.npcReceivedFailure = "Jak to si� sta�o? Eh, trudno. Powinienem by� sam si� po ni� wybra�.";
        else
            result.npcReceivedFailure = "Jak to si� sta�o? Eh, trudno. Powinnam by�a sama si� po ni� wybra�.";
        result.npcAfterSuccess = "Moje transmutowane produkty zdaj� si� by� wadliwe. Chyba z�o�� reklamacj�.";
        result.npcAfterFailure = firstName+" "+lastName+" pewnie nie b�dzie skory w zaistnia�ych okoliczno�ciach zwr�ci� mi mojego z�ota...";
        result.npcAfterRefusal = "Nie prosz� o eksploracj� �miertelnie niebezpiecznych loch�w, tylko o dostarczenie mi paczki. To chyba lepsza praca?";
    }
    else
    {
        result.journalName = "Transmuter's package";
        if (isMale)
            result.journalInitial = questgiverName+" of "+townName+" has employed me to retrieve a package from a transmuter named "+firstName+" "+lastName+" and deliver it to him. The reward for this simple task is "+IntToString(questReward.gold)+" gold pieces.";
        else
            result.journalInitial = questgiverName+" of "+townName+" has employed me to retrieve a package from a transmuter named "+firstName+" "+lastName+" and deliver it to her. The reward for this simple task is "+IntToString(questReward.gold)+" gold pieces.";
        result.journalSuccess = "I've got the package. Now all I need to do is get it safely to "+townName+", where "+questgiverName+" will trade it for "+IntToString(questReward.gold)+" gold.";
        result.journalFailure = "The transmuter's package has been lost permanently. "+questgiverName+" of "+townName+" is unlikely to be happy about it.";
        result.journalSuccessFinished = "The package has been safely delivered to the employer. The task is complete.";
        if (isMale)
            result.journalAbandoned = questgiverName+" is searching for an errand boy. I wish him luck, because I'm definitely not going to be one.";
        else
            result.journalAbandoned = questgiverName+" is searching for an errand boy. I wish her luck, because I'm definitely not going to be one.";
        result.npcIntroLine = "Greetings! I'm looking for someone to retrieve a package in my stead - perhaps that would be you?";
        result.npcContentLine = "A transmuter named "+firstName+" "+lastName+" has a package for me containing some goods I've bought. I agreed to visit him in his camp when he's passing through the region, but to be honest I'm a bit afraid of leaving the town.";
        result.npcRewardLine = "If you go there in my place, take the package and deliver it to me, I'll reward your efforts with "+IntToString(questReward.gold)+" gold coins. Do we have a deal?";
        result.npcBeforeCompletionLine = "Back already? Did "+firstName+" agree to give you my package?";
        result.playerReportingSuccess = "Yes. Here it is.";
        result.playerReportingFailure = "It didn't work out. The package has been lost.";
        result.npcReceivedSuccess = "Great! Here is your payment, every single coin of it.";
        result.npcReceivedFailure = "How did this happen? Eh, no matter. I should have gone for it myself.";
        result.npcAfterSuccess = "My transmuted products appear to be faulty. Maybe I should get a refund.";
        result.npcAfterFailure = firstName+" "+lastName+" probably won't agree to give me a refund under these circumstances...";
        result.npcAfterRefusal = "I'm not asking you to explore deadly dungeons, only to deliver a package to me. Wasn't that a better job?";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    object encounter = GetQuestEncounter(oQuest);
    string firstName = GetLocalString(encounter, "TransmuterFirstName");
    string lastName = GetLocalString(encounter, "TransmuterLastName");

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return isMale
            ? questgiverName+" ordered some transmuted items from "+firstName+" "+lastName+" a few weeks ago. I wonder if he had them delivered already."
            : questgiverName+" ordered some transmuted items from "+firstName+" "+lastName+" a few weeks ago. I wonder if she had them delivered already.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale 
            ? questgiverName+" zam�wi� par� tygodni temu jakie� transmutowane przedmioty od tego alchemika imieniem "+firstName+" "+lastName+". Ciekawe, czy ju� mu je dostarczy�."
            : questgiverName+" zam�wi�a par� tygodni temu jakie� transmutowane przedmioty od tego alchemika imieniem "+firstName+" "+lastName+". Ciekawe, czy ju� jej je dostarczy�.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{
    object package = GetLocalObject(oQuest, "Package");
    DestroyObject(package);
}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{
    object encounter = GetQuestEncounter(oQuest);
    object transmuter = GetLocalObject(encounter, "Transmuter");
    object package = CreateItemOnObject("it_transpackage", transmuter);
    SetDroppableFlag(package, TRUE);
    SetPickpocketableFlag(package, TRUE);
    SetLocalObject(oQuest, "Package", package);
    SetLocalObject(transmuter, "Package", package);
    SetLocalObject(package, "Quest", oQuest);
    SetLocalObject(transmuter, "Quest", oQuest);

    SetLocalInt(transmuter, "HESISTANT", RandomNext(2, sSeedName));
    SetLocalInt(transmuter, "AMBUSH", RandomNext(2, sSeedName));
    SetLocalInt(transmuter, "PERSUADE_DC", 11);

    //Select enemies to spawn
    string array = "EnemyResRefs";
    string shout;
    int enemyType = RandomNext(3, sSeedName);

    //Numbers and shouts
    int enemyNum;
    if (enemyType == 0) //Orcs
    {
        enemyNum = 4;
        shout = GetLocalizedString("We take treasure from magic man!", "My bra� skarb magicznego cz�owieka!");
    }
    else if (enemyType == 1) //Goblins
    {
        enemyNum = 5;
        shout = GetLocalizedString("Grab the treasure! Grab the treasure!", "�apa� skarby! �apa� skarby!");
    }
    else if (enemyType == 2) //Bandits
    {
        enemyNum = 4;
        shout = GetLocalizedString("Kill them and seize the package!", "Zabi� ich i przej�� paczk�!");
    }
    SetLocalString(oQuest, "ENEMYSHOUT", shout);

    //ResRefs
    CreateStringArray(array, 0, oQuest);
    int i;
    for (i = 0; i < enemyNum; i++)
    {
        if (enemyType == 0) //Orcs
        {
            if (RandomNext(3, sSeedName))
                AddStringArrayElement(array, "cre_orcyoungb", FALSE, oQuest);
            else
                AddStringArrayElement(array, "cre_orcyounga", FALSE, oQuest);
        }
        if (enemyType == 1) //Goblins
        {
            if (RandomNext(3, sSeedName))
                AddStringArrayElement(array, "cre_goblinb", FALSE, oQuest);
            else
                AddStringArrayElement(array, "cre_goblina", FALSE, oQuest);
        }
        if (enemyType == 2) //Bandits
        {
            if (RandomNext(3, sSeedName))
                AddStringArrayElement(array, "cre_banditb", FALSE, oQuest);
            else
                AddStringArrayElement(array, "cre_bandita", FALSE, oQuest);
        }
    }
}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{
    object package = GetLocalObject(oQuest, "Package");
    object possessor = GetItemPossessor(package);
    
    //Fail the quest if the person carrying the package is killed - or if no one was carrying the package at the time of death,
    //to prevent players from being smartasses and dropping the package to the ground before a fight
    if (!GetLocalInt(package, "PickedUp") || (GetIsObjectValid(possessor) && oDead != possessor && GetPCMaster(possessor) != OBJECT_INVALID))
        return;

    UpdateQuestState(oQuest, QUEST_STATE_FAILED);
    DestroyObject(package);

    //Make the ambushers run away
    int enemyNum = GetObjectArraySize("Ambushers", oQuest);
    int i;
    for (i = 0; i < enemyNum; i++)
    {
        object ambusher = GetObjectArrayElement("Ambushers", i, oQuest);
        if (!GetIsObjectValid(ambusher) || GetIsDead(ambusher))
            continue;
        AssignCommand(ambusher, ClearAllActions());
        AssignCommand(ambusher, ActionMoveAwayFromLocation(GetLocation(ambusher), TRUE));
        //SetCommandable(FALSE, ambusher);
        SetPlotFlag(ambusher, TRUE);
        DestroyObject(ambusher, 10.0f);
    }
}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}