#include "inc_random"
#include "inc_language"
#include "hnd_encounters"
#include "inc_encounters"
#include "inc_scriptevents"
#include "inc_arrays"
#include "inc_quests"
#include "inc_common"
#include "inc_loot"
#include "inc_biomes"
#include "x0_i0_position"

//////////////////////
// ---------------- //
// Encounter script //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "enc_transmuter";

// Number of available regular (non-specialty) quests for this encounter
int regularQuestsNumber = 1;

// Number of available specialty quests for this encounter (can be 0)
int specialtyQuestsNumber = 0;

// Minimum PC level this encounter supports
int minPlayerLevel = 1;

// Maximum PC level this encounter supports
int maxPlayerLevel = 40;

// Number of rumors regarding this encounter (can be 0)
int rumorsNumber = 1;

// Number of available champions for this encounter (can be and should be 0 for all encounters other than simple hostile encounters)
int championsNumber = 0;

// Set this to TRUE if you want quests for this encounter only to generate if no preferred ones are available
int lowQuestGenerationPriority = FALSE;

//////////////////////
// Script functions //
//////////////////////

// Function that should spawn an encounter for a given object representation of an encounter.
// Use the helper function GetLocationWithinEncounterBounds from inc_enc_instanc to access the encounter bounds.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed.
void OnSpawnEncounter(object oEncounter, string sSeedName)
{
    //Spawn campfire
    location loc = GetLocationWithinEncounterBounds(oEncounter, 6.0, 6.0);
    object campfire = CreateObject(OBJECT_TYPE_PLACEABLE, "plc_campfr", loc);
    object lightWp = CreateObject(OBJECT_TYPE_WAYPOINT, "wp_light10", loc);

    //Spawn transmuter
    loc = GetLocationWithinEncounterBounds(oEncounter, 7.0, 6.0);
    object transmuter = CreateCreature("cre_hu_m_transmu", loc);
    AssignCommand(transmuter, SetFacingObject(campfire));
    SetLocalObject(transmuter, "Campfire", campfire);

    //Set maximum tier that can be transmuted
    object area = GetEncounterArea(oEncounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    int level = GetRegionStartingLevel(biome);
    SetLocalInt(transmuter, "MAXTIER", level+1);

    //Spawn transmuter's chest
    loc = GetFlankingLeftLocation(transmuter);
    loc = GetRotatedLocation(loc);
    object chest = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_transchest", loc);
    SetLocalObject(transmuter, "Chest", chest);

    //Randomize name
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(transmuter), GetGender(transmuter), GetServerLanguage(), sSeedName);
    string lastName = GetRandomCommonerFirstName(townScript, GetRacialType(transmuter), GetGender(transmuter), GetServerLanguage(), sSeedName);
    SetLocalString(oEncounter, "TransmuterFirstName", firstName);
    SetLocalString(oEncounter, "TransmuterLastName", lastName);
    SetName(transmuter, firstName + " " + lastName);

    //Make the transmuter sit in front of the campfire
    AssignCommand(transmuter, PlayAnimation(ANIMATION_LOOPING_SIT_CROSS, 1.0f, 8.0f));

    //Randomize encounter variables
    SetLocalInt(transmuter, "PRICE", 90 + RandomNext(111, sSeedName));

    //Randomize pickpocketable loot
    int items = RandomNext(3);
    struct TreasureChestLoot loot;
    if (items > 0)
    {
        loot = GetLoot(sSeedName, level, LOOT_QUALITY_AVERAGE, TRUE);
        SetPickpocketableFlag(CreateItemOnObject(loot.itemResRef, transmuter), TRUE);
    }
    if (items > 1)
    {
        loot = GetLoot(sSeedName, level, LOOT_QUALITY_GOOD, TRUE);
        SetPickpocketableFlag(CreateItemOnObject(loot.itemResRef, transmuter), TRUE);
    }

    //Add scripts
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", transmuter);
    AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_evilondeath", transmuter);
    AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_transmudeth", transmuter);
    AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "cre_sitatcamp", transmuter);
    AddEventScript(SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED, "cre_stndatk", transmuter);
    AddEventScript(SUBEVENT_CREATURE_ON_SPELL_CAST_AT, "cre_stndatk", transmuter);

    //Set conversation (to use with the OnConversation script)
    SetLocalString(transmuter, "Conversation", "transmuter");

    //Set position to return to on heartbeat
    SetLocalLocation(transmuter, "Position", GetLocation(transmuter));

    //Store ranger on encounter and vice versa
    SetLocalObject(oEncounter, "Transmuter", transmuter);
    SetLocalObject(transmuter, "Encounter", oEncounter);
}

// Function that should return available regular quest script names for nIndex in range [0, regularQuestsNumber-1]
string OnGetEncounterRegularQuestScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "que_delivertran";
    }
    return "";
}

// Function that should return available specialty quest script names for nIndex in range [0, specialtyQuestsNumber-1];
// A specialty quest is one that may either not be doable by the player(s) because it requires a certain skill or attributes
// (for example, a quest to pick pocket something from an NPC), or is inherently evil (for example murder)
// and/or bad for reputation thus good-aligned PCs or anyone caring about their reputation would refuse it;
// This function never runs if specialtyQuestsNumber is 0, so it does not matter what it returns in that case (it still needs to exist and compile)
string OnGetEncounterSpecialtyQuestScript(int nIndex)
{
    return "";
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of an encounter instance
string OnGetEncounterRumor(object oEncounter, int nIndex, int nLanguage)
{
    string firstName = GetLocalString(oEncounter, "TransmuterFirstName");
    string lastName = GetLocalString(oEncounter, "TransmuterLastName");
    return GetLocalizedString("That famous transmuter, "+firstName+" "+lastName+", has set a camp somewhere near our town. They say he specializes in weapon transmutation.",
                              "Ten s�ynny alchemik, "+firstName+" "+lastName+", rozbi� ob�z gdzie� w pobli�u naszej wioski. Podobno specjalizuje si� w transmutacji or�a.");
}

// Function that should return available champion script names for nIndex in range [0, championsNumber-1]
string OnGetEncounterChampion(object oEncounter, int nIndex)
{
    return "";
}