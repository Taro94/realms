#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_fbeetles";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 400;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na ogniste �uki";
        result.journalInitial = "Nawet pozornie prosty problem taki jak inwazja ognistych �uk�w wymaga interwencji specjalisty. Za oczyszczenie terenu z tych owad�w "+questgiverName+" z "+townName+" proponuje mi "+IntToString(questReward.gold)+" sztuk z�ota. �adna praca nie ha�bi.";
        if (isMale)
            result.journalSuccess = "Unicestwienie paskudnych �uk�w w ko�cu zako�czone. Ch�tnie wr�c� do "+townName+" po moje "+IntToString(questReward.gold)+"  sztuk z�ota, kt�re obieca� mi "+questgiverName+", a przy okazji ch�tnie wezm� k�piel.";
        else
            result.journalSuccess = "Unicestwienie paskudnych �uk�w w ko�cu zako�czone. Ch�tnie wr�c� do "+townName+" po moje "+IntToString(questReward.gold)+" sztuk z�ota, kt�re obieca�a mi "+questgiverName+", a przy okazji ch�tnie wezm� k�piel.";
        result.journalFailure = "Te �uki by�y wyj�tkowo agresywne! Pomy�le�, �e mog�y mnie pokona� durne insekty. "+questgiverName+" z "+townName+" niestety musi si� o tym dowiedzie�.";
        result.journalSuccessFinished = "Zgodnie z oczekiwaniami, stado �uk�w nie stanowi�o dla mnie wi�kszego zagro�enia. Zlecenie zako�czone.";
        result.journalAbandoned = "Nie jestem od tego, by zajmowa� si� oczyszczaniem terenu z �uk�w. Mam wa�niejsze sprawy na g�owie.";
        result.npcIntroLine = "Potrzebuj� fachowca od dezynfekcji i sterylizacji. Dobrze zap�ac�.";
        result.npcContentLine = "K�opotliwa sprawa - w okolicy moich ziem zal�g�y si� ogniste �uki. Szkodniki s� odporne na ogie� wi�c pr�by zwalczenia ich tym �ywio�em niewiele da�y. Potrzebuj�, by kto� mi z nich dobrze wyczy�ci� ten teren.";
        result.npcRewardLine = "Proponuj� "+IntToString(questReward.gold)+" sztuk z�ota za wykonanie us�ugi. �rodki do dezynfekcji w twoim zakresie. Mamy umow�?";
        result.npcBeforeCompletionLine = "Czy us�uga dezyfekcji zosta�a wykonana?";
        result.playerReportingSuccess = "Tak jest, ogniste �uki s� ju� przesz�o�ci�.";
        result.playerReportingFailure = "Niestety, nie uda�o mi si� wykona� zlecenia. Zakres moich us�ug nie przewidzia� tak gro�nych owad�w.";
        if (isMale)
            result.npcReceivedSuccess = "Takich fachowc�w to ja rozumiem. Prosz�, przygotowa�em ju� mieszek z wynagrodzeniem.";
        else
            result.npcReceivedSuccess = "Takich fachowc�w to ja rozumiem. Prosz�, przygotowa�am ju� mieszek z wynagrodzeniem.";
        if (isMale)
            result.npcReceivedFailure = "Po co bierzesz zlecenie, je�li nie potrafisz go wykona�? Straci�em czas, mog�em zleci� us�ug� komu� innemu.";
        else
            result.npcReceivedFailure = "Po co bierzesz zlecenie, je�li nie potrafisz go wykona�? Straci�am czas, mog�am zleci� us�ug� komu� innemu.";
        result.npcAfterSuccess = "Szybko i profesjonalnie. Na pewno polec� twoje us�ugi znajomym.";
        result.npcAfterFailure = "Zlecanie tobie us�ug to strata czasu i nerw�w! Musz� znale�� lepszego fachowca.";
        result.npcAfterRefusal = "�ycie to nie tylko walka ze smokami i ratowanie ksi�niczek, czasem trzeba wybi� �uki.";
    }
    else
    {
        result.journalName = "Hunting fire beetles";
        result.journalInitial = "Even a seemingly trivial problem like a fire beetle infestation calls for a professional. "+questgiverName+" of "+townName+" has offered me "+IntToString(questReward.gold)+" gold for ridding the land of these insects. I guess no job is shameful.";
        result.journalSuccess = "Ugly beetle annihilation is complete. I'll gladly return to "+townName+" to get my "+IntToString(questReward.gold)+" gold from "+questgiverName+". And maybe take a bath.";
        result.journalFailure = "These beetles were very aggressive! To think that I was bested by puny insects. Well, "+questgiverName+" of "+townName+" must learn of it either way.";
        result.journalSuccessFinished = "Just like expected, the group of beetles was no threat to me. The task is done.";
        result.journalAbandoned = "I'm not the one who should deal with ridding the region of beetles. I have better things to do.";
        result.npcIntroLine = "I need someone skilled in the arts of disinfection and sterilization. I will pay you good.";
        result.npcContentLine = "A troublesome matter - fire beetles have made a nest on my land. These pests are immune to fire, so all attempts to deal with them using flames have been unsuccessful. I need someone to rid my land of them good.";
        result.npcRewardLine = "My offer is "+IntToString(questReward.gold)+" pieces of gold for the service. Any chemicals you may require are on you. Do we have a deal?";
        result.npcBeforeCompletionLine = "Has the desinfection been performed yet?";
        result.playerReportingSuccess = "That it has, your beetle troubles are firmly in the past.";
        result.playerReportingFailure = "Unfortunately, I've failed the task. I did not foresee these insects to be so fierce.";
        result.npcReceivedSuccess = "Now that's what I need, a professional who knows how to do their job. Here, I have the reward prepared for you.";
        result.npcReceivedFailure = "Why would you even take a task you can't complete? I've only wasted my time, I should have offered this gold to someone else.";
        result.npcAfterSuccess = "That was fast and professional. I'll be sure to recommend your services to my friends.";
        result.npcAfterFailure = "Making use of your services was a waste of time and nerves! I need to find a better professional.";
        result.npcAfterRefusal = "Life is more than fighting dragons and rescuing princesses, you know. At times you need to exterminate beetles.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return questgiverName+" is looking for someone to squash a group of fire beetles. Maybe you'd be interested in such a task?";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return questgiverName+" szuka kogo� do uporania si� z grup� ognistych �uk�w. Mo�e interesuje ci� takie zlecenie?";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}