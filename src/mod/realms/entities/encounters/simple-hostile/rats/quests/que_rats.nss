#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_rats";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 350;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na szczury";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" poprosi� mnie o zabicie kr�c�cych si� w pobli�u szczur�w. Dostan� w zamian "+IntToString(questReward.gold)+" sztuk z�ota. To nie powinno by� trudne.";
        else
            result.journalInitial = questgiverName+" z "+townName+" poprosi�a mnie o zabicie kr�c�cych si� w pobli�u szczur�w. Dostan� w zamian "+IntToString(questReward.gold)+" sztuk z�ota. To nie powinno by� trudne.";
        result.journalSuccess = "Szczury sprawiaj�ce tubylcom k�opot nie �yj�. Mog� wr�ci� do "+townName+", gdzie "+questgiverName+" czeka z moj� nagrod�, "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = "Nie uda�o mi si� wykona� zlecenia, szczury uciek�y i pewnie dalej b�d� dr�czy� mieszka�c�w "+townName+". "+questgiverName+" nie b�dzie z tego powodu zachwycony.";
        else
            result.journalFailure = "Nie uda�o mi si� wykona� zlecenia, szczury uciek�y i pewnie dalej b�d� dr�czy� mieszka�c�w "+townName+". "+questgiverName+" nie b�dzie z tego powodu zachwycona.";
        result.journalSuccessFinished = "Szczury sprawiaj�ce tubylcom k�opot zosta�y zabite. Zadanie wykonane.";
        result.journalAbandoned = "Zabicie kilku szczur�w nie by�o warte mojego wysi�ku. Mo�e "+questgiverName+" znajdzie kogo� innego, kto b�dzie zainteresowany tym zleceniem.";
        result.npcIntroLine = "Co powiesz na �atwy zarobek? Wystarczy, �e pozabijasz kilka szkodnik�w.";
        result.npcContentLine = "Mamy od pewnego czasu problem z plag� szczur�w. Tak si� sk�ada, �e wiem, gdzie du�a grupa tych szkodnik�w obecnie przebywa.";
        result.npcRewardLine = "Pozb�d� si� ich, a otrzymasz "+IntToString(questReward.gold)+" sztuk z�ota. Przyjmujesz to zlecenie, czy jest poni�ej twojej godno�ci?";
        result.npcBeforeCompletionLine = "Masz dla mnie jakie� wie�ci? Czy te obrzydliwe zwierz�ta wci�� kr�c� si� przy "+townName+"?";
        result.playerReportingSuccess = "Nie, wszystkie szczury zosta�y zabite.";
        result.playerReportingFailure = "Nie uda�o mi si�. Szczury mnie otoczy�y i powali�y, a nast�pnie uciek�y. Nie by�oby �atwo je odszuka�.";
        result.npcReceivedSuccess = "�wietnie! Oto twoje z�oto, mo�esz przeliczy�, je�li taka jest twoja wola. Bywaj.";
        if (isMale)
            result.npcReceivedFailure = "Czyli pokona�y ci� szczury... Przyznam, �e nie tego si� spodziewa�em. Nie mamy sobie chyba wi�cej do powiedzenia.";
        else
            result.npcReceivedFailure = "Czyli pokona�y ci� szczury... Przyznam, �e nie tego si� spodziewa�am. Nie mamy sobie chyba wi�cej do powiedzenia.";
        result.npcAfterSuccess = "Mo�e szczury nie brzmi� jak wielkie zagro�enie, ale l�ej mi na sercu, �e rozprawiono si� z tymi paskudami.";
        result.npcAfterFailure = "Te szczury pewnie roznosz� niejedn� chorob�. Mam nadziej�, �e nie stanowi� zagro�enia dla "+townName+".";
        result.npcAfterRefusal = "Nie wiem, w jaki spos�b chcesz zmierzy� si� z prawdziwymi niebezpiecze�stwami, skoro przestraszy�a ci� zgraja szczur�w.";
    }
    else
    {
        result.journalName = "Hunting rats";
        result.journalInitial = questgiverName+" from "+townName+" asked me to kill some rat pests plaguing the village of "+townName+". I am to receive "+IntToString(questReward.gold)+" pieces of gold if I succeed. This shouldn't prove too difficult.";
        result.journalSuccess = "Rats plaguing the villagers have been killed. I can return to "+townName+" and receive my reward of "+IntToString(questReward.gold)+" gold pieces from "+questgiverName+".";
        result.journalFailure = "I was not able to complete my task. The rats have fled and will probably continue to trouble the inhabitants of "+townName+". "+questgiverName+" will definitely not be happy to hear this.";
        result.journalSuccessFinished = "Rats plaguing the villagers have been killed. The task is complete.";
        result.journalAbandoned = "A few pesky rats were not worth my efforts. Perhaps "+questgiverName+" will find someone else to kill them.";
        result.npcIntroLine = "What would you say to some easy money? You'd only have to get rid of a few pests.";
        result.npcContentLine = "We have recently been troubled by a plague of rats. It so happens that I know the location of a large group of them.";
        result.npcRewardLine = "Get rid of them and you will be richer by " + IntToString(questReward.gold) + " gold pieces. Do you accept the task or is it below you?";
        result.npcBeforeCompletionLine = "Do you bring any news? Are these disgusting pests still somewhere near "+townName+"?";
        result.playerReportingSuccess = "No, all of them are dead.";
        result.playerReportingFailure = "I have failed. The rats have surrounded me and defeated me and then fled. It wouldn't be easy to find them now.";
        result.npcReceivedSuccess = "Great! Here is your gold, you can count it if you wish. Farewell.";
        result.npcReceivedFailure = "So you have been defeated by rats... I admit I did not expect that. I don't think we have much more to say to each other.";
        result.npcAfterSuccess = "Those rats may have not sounded like a huge threat, but I am happy that they have been dealt with.";
        result.npcAfterFailure = "These rats probably carry multiple diseases. I hope they won't be a threat to "+townName+".";
        result.npcAfterRefusal = "I do not know how you are going to face real dangers if you are afraid of a bunch of rats.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "The rats problem is getting out of hand. I think "+questgiverName+" is especially concerned.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return "Problem szczur�w w okolicy staje si� powoli powa�ny. "+questgiverName+" chyba szczeg�lnie si� tym martwi.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}