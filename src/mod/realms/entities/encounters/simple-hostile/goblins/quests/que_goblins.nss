#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_goblins";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 450;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na gobliny";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" poprosi� mnie o zabicie kr�c�cych si� w pobli�u goblin�w. Dostan� w zamian "+IntToString(questReward.gold)+" sztuk z�ota. To nie powinno by� trudne.";
        else
            result.journalInitial = questgiverName+" z "+townName+" poprosi�a mnie o zabicie kr�c�cych si� w pobli�u goblin�w. Dostan� w zamian "+IntToString(questReward.gold)+" sztuk z�ota. To nie powinno by� trudne.";
        result.journalSuccess = "Gobliny sprawiaj�ce tubylcom k�opot nie �yj�. Mog� wr�ci� do "+townName+", gdzie "+questgiverName+" czeka z moj� nagrod�, "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = "Nie uda�o mi si� wykona� zlecenia, gobliny uciek�y i pewnie dalej b�d� dr�czy� mieszka�c�w "+townName+". "+questgiverName+" nie b�dzie z tego powodu zachwycony.";
        else
            result.journalFailure = "Nie uda�o mi si� wykona� zlecenia, gobliny uciek�y i pewnie dalej b�d� dr�czy� mieszka�c�w "+townName+". "+questgiverName+" nie b�dzie z tego powodu zachwycona.";
        result.journalSuccessFinished = "Gobliny sprawiaj�ce tubylcom k�opot nie �yj�. Zadanie wykonane.";
        result.journalAbandoned = "Zabicie kilku goblin�w nie by�o warte mojego wysi�ku. Mo�e "+questgiverName+" znajdzie kogo� innego, kto b�dzie zainteresowany tym zleceniem.";
        result.npcIntroLine = "Wygl�dasz na kogo�, dla kogo par� goblin�w nie stanowi zagro�enia. Mo�e chcesz zarobi� troch� z�ota?";
        result.npcContentLine = "Twoje zadanie jest proste. Niewielka grupka goblin�w naprzykrza si� mieszka�com "+townName+", w tym mnie. Rabuj� domostwa pod nieobecno�� w�a�cicieli, czasami nawet napadaj� podr�nych.";
        result.npcRewardLine = "Je�li si� ich pozb�dziesz, wynagrodz� twoje wysi�ki mieszkiem o zawarto�ci "+IntToString(questReward.gold)+" sztuk z�ota. Co ty na to?";
        result.npcBeforeCompletionLine = "Jakie wie�ci przynosisz? Czy te parszywe ma�e stworzenia wci�� stanowi� dla nas zagro�enie?";
        result.playerReportingSuccess = "Nie, nigdy wi�cej �adnego zagro�enia ju� nie stworz�.";
        result.playerReportingFailure = "Nie uda�o mi si�. Gobliny mnie pokona�y i wzi�y nogi za pas. Nie by�oby �atwo ich odszuka�.";
        result.npcReceivedSuccess = "Wspania�e wie�ci! Prosz�, oto obiecane z�oto. Powodzenia w twoich podr�ach.";
        if (isMale)
            result.npcReceivedFailure = "My�la�em, �e dla kogo� takiego jak ty to zadanie oka�e si� �atwe. Mo�e �le ci� oceni�em. A teraz odejd�, prosz�, chc� poby� sam.";
        else
            result.npcReceivedFailure = "My�la�am, �e dla kogo� takiego jak ty to zadanie oka�e si� �atwe. Mo�e �le ci� oceni�am. A teraz odejd�, prosz�, chc� poby� sama.";
        result.npcAfterSuccess = "Wszyscy odetchn� z ulg�, gdy dowiedz� si�, �e gobliny zosta�y zabite. Dzi�kuj�.";
        result.npcAfterFailure = "Mo�e gobliny ju� nie wr�c� nas dr�czy�... nie, z pewno�ci� wr�c�.";
        result.npcAfterRefusal = "Mo�e kto� inny b�dzie chcia� zarobi� troch� grosza.";
    }
    else
    {
        result.journalName = "Hunting goblins";
        result.journalInitial = questgiverName+" from "+townName+" asked me to kill some goblins tormenting the village. I am to receive "+IntToString(questReward.gold)+" pieces of gold if I succeed. This shouldn't prove too difficult.";
        result.journalSuccess = "Goblins tormenting the villagers have been killed. I can return to "+townName+" and receive my reward of "+IntToString(questReward.gold)+" gold pieces from "+questgiverName+".";
        result.journalFailure = "I was not able to complete my task. The goblins have fled and will probably continue to trouble the inhabitants of "+townName+". "+questgiverName+" will definitely not be happy to hear this.";
        result.journalSuccessFinished = "Goblins tormenting the villagers have been killed. The task is complete.";
        result.journalAbandoned = "A few goblins were not worth my efforts. Perhaps "+questgiverName+" will find someone else to kill them.";
        result.npcIntroLine = "You look like someone who is not scared of a few goblins. Would you like to earn some coin?";
        result.npcContentLine = "Your task is simple. A small group of goblins is troubling the inhabitants of "+townName+", including me. They rob houses in their owners' absence, sometimes they even ambush travellers.";
        result.npcRewardLine = "If you get rid of them, I will reward your efforts with a sack of " + IntToString(questReward.gold) + " gold pieces. Are you interested?";
        result.npcBeforeCompletionLine = "What news do you bring? Are these little pesky creatures still a threat to us?";
        result.playerReportingSuccess = "They're not and never again shall they be.";
        result.playerReportingFailure = "I have failed. The goblins have got the best of me and then fled. It wouldn't be easy to find them now.";
        result.npcReceivedSuccess = "Great news! Here, your promised gold. Good luck on your journeys.";
        result.npcReceivedFailure = "I thought this should be easy for someone like you. Looks like I was wrong. Be gone, I want to be alone now.";
        result.npcAfterSuccess = "Everyone will be relieved upon hearing the goblins are no more. Thank you.";
        result.npcAfterFailure = "Maybe the goblins will not return... no, I'm sure they will.";
        result.npcAfterRefusal = "Perhaps someone else will be interested.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "I think "+questgiverName+" wanted to recruit someone to finally deal with our goblin problems.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return "Podobno "+questgiverName+" przygotowuje zlecenie na pozbycie si� dr�cz�cych nas goblin�w.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}