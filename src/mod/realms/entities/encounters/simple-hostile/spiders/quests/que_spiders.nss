#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_spiders";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    //SCALING
    result.gold = RandomNext(101, sSeedName) + 450;
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Polowanie na paj�ki";
        if (isMale)
            result.journalInitial = questgiverName+" dr��cym ze strachu g�osem opowiedzia� mi o przera�aj�cych paj�kach w okolicach "+townName+". Za zako�czenie koszmaru jego mieszka�c�w zaoferowano mi "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = questgiverName+" dr��cym ze strachu g�osem opowiedzia�a mi o przera�aj�cych paj�kach w okolicach "+townName+". Za zako�czenie koszmaru jego mieszka�c�w zaoferowano mi "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = "�lepia paj�k�w b�d� mi si� �ni� po nocach, ale uda�o mi si� uwolni� od nich mieszka�c�w "+townName+". "+questgiverName+" czeka w osadzie z moj� nagrod� "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalFailure = "Ma�o jest tak przera�aj�cych stworze� jak paj�ki. Nie uda�o mi si� sprosta� temu koszmarowi, ale czeka mnie jeszcze poinformowanie o niepowodzeniu zleceniodawcy. "+questgiverName+" czeka na wie�ci w "+townName+".";
        result.journalSuccessFinished = "W "+townName+" mieszka�c�w ogarn�a ulga i rado��, przera�aj�ce paj�ki odesz�y w niepami��.";
        result.journalAbandoned = "Po pewnym namy�l� dochodz� do wniosku, �e nic nie jest warte walki z paj�kami. Smoki, trolle, gobliny - owszem. Paj�ki sobie odpuszcz�.";
        result.npcIntroLine = "To prawdzimy koszmar, wszyscy s� przera�eni. Potrafisz walczy� z paj�kami?";
        result.npcContentLine = "Nie wiem ju�, co mamy robi�. Zgraja paj�k�w n�ka mieszka�c�w "+townName+" wij�c lepkie sieci i atakuj�c podr�nych. Uk�szenia cz�sto skutkuj� �mierci�, bowiem jad tych potwor�w jest silnie truj�cy. Jeste�my przera�eni.";
        result.npcRewardLine = "Pozbycie si� tych kreatur jest warte ka�dych pieni�dzy, dlatego damy ci wszystko, co zdo�ali�my uzbiera�, "+IntToString(questReward.gold)+" sztuk z�ota. Prosz�, powiedz, �e si� zgadzasz!";
        result.npcBeforeCompletionLine = "Uda�o ci si� zabi� paj�ki, tak jak si� umawiali�my? Uda�o ci si� po�o�y� kres temu koszmarowi?";
        result.playerReportingSuccess = "Spokojnie, wszystkie paj�ki zosta�y zg�adzone, jeste�cie bezpieczni.";
        result.playerReportingFailure = "Przykro mi, ale nie. Kreatury mnie pokona�y. Te paj�ki s� zaprawd� przera�aj�ce...";
        result.npcReceivedSuccess = "C� za rado��! Doprawdy nie wiem jak ci dzi�kowa�! Oto twoje z�oto!";
        result.npcReceivedFailure = "Zatem "+townName+" musi wytrwa� w tym koszmarze jeszcze troch�. Nie wiem, ile jeste�my w stanie jeszcze znie��...";
        result.npcAfterSuccess = "To prawdziwe szcz�cie m�c wys�a� dzieci do pasieki i nie ba� si�, �e zostan� po�arte przez paj�ki.";
        result.npcAfterFailure = "Mam do��. Mam do�� paj�k�w. D�u�ej... d�u�ej ju� nie wytrzymam.";
        result.npcAfterRefusal = "Dlaczego nie chcesz nam pom�c? Kto� musi nam pom�c...";
    }
    else
    {
        result.journalName = "Hunting spiders";
        result.journalInitial = questgiverName+" told me in shaking voice about horrifying large spiders outside of "+townName+". I have been asked to end the spider nightmare of villagers in exchange for "+IntToString(questReward.gold)+" gold pieces.";
        result.journalSuccess = "Spider eyes will haunt my dreams, but I've managed to free "+townName+" from this menace. "+questgiverName+" probably awaits my arrival with my reward of "+IntToString(questReward.gold)+" gold.";
        result.journalFailure = "Few creatures are as frightening as spiders. I haven't managed to deal with this menace, but I still need to return to "+townName+" and let "+questgiverName+" know about my failure.";
        result.journalSuccessFinished = "The folk of "+townName+" were happy to hear that they no longer needed to worry about the spider threat.";
        result.journalAbandoned = "On second thought, I don't think anything is worth dealing with spiders. Dragons, trolls and goblins - sure, but spiders? I'll pass.";
        result.npcIntroLine = "It's horrible, everyone is frightened. Hey, have you ever fought against large spiders before?";
        result.npcContentLine = "I don't know what to do anymore. A large group of spiders is tormenting the good folk of "+townName+" by sewing sticky webs and attacking travellers. Their bites are often lethal, as their venom is very strong. We are scared of leaving the town.";
        result.npcRewardLine = "Freeing ourselves from these creatures is worth every price, so will happily pay you everything we can spare, "+IntToString(questReward.gold)+" coins. I'm begging you, promise me you will kill them!";
        result.npcBeforeCompletionLine = "Have you managed to kill the spiders, as agreed? Have you managed to put an end to your misery?";
        result.playerReportingSuccess = "You can stop worrying, all of the spiders have been dealt with. You are safe.";
        result.playerReportingFailure = "I am sorry, but this is not the case. I've tried and failed. These spiders truly are terrifying...";
        result.npcReceivedSuccess = "Such good news! Really, I can't express how grateful I am! Here is your gold!";
        result.npcReceivedFailure = "So "+townName+" needs to endure this for some time longer. I don't know how much more we'll be able to stand...";
        result.npcAfterSuccess = "It's truly marvelous that I don't need to worry about spiders eating my children anymore.";
        result.npcAfterFailure = "I've had enough. I've had enough of these spiders. I can't... I can't take it anymore.";
        result.npcAfterRefusal = "Why won't you help us? Someone has to help us...";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return "I'm worrying about "+questgiverName+". They've become a bit paranoid regarding spiders outside the village.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale ? 
            questgiverName+" strasznie obawia si� paj�k�w za miastem. Troch� si� o niego martwi�." :
            questgiverName+" strasznie obawia si� paj�k�w za miastem. Troch� si� o ni� martwi�.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}