#include "inc_encounters"
#include "inc_quests"

//Return TRUE if the PC can toss a quest coin into the fountain.
int StartingConditional()
{
    //Can't toss if there is a fount enemy spawned and not dealt with yet
    object fountEnemy = GetNearestObjectByTag("cre_founthost", OBJECT_SELF);
    if (GetIsObjectValid(fountEnemy) && !GetIsDead(fountEnemy) && GetLocalInt(OBJECT_SELF, "EnemiesDealtWith") == FALSE)
        return FALSE;

    object PC = GetPCSpeaker();
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    object quest = GetQuestOfEncounter(encounter);
    object coin = GetLocalObject(quest, "Coin");

    if (GetIsObjectValid(quest) && GetQuestScript(quest) == "que_tosscoin" && GetItemPossessor(coin) == PC)
        return TRUE;
    return FALSE;
}
