//Returns TRUE if the fountain has not been used and the PC has at least 1 gold coin
int StartingConditional()
{
    object PC = GetPCSpeaker();
    return !GetLocalInt(OBJECT_SELF, "Used") && GetGold(PC) >= 1;
}
