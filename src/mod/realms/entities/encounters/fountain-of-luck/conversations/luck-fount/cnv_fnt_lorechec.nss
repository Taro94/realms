#include "inc_partyskills"

//Do a party lore check and set variable on the fountain to TRUE if it succeeds
void main()
{
    int dc = GetLocalInt(OBJECT_SELF, "LoreDC");
    object PC = GetPCSpeaker();
    if (GetIsPartySkillSuccessful(PC, SKILL_LORE, dc))
        SetLocalInt(OBJECT_SELF, "LoreSuccess", TRUE);
}
