//Returns TRUE if fountain has not been used
int StartingConditional()
{
    return !GetLocalInt(OBJECT_SELF, "Used");
}
