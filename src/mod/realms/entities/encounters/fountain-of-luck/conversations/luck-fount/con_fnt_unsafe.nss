//Return TRUE if PC has 1 gold and the fountain is unsafe
int StartingConditional()
{
    object PC = GetPCSpeaker();
    return GetGold(PC) >= 1 && GetLocalInt(OBJECT_SELF, "Unsafe");
}
