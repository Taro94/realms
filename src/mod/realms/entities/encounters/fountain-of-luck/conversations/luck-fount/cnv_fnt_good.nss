#include "inc_common"

//Mark the fountain as used, take a coin from the PC and give the present party members buffs
void Buff(object oParty)
{
    RemoveEffectsByTag(oParty, "LuckFountainBuff");

    effect buff = EffectSavingThrowIncrease(SAVING_THROW_ALL, 3);
    buff = TagEffect(buff, "LuckFountainBuff");
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, buff, oParty);
}

void main()
{
    object PC = GetPCSpeaker();

    TakeGoldFromCreature(1, PC, TRUE);
    SetLocalInt(OBJECT_SELF, "Used", TRUE);

    object party = GetFirstFactionMember(PC, FALSE);
    while (GetIsObjectValid(party))
    {
        Buff(party);
        party = GetNextFactionMember(PC, FALSE);
    }
}
