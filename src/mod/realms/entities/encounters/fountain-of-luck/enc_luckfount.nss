#include "inc_random"
#include "inc_language"
#include "hnd_encounters"
#include "inc_encounters"
#include "inc_scriptevents"
#include "inc_arrays"
#include "inc_quests"
#include "inc_common"
#include "inc_loot"
#include "inc_biomes"
#include "inc_tiles"
#include "x0_i0_position"

//////////////////////
// ---------------- //
// Encounter script //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "enc_luckfount";

// Number of available regular (non-specialty) quests for this encounter
int regularQuestsNumber = 1;

// Number of available specialty quests for this encounter (can be 0)
int specialtyQuestsNumber = 0;

// Minimum PC level this encounter supports
int minPlayerLevel = 1;

// Maximum PC level this encounter supports
int maxPlayerLevel = 1;

// Number of rumors regarding this encounter (can be 0)
int rumorsNumber = 1;

// Number of available champions for this encounter (can be and should be 0 for all encounters other than simple hostile encounters)
int championsNumber = 0;

// Set this to TRUE if you want quests for this encounter only to generate if no preferred ones are available
int lowQuestGenerationPriority = FALSE;

//////////////////////
// Script functions //
//////////////////////

// Function that should spawn an encounter for a given object representation of an encounter.
// Use the helper function GetLocationWithinEncounterBounds from inc_enc_instanc to access the encounter bounds.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed.
void OnSpawnEncounter(object oEncounter, string sSeedName)
{
    object area = GetEncounterArea(oEncounter);
    object biome = GetTileBiome(area);
    int level = GetRegionStartingLevel(biome);
    
    location loc = GetLocationWithinEncounterBounds(oEncounter, 6.0, 6.0);
    object fountain = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_fountain", loc);
    
    string name = GetLocalizedString("Fountain of Fortune", "Fontanna Pomy�lno�ci");
    SetName(fountain, name);
    SetLocalObject(fountain, "Encounter", oEncounter);
    SetLocalObject(oEncounter, "Fountain", fountain);
    SetEventScript(fountain, EVENT_SCRIPT_PLACEABLE_ON_USED, "obj_convonuse");
    SetLocalString(fountain, "Conversation", "luck_fount");

    //Encounter parameters
    int dc = RandomNext(6, sSeedName) + level + 12;
    int unsafe = RandomNext(4, sSeedName) == 0;
    int fountainCoins = 50 + RandomNext(150, sSeedName);

    SetLocalInt(fountain, "Unsafe", unsafe);
    SetLocalInt(fountain, "LoreDC", dc);
    SetLocalInt(fountain, "Coins", fountainCoins);
}

// Function that should return available regular quest script names for nIndex in range [0, regularQuestsNumber-1]
string OnGetEncounterRegularQuestScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "que_tosscoin";
    }
    return "";
}

// Function that should return available specialty quest script names for nIndex in range [0, specialtyQuestsNumber-1];
// A specialty quest is one that may either not be doable by the player(s) because it requires a certain skill or attributes
// (for example, a quest to pick pocket something from an NPC), or is inherently evil (for example murder)
// and/or bad for reputation thus good-aligned PCs or anyone caring about their reputation would refuse it;
// This function never runs if specialtyQuestsNumber is 0, so it does not matter what it returns in that case (it still needs to exist and compile)
string OnGetEncounterSpecialtyQuestScript(int nIndex)
{
    return "";
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of an encounter instance
string OnGetEncounterRumor(object oEncounter, int nIndex, int nLanguage)
{
    return GetLocalizedString("A neighbor told me that he had discovered a real Fountain of Fortune nearby the road! He tossed a coin into it and fortune smiled upon him for the rest of the week.",
                              "S�siad opowiada� mi, �e odkry� przy drodze prawdziw� Fontann� Pomy�lno�ci! Wrzuci� do niej monet� i szcz�cie dopisywa�o mu przez nast�pny tydzie�.");
}

// Function that should return available champion script names for nIndex in range [0, championsNumber-1]
string OnGetEncounterChampion(object oEncounter, int nIndex)
{
    return "";
}