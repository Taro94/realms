//Successfully persuade the Fountain of Fortune hostiles (and lose 10 gold)
void main()
{
    SetLocalInt(OBJECT_SELF, "Escape", TRUE);
    object PC = GetPCSpeaker();
    TakeGoldFromCreature(10, PC, TRUE);
}
