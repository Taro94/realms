#include "inc_quests"

//Give quest coin to the hostile
void main()
{
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object coin = GetLocalObject(quest, "Coin");
    DestroyObject(coin);
    UpdateQuestState(quest, QUEST_STATE_FAILED);
    SetLocalInt(OBJECT_SELF, "Escape", TRUE);
}
