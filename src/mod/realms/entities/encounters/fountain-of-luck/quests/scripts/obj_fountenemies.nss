#include "inc_scriptevents"
#include "inc_encounters"
#include "inc_arrays"
#include "inc_factions"
#include "inc_groups"

void main()
{
    SetEventScript(OBJECT_SELF, EVENT_SCRIPT_PLACEABLE_ON_USED, "");

    object PC = GetLastUsedBy();
    object quest = GetLocalObject(OBJECT_SELF, "Quest");

    location center = GetLocation(PC);
    string seedName = GetLocalString(quest, "SpawnerSeed");
    string group = GetLocalString(quest, "CreatureGroup");
    object mainHostile = SpawnCreatureGroup(group, 1, center, 6.0f, TRUE, seedName, TRUE, "grp_fountenemy");

    AssignCommand(PC, ClearAllActions());
    DelayCommand(0.5, AssignCommand(mainHostile, ActionStartConversation(PC, "fount_enemy")));
}