#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_common"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_rangrescue";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    result.gold = 400 + RandomNext(201, sSeedName);
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    string rangerFirstName = GetLocalString(encounter, "RangerFirstName");
    string rangerLastName = GetLocalString(encounter, "RangerLastName");

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Na ratunek �owcy";
        if (isMale)
            result.journalInitial = questgiverName+" z "+townName+" poprosi� mnie o odnalezienie zaginionego w g�uszy �owcy, znanego jako "+rangerFirstName+" "+rangerLastName+", i udzielenie mu pomocy, je�li jej potrzebuje. Otrzymam za to "+IntToString(questReward.gold)+" sztuk z�ota.";
        else
            result.journalInitial = questgiverName+" z "+townName+" poprosi�a mnie o odnalezienie zaginionego w g�uszy �owcy, znanego jako "+rangerFirstName+" "+rangerLastName+", i udzielenie mu pomocy, je�li jej potrzebuje. Otrzymam za to "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = rangerFirstName+" by� w z�ym stanie, ale uda�o mi si� uratowa� mu �ycie. Pozostaje mi wr�ci� do "+townName+". "+questgiverName+" z niecierpliwo�ci� czeka, by wyrazi� swoj� wdzi�czno�� za uratowanie przyjaciela mieszkiem o zawarto�ci "+IntToString(questReward.gold)+" sztuk z�ota.";
        if (isMale)
            result.journalFailure = rangerFirstName+" "+rangerLastName+" nie �yje. "+questgiverName+" z "+townName+" powinien si� o tym dowiedzie�.";
        else
            result.journalFailure = rangerFirstName+" "+rangerLastName+" nie �yje. "+questgiverName+" z "+townName+" powinna si� o tym dowiedzie�.";
        result.journalSuccessFinished = "Akcja ratunkowa zako�czy�a si� szcz�liwie. Oby "+rangerFirstName+" trzyma� si� w przysz�o�ci z daleka od k�opot�w.";
        if (isMale)
            result.journalAbandoned = questgiverName+" b�aga� mnie, bym uratowa� jego przyjaciela, ale nie mog� si� tym teraz zaj��. Kto wie, mo�e z tym �owc� jednak wszystko jest w porz�dku.";
        else
            result.journalAbandoned = questgiverName+" b�aga�a mnie, bym uratowa� jego przyjaciela, ale nie mog� si� tym teraz zaj��. Kto wie, mo�e z tym �owc� jednak wszystko jest w porz�dku.";
        result.npcIntroLine = "Szukam kogo�, kto podj��by si� misji ratunkowej. Czas nagli, czy mog� liczy� na twoj� pomoc?";
        result.npcContentLine = "W�drowny �owca i m�j dobry przyjaciel, "+rangerFirstName+" "+rangerLastName+", nie wr�ci� do "+townName+" po ostatniej wyprawie, cho� obiecywa�, �e to zrobi. Obawiam si� najgorszego. Prosz�, udaj si�, jego tropem, znajd� go i upewnij si�, �e jest ca�y i zdrowy!";
        if (isMale)
            result.npcRewardLine = "Jestem gotowy zap�aci� ci za tw�j wysi�ek "+IntToString(questReward.gold)+" sztuk z�ota. Zrobisz to dla mnie?";
        else
            result.npcRewardLine = "Jestem gotowa zap�aci� ci za tw�j wysi�ek "+IntToString(questReward.gold)+" sztuk z�ota. Zrobisz to dla mnie?";
        result.npcBeforeCompletionLine = "Gdzie jest "+rangerFirstName+"? Czy jest ca�y i zdrowy? Czy �yje?";
        result.playerReportingSuccess = "Tak. Szcz�liwie uda�o mi si� go odnale��. By� ci�ko ranny, zosta� uratowany w ostatniej chwili, ale ju� wszystko z nim dobrze.";
        result.playerReportingFailure = "Niestety, nie. By�o ju� dla niego za p�no.";
        result.npcReceivedSuccess = "Tak si� ciesz�! �wietna robota, oto twoje z�oto.";
        if (isMale)
            result.npcReceivedFailure = "Nie... tylko nie to... "+rangerFirstName+", prosi�em ci�, by� zosta�, dlaczego mnie nie us�ucha�e�...";
        else
            result.npcReceivedFailure = "Nie... tylko nie to... "+rangerFirstName+", prosi�am ci�, by� zosta�, dlaczego mnie nie us�ucha�e�...";
        result.npcAfterSuccess = "Teraz mog� spokojnie wyczekiwa� ponownego spotkania z przyjacielem. Dzi�kuj�.";
        result.npcAfterFailure = rangerFirstName+", dlaczego nie s�ucha�e� moich rad i wyruszy�e� w t� przekl�t� podr�...";
        result.npcAfterRefusal = "Niech kto� go odnajdzie! Prosz�!";
    }
    else
    {
        result.journalName = "Ranger rescue";
        result.journalInitial = questgiverName+" from "+townName+" asked me to find a ranger missing in the wilderness named "+rangerFirstName+" "+rangerLastName+". I have been offered "+IntToString(questReward.gold)+" pieces of gold to do this.";
        result.journalSuccess = rangerFirstName+" was in a bad shape when I found him, but I managed to save his life. I should return to "+townName+". "+questgiverName+" is bound to express their gratitude with a sack of "+IntToString(questReward.gold)+" gold pieces.";
        result.journalFailure = rangerFirstName+" "+rangerLastName+" is dead. "+questgiverName+" of "+townName+" should know this.";
        result.journalSuccessFinished = "The rescue was a success. Hopefully, "+rangerFirstName+" will stay away from trouble from now on.";
        if (isMale)
            result.journalAbandoned = questgiverName+" begged me to save his friend, but I can't bother to deal with this now. Who knows, maybe the ranger is just fine after all.";
        else
            result.journalAbandoned = questgiverName+" begged me to save her friend, but I can't bother to deal with this now. Who knows, maybe the ranger is just fine after all.";
        result.npcIntroLine = "I'm looking for someone to go on a rescue mission. Time runs short, can I count on you?";
        result.npcContentLine = "A travelling ranger and a good friend of mine, "+rangerFirstName+" "+rangerLastName+", has not yet come back to "+townName+" after his last adventure, despite promising to do so. I'm afraid of the worst. Please, go after him, find him and make sure he is alive and well!";
        result.npcRewardLine = "I'll happily pay you "+IntToString(questReward.gold)+" gold pieces for your efforts. Can you do this for me?";
        result.npcBeforeCompletionLine = "Where is "+rangerFirstName+"? Is he alright? Is he alive?";
        result.playerReportingSuccess = "Yes. Fortunately, I managed to find him. He was heavily wounded and I saved him in the last possible moment, but everything is well now.";
        result.playerReportingFailure = "Unfortunately, no. It was too late for him.";
        result.npcReceivedSuccess = "I'm so glad! You've done a great job, here is your gold.";
        result.npcReceivedFailure = "No... not this... "+rangerFirstName+", I begged you not to go, why wouldn't you listen...";
        result.npcAfterSuccess = "I can now wait forward to the next meeting with my friend. Thank you.";
        result.npcAfterFailure = rangerFirstName+", why didn't you listen to my advice and embarked on this cursed journey...";
        result.npcAfterRefusal = "Someone save him! Please!";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    object encounter = GetQuestEncounter(oQuest);
    string rangerFirstName = GetLocalString(encounter, "RangerFirstName");
    string rangerLastName = GetLocalString(encounter, "RangerLastName");

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return isMale
            ? "Have you heard about that missing ranger named "+rangerFirstName+" "+rangerLastName+"? "+questgiverName+" is an old friend of his, I wonder how he's holding on."
            : "Have you heard about that missing ranger named "+rangerFirstName+" "+rangerLastName+"? "+questgiverName+" is an old friend of his, I wonder how she's holding on.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale 
            ? "Dosz�y ci� wie�ci o tym zaginionym �owcy imieniem "+rangerFirstName+" "+rangerLastName+"? "+questgiverName+" to jego stary przyjaciel, ciekawe, jak to znosi."
            : "Dosz�y ci� wie�ci o tym zaginionym �owcy imieniem "+rangerFirstName+" "+rangerLastName+"? "+questgiverName+" to jego stara przyjaci�ka, ciekawe, jak to znosi.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}