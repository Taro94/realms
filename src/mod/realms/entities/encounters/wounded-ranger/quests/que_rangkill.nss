#include "inc_random"
#include "inc_language"
#include "hnd_quests"
#include "inc_quests"
#include "inc_encounters"
#include "inc_towns"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_common"

//////////////////////
// ---------------- //
//   Quest script   //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "que_rangkill";

// Number of questgiver blueprints that can spawn with this quest
int questgiversNumber = 1;

// Number of rumors regarding this quest (can be 0)
int rumorsNumber = 1;

// Minimum PC level this quest supports
int minPlayerLevel = 1;

// Maximum PC level this quest supports
int maxPlayerLevel = 40;

//////////////////////
// Script functions //
//////////////////////

// Function that should return available questgiver ResRef for nIndex [0, questgiversNumber-1];
// Questgiver template may depend on the town (for example to spawn a questgiver of appropriate race)
string OnGetQuestgiverResRef(int nIndex, object oTown)
{
    string townScript = GetTownScript(oTown);
    int num = GetTownCommonersNumber(townScript);
    string resref = GetTownCommonerResRef(townScript, RandomNext(num, "default"));
    return resref;
}

// Function that should return a random first name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverFirstName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a random last name for a questgiver of this town, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomQuestgiverLastName(object oQuestGiver, string sSeedName, int nLanguage)
{
    object quest = GetQuestFromQuestgiver(oQuestGiver);
    object encounter = GetQuestEncounter(quest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerLastName(townScript, GetRacialType(oQuestGiver), GetGender(oQuestGiver), nLanguage, sSeedName);
    return firstName;
}

// Function that should return a a QuestReward struct for this quest.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
struct QuestReward OnGetQuestReward(string sSeedName)
{
    struct QuestReward result;
    result.gold = 450 + RandomNext(201, sSeedName);
    result.itemResRef = "";
    result.itemStackSize = 0;

    return result;
}

// Function that should return a a QuestDialogLines struct for this quest, based on a LANGUAGE_* constant from inc_language, a quest object and a QuestReward struct.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
struct QuestDialogLines OnGetQuestDialogLines(struct QuestReward questReward, object oQuest, string sSeedName, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    struct QuestDialogLines result;

    string questgiverName = GetName(questGiver);
    object encounter = GetQuestEncounter(oQuest);
    object area = GetEncounterArea(encounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townName = GetLocalString(town, "Name");
    int isMale = GetGender(questGiver) == GENDER_MALE;

    string rangerFirstName = GetLocalString(encounter, "RangerFirstName");
    string rangerLastName = GetLocalString(encounter, "RangerLastName");

    if (nLanguage == LANGUAGE_POLISH)
    {
        result.journalName = "Zab�jstwo �owcy";
        result.journalInitial = questgiverName+" z "+townName+" chce dopa�� i zabi� �owc� znanego jako "+rangerFirstName+" "+rangerLastName+". Cel jest prawdopodobnie ci�ko ranny, zadanie nie powinno by� wi�c trudne. Za dobicie �owcy otrzymam "+IntToString(questReward.gold)+" sztuk z�ota.";
        result.journalSuccess = rangerFirstName+" i tak by� ju� umieraj�cy, gdy gin�� z mojej r�ki. Zadanie okaza�o si� prostsze, ni� mo�na by�o si� spodziewa�. Czas wr�ci� do "+questgiverName+" w "+townName+" i odebra� "+IntToString(questReward.gold)+" sztuk z�ota nagrody.";
        if (isMale)
            result.journalFailure = "To zadanie trudno by�o spartaczy�, ale uda�o mi si�. "+rangerFirstName+" "+rangerLastName+" prze�y�. "+questgiverName+" z "+townName+" pewnie mnie wy�mieje, o ile b�dzie mu do �miechu.";
        else
            result.journalFailure = "To zadanie trudno by�o spartaczy�, ale uda�o mi si�. "+rangerFirstName+" "+rangerLastName+" prze�y�. "+questgiverName+" z "+townName+" pewnie mnie wy�mieje, o ile b�dzie jej do �miechu.";
        result.journalSuccessFinished = "Mo�na powiedzie�, �e zabijaj�c tego �owc�, wy�wiadczy�em mu przys�ug�. C�, sobie na pewno. Zap�ata by�a sowita.";
        result.journalAbandoned = "Zabijanie niewinnych mo�e zaszkodzi� mojej reputacji, a to mo�e zaszkodzi� mi. Niech "+questgiverName+" poszuka kogo� innego do brudnej roboty.";
        result.npcIntroLine = "�cisz g�os. Mam nadziej�, �e jeste� kim�, komu nie przeszkadza ubrudzenie sobie r�k krwi� tak zwanych \"niewinnych\"?";
        result.npcContentLine = "W�drowny �owca i m�j �miertelny wr�g, "+rangerFirstName+" "+rangerLastName+", prawdopodobnie le�y obecnie ranny gdzie� w krzakach za miastem. Przynajmniej tak� mam nadziej�. To idealny moment, �eby si� go pozby�. Znajd� go i upewnij si�, �e b�dzie gryz� piach.";
        result.npcRewardLine = "Zr�b to, a dam ci mieszek wype�niony z�otem - dok�adnie "+IntToString(questReward.gold)+" sztuk. Bierzesz t� robot�?";
        result.npcBeforeCompletionLine = "Nie zwracaj na siebie takiej uwagi. Czy cel jest ju� martwy?";
        result.playerReportingSuccess = "Tak. Zosta� dobity, bo nie mo�na tu m�wi� o walce.";
        result.playerReportingFailure = "Przeszkodzono mi w zrealizowaniu zadania. Prze�y�.";
        result.npcReceivedSuccess = "Oto twoje z�oto, a teraz znikaj st�d, zanim kto� nas zobaczy.";
        result.npcReceivedFailure = "Szlag by to, to mia�a by� �atwa robota! Dopadn� tego �owczyn� innym razem. A ty si� st�d wyno�.";
        result.npcAfterSuccess = "Co tu jeszcze robisz, piwa chcesz si� ze mn� napi�? Zje�d�aj!";
        if (isMale)
            result.npcAfterFailure = "Uszy ci nie domagaj�? Kaza�em ci si� wynosi�!";
        else
            result.npcAfterFailure = "Uszy ci nie domagaj�? Kaza�am ci si� wynosi�!";
        result.npcAfterRefusal = "Je�li nie chcesz tej roboty, to nie zawracaj mi g�owy.";
    }
    else
    {
        result.journalName = "Ranger murder";
        result.journalInitial = questgiverName+" of "+townName+" wants me to kill a ranger known as "+rangerFirstName+" "+rangerLastName+". The target is likely heavily wounded, so the task should not be difficult. For finishing off the ranger I am to receive "+IntToString(questReward.gold)+" pieces of gold.";
        result.journalSuccess = rangerFirstName+" was near death already when I finished him off. The task turned out to be even simpler than I thought it would be. Time to go back to "+questgiverName+" in "+townName+" and get my "+IntToString(questReward.gold)+" gold coins of reward.";
        result.journalFailure = "The task was not easy to mess up, but I've managed to mess it up anyway. "+rangerFirstName+" "+rangerLastName+" is still alive. "+questgiverName+" of "+townName+" will probably laugh at me when they hear it, unless they don't feel like laughing.";
        result.journalSuccessFinished = "You could probably say that I did that ranger a favor by killing him. Well, I did myself a favor, that's for sure. The payment was good.";
        result.journalAbandoned = "Killing the innocent can hurt my reputation, which in turn can hurt me. I'll let "+questgiverName+" find someone else to do their dirty work.";
        result.npcIntroLine = "Lower your voice. I hope you are someone who isn't bothered by killing the so-called \"innocent\".";
        result.npcContentLine = "A travelling ranger and my mortal enemy, "+rangerFirstName+" "+rangerLastName+", is probably lying somewhere in bushes out there as we speak. At least I hope so. It's the perfect time to get rid of him. Find him and make sure he bites the dust.";
        result.npcRewardLine = "Do it and I'll give you a sack of gold - precisely "+IntToString(questReward.gold)+" coins. Are you in?";
        result.npcBeforeCompletionLine = "Don't bring any attention to yourself. Is the target dead?";
        result.playerReportingSuccess = "Yes. I finished him off, because you could hardly call it a fight.";
        result.playerReportingFailure = "There was an interference. He's still alive.";
        result.npcReceivedSuccess = "Here's your gold, now disappear before someone sees us.";
        result.npcReceivedFailure = "To hells with it, it was supposed to be an easy job! I'll get that puny ranger next time. As for you, get the heck out of here.";
        result.npcAfterSuccess = "What are you still doing here, want to have a drink with me or what? Get lost.";
        result.npcAfterFailure = "Got a hearing problem? I told you to get the heck out of here!";
        result.npcAfterRefusal = "Don't waste my time if you're not interested in the job.";
    }
    return result;
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of a quest instance
string OnGetQuestRumor(object oQuest, int nIndex, int nLanguage)
{
    object questGiver = GetQuestGiver(oQuest);
    string questgiverName = GetName(questGiver);
    int isMale = GetGender(questGiver) == GENDER_MALE;

    object encounter = GetQuestEncounter(oQuest);
    string rangerFirstName = GetLocalString(encounter, "RangerFirstName");
    string rangerLastName = GetLocalString(encounter, "RangerLastName");

    if (nLanguage == LANGUAGE_ENGLISH)
    switch (nIndex)
    {
        case 0: return isMale
            ? "Have you heard about that missing ranger named "+rangerFirstName+" "+rangerLastName+"? Everyone know "+questgiverName+" hates him, I hope this is not his doing."
            : "Have you heard about that missing ranger named "+rangerFirstName+" "+rangerLastName+"? Everyone know "+questgiverName+" hates him, I hope this is not her doing.";
    }
    if (nLanguage == LANGUAGE_POLISH)
    switch (nIndex)
    {
        case 0: return isMale 
            ? "Dosz�y ci� wie�ci o tym zaginionym �owcy imieniem "+rangerFirstName+" "+rangerLastName+"? Wszyscy wiedz�, �e "+questgiverName+" go nienawidzi, mam nadziej�, �e to nie jego robota."
            : "Dosz�y ci� wie�ci o tym zaginionym �owcy imieniem "+rangerFirstName+" "+rangerLastName+"? Wszyscy wiedz�, �e "+questgiverName+" go nienawidzi, mam nadziej�, �e to nie jej robota.";
    }
    return "";
}

// Function that may perform custom logic when a PC accepts the quest. Note that all server PCs will get their journals updated,
// but you may want to, for example, add an item to be delivered to a single PC (oConversingPC) or spawn an NPC to be escorted.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestTakingLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC completes the quest and returns it to the questgiver. Note that all server PCs will get their journals updated,
// but you may want to, for example, remove specific items from inventory of a PC or all PCs.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestCompletionLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when a PC fails the quest and talks to the questgiver about it.
// It also fires when a PC refuses a quest, but only if the quest was taken before.
// This function body may also be empty if custom logic is not needed.
void OnCustomQuestFailureLogic(object oQuest, object oConversingPC)
{

}

// Function that may perform custom logic when the quest is generated.
// This should be avoided when possible, because conceptually an encounter
// or a special area shouldn't change based on quest spawned, but it may sometimes
// be the best option (for example to give a creature an item the PC is supposed to bring back).
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed. 
void OnCustomQuestInitializationLogic(string sSeedName, object oQuest)
{

}

// Function that may perform custom logic when a PC or a henchman dies (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestDeathLogic(object oQuest, object oDead)
{

}

// Function that may perform custom logic when a PC respawns (like failing a quest) when the quest is in progress or completed, but not yet returned
void OnCustomQuestRespawnLogic(object oQuest, object oRespawningPC)
{

}