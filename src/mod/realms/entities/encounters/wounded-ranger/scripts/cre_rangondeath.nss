#include "inc_encounters"
#include "inc_quests"
#include "inc_common"
#include "inc_arrays"
#include "inc_reputation"
#include "inc_scriptevents"

void main()
{
    //Fail the quest if you were to save him and complete if you were to kill him
    object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
    object quest = GetQuestOfEncounter(encounter);
    string questScript = GetQuestScript(quest);
    if (questScript == "que_rangrescue")
        UpdateQuestState(quest, QUEST_STATE_FAILED);
    else if (questScript == "que_rangkill")
        UpdateQuestState(quest, QUEST_STATE_COMPLETED);

    //Spawn militia and make them start conversation with the killer, also adjust reputation
    ModifyPartyReputation(-10);
    int readyToDie = GetLocalInt(OBJECT_SELF, "ReadyToDie");
    object killer = GetPCMaster(GetLastKiller());
    if ((readyToDie || GetIsPC(killer)) && GetLocalInt(OBJECT_SELF, "MILITIA"))
    {
        int i;
        int militiaNum = 3 + GetLocalInt(OBJECT_SELF, "EXTRA_MILITIA");
        object captain;
        CreateObjectArray("Militia", 0, captain);
        for (i = 0; i < militiaNum; i++)
        {
            location loc = GetLocationWithinEncounterBounds(encounter, Random(2)*12.0, Random(2)*12.0);
            object creature = CreateCreature("cre_hu_m_militia", loc);
            SetPlotFlag(creature, TRUE);
            AssignCommand(creature, SetFacingObject(killer));
            if (i == 0)
                captain = creature;
            else
                AddObjectArrayElement("Militia", creature, FALSE, captain);
        }
        AssignCommand(killer, ClearAllActions(TRUE));
        AssignCommand(captain, ActionStartConversation(killer, "rangmilitia"));
        SetLocalString(captain, "Conversation", "rangmilitia");
        SetLocalObject(captain, "PCToTalkTo", killer);
        AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "cre_trytotalk", captain);
        AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_varconvo", captain);
        SetLocalObject(captain, "Encounter", encounter);
    }

    if (readyToDie)
        return;
    
    //Loot!
    int gold = GetLocalInt(OBJECT_SELF, "GOLD");
    string resref = GetLocalString(OBJECT_SELF, "LOOT");
    GiveGoldToCreature(OBJECT_SELF, gold);
    object item = CreateItemOnObject(resref, OBJECT_SELF);
    SetDroppableFlag(item, TRUE);
    object goldItem = GetItemPossessedBy(OBJECT_SELF, "NW_IT_GOLD001");
    SetDroppableFlag(goldItem, TRUE);
}