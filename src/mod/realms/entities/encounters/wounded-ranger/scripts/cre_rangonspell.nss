#include "inc_language"

void GetUp()
{
    string msg = GetLocalizedString("W-what... oh... I remember.", "C-co... ah... ju� pami�tam.");
    SpeakString(msg);
    PlayAnimation(ANIMATION_LOOPING_DEAD_FRONT, 1.0, 0.0);
}

void main()
{
    if (GetLocalInt(OBJECT_SELF, "Rescued"))
        return;

    int spell = GetLastSpell();
    if (spell == SPELL_CURE_CRITICAL_WOUNDS
            || spell == SPELL_CURE_LIGHT_WOUNDS
            || spell == SPELL_CURE_MINOR_WOUNDS
            || spell == SPELL_CURE_MODERATE_WOUNDS
            || spell == SPELL_CURE_SERIOUS_WOUNDS
            || spell == SPELL_HEAL
            || spell == SPELL_HEALINGKIT)
    {
        SetLocalInt(OBJECT_SELF, "Rescued", TRUE);
        DelayCommand(1.0f, GetUp());
    }
}