#include "inc_random"
#include "inc_language"
#include "hnd_encounters"
#include "inc_encounters"
#include "inc_scriptevents"
#include "inc_arrays"
#include "inc_quests"
#include "inc_common"
#include "inc_loot"
#include "inc_biomes"

//////////////////////
// ---------------- //
// Encounter script //
// ---------------- //
//////////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "enc_ranger";

// Number of available regular (non-specialty) quests for this encounter
int regularQuestsNumber = 1;

// Number of available specialty quests for this encounter (can be 0)
int specialtyQuestsNumber = 1;

// Minimum PC level this encounter supports
int minPlayerLevel = 1;

// Maximum PC level this encounter supports
int maxPlayerLevel = 40;

// Number of rumors regarding this encounter (can be 0)
int rumorsNumber = 1;

// Number of available champions for this encounter (can be and should be 0 for all encounters other than simple hostile encounters)
int championsNumber = 0;

// Set this to TRUE if you want quests for this encounter only to generate if no preferred ones are available
int lowQuestGenerationPriority = FALSE;

//////////////////////
// Script functions //
//////////////////////

// Function that should spawn an encounter for a given object representation of an encounter.
// Use the helper function GetLocationWithinEncounterBounds from inc_enc_instanc to access the encounter bounds.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
// DON'T CALL RandomNext() in OnSpawn event of creatures created in this function, as this can create non-determinism in map generation procedure.
// You should perform all logic requiring RandomNext() directly in this function after creating a creature.
// If you absolutely need to use OnSpawn for this, call "SetRandomSeed(RandomNextAny(sSeedName), ObjectToString(creature));"
// and then use ObjectToString(OBJECT_SELF) in OnSpawn script(s) as the RandomNext()'s seed.
void OnSpawnEncounter(object oEncounter, string sSeedName)
{
    //Spawn ranger
    location loc = GetLocationWithinEncounterBounds(oEncounter, 6.0, 6.0);
    object ranger = CreateCreature("cre_woundrang", loc);

    //Randomize name
    object area = GetEncounterArea(oEncounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    string townScript = GetTownScript(town);
    string firstName = GetRandomCommonerFirstName(townScript, GetRacialType(ranger), GetGender(ranger), GetServerLanguage(), sSeedName);
    string lastName = GetRandomCommonerFirstName(townScript, GetRacialType(ranger), GetGender(ranger), GetServerLanguage(), sSeedName);
    SetLocalString(oEncounter, "RangerFirstName", firstName);
    SetLocalString(oEncounter, "RangerLastName", lastName);
    SetName(ranger, firstName + " " + lastName);

    //Make the ranger knocked out
    SetCurrentHitPoints(ranger, 1);
    AssignCommand(ranger, PlayAnimation(ANIMATION_LOOPING_DEAD_FRONT, 1.0f, 5000000.0f));

    //Randomize encounter variables
    SetLocalInt(ranger, "REWARD", RandomNext(3, sSeedName)); //0 - nothing, 1 - spell buff, 2 - fight against enemies
    SetLocalInt(ranger, "PARAM", RandomNext(3, sSeedName)); //0 - cat's grace / wolves, 1 - protection from elements / goblins, 2 - aid / bandits
    SetLocalInt(ranger, "GOLD", RandomNext(100) + 80);
    SetLocalInt(ranger, "MILITIA", RandomNext(2));
    SetLocalInt(ranger, "EXTRA_MILITIA", RandomNext(2));
    int level = GetRegionStartingLevel(biome);
    struct TreasureChestLoot loot = GetLoot(sSeedName, level, LOOT_QUALITY_AVERAGE, TRUE);
    SetLocalString(ranger, "LOOT", loot.itemResRef);

    //Add scripts
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, "cre_rangerconv", ranger);
    AddEventScript(SUBEVENT_CREATURE_ON_SPELL_CAST_AT, "cre_rangonspell", ranger);
    AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_evilondeath", ranger);
    AddEventScript(SUBEVENT_CREATURE_ON_DEATH, "cre_rangondeath", ranger);
    
    //Set conversation (to use with the OnConversation script)
    SetLocalString(ranger, "Conversation", "wounded_ranger");
    
    //Set facing to use in conversation
    SetLocalFloat(ranger, "Facing", GetFacing(ranger));

    //Store ranger on encounter and vice versa
    SetLocalObject(oEncounter, "Ranger", ranger);
    SetLocalObject(ranger, "Encounter", oEncounter);
}

// Function that should return available regular quest script names for nIndex in range [0, regularQuestsNumber-1]
string OnGetEncounterRegularQuestScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "que_rangrescue";
    }
    return "";
}

// Function that should return available specialty quest script names for nIndex in range [0, specialtyQuestsNumber-1];
// A specialty quest is one that may either not be doable by the player(s) because it requires a certain skill or attributes
// (for example, a quest to pick pocket something from an NPC), or is inherently evil (for example murder)
// and/or bad for reputation thus good-aligned PCs or anyone caring about their reputation would refuse it;
// This function never runs if specialtyQuestsNumber is 0, so it does not matter what it returns in that case (it still needs to exist and compile)
string OnGetEncounterSpecialtyQuestScript(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "que_rangkill";
    }
    return "";
}

// Function that should return available NPC rumor for nIndex in range [0, rumorsNumber-1] based on an object representation of an encounter instance
string OnGetEncounterRumor(object oEncounter, int nIndex, int nLanguage)
{
    string firstName = GetLocalString(oEncounter, "RangerFirstName");
    string lastName = GetLocalString(oEncounter, "RangerLastName");
    return GetLocalizedString(firstName+" "+lastName+" hasn't been seen in town for days now. He's a skilled ranger, so I hope he's fine out there in the wilderness.",
        firstName+" "+lastName+" nie by� widziany w osadzie od wielu dni. Jest zdolnym �owc�, wi�c mam nadziej�, �e nie jest w �adnym niebezpiecze�stwie w dziczy.");
}

// Function that should return available champion script names for nIndex in range [0, championsNumber-1]
string OnGetEncounterChampion(object oEncounter, int nIndex)
{
    return "";
}