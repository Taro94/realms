#include "inc_language"
#include "inc_tokens"
#include "inc_debug"

int StartingConditional()
{
    //return TRUE if fight awaits you after rescuing the ranger, but first set CUSTOM1001 token to the name of the group of enemies that attacked
    //(i.e. "wolves"/"wilki" - "zosta�em zaatakowany przez ..." in Polish)
    int reward = GetLocalInt(OBJECT_SELF, "REWARD");
    int rescued = GetLocalInt(OBJECT_SELF, "Rescued");
    if (rescued && reward == 2)
    {
        int enemies = GetLocalInt(OBJECT_SELF, "PARAM");
        string token;
        switch (enemies)
        {
            case 0:
                token = GetLocalizedString("wolves", "wilki");
                break;
            case 1:
                token = GetLocalizedString("goblins", "gobliny");
                break;
            case 2:
                token = GetLocalizedString("bandits", "bandyt�w");
                break;
            default:
                LogWarning("Invalid enemy parameter: %n (con_rangwillfigh, enemies)", enemies);
        }
        SetCustomTokenEx(1001, token);
        return TRUE;
    }
    return FALSE;
}
