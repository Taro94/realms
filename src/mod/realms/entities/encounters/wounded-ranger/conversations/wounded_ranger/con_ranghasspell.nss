int StartingConditional()
{
    //Return TRUE if ranger has been healed and has a spell as thanks
    int reward = GetLocalInt(OBJECT_SELF, "REWARD");
    int rescued = GetLocalInt(OBJECT_SELF, "Rescued");
    if (rescued && reward == 1)
        return TRUE;
    return FALSE;
}
