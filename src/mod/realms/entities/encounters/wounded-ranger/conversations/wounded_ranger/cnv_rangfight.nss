#include "inc_convabort"

void main()
{
    //Mark the ranger as ready to fight so the fight begins on abort
    MarkConversationStart("cnv_rangabort");
    SetLocalInt(OBJECT_SELF, "ReadyToFight", TRUE);
}
