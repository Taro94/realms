int StartingConditional()
{
    //Return TRUE if ranger has no spells to cast on the party upon being rescued
    int reward = GetLocalInt(OBJECT_SELF, "REWARD");
    int rescued = GetLocalInt(OBJECT_SELF, "Rescued");
    if (rescued && reward == 0)
        return TRUE;
    return FALSE;
}
