#include "inc_debug"
#include "inc_convabort"

void main()
{
    //Cast some ranger buffing spell on the party, then continue
    MarkConversationStart("cnv_rangabort");
    SetLocalInt(OBJECT_SELF, "ReadyToLeave", TRUE);
    int spellParam = GetLocalInt(OBJECT_SELF, "PARAM");
    int spellId;
    switch (spellParam)
    {
        case 0:
            spellId = SPELL_CATS_GRACE;
            break;
        case 1:
            spellId = SPELL_PROTECTION_FROM_ELEMENTS;
            break;
        case 2:
            spellId = SPELL_AID;
            break;
        default:
            LogWarning("Invalid spell parameter: %n (cnv_rangspell, spellParam)", spellParam);
    }

    ActionPauseConversation();
    object PC = GetPCSpeaker();
    object partyMember = GetFirstFactionMember(PC, FALSE);
    while (GetIsObjectValid(partyMember))
    {
        float distance = GetDistanceBetween(OBJECT_SELF, partyMember);
        if (distance > 0.0f && distance < 10.0f)
        {
            int instant = partyMember != PC;
            ActionCastSpellAtObject(spellId, partyMember, METAMAGIC_ANY, TRUE, 0, PROJECTILE_PATH_TYPE_DEFAULT, instant);
        }
        partyMember = GetNextFactionMember(PC, FALSE);
    }
    ActionResumeConversation();
}
