int StartingConditional()
{
    //Return TRUE if ranger has started fighting and there are no enemies in the area
    if (GetLocalInt(OBJECT_SELF, "StartedFighting") == FALSE)
        return FALSE;
    
    object area = GetArea(OBJECT_SELF);
    object creature = GetFirstObjectInArea(area);
    while (GetIsObjectValid(creature))
    {
        if (GetObjectType(creature) == OBJECT_TYPE_CREATURE && GetIsReactionTypeHostile(OBJECT_SELF, creature))
            return FALSE;
        creature = GetNextObjectInArea(area);
    }

    return TRUE;
}
