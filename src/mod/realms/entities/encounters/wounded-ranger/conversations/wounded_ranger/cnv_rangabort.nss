#include "inc_tiles"
#include "inc_factions"
#include "inc_scriptevents"
#include "inc_encounters"
#include "inc_quests"
#include "hench_i0_ai"
#include "inc_convabort"

void main()
{
    MarkConversationEnd();

    //Make the ranger leave if he's been marked as ready to leave
    int readyToLeave = GetLocalInt(OBJECT_SELF, "ReadyToLeave");
    int readyToFight = GetLocalInt(OBJECT_SELF, "ReadyToFight");
    int readyToDie = GetLocalInt(OBJECT_SELF, "ReadyToDie");
    if (readyToLeave)
    {
        SetPlotFlag(OBJECT_SELF, TRUE);
        object exitWp = GetNearestTileExitWaypoint(OBJECT_SELF);
        ActionMoveToObject(exitWp);
        ActionDoCommand(DestroyObject(OBJECT_SELF));
        DestroyObject(OBJECT_SELF, 30.0f);
        SetCommandable(FALSE);
        DeleteLocalString(OBJECT_SELF, "Conversation");

        //Complete the rescue quest (or fail the killing quest)
        object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
        object quest = GetQuestOfEncounter(encounter);
        string questScript = GetQuestScript(quest);
        if (questScript == "que_rangrescue")
            UpdateQuestState(quest, QUEST_STATE_COMPLETED);
        else if (questScript == "que_rangkill")
            UpdateQuestState(quest, QUEST_STATE_FAILED);
    }
    else if (readyToFight)
    {
        SetFaction(OBJECT_SELF, FACTION_ALLY);
        int creatureParam = GetLocalInt(OBJECT_SELF, "PARAM");
        string creatureResRef;
        int creatureNum;
        switch (creatureParam)
        {
            case 0:
                creatureResRef = "cre_wolf";
                creatureNum = 4;
                break;
            case 1:
                creatureResRef = "cre_goblina";
                creatureNum = 6;
                break;
            case 2:
                creatureResRef = "cre_bandita";
                creatureNum = 5;
                break;
            default:
                LogWarning("Invalid enemy creature parameter: %n (cnv_rangabort, creatureResRef)", creatureParam);
        }
        SetLocalInt(OBJECT_SELF, "StartedFighting", TRUE);
        object encounter = GetLocalObject(OBJECT_SELF, "Encounter");
        int i;
        for (i = 0; i < creatureNum; i++)
        {
            location loc = GetLocationWithinEncounterBounds(encounter, Random(2)*12.0, Random(2)*12.0);
            object creature = CreateCreature(creatureResRef, loc);
            SetLocalObject(creature, "Ranger", OBJECT_SELF);
            AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, "cre_runtorang", creature);
            AssignCommand(creature, HenchDetermineCombatRound());
        }
    }
    else if (readyToDie)
    {
        effect death = EffectDeath();
        ApplyEffectToObject(DURATION_TYPE_INSTANT, death, OBJECT_SELF);
    }
}
