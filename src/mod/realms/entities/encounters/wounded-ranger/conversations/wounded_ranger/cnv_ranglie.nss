#include "inc_convabort"

void main()
{
    //Make sure the ranger keeps on lying on the ground
    MarkConversationStart("cnv_rangabort");
    ClearAllActions();
    SetFacing(GetLocalFloat(OBJECT_SELF, "Facing"));
    PlayAnimation(ANIMATION_LOOPING_DEAD_FRONT, 1.0f, 50000000.0f);
}
