#include "inc_arrays"
#include "inc_convabort"

void main()
{
    //Set militia group to non-plot (they were set to plot earlier to avoid PC-set traps)
    MarkConversationStart("cnv_rnmilabort");
    SetLocalInt(OBJECT_SELF, "NoTalk", TRUE);
    SetPlotFlag(OBJECT_SELF, FALSE);
    int i;
    for (i = 0; i < GetObjectArraySize("Militia", OBJECT_SELF); i++)
    {
        object militia = GetObjectArrayElement("Militia", i, OBJECT_SELF);
        SetPlotFlag(militia, FALSE);
    }
}
