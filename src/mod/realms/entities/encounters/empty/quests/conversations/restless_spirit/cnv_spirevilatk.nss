#include "inc_factions"
#include "hench_i0_ai"
#include "inc_common"

void main()
{
    //Give evil points to PC(s) and start fighting
    AdjustAligmentOfAllPCs(ALIGNMENT_EVIL, 5);
    SetLocalInt(OBJECT_SELF, "STARTED_ATTACKING", TRUE);
    SetFaction(OBJECT_SELF, FACTION_UNDEAD);
    HenchDetermineCombatRound();
    SetPlotFlag(OBJECT_SELF, FALSE);
}
