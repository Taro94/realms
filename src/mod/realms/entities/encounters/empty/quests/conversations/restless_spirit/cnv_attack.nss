#include "inc_factions"
#include "hench_i0_ai"

void main()
{
    //Start a fight with PCs
    SetLocalInt(OBJECT_SELF, "STARTED_ATTACKING", TRUE);
    SetFaction(OBJECT_SELF, FACTION_UNDEAD);
    HenchDetermineCombatRound();
    SetPlotFlag(OBJECT_SELF, FALSE);
}
