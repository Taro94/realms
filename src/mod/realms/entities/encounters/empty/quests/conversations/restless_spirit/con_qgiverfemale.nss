#include "inc_quests"

int StartingConditional()
{
    object quest = GetLocalObject(OBJECT_SELF, "Quest");
    object questgiver = GetQuestGiver(quest);
    return GetGender(questgiver) == GENDER_FEMALE;
}
