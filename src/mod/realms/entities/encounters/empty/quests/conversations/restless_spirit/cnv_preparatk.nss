#include "inc_factions"
#include "hench_i0_ai"

void _Attack()
{
    if (GetLocalInt(OBJECT_SELF, "STARTED_ATTACKING") == TRUE)
        return;
    SetLocalInt(OBJECT_SELF, "STARTED_ATTACKING", TRUE);
    SetFaction(OBJECT_SELF, FACTION_UNDEAD);
    HenchDetermineCombatRound();
    SetPlotFlag(OBJECT_SELF, FALSE);
}

void main()
{
    //Make the caller attack the PC in 6 seconds (unless the conversation is cancelled earlier)
    SetLocalInt(OBJECT_SELF, "READY_TO_ATTACK", TRUE);
    DelayCommand(6.0f, _Attack());
}
