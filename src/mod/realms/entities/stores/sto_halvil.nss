#include "inc_language"
#include "inc_random"
#include "hnd_stores"
#include "inc_rngnames"
#include "inc_loot"

//////////////////
// ------------ //
// Store script //
// ------------ //
//////////////////

void main()
{
    ScriptHandler();
}

///////////////////
// Script fields //
///////////////////

// Name of this script, should match the filename of the *.nss file (without the extension)
string scriptName = "sto_halvil";

// Number of available store area templates for this store
int areasNumber = 5;

// Number of available storekeeper NPCs for this store
int storekeepersNumber = 2;

// Number of tracks that the store may have as a day theme
int storeDayTracksNumber = 1;

// Number of tracks that the store may have as a night theme
int storeNightTracksNumber = 1;

// Number of tracks that the store may have as a battle theme
int storeBattleTracksNumber = 1;

//////////////////////
// Script functions //
//////////////////////

// Function that should return ResRef of an available store area to spawn for nIndex in range [0, storesNumber-1]
string OnGetStoreAreaResRef(int nIndex)
{
    switch (nIndex)
    {
        case 0: return "store01";
        case 1: return "store02";
        case 2: return "store03";
        case 3: return "store04";
        case 4: return "store05";
    }
    return "";
}

// Function that should return available store day track constant for nIndex in range [0, storeDayTracksNumber-1]
int OnGetStoreDayTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0: return TRACK_STORE;
    }
    return -1;
}

// Function that should return available store night track constant for nIndex in range [0, storeNightTracksNumber-1]
int OnGetStoreNightTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0: return TRACK_STORE;
    }
    return -1;
}

// Function that should return available store battle track constant for nIndex in range [0, storeBattleTracksNumber-1]
int OnGetStoreBattleTrack(int nIndex)
{
    switch (nIndex)
    {
        case 0: return TRACK_BATTLE_CITY2;
    }
    return -1;
}

// Function that should return a random name for a general store, based on a LANGUAGE_* constant from inc_language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomStoreName(string sSeedName, int nLanguage)
{
    if (nLanguage == LANGUAGE_POLISH)
    {
        switch (RandomNext(1))
        {
            case 0: return "Sklep og�lny";
        }
    }
    else
    {
        switch (RandomNext(1))
        {
            case 0: return "General store";
        }
    }
    return "";
}

// Function that should return available storekeeper ResRef for nIndex [0, storekeepersNumber-1].
// If you want to use RandomNext() in the OnSpawn script of a storekeeper, use ObjectToString(OBJECT_SELF) as the RandomNext()'s seed.
// Random() should be avoided altogether.
string OnGetStorekeeperResRef(int nIndex)
{
    switch (nIndex)
    {
        case 0:
            return "npc_ha_m";
        case 1:
            return "npc_ha_f";
    }
    return "";
}

// Function that should create a shop's store object at oDestinationWaypoint and return it.
// You can use nBiomeStartingLevel to spawn merchant's items appropriate to the level the PCs will typically have.
// You can generate some inventory programatically and randomly, or you can simply create a store of a given ResRef.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
object OnSpawnStoreInventory(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    int firstRegion = nBiomeStartingLevel == 1;
    return CreateStoreWithItems(sSeedName, nBiomeStartingLevel, oDestinationWaypoint, firstRegion);
}

// Function that should return a random first name for a store owner NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomStoreOwnerFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return nGender == GENDER_FEMALE ? RandomFemaleHalflingName(sSeedName) : RandomMaleHalflingName(sSeedName);
}

// Function that should return a random last name for a store owner NPC based on their race, gender and possibly server language.
// Random() should not be used. Use the RandomNext() function from inc_random in conjunction with sSeedName instead
// in order to make the selection respect the realm's random seed.
// Note: Ensure that RandomNext() is called the same number of times no matter the language. If it's not, you risk making the same seeds
// generate different maps for different languages, which should be avoided.
string OnGetRandomStoreOwnerLastName(int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    return RandomHalflingSurname(sSeedName);
}