int StartingConditional()
{
    int i;
    for (i = 0; i < 5; i++)
    {
        int classType;
        switch (i)
        {
            case 0: classType = CLASS_TYPE_BARD; break;
            case 1: classType = CLASS_TYPE_CLERIC; break;
            case 2: classType = CLASS_TYPE_DRUID; break;
            case 3: classType = CLASS_TYPE_RANGER; break;
            case 4: classType = CLASS_TYPE_PALADIN; break;
        }

        if (GetLevelByClass(classType) > 0)
            return TRUE;
    }

    return FALSE;
}
