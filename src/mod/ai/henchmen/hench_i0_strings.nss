#include "inc_language"
/*

    Henchman Inventory And Battle AI

    This file is used for strings for the henchman ai.

    This file contains all strings shown to the PC. (Useful for
    multilanguage support and customization.)

*/


// void main() {    }

// weapons equipping
string sHenchCantUseShield = GetLocalizedString("I don't know how to use this shield!", "Nie wiem, jak u�ywa� tej tarczy!");
string sHenchCantUseRanged = GetLocalizedString("I can't use ranged weapons.", "Nie mog� u�ywa� broni dystansowej.");
string sHenchSwitchToMissle = GetLocalizedString("I'm switching back to my ranged weapon!", "Wyci�gam ponownie moj� bro� dystansow�!");
string sHenchSwitchToRanged = GetLocalizedString("I'm switching to my melee weapon for now!", "Tymczasowo wyci�gam moj� bro� do walki wr�cz!");
// generic
string sHenchSomethingFishy = GetLocalizedString("There's something fishy near that door...", "Nie podobaj� mi si� te drzwi...");
// hen shout
string sHenchPeacefulModeCancel = GetLocalizedString("***Peaceful Follow Mode Cancelled***", "***Tryb Pasywnego Pod��ania Anulowany***");
string sHenchHenchmanFollow = GetLocalizedString("I will follow but not attack our enemies until you tell me otherwise.", "B�d� za tob� i��, ale nie wdam si� w walk�, dop�ki mi nie ka�esz.");
string sHenchFamiliarFollow = GetLocalizedString("I will be happy to follow you, avoiding combat until you tell me otherwise!", "Z przyjemno�ci� b�d� za tob� pod��a� unikaj�c walki, dop�ki nie ka�esz mi atakowa�!");
string sHenchAnCompFollow = GetLocalizedString(" understands that it should follow, waiting for your command to attack.>", " rozumie, �e ma pod��a�, oczekuj�c twojego rozkazu do ataku.>");
string sHenchOtherFollow1 = GetLocalizedString("<The ", "< ");
string sHenchOtherFollow2 = GetLocalizedString(" will now follow you, and be peaceful until told otherwise.>", " b�dzie za tob� pod��a� i nie b�dzie atakowa�, dop�ki nie wydasz rozkazu.>");
// hen util
string sHenchUnableToEquip1 = GetLocalizedString("I was physically unable to equip the ", "Nie mog� wyposa�y� si� w ");
string sHenchUnableToEquip2 = GetLocalizedString("! I'll keep it in my backpack.", "! B�d� trzyma� ten przedmiot w plecaku.");
string sHenchUnableToEquip3 = GetLocalizedString("I decided not to equip the ", "Nie chc� wyposa�a� si� w ");
string sHenchUnableToEquip4 = GetLocalizedString(". It's in my backpack.", ". Jest w moim plecaku.");
string sHenchAbleToEquip = GetLocalizedString("I managed to equip the ", "Uda�o mi si� wyposa�y� w ");
string sHenchSelectItem1 = GetLocalizedString("I really like the ", "Podoba mi si� ");
string sHenchSelectItem2 = GetLocalizedString("I like my ", "Lubi� ");
string sHenchSelectItem3 = GetLocalizedString(" better than the ", " bardziej, ni� ");
string sHenchDecisions = GetLocalizedString("Hmmm, decisions, decisions...", "Hmmm, decyzje, decyzje...");
string sHenchEquipArmor = GetLocalizedString("piece of armor", "zbroja");
string sHenchEquipShield = GetLocalizedString("Thanks for the shield... I'll try to use it well.", "Dzi�ki za tarcz�... Zrobi� z niej dobry u�ytek.");
string sHenchEquipCloak = GetLocalizedString("cloak", "p�aszcz");
string sHenchEquipBoots = GetLocalizedString("pair of boots", "para but�w");
string sHenchEquipHandwear = GetLocalizedString("handwear", "r�kawice lub karwasze");
string sHenchEquipHelmet = GetLocalizedString("helmet", "he�m");
string sHenchEquipBelt = GetLocalizedString("belt", "pas");
string sHenchEquipRing = GetLocalizedString("ring", "pier�cie�");
string sHenchEquipAmulet = GetLocalizedString("amulet", "amulet");
string sHenchEquipWeapon = GetLocalizedString("Thanks for the weaponry. We'll have to wait until combat to see how well I can use it!", "Dzi�ki za bro�. B�dziemy musieli zaczeka� do nast�pnej walki, �eby przekona� si�, jak dobrze umiem si� ni� pos�ugiwa�!");
string sHenchEquipWait = GetLocalizedString("Please wait a little while I try to sort out my equipment.", "Spr�buj� zrobi� porz�dek w moim ekwipunku. Zaczekaj chwil�.");
string sHenchEquipDecency = GetLocalizedString("Will you give me some clothes to wear for goodness' sake?", "Na bog�w, czy mo�esz da� mi co� do ubrania?");
// hench heartbeat
string sHenchWaitTrapsCleared = GetLocalizedString("I'm not doing anything until these traps are cleared!", "Nic nie zrobi�, dop�ki te pu�apki nie zostan� rozbrojone!");
string sHenchSomethingImportant = GetLocalizedString("There is something important here you should look at.", "Mam tu co� wa�nego, rzu� na to okiem.");
string sHenchFoundSomething = GetLocalizedString("Look what I found.", "Zobacz, co tu mam.");
string sHenchGiveThings = GetLocalizedString("Here are some things for you.", "Mam dla ciebie par� przedmiot�w, oto one.");
string sHenchGiveGold = GetLocalizedString("Here's some gold for you.", "Mam dla ciebie troch� z�ota, oto ono.");
// main ai
string sHenchCantHealMaster = GetLocalizedString("Sorry, I can't heal you!", "Wybacz, nie mog� ci� uzdrowi�!");
string sHenchAskHealMaster = GetLocalizedString("Should I heal you?", "Mam ci� uzdrowi�?");
string sHenchHenchmanAskAttack = GetLocalizedString("Should I attack?", "Mam atakowa�?");
string sHenchFamiliarAskAttack = GetLocalizedString("Be careful! Let me know if I should attack!", "Uwa�aj! Daj mi zna�, kiedy mam atakowa�!");
string sHenchAnCompAskAttack = GetLocalizedString(" is waiting for you to give the command to attack.>", " czeka na tw�j rozkaz do ataku.>");
string sHenchOtherAskAttack = GetLocalizedString(" patiently awaits your command to attack.>", " cierpliwie oczekuje twojego rozkazu, by zaatakowa�.>");
string sHenchWeakAttacker = GetLocalizedString("Don't make me laugh!", "Nie roz�mieszaj mnie!");
string sHenchModAttacker = GetLocalizedString("We'll best them yet!", "Poradzimy sobie!");
string sHenchStrongAttacker = GetLocalizedString("Watch out for this one!", "Tego przeciwnika lepiej nie lekcewa�y�!");
string sHenchOverpoweringAttacker = GetLocalizedString("Gods help us!", "Bogowie, wspom�cie!");
string sHenchFamiliarFlee1 = GetLocalizedString("Time for me to get out of here!", "Czas bra� nogi za pas!");
string sHenchFamiliarFlee2 = GetLocalizedString("Eeeeek!", "Aaaaaa!");
string sHenchFamiliarFlee3 = GetLocalizedString("Make way, make way!", "Z drogi, z drogi!");
string sHenchFamiliarFlee4 = GetLocalizedString("I'll be back!", "Jeszcze tu wr�c�!");
string sHenchAniCompFlee = GetLocalizedString("<Danger>", "<Niebezpiecze�stwo>");
string sHenchHealMe = GetLocalizedString("Help! I can't heal myself!", "Niech mnie kto� uleczy!");
// hen identify
string sHenchIdentObject = GetLocalizedString("Object #", "Object #");
string sHenchIdentSuccess = GetLocalizedString(": This looks like a ", ": To wygl�da jak ");
string sHenchIdentFail = GetLocalizedString(": I'm not sure what this thing is.", ": Nie mam pewno�ci co to jest.");
string sHenchIdentNoItems = GetLocalizedString("Are you playing games with me?", "Pogrywasz sobie ze mn�?");
// hen show items
string sHenchShowEquipKnown = GetLocalizedString(": equipped. Quantity: ", ": wyposa�one. Ilo��: ");
string sHenchShowEquipUnknown = GetLocalizedString("Unidentified object: equipped. Quantity: ", "Niezidentyfikowany obiekt: wyposa�one. Ilo��: ");
string sHenchShowInventoryKnown = GetLocalizedString(": in backpack. Quantity: ", ": w plecaku. Ilo��: ");
string sHenchShowInventoryUnknown = GetLocalizedString("Unidentified object: in backpack. Quantity: ", "Niezidentyfikowany obiekt: w plecaku. Ilo��: ");
string sHenchShowNoItems = GetLocalizedString("I don't have anything!", "Nic nie mam!");
// hench heartbeat
string sHenchGetOutofWay = GetLocalizedString("Please get out of my way.", "Zejd� mi z drogi.");
// hench block
string sHenchMonsterOnOtherSide = GetLocalizedString("Something is on the other side of this door.", "Co� jest po drugiej stronie drzwi.");
// hench heal
string sHenchCantSeeTarget = GetLocalizedString("I can't see ", "Nie widz� nigdzie ");


void HenchBattleCry()
{
    string sName = GetName(OBJECT_SELF);
    // Probability of Battle Cry. MUST be a number from 1 to at least 8
    int iSpeakProb = Random(125)+1;
    if (FindSubString(sName,"Sharw") == 0)
    switch (iSpeakProb) {
       case 1: SpeakString("Take this, fool!"); break;
       case 2: SpeakString("Spare me your song and dance!"); break;
       case 3: SpeakString("To hell with you, hideous fiend!"); break;
       case 4: SpeakString("Come here. Come here I say!"); break;
       case 5: SpeakString("How dare you, impetuous beast?"); break;
       case 6: SpeakString("Pleased to meet you!"); break;
       case 7: SpeakString("Fantastic. Just fantastic!"); break;
       case 8: SpeakString("You CAN do better than this, can you not?"); break;

       default: break;
    }

    if (FindSubString(sName,"Tomi") == 0)
    switch (iSpeakProb) {
       case 1: SpeakString("Tomi's got a little present for you here!"); break;
       case 2: SpeakString("Poor sod, soon to bite the earth!"); break;
       case 3: SpeakString("Think twice before messing with Tomi!"); break;
       case 4: SpeakString("Tomi's fast; YOU are slow!"); break;
       case 5: SpeakString("Your momma raised ya to become THIS?"); break;
       case 6: SpeakString("Hey! Where's your manners!"); break;
       case 7: SpeakString("Tomi's got a BIG problem with you. Scram!"); break;
       case 8: SpeakString("You're an ugly little beastie, ain't ya?"); break;

       default: break;
    }

    if (FindSubString(sName,"Grim") == 0)
    switch (iSpeakProb) {
       case 1: SpeakString("Destruction for all!"); break;
       case 2: SpeakString("Embrace Death, and long for it!"); break;
       case 3: SpeakString("My Silent Lord comes to take you!"); break;
       case 4: SpeakString("Be still: your End approaches."); break;
       case 5: SpeakString("Prepare yourself! Your time is near!"); break;
       case 6: SpeakString("Eternal Silence engulfs you!"); break;
       case 7: SpeakString("I am at one with my End. And you?"); break;
       case 8: SpeakString("Suffering ends; but Death is eternal!"); break;
       default: break;
    }

    if (FindSubString(sName,"Dael") == 0)
    switch (iSpeakProb) {
       case 1: SpeakString("I'd spare you if you would only desist."); break;
       case 2: SpeakString("It needn't end like this. Leave us be!"); break;
       case 3: SpeakString("You attack us, only to die. Why?"); break;
       case 4: SpeakString("Must you all chase destruction? Very well!"); break;
       case 5: SpeakString("It does not please me to crush you like this."); break;
       case 6: SpeakString("Do not provoke me!"); break;
       case 7: SpeakString("I am at my wit's end with you all!"); break;
       case 8: SpeakString("Do you even know what you face?"); break;
       default: break;
    }

    if (FindSubString(sName,"Linu") == 0)
    switch (iSpeakProb) {
       case 1: SpeakString("Oooops! I nearly fell!"); break;
       case 2: SpeakString("What is your grievance? Begone!"); break;
       case 3: SpeakString("I won't allow you to harm anyone else!"); break;
       case 4: SpeakString("Retreat or feel Sehanine's wrath!"); break;
       case 5: SpeakString("By Sehanine Moonbow, you will not pass unchecked."); break;
       case 6: SpeakString("Smite you I will, though unwillingly."); break;
       case 7: SpeakString("Sehanine willing, you'll soon be undone!"); break;
       case 8: SpeakString("Have you no shame? Then suffer!"); break;
       default: break;
    }

    if (FindSubString(sName,"Boddy") == 0)
    switch (iSpeakProb) {
       case 1: SpeakString("You face a sorcerer of considerable power!"); break;
       case 2: SpeakString("I find your resistance illogical."); break;
       case 3: SpeakString("I bind the powers of the very Planes!"); break;
       case 4: SpeakString("Fighting for now, and research for later."); break;
       case 5: SpeakString("Sad to destroy a fine specimen such as yourself."); break;
       case 6: SpeakString("Your chances of success are quite low, you know?"); break;
       case 7: SpeakString("It's hard to argue with these fools."); break;
       case 8: SpeakString("Now you are making me lose my patience."); break;
       default: break;
    }
}


void MonsterBattleCry()
{


}

