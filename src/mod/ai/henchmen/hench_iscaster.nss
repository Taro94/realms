#include "inc_convoptions"

int StartingConditional()
{
    ClearConversationOptions();
    int i;
    for (i = 0; i < 7; i++)
    {
        int classType;
        switch (i)
        {
            case 0: classType = CLASS_TYPE_BARD; break;
            case 1: classType = CLASS_TYPE_WIZARD; break;
            case 2: classType = CLASS_TYPE_SORCERER; break;
            case 3: classType = CLASS_TYPE_CLERIC; break;
            case 4: classType = CLASS_TYPE_DRUID; break;
            case 5: classType = CLASS_TYPE_RANGER; break;
            case 6: classType = CLASS_TYPE_PALADIN; break;
        }

        if (GetLevelByClass(classType) > 0)
            return TRUE;
    }

    return FALSE;
}
