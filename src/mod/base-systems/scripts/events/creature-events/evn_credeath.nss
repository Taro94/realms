// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for every creature's OnDeath event.

#include "inc_scriptevents"
#include "inc_debug"

void main()
{
    //Run preset script for that event from a variable if it exists
    string presetScript = GetLocalString(OBJECT_SELF, SUBEVENT_CREATURE_ON_DEATH);
    if (presetScript != "")
    {
        if (presetScript == "evn_credeath")
            LogWarning("Creature with ResRef "+ GetResRef(OBJECT_SELF) +" attempted to call a looped master event script "+ presetScript);
        else
            ExecuteScript(presetScript);
    }

    //Run standard summon script if creature is marked as a summon with a variable (and didn't have any specific preset script)
    else if (GetLocalInt(OBJECT_SELF, "StandardSummon") == TRUE)
        ExecuteScript("nw_ch_ac7");

    RunEventScripts(SUBEVENT_CREATURE_ON_DEATH);
}