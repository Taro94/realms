// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details).
// This one is for every creature's OnSpawn event.

#include "x0_i0_spawncond"
#include "inc_scriptevents"
#include "inc_ruleset"
#include "inc_debug"

void main()
{
    //Very Difficult: apply buffs and store the difficulty to check for in the heartbeat
    int difficulty = GetGameDifficulty();
    SetLocalInt(OBJECT_SELF, "RememberedGameDifficulty", difficulty);
    if (difficulty == GAME_DIFFICULTY_DIFFICULT && !GetIsPC(OBJECT_SELF))
        RecalculateDifficultyBuffs(OBJECT_SELF);

    //Run preset script for that event from a variable if it exists
    string presetScript = GetLocalString(OBJECT_SELF, SUBEVENT_CREATURE_ON_SPAWN);
    if (presetScript != "")
    {
        if (presetScript == "evn_crespawn")
            LogWarning("Creature with ResRef "+ GetResRef(OBJECT_SELF) +" attempted to call a looped master event script "+ presetScript);
        else
            ExecuteScript(presetScript);
    }

    //Run standard summon script if creature is marked as a summon with a variable (and didn't have any specific preset script)
    else if (GetLocalInt(OBJECT_SELF, "StandardSummon") == TRUE)
        ExecuteScript("nw_ch_ac9");

    RunEventScripts(SUBEVENT_CREATURE_ON_SPAWN);
}
