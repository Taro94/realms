// This master event handler processes custom event logic in a modular manner (see inc_scriptevents for details)

#include "inc_scriptevents"
#include "inc_arrays"
#include "inc_onexitfix"
#include "inc_debug"

void main()
{
    LogInfo("OnClientLeave");
    //A workaround for a bug preventing trigger and area OnExit (for dead PCs only) events from firing when PCs leave the game
    ExecuteOnExitEvents();

    //Custom logic processing
    RunEventScripts(SUBEVENT_MODULE_ON_CLIENT_LEAVE);
}