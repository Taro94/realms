// This master event handler has two functions:
// - executes default BioWare event logic (content of x2_mod_def_act)
// - processes custom event logic in a modular manner (see inc_scriptevents for details)

#include "x2_inc_switches"
#include "inc_scriptevents"

void main()
{
    object oItem = GetItemActivated();
     // * Generic Item Script Execution Code
     // * If MODULE_SWITCH_EXECUTE_TAGBASED_SCRIPTS is set to TRUE on the module,
     // * it will execute a script that has the same name as the item's tag
     // * inside this script you can manage scripts for all events by checking against
     // * GetUserDefinedItemEventNumber(). See x2_it_example.nss
     if (GetModuleSwitchValue(MODULE_SWITCH_ENABLE_TAGBASED_SCRIPTS) == TRUE)
     {
        SetUserDefinedItemEventNumber(X2_ITEM_EVENT_ACTIVATE);
        int nRet = ExecuteScriptAndReturnInt(GetUserDefinedItemEventScriptName(oItem), OBJECT_SELF);
        if (nRet == X2_EXECUTE_SCRIPT_END)
           return;
     }

    //Custom processing
    RunEventScripts(SUBEVENT_MODULE_ON_ACTIVATE_ITEM);
}
