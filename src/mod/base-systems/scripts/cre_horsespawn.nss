// This is an on-spawn script that horses should have (not directly, but via the OnSpawn variable, see inc_scriptevents for details).
// It checks if the horse has an owner (after a short delay, to ensure ownership is assigned) and if it's an NPC that has a master, gives the horse
// to the master. This is to avoid the annoyance of not being able to give orders to your henchmen's horse.

#include "inc_horses"
#include "inc_debug"

void main()
{
    DelayCommand(1.0, VerifyOwnership(OBJECT_SELF));
    ExecuteScript("nw_ch_ac9");
}