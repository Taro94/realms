int StartingConditional()
{
    string param = GetScriptParam("Gold");
    int gold = StringToInt(param);
    
    return GetGold(GetPCSpeaker()) >= gold;
}
