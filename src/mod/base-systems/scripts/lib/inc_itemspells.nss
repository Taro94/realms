#include "inc_debug"

// Library that provides replacements for stock spell and feat (TODO) functions for the AI to use items properly

// Replacement for GetHasSpell that additionally returns TRUE if the creature is capable of casting the spell from an item.
int GetHasSpellIncludingItems(int nSpell, object oCreature=OBJECT_SELF);

// Replacement for ActionCastSpellAtLocation that will cast the spell from an item if possible and if the creature can't cast it on its own.
void ActionCastSpellAtLocationIncludingItems(int nSpell, location lTargetLocation, int nMetaMagic=METAMAGIC_ANY, int bCheat=FALSE, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE);

// Replacement for ActionCastSpellAtObject that will cast the spell from an item if possible and if the creature can't cast it on its own.
void ActionCastSpellAtObjectIncludingItems(int nSpell, object oTarget, int nMetaMagic=METAMAGIC_ANY, int bCheat=FALSE, int nDomainLevel=0, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE);

struct ItemWithIP
{
    object item;
    itemproperty property;
};

int _GetUseMagicDeviceDC(object oItem, int nSpell)
{
    int type = GetBaseItemType(oItem);
    if (GetGameDifficulty() >= GAME_DIFFICULTY_CORE_RULES && (type == BASE_ITEM_SPELLSCROLL || type == BASE_ITEM_ENCHANTED_SCROLL))
    {
        //For scrolls, let's assume a d20 roll of 10 to determine whether the skill is enough to use the item
        int level = StringToInt(Get2DAString("spells", "Innate", nSpell));
        return 25 + level - 10;
    }
    
    int itemValue = GetGoldPieceValue(oItem);
    int row = -1;
    int i = 0;
    while (TRUE)
    {
        string maxCostString = Get2DAString("skillvsitemcost", "DeviceCostMax", i);
        int maxCost = maxCostString == "" ? -1 : StringToInt(maxCostString);
        if (itemValue > maxCost)
            break;
        row = i++;
    }

    int dc = 0;
    if (GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_ALIGNMENT_GROUP) || GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_SPECIFIC_ALIGNMENT))
    {
        int thisDc = StringToInt(Get2DAString("skillvsitemcost", "SkillReq_Align", row));
        if (thisDc > dc)
            dc = thisDc;
    }
    if (GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_RACIAL_TYPE))
    {
        int thisDc = StringToInt(Get2DAString("skillvsitemcost", "SkillReq_Race", row));
        if (thisDc > dc)
            dc = thisDc;
    }
    if (GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_CLASS))
    {
        int thisDc = StringToInt(Get2DAString("skillvsitemcost", "SkillReq_Class", row));
        if (thisDc > dc)
            dc = thisDc;
    }
    return dc;
}

int _GetIsItemUsable(object oCreature, object oItem, int nSpell)
{
    //Potions should not be considered, because it would be difficult to control that they can't be cast on someone else
    //(besides, AI can already use potions just fine)
    int baseItem = GetBaseItemType(oItem);
    if (baseItem == BASE_ITEM_POTIONS || baseItem == BASE_ITEM_ENCHANTED_POTION)
        return FALSE;

    //Equippable items must be equipped to be used
    string slots = Get2DAString("baseitems", "EquipableSlots", GetBaseItemType(oItem));
    if (slots != "0x00000"
            && GetItemInSlot(INVENTORY_SLOT_HEAD, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_CHEST, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_BELT, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_BOOTS, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_CLOAK, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_LEFTHAND, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_NECK, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_LEFTRING, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_RIGHTRING, oCreature) != oItem
            && GetItemInSlot(INVENTORY_SLOT_ARMS, oCreature) != oItem)
        return FALSE;

    //If no other restrictions, then the item can be used
    if (slots != "0x00000" || (!GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_ALIGNMENT_GROUP)
            && !GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_CLASS)
            && !GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_SPECIFIC_ALIGNMENT)
            && !GetItemHasItemProperty(oItem, ITEM_PROPERTY_USE_LIMITATION_RACIAL_TYPE)))
        return TRUE;

    //If restrictions present, verify we meet at least one of them or we have good enough UMD
    itemproperty ip = GetFirstItemProperty(oItem);
    while (GetIsItemPropertyValid(ip))
    {
        if (GetItemPropertyType(ip) == ITEM_PROPERTY_USE_LIMITATION_ALIGNMENT_GROUP)
        {
            int alignment = GetItemPropertySubType(ip);
            int match = FALSE;
            switch (alignment)
            {
                case ALIGNMENT_CHAOTIC:
                case ALIGNMENT_LAWFUL:
                    match = GetAlignmentLawChaos(oCreature) == alignment;
                    break;
                case ALIGNMENT_GOOD:
                case ALIGNMENT_EVIL:
                    match = GetAlignmentGoodEvil(oCreature) == alignment;
                    break;
                case ALIGNMENT_NEUTRAL:
                    match = GetAlignmentGoodEvil(oCreature) == alignment || GetAlignmentLawChaos(oCreature) == alignment;
                    break;
            }
            if (match == TRUE)
                return TRUE;
        }
        if (GetItemPropertyType(ip) == ITEM_PROPERTY_USE_LIMITATION_CLASS)
        {
            int class = GetItemPropertySubType(ip);
            if (GetLevelByClass(class, oCreature) > 0)
                return TRUE;
        }
        if (GetItemPropertyType(ip) == ITEM_PROPERTY_USE_LIMITATION_SPECIFIC_ALIGNMENT)
        {
            int alignment = GetItemPropertySubType(ip);
            int match = FALSE;
            switch (alignment)
            {
                case 112:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_LAWFUL && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_GOOD;
                    break;
                case 113:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_LAWFUL && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_NEUTRAL;
                    break;
                case 114:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_LAWFUL && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_EVIL;
                    break;
                case 115:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_NEUTRAL && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_GOOD;
                    break;
                case 116:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_NEUTRAL && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_NEUTRAL;
                    break;
                case 117:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_NEUTRAL && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_EVIL;
                    break;
                case 118:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_CHAOTIC && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_GOOD;
                    break;
                case 119:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_CHAOTIC && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_NEUTRAL;
                    break;
                case 120:
                    match = GetAlignmentLawChaos(oCreature) == ALIGNMENT_CHAOTIC && GetAlignmentGoodEvil(oCreature) == ALIGNMENT_EVIL;
                    break;
            }
            if (match == TRUE)
                return TRUE;
        }
        if (GetItemPropertyType(ip) == ITEM_PROPERTY_USE_LIMITATION_RACIAL_TYPE)
        {
            int race = GetItemPropertySubType(ip);
            if (GetRacialType(oCreature) == race)
                return TRUE;
        }
        ip = GetNextItemProperty(oItem);
    }

    //Maybe we can still use the UMD successfully
    int dc = _GetUseMagicDeviceDC(oItem, nSpell);
    int umd = GetSkillRank(SKILL_USE_MAGIC_DEVICE, oCreature);
    if (umd >= dc)
        return TRUE;

    return FALSE;
}

itemproperty _GetSpellIPFromItem(int nSpell, object oItem, object oCreature)
{
    itemproperty result;
    if (!GetItemHasItemProperty(oItem, ITEM_PROPERTY_CAST_SPELL) || !_GetIsItemUsable(oCreature, oItem, nSpell))
        return result;

    itemproperty ip = GetFirstItemProperty(oItem);
    while (GetIsItemPropertyValid(ip))
    {
        int type = GetItemPropertyType(ip);
        if (type == ITEM_PROPERTY_CAST_SPELL)
        {
            int spell = StringToInt(Get2DAString("iprp_spells", "SpellIndex", GetItemPropertySubType(ip)));
            if (spell == nSpell)
                return ip;
        }
        ip = GetNextItemProperty(oItem);
    }
    return result;
}

struct ItemWithIP _GetItemWithSpell(int nSpell, object oCreature)
{
    struct ItemWithIP result;
    result.item = OBJECT_INVALID;
    //higher cost table value Cast Spell properties should be used over lower ones,
    //for example Unlimited Uses is better to use than 2 uses per day which in turn is better than Single Use
    int highestCostTableValue = -1;
    object item = GetFirstItemInInventory(oCreature);
    while (GetIsObjectValid(item))
    {
        itemproperty ip = _GetSpellIPFromItem(nSpell, item, oCreature);
        int costTableValue = GetItemPropertyCostTableValue(ip);
        int charges = GetItemCharges(item);
        if (GetIsItemPropertyValid(ip) && costTableValue > highestCostTableValue)
        {
            int valid = FALSE;
            if (costTableValue < 8 || costTableValue > 12 || GetItemPropertyUsesPerDayRemaining(item, ip) > 0)
                valid = TRUE;
            else if (costTableValue == 2 && charges >= 5)
                valid = TRUE;
            else if (costTableValue == 3 && charges >= 4)
                valid = TRUE;
            else if (costTableValue == 4 && charges >= 3)
                valid = TRUE;
            else if (costTableValue == 5 && charges >= 2)
                valid = TRUE;
            else if (costTableValue == 6 && charges >= 1)
                valid = TRUE;
            else if (costTableValue == 1 || costTableValue == 13)
                valid = TRUE;
            if (valid)
            {
                result.item = item;
                result.property = ip;
            }
        }
        item = GetNextItemInInventory(oCreature);
    }
    int i;
    for (i = 0; i < 11; i++)
    {
        item = GetItemInSlot(i, oCreature);
        itemproperty ip = _GetSpellIPFromItem(nSpell, item, oCreature);
        int costTableValue = GetItemPropertyCostTableValue(ip);
        int charges = GetItemCharges(item);
        if (GetIsItemPropertyValid(ip) && costTableValue > highestCostTableValue)
        {
            int valid = FALSE;
            if (costTableValue < 8 || costTableValue > 12 || GetItemPropertyUsesPerDayRemaining(item, ip) > 0)
                valid = TRUE;
            else if (costTableValue == 2 && charges >= 5)
                valid = TRUE;
            else if (costTableValue == 3 && charges >= 4)
                valid = TRUE;
            else if (costTableValue == 4 && charges >= 3)
                valid = TRUE;
            else if (costTableValue == 5 && charges >= 2)
                valid = TRUE;
            else if (costTableValue == 6 && charges >= 1)
                valid = TRUE;
            else if (costTableValue == 1 || costTableValue == 13)
                valid = TRUE;
            if (valid)
            {
                result.item = item;
                result.property = ip;
            }
        }
        item = GetNextItemInInventory(oCreature);
    }
    return result;
}

int GetHasSpellIncludingItems(int nSpell, object oCreature=OBJECT_SELF)
{
    int getHasSpellResult = GetHasSpell(nSpell, oCreature);
    if (getHasSpellResult)
        return getHasSpellResult;

    struct ItemWithIP itemWithIp = _GetItemWithSpell(nSpell, oCreature);
    return itemWithIp.item != OBJECT_INVALID;
}

void ActionCastSpellAtLocationIncludingItems(int nSpell, location lTargetLocation, int nMetaMagic=METAMAGIC_ANY, int bCheat=FALSE, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE)
{
    if (bCheat || GetHasSpell(nSpell))
    {
        ActionCastSpellAtLocation(nSpell, lTargetLocation, nMetaMagic, bCheat, nProjectilePathType, bInstantSpell);
        return;
    }

    struct ItemWithIP itemWithIp = _GetItemWithSpell(nSpell, OBJECT_SELF);
    if (itemWithIp.item == OBJECT_INVALID)
        return;

    ActionUseItemAtLocation(itemWithIp.item, itemWithIp.property, lTargetLocation);
}

void ActionCastSpellAtObjectIncludingItems(int nSpell, object oTarget, int nMetaMagic=METAMAGIC_ANY, int bCheat=FALSE, int nDomainLevel=0, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE)
{
    if (bCheat || GetHasSpell(nSpell))
    {
        ActionCastSpellAtObject(nSpell, oTarget, nMetaMagic, bCheat, nDomainLevel, nProjectilePathType, bInstantSpell);
        return;
    }

    struct ItemWithIP itemWithIp = _GetItemWithSpell(nSpell, OBJECT_SELF);
    if (itemWithIp.item == OBJECT_INVALID)
        return;

    ActionUseItemOnObject(itemWithIp.item, itemWithIp.property, oTarget);
}