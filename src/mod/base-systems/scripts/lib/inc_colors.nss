// String-coloring library

const string COLORTOKEN = "                  ##################$%&'()*+,-./0123456789:;;==?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[[]^_`abcdefghijklmnopqrstuvwxyz{|}~~?�?�????�???????�???????�???????������������������������������������������������������������������������������������������������";

// Returns a properly color-coded sText string based on specified RGB values
string ColorString(string sText, int nRed=255, int nGreen=255, int nBlue=255);

// Returns the raw value of the RGB color selected - this can be used to set custom coloring tokens like "<c[result]>"
string RGBToString(int nRed=255, int nGreen=255, int nBlue=255);

// Store color obtained with the RGBToString() function to be used later with the ColorStringWithStoredColor() function
void StoreColor(string sColorName, string sRGB);

// Returns a properly color-coded sText string based on RGB values stores under sColorName with the StoreColor() function
string ColorStringWithStoredColor(string sText, string sColorName);



string ColorString(string sText, int nRed=255, int nGreen=255, int nBlue=255)
{
    return "<c" + RGBToString(nRed, nGreen, nBlue) + ">" + sText + "</c>";
}

string RGBToString(int nRed=255, int nGreen=255, int nBlue=255)
{
    return GetSubString(COLORTOKEN, nRed, 1) + GetSubString(COLORTOKEN, nGreen, 1) + GetSubString(COLORTOKEN, nBlue, 1);
}

void StoreColor(string sColorName, string sRGB)
{
    SetLocalString(GetModule(), "RGB_"+sColorName, sRGB);
}

string ColorStringWithStoredColor(string sText, string sColorName)
{
    return "<c" + GetLocalString(GetModule(), "RGB_"+sColorName) + ">" + sText + "</c>";
}