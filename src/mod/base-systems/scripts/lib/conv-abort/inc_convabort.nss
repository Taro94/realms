#include "inc_scriptevents"

// Library containing functions for a workaround of abort scripts:
// if the game is quicksaved or autosaved during a conversation,
// loading the save file won't restore the conversation despite the fact the abort script never run.
// The idea is for every conversation start to be denoted with a call to MarkConversationStart()
// and every conversation end to be denoted with MarkConversationEnd(). If conversation has been denoted
// as started, but not denoted as ended, yet the creature is not in conversation,
// this is a sign that the conversation abort script should be executed.

// Call this in every conversation's first node (as long as you have an important conversation abort script).
// It can be safely called multiple times in conversation scripts (but not after MarkConversationEnd() is called at the end of the convo).
void MarkConversationStart(string sAbortScriptName);

// Call this at the end (abort script and regular end script or all exit nodes) of every conversation with an important abort script
// DON'T FORGET TO CALL THIS after MakeConversationStart() or the abort script will continue running an infinite number of times!
void MarkConversationEnd();

// A substitute for GetPCSpeaker() that should be called by every abort script;
// It can retrieve the PC the caller conversed with even if the abort script has been called after the conversation ended
// and if that player is gone (after loading a save file, for example).
// It can be safely used in other conversation scripts, too - will then act like a regular GetPCSpeaker().
// Note that the PC retrieved may no longer be valid if an abort script was not run "properly", but with a delay.
// Extra checks should be made to handle these scenarios.
object GetLastPCSpeaker();

// Call this at the beginning of every event (except OnDeath, OnSpawn and OnUserDefined) of a creature that can have a conversation with an important abort script
void EnforceConversationAbortScript();

// Call this to add event scripts with EnforceConversationAbortScript calls.
// In other words, this call prepares a creature and you don't need to add EnforceConversationAbortScript manually to its event scripts.
void EnableConversationAbortScriptEnforcement(object oCreature);


void MarkConversationStart(string sAbortScriptName)
{
    SetLocalString(OBJECT_SELF, "CONVABORT_SCRIPTNAME", sAbortScriptName);
    SetLocalInt(OBJECT_SELF, "CONVABORT_STARTED", TRUE);
    SetLocalObject(OBJECT_SELF, "CONVABORT_PCSPEAKER", GetPCSpeaker());
}

void MarkConversationEnd()
{
    DeleteLocalString(OBJECT_SELF, "CONVABORT_SCRIPTNAME");
    DeleteLocalInt(OBJECT_SELF, "CONVABORT_STARTED");
    DeleteLocalObject(OBJECT_SELF, "CONVABORT_PCSPEAKER");
}

object GetLastPCSpeaker()
{
    object storedSpeaker = GetLocalObject(OBJECT_SELF, "CONVABORT_PCSPEAKER");
    if (storedSpeaker != OBJECT_INVALID)
        return storedSpeaker;
    return GetPCSpeaker();
}

void EnforceConversationAbortScript()
{
    if (GetLocalInt(OBJECT_SELF, "CONVABORT_STARTED") == FALSE)
        return;

    if (IsInConversation(OBJECT_SELF))
        return;

    string abortScript = GetLocalString(OBJECT_SELF, "CONVABORT_SCRIPTNAME");
    ExecuteScript(abortScript);
    MarkConversationEnd();
}

void EnableConversationAbortScriptEnforcement(object oCreature)
{
    if (GetLocalInt(OBJECT_SELF, "CONVABORT_ENABLED"))
        return;

    string scriptName = "cre_convabort";
    AddEventScript(SUBEVENT_CREATURE_ON_BLOCKED, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_COMBAT_ROUND_END, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_CONVERSATION, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_DAMAGED, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_DISTURBED, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_HEARTBEAT, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_PERCEPTION, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_SPELL_CAST_AT, scriptName, oCreature);
    AddEventScript(SUBEVENT_CREATURE_ON_RESTED, scriptName, oCreature);

    SetLocalInt(OBJECT_SELF, "CONVABORT_ENABLED", TRUE);
}