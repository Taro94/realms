#include "inc_random"
#include "inc_debug"
#include "inc_arrays"
#include "inc_scriptevents"
#include "x0_i0_stringlib"

// Library with functions for spawning and managing groups of enemies defined in 2da files.
// A creature group 2da is defined as follows:
// - the 2da file contains the following columns: Level, MinNumber, MaxNumber, BossResRef, Creature1, Creature2, Creature3, etc,
// - rows need to be in a non-descending order according to the Level column value,
// - creatures need to be defined in consecutive creature columns (for example, if the group should have three spawnable creature blueprints other than a boss creature, they need to be defined in columns Creature1, Creature2 and Creature3),
// - if a row's BossResRef value is defined (different than "****"), the first creature spawned in this group will be the one with this ResRef,
// - further spawned creatures (or all creatures if BossResRef is undefined) are picked randomly from the defined creature column values,
// - each creature column value should be defined in one of the following ways: "<resref>,<chance>,<value>", "<resref>,<chance>" or "<resref>", for example "cre_goblina,1,2"; if value or value and chance are omitted, they are assumed to be "1",
// - creatures will be picked randomly according to their chances: for example, for a group of two creatures with chances 2 and 1, respectively, the first one has 2/3 or 66.6% chance of being picked and the second one 1/3 or 33.3%,
// - creatures will be picked the number of times that is randomly picked between MinNumber and MaxNumber inclusive; a boss creature is included in the count,
// - creature's value higher than 1 means that if this creature is picked, it should count as multiple creatures;
//   for example, if a group has MinNumber and MaxNumber equal to 2 and two creatures with values 1 and 2 respectively, the spawned group may consist either of two creatures of the first type or a single creature of the second type,
// - a creature with value higher than 1 may be skipped in the creature picking process when the number of creatures remaining to spawn is lower than that creature's value;
//   using the previous example, if the first creature spawned is of the first type, the second creature to spawn will not be the second creature type, because only one creature remains to be spawned and the value of the second creature's type is 2,
// - if more than one row share the same Level value, one of them will be picked randomly when a group of this level is to be spawned,
// - a 2da file does not need to define rows for every level, but calling the SpawnCreatureGroup function with a level value not present in the 2da will result in erroneous behavior.


struct GroupCreature
{
    string resRef;
    int chance;
    int value;
};

// Picks a group 2da's row corresponding to a group appropriate to the given level.
int PickGroupForLevel(string sGroup2da, int nLevel, string sSeedName="default");

// Picks a random number between MinNumber and MaxNumber inclusive for a given row in the given group 2da.
int PickRandomNumberOfCreaturesInGroup(string sGroup2da, int nRow, string sSeedName="default", int nMinNumberOverride=0, int nMaxNumberOverride=0);

// Randomly picks a creature from a given group to spawn based on their chance, value and the provided number of creatures that are still left to be spawned;
// will return a struct with default field values if no appropriate creature could be found given nNumLeftToSpawn value.
struct GroupCreature PickCreatureToSpawnInGroup(string sGroup2da, int nRow, int nNumLeftToSpawn, string sSeedName="default");

// Spawns a creature group defined by a given 2DA file. Returns the first creature spawned in the group (boss if one exists).
// - sGroup2da - name of the 2da file (without extension) with creature group data,
// - nLevel - level of the group,,
// - lSpawnLocation - location of the spawn,
// - fSpawnRadius - defines the circular area in which creatures will randomly spawn with lSpawnLocation at its centre,
// - nAppearAnimation - as in bUseAppearAnimation in CreateCreature/CreateObject
// - sSeedName - name of the seed used for randomness (except spawn location),
// - nFirstSpawnAtCenter - if set to TRUE, the first creature spawned will spawn precisely at lSpawnLocation, disregarding fSpawnRadius; if a boss creature is spawned, it is always the first spawn,
// - sOnCreatureCreateScript - script that every spawned creature will execute after creation (before OnSpawn); also see functions usable in the passed script, GetIsCreatureGroupLeader and GetCreatureGroupSpawnerObject,
// - nMinNumberOverride - overrides the MinNumber column value from the group's 2da file if different than 0,
// - nMaxNumberOverride - overrides the MaxNumber column value from the group's 2da file if different than 0,
// - sBossResRefOverride - overrides the BossResRef column value from the group's 2da file if not empty (use "****" to signify no boss creature in the spawned group).
object SpawnCreatureGroup(string sGroup2da, int nLevel, location lSpawnLocation, float fSpawnRadius=0.0f, int nAppearAnimation=FALSE, string sSeedName="default", int nFirstSpawnAtCenter=FALSE, string sOnCreatureCreateScript="", int nMinNumberOverride=0, int nMaxNumberOverride=0, string sBossResRefOverride="");

// Returns TRUE if the caller is the first creature spawned in a group (if a boss is defined for a group, it will always be the first creature);
// This function can be called in the script to be executed on creature creation (passed as a parameter in SpawnCreatureGroup).
int GetIsCreatureGroupLeader();

// Returns the object that called SpawnCreatureGroup which created the caller.
// This function can be called in the script to be executed on creature creation (passed as a parameter in SpawnCreatureGroup).
// You can use it to set variables on the object calling SpawnCreatureGroup and then access them in said script.
object GetCreatureGroupSpawnerObject();


int PickGroupForLevel(string sGroup2da, int nLevel, string sSeedName="default")
{
    int i = 0;
    string levelString;
    int storedLvl = 0;
    int storedRow = -1;

    while (TRUE)
    {
        levelString = Get2DAString(sGroup2da, "Level", i);
        int rowLevel = StringToInt(levelString);

        if (rowLevel < 1 || rowLevel > nLevel)
            break;

        if (rowLevel <= nLevel && storedLvl < rowLevel)
        {
            storedLvl = rowLevel;
            storedRow = i;

            if (rowLevel == nLevel)
                break;
        }
        i++;
    }

    string array = "AppropriateRows";
    CreateIntArray(array);
    i = storedRow;
    while (TRUE)
    {
        levelString = Get2DAString(sGroup2da, "Level", i);
        int rowLevel = StringToInt(levelString);

        if (rowLevel < 1 || rowLevel > storedLvl)
            break;

        AddIntArrayElement(array, i);
        i++;
    }

    int arraySize = GetIntArraySize(array);
    int arrayIndex = RandomNext(arraySize, sSeedName);
    int selectedRow = GetIntArrayElement(array, arrayIndex);

    ClearIntArray(array);
    return selectedRow;
}

int PickRandomNumberOfCreaturesInGroup(string sGroup2da, int nRow, string sSeedName="default", int nMinNumberOverride=0, int nMaxNumberOverride=0)
{
    int minNum = nMinNumberOverride > 0 ? nMinNumberOverride : StringToInt(Get2DAString(sGroup2da, "MinNumber", nRow));
    int maxNum = nMaxNumberOverride > 0 ? nMaxNumberOverride : StringToInt(Get2DAString(sGroup2da, "MaxNumber", nRow));
    int num;
    if (maxNum < minNum)
        num = minNum;
    else
    {
        num = minNum + RandomNext(maxNum - minNum + 1, sSeedName);
    }
    return num;
}

struct GroupCreature PickCreatureToSpawnInGroup(string sGroup2da, int nRow, int nNumLeftToSpawn, string sSeedName="default")
{
    struct GroupCreature result;
    string resrefArray = "ResRefs";
    string chanceArray = "Chances";
    string valueArray = "Values";
    CreateStringArray(resrefArray);
    CreateIntArray(chanceArray);
    CreateIntArray(valueArray);

    //Get all creatures suitable for spawning
    int chanceTotal = 0;
    int i = 1;
    while (TRUE)
    {
        string valueString = Get2DAString(sGroup2da, "Creature"+IntToString(i), nRow);
        if (valueString == "")
            break;

        CreateStringArray("Tokens");
        struct sStringTokenizer tokenizer = GetStringTokenizer(valueString, ",");
        while (HasMoreTokens(tokenizer)) 
        {
            tokenizer = AdvanceToNextToken(tokenizer);
            AddStringArrayElement("Tokens", GetNextToken(tokenizer));
        }
        string resref;
        int chance = 1;
        int value = 1;
        int j;
        for (j = 0; j < GetStringArraySize("Tokens"); j++)
        {
            string element = GetStringArrayElement("Tokens", j);
            switch (j)
            {
                case 0:
                    resref = element;
                    break;
                case 1:
                    chance = StringToInt(element);
                    break;
                case 2:
                    value = StringToInt(element);
                    break;
            }
        }
        ClearStringArray("Tokens");
        if (value <= nNumLeftToSpawn)
        {
            AddStringArrayElement(resrefArray, resref);
            AddIntArrayElement(chanceArray, chance);
            AddIntArrayElement(valueArray, value);
            chanceTotal += chance;
        }

        i++;
    }

    //Randomly pick creature based on creature chances
    int rand = RandomNext(chanceTotal, sSeedName);
    int chanceTotalSoFar = 0;
    int arraySize = GetIntArraySize(chanceArray);
    if (arraySize == 0)
        return result;
    for (i = 0; i < arraySize; i++)
    {
        int chance = GetIntArrayElement(chanceArray, i);
        chanceTotalSoFar += chance;
        if (chanceTotalSoFar > rand)
        {
            result.resRef = GetStringArrayElement(resrefArray, i);
            result.chance = GetIntArrayElement(chanceArray, i);
            result.value = GetIntArrayElement(valueArray, i);
            break;
        }
    }
    ClearStringArray(resrefArray);
    ClearStringArray(chanceArray);
    ClearStringArray(valueArray);

    return result;
}

location _GetRandomLocation(location lSpawnLocation, float fSpawnRadius)
{
    vector position = GetPositionFromLocation(lSpawnLocation);
    object area = GetAreaFromLocation(lSpawnLocation);

    float distance = fSpawnRadius * (IntToFloat(Random(101)) / 100.0f);
    float angle = IntToFloat(Random(360));
    float x = distance * cos(angle);
    float y = distance * sin(angle);

    position.x += x;
    position.y += y;

    return Location(area, position, IntToFloat(Random(360)));
}

object SpawnCreatureGroup(string sGroup2da, int nLevel, location lSpawnLocation, float fSpawnRadius=0.0f, int nAppearAnimation=FALSE, string sSeedName="default", int nFirstSpawnAtCenter=FALSE, string sOnCreatureCreateScript="", int nMinNumberOverride=0, int nMaxNumberOverride=0, string sBossResRefOverride="")
{
    location spawnLocation;
    
    int row = PickGroupForLevel(sGroup2da, nLevel, sSeedName);
    int numLeft = PickRandomNumberOfCreaturesInGroup(sGroup2da, row, sSeedName, nMinNumberOverride, nMaxNumberOverride);

    //Spawn boss if there is one to spawn
    object firstCreature = OBJECT_INVALID;
    string bossResRef = sBossResRefOverride != "" ? sBossResRefOverride : Get2DAString(sGroup2da, "BossResref", row);
    if (bossResRef != "" && bossResRef != "****")
    {
        spawnLocation = nFirstSpawnAtCenter ? lSpawnLocation : _GetRandomLocation(lSpawnLocation, fSpawnRadius);
        object boss = CreateCreature(bossResRef, spawnLocation, nAppearAnimation);
        firstCreature = boss;
        SetLocalInt(boss, "GRP_LEADER", TRUE);
        SetLocalObject(boss, "GRP_SPAWNER", OBJECT_SELF);
        ExecuteScript(sOnCreatureCreateScript, boss);
        numLeft--;
    }

    while (numLeft > 0)
    {
        struct GroupCreature creature = PickCreatureToSpawnInGroup(sGroup2da, row, numLeft, sSeedName);
        if (creature.resRef == "")
            return firstCreature;

        spawnLocation = nFirstSpawnAtCenter && firstCreature == OBJECT_INVALID ? lSpawnLocation : _GetRandomLocation(lSpawnLocation, fSpawnRadius);
        object spawned = CreateCreature(creature.resRef, spawnLocation, nAppearAnimation);
        if (firstCreature == OBJECT_INVALID)
        {
            firstCreature = spawned;
            SetLocalInt(spawned, "GRP_LEADER", TRUE);
        }
        SetLocalObject(spawned, "GRP_SPAWNER", OBJECT_SELF);
        ExecuteScript(sOnCreatureCreateScript, spawned);
        numLeft -= creature.value;
    }

    return firstCreature;
}

int GetIsCreatureGroupLeader()
{
    return GetLocalInt(OBJECT_SELF, "GRP_LEADER");
}

object GetCreatureGroupSpawnerObject()
{
    return GetLocalObject(OBJECT_SELF, "GRP_SPAWNER");
}