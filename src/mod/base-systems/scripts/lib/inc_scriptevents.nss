#include "inc_arrays"
#include "inc_debug"

// Library expanding event functionality, highly dependant on the included evn_* master scripts and their contents and on manual adjustments in the module
// (like modifying the familiar/animal companion summoning scripts, summoned creatures' blueprints, etc)

// Constants
const string SUBEVENT_MODULE_ON_ACQUIRE_ITEM = "OnAcquireItem";
const string SUBEVENT_MODULE_ON_ACTIVATE_ITEM = "OnActivateItem";
const string SUBEVENT_MODULE_ON_CLIENT_ENTER = "OnClientEnter";
const string SUBEVENT_MODULE_ON_CLIENT_LEAVE = "OnClientLeave";
const string SUBEVENT_MODULE_ON_CUTSCENE_ABORT = "OnCutsceneAbort";
const string SUBEVENT_MODULE_ON_HEARTBEAT = "OnHeartbeat";
const string SUBEVENT_MODULE_ON_PLAYER_CHAT = "OnPlayerChat";
const string SUBEVENT_MODULE_ON_PLAYER_DEATH = "OnPlayerDeath";
const string SUBEVENT_MODULE_ON_PLAYER_DYING = "OnPlayerDying";
const string SUBEVENT_MODULE_ON_PLAYER_EQUIP_ITEM = "OnPlayerEquipItem";
const string SUBEVENT_MODULE_ON_PLAYER_LEVEL_UP = "OnPlayerLevelUp";
const string SUBEVENT_MODULE_ON_PLAYER_RESPAWN = "OnPlayerRespawn";
const string SUBEVENT_MODULE_ON_PLAYER_REST = "OnPlayerRest";
const string SUBEVENT_MODULE_ON_PLAYER_UNEQUIP_ITEM = "OnPlayerUnEquipItem";
const string SUBEVENT_MODULE_ON_UNACQUIRE_ITEM = "OnUnAcquireItem";
const string SUBEVENT_MODULE_ON_PLAYER_TARGET = "OnPlayerTarget";
const string SUBEVENT_MODULE_ON_USER_DEFINED = "OnUserDefined";
const string SUBEVENT_MODULE_ON_LOAD = "OnModuleLoad";

const string SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED = "OnPhysicalAttacked";
const string SUBEVENT_CREATURE_ON_BLOCKED = "OnBlocked";
const string SUBEVENT_CREATURE_ON_SPELL_CAST_AT = "OnSpellCastAt";
const string SUBEVENT_CREATURE_ON_CONVERSATION = "OnConversation";
const string SUBEVENT_CREATURE_ON_DAMAGED = "OnDamaged";
const string SUBEVENT_CREATURE_ON_DEATH = "OnDeath";
const string SUBEVENT_CREATURE_ON_DISTURBED = "OnDisturbed";
const string SUBEVENT_CREATURE_ON_HEARTBEAT = "OnHeartbeat";
const string SUBEVENT_CREATURE_ON_PERCEPTION = "OnPerception";
const string SUBEVENT_CREATURE_ON_RESTED = "OnRested";
const string SUBEVENT_CREATURE_ON_COMBAT_ROUND_END = "OnCombatRoundEnd";
const string SUBEVENT_CREATURE_ON_SPAWN = "OnSpawn";
const string SUBEVENT_CREATURE_ON_USER_DEFINED = "OnUserDefined";

const string SUBEVENT_AREA_ON_ENTER = "OnEnter";
const string SUBEVENT_AREA_ON_EXIT = "OnExit";
const string SUBEVENT_AREA_ON_HEARTBEAT = "OnHeartbeat";
const string SUBEVENT_AREA_ON_USER_DEFINED = "OnUserDefined";

const string SUBEVENT_TRIGGER_ON_ENTER = "OnEnter";
const string SUBEVENT_TRIGGER_ON_EXIT = "OnExit";
const string SUBEVENT_TRIGGER_ON_HEARTBEAT = "OnHeartbeat";
const string SUBEVENT_TRIGGER_ON_USER_DEFINED = "OnUserDefined";

// Adds a script to be executed on a given event.
void AddEventScript(string sSubevent, string sScriptName, object oObject=OBJECT_SELF);

// Runs all the scripts stored in variables on the object.
// Scripts are executed in an order in which they were added.
void RunEventScripts(string sSubevent, object oObject=OBJECT_SELF);

// Stores the creature's current set of scripts as variables from which they can be run by master scripts
// and replaces them with master scripts. Be careful not to use this function on a creature that already has master scripts set.
// You can set nKeepPreviousEventScripts to FALSE to not keep the creature's current set of scripts.
void SetCreatureMasterScripts(object oCreature, int nKeepPreviousEventScripts=TRUE);

// A wrapper for CreateObject when applied to creatures.
// It stores the creature's original blueprints scripts in variables and sets master event scripts instead.
// Master event scripts will execute the original scripts at the end of their logic,
// but provide global hooks to every creature's events (if you ensure to only spawn creatures using this function)
// and let you add multiple event scripts by calling AddEventScript on the creature.
// Make sure to use it instead of CreateObject in the whole module if you want every creature to have master scripts.
// Note: summons need to have master event scripts set manually, while familiars and animal companions need to have their summoning scripts replaced with Summon[Familiar/AnimalCompanion]WithMasterEventScripts.
object CreateCreature(string sTemplate, location lLocation, int bUseAppearAnimation=FALSE, string sNewTag="", int nKeepPreviousEventScripts=TRUE);

// A wrapper for SummonFamiliar similar to CreateCreature
void SummonFamiliarWithMasterEventScripts(object oMaster=OBJECT_SELF);

// A wrapper for SummonAnimalCompanion similar to CreateCreature
void SummonAnimalCompanionWithMasterEventScripts(object oMaster=OBJECT_SELF);

// Sets PC event scripts to master scripts (with some exceptions, see the function for details)
void SetPCEventScripts(object oPC);

// Returns TRUE if the module has been loaded from a save file and FALSE otherwise.
// Should only be used in the OnModuleLoad event scripts registered with AddEventScript (after that it will always return TRUE).
int GetIsModuleLoadedFromSaveFile();

// Sets module event scripts to master scripts
void RegisterMasterModuleEvents();

// Sets event scripts of all areas and triggers within them to respective sets of master scripts
void RegisterMasterAreaAndTriggerEvents();

// SetMasterScripts moves event scripts that were previously set for a creature to variables.
// These scripts, if they existed, are then run by the master event creature scripts before those added with AddEventScript calls.
// Use this function to delete an event script variable if you don't want it to fire.
void ClearCreatureEventScriptFromVariable(object oCreature, string sSubevent);

void RunEventScripts(string sSubevent, object oObject=OBJECT_SELF)
{
    int scriptNum = GetStringArraySize(sSubevent, oObject);
    int i;
    for (i = 0; i < scriptNum; i++)
    {
        string script = GetStringArrayElement(sSubevent, i, oObject);
        ExecuteScript(script, oObject);
        scriptNum = GetStringArraySize(sSubevent, oObject);
    }
}

void AddEventScript(string sSubevent, string sScriptName, object oObject=OBJECT_SELF)
{
    AddStringArrayElement(sSubevent, sScriptName, FALSE, oObject);
}

void SetCreatureMasterScripts(object oCreature, int nKeepPreviousEventScripts=TRUE)
{
    string eventScript;

    if (nKeepPreviousEventScripts)
    {
        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_BLOCKED_BY_DOOR);
        if (eventScript != "evn_creblocked")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_BLOCKED, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DAMAGED);
        if (eventScript != "evn_credamage")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_DAMAGED, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DEATH);
        if (eventScript != "evn_credeath")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_DEATH, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DIALOGUE);
        if (eventScript != "evn_creconvo")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_CONVERSATION, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DISTURBED);
        if (eventScript != "evn_credisturbed")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_DISTURBED, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_END_COMBATROUND);
        if (eventScript != "evn_creroundend")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_COMBAT_ROUND_END, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_HEARTBEAT);
        if (eventScript != "evn_crehb")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_HEARTBEAT, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_MELEE_ATTACKED);
        if (eventScript != "evn_creattacked")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_NOTICE);
        if (eventScript != "evn_crepercept")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_PERCEPTION, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_RESTED);
        if (eventScript != "evn_crerested")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_RESTED, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_SPAWN_IN);
        if (eventScript != "evn_crespawn")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_SPAWN, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_SPELLCASTAT);
        if (eventScript != "evn_crecastat")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_SPELL_CAST_AT, eventScript);

        eventScript = GetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_USER_DEFINED_EVENT);
        if (eventScript != "evn_creuserdef")
            SetLocalString(oCreature, SUBEVENT_CREATURE_ON_USER_DEFINED, eventScript);
    }

    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_BLOCKED_BY_DOOR, "evn_creblocked");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DAMAGED, "evn_credamage");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DEATH, "evn_credeath");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DIALOGUE, "evn_creconvo");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_DISTURBED, "evn_credisturbed");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_END_COMBATROUND, "evn_creroundend");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_HEARTBEAT, "evn_crehb");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_MELEE_ATTACKED, "evn_creattacked");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_NOTICE, "evn_crepercept");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_RESTED, "evn_crerested");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_SPAWN_IN, "evn_crespawn");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_SPELLCASTAT, "evn_crecastat");
    SetEventScript(oCreature, EVENT_SCRIPT_CREATURE_ON_USER_DEFINED_EVENT, "evn_creuserdef");
}

object CreateCreature(string sTemplate, location lLocation, int bUseAppearAnimation=FALSE, string sNewTag="", int nKeepPreviousEventScripts=TRUE)
{
    object creature = CreateObject(OBJECT_TYPE_CREATURE, sTemplate, lLocation, bUseAppearAnimation, sNewTag);
    
    //Lootable corpse is bugged as of NWN version .33 in that it can cause the loot to disappear if game is loaded from a save file made just as a creature was dying.
    //It's better to turn it off altogether, then.
    SetIsDestroyable(TRUE, FALSE, FALSE);
    SetLootable(creature, FALSE);

    SetCreatureMasterScripts(creature, nKeepPreviousEventScripts);
    return creature;
}

void SummonFamiliarWithMasterEventScripts(object oMaster=OBJECT_SELF)
{
    SummonFamiliar(oMaster);
    object creature = GetAssociate(ASSOCIATE_TYPE_FAMILIAR, oMaster);
    SetCreatureMasterScripts(creature);
}

void SummonAnimalCompanionWithMasterEventScripts(object oMaster=OBJECT_SELF)
{
    SummonAnimalCompanion(oMaster);
    object creature = GetAssociate(ASSOCIATE_TYPE_ANIMALCOMPANION, oMaster);
    SetCreatureMasterScripts(creature);
}

void SetPCEventScripts(object oPC)
{
    //Clear all events first
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_BLOCKED_BY_DOOR, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_DAMAGED, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_DEATH, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_DIALOGUE, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_DISTURBED, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_END_COMBATROUND, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_HEARTBEAT, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_MELEE_ATTACKED, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_NOTICE, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_RESTED, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_SPAWN_IN, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_SPELLCASTAT, "");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_USER_DEFINED_EVENT, "");

    //Set SOME master scripts now (OnConversation messes up ActionStartConversation for that PC if the conversing object is a placeable, OnSpawn is useless)
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_BLOCKED_BY_DOOR, "evn_creblocked");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_DAMAGED, "evn_credamage");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_DEATH, "evn_credeath");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_DISTURBED, "evn_credisturbed");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_END_COMBATROUND, "evn_creroundend");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_HEARTBEAT, "evn_crehb");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_MELEE_ATTACKED, "evn_creattacked");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_NOTICE, "evn_crepercept");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_RESTED, "evn_crerested");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_SPELLCASTAT, "evn_crecastat");
    SetEventScript(oPC, EVENT_SCRIPT_CREATURE_ON_USER_DEFINED_EVENT, "evn_creuserdef");
}

int GetIsModuleLoadedFromSaveFile()
{
    return GetLocalInt(GetModule(), "MOD_IS_LAUNCHED");
}

void RegisterMasterModuleEvents()
{
    object mod = GetModule();
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_ACQUIRE_ITEM, "evn_modacquire"); //OnAcquireItem
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_ACTIVATE_ITEM, "evn_modactivate"); //OnActivateItem
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_CLIENT_ENTER, "evn_modenter"); //OnClientEnter
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_CLIENT_EXIT, "evn_modleave"); //OnClientLeave
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_PLAYER_CANCEL_CUTSCENE, "evn_modabortcs"); //OnCutsceneAbort
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_HEARTBEAT, "evn_modhb"); //OnHeartbeat
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_PLAYER_CHAT, "evn_modchat"); //OnPlayerChat
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_PLAYER_DEATH, "evn_moddeath"); //OnPlayerDeath
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_PLAYER_DYING, "evn_moddying"); //OnPlayerDying
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_EQUIP_ITEM, "evn_modequip"); //OnPlayerEquipItem
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_PLAYER_LEVEL_UP, "evn_modlevelup"); //OnPlayerLevelUp
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_RESPAWN_BUTTON_PRESSED, "evn_modrespawn"); //OnPlayerRespawn
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_PLAYER_REST, "evn_modrest"); //OnPlayerRest
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_UNEQUIP_ITEM, "evn_modunequip"); //OnPlayerUnEquipItem
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_LOSE_ITEM, "evn_modunacquire"); //OnUnAcquireItem
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_PLAYER_TARGET, "evn_modtarget"); //OnPlayerTarget
    SetEventScript(mod, EVENT_SCRIPT_MODULE_ON_USER_DEFINED_EVENT, "evn_moduserdef"); //OnUserDefined
}

void RegisterMasterAreaAndTriggerEvents()
{
    object area = GetFirstArea();
    while (GetIsObjectValid(area))
    {
        string previousEvent = GetEventScript(area, EVENT_SCRIPT_AREA_ON_ENTER);
        SetEventScript(area, EVENT_SCRIPT_AREA_ON_ENTER, "evn_arenter");
        if (previousEvent != "")
            AddEventScript(SUBEVENT_AREA_ON_ENTER, previousEvent, area);

        previousEvent = GetEventScript(area, EVENT_SCRIPT_AREA_ON_EXIT);
        SetEventScript(area, EVENT_SCRIPT_AREA_ON_EXIT, "evn_arexit");
        if (previousEvent != "")
            AddEventScript(SUBEVENT_AREA_ON_EXIT, previousEvent, area);

        object trigger = GetFirstObjectInArea(area);
        while (GetIsObjectValid(trigger))
        {
            if (GetObjectType(trigger) == OBJECT_TYPE_TRIGGER)
            {
                previousEvent = GetEventScript(trigger, EVENT_SCRIPT_TRIGGER_ON_OBJECT_ENTER);
                SetEventScript(trigger, EVENT_SCRIPT_TRIGGER_ON_OBJECT_ENTER, "evn_trenter");
                if (previousEvent != "")
                    AddEventScript(SUBEVENT_TRIGGER_ON_ENTER, previousEvent, trigger);

                previousEvent = GetEventScript(trigger, EVENT_SCRIPT_TRIGGER_ON_OBJECT_EXIT);
                SetEventScript(trigger, EVENT_SCRIPT_TRIGGER_ON_OBJECT_EXIT, "evn_trexit");
                if (previousEvent != "")
                    AddEventScript(SUBEVENT_TRIGGER_ON_EXIT, previousEvent, trigger);
            }
            trigger = GetNextObjectInArea(area);
        }
        area = GetNextArea();
    }
}

void ClearCreatureEventScriptFromVariable(object oCreature, string sSubevent)
{
    if (sSubevent != SUBEVENT_CREATURE_ON_BLOCKED &&
            sSubevent != SUBEVENT_CREATURE_ON_DAMAGED &&
            sSubevent != SUBEVENT_CREATURE_ON_DEATH &&
            sSubevent != SUBEVENT_CREATURE_ON_CONVERSATION &&
            sSubevent != SUBEVENT_CREATURE_ON_DISTURBED &&
            sSubevent != SUBEVENT_CREATURE_ON_COMBAT_ROUND_END &&
            sSubevent != SUBEVENT_CREATURE_ON_HEARTBEAT &&
            sSubevent != SUBEVENT_CREATURE_ON_PHYSICAL_ATTACKED &&
            sSubevent != SUBEVENT_CREATURE_ON_PERCEPTION &&
            sSubevent != SUBEVENT_CREATURE_ON_RESTED &&
            sSubevent != SUBEVENT_CREATURE_ON_SPAWN &&
            sSubevent != SUBEVENT_CREATURE_ON_SPELL_CAST_AT &&
            sSubevent != SUBEVENT_CREATURE_ON_USER_DEFINED)
        return;

    DeleteLocalString(oCreature, sSubevent);
}