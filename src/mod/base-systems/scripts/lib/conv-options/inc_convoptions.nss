#include "inc_arrays"
#include "inc_tokens"
#include "inc_debug"

// This library contains functions for easy addition of dynamic conversation options (nodes) with paging

const string CONV_INIT_VAR = "CONV_INIT";
const string CONV_OPTIONS = "CONV_OPTIONS";
const string CONV_VALUES = "CONV_VALUES";
const string CONV_PAGE_SIZE_VAR = "CONV_PSIZE";
const string CONV_PAGE_VAR = "CONV_PAGE";
const string CONV_SELECTED_VAR = "CONV_SELECTED";
const string CONV_CHECKED_OPTION_VAR = "CONV_CHECKED_OPTION";
const string CONV_START_TOKEN_VAR = "CONV_START_TOKEN";
const string CONV_PAGES_NUM_VAR = "CONV_PAGES_NUM";

// Initializes conversation options. This function should be called in the action taken script (or the conditional) of the conversation's owner node just before the options are displayed.
// Option nodes in the conversation should be tokens ranging from nStartingToken to nStartingToken+nOptionsPerPage-1.
// sOptionsList - name of the string array (on OBJECT_INVALID) with conversation options to be displayed
// sValuesList - name of a string array (on OBJECT_INVALID) with values behind conversation options, array size should match optionsList
// nOptionsPerPage - number of options displayed per page, 6 by default
void LoadConversationOptions(string sOptionsList, string sValuesList, int nOptionsPerPage=6, int nStartingToken=101);

// Sets the value of an option indicated by nOption to be retrieved with GetSelectedOptionValue() afterward.
// It should be called in the action taken script of every option node with a corresponding nOption
void SetConversationOptionValue(int nOption);

// Returns TRUE if the next page of options is available and FALSE otherwise.
// It should be called in the conditional script of a "Next page" node.
int GetIsNextPageAvailable();

// Returns TRUE if the previous page of options is available and FALSE otherwise.
// It should be called in the conditional script of a "Previous page" node.
int GetIsPreviousPageAvailable();

// Returns TRUE if the first page of options is available and FALSE otherwise.
// The first page is available for selection if the current page is the last one and there are more than two pages.
// It should be called in the conditional script of a "First page" node.
int GetIsFirstPageAvailable();

// Returns TRUE if the last page of options is available and FALSE otherwise.
// The last page is available for selection if the current page is the first one and there are more than two pages.
// It should be called in the conditional script of a "Last page" node.
int GetIsLastPageAvailable();

// Loads the next page of options.
// It should be called in the action taken script of a "Next page" node.
void LoadNextPage();

// Loads the previous page of options.
// It should be called in the action taken script of a "Previous page" node.
void LoadPreviousPage();

// Loads the first page of options.
// It should be called in the action taken script of a "First page" node.
void LoadFirstPage();

// Loads the last page of options.
// It should be called in the action taken script of a "Last page" node.
void LoadLastPage();

// Sets the option's token content and returns TRUE if the option is available and FALSE otherwise.
// It should be called in the conditional script of all option nodes.
int InitializeOption();

// Returns the value of the selected conversation option. It should be called only once, as it clears the dynamic conversation data when called.
// This function should be called in the action taken script (or the conditional) of the conversation's owner node just after the option is selected.
string GetSelectedOptionValue();

// Returns TRUE if LoadConversationOptions() has already been called, but GetSelectedOptionValue() not.
// Recommended to use in the same script as LoadConversationOptions() to prevent unnecessary code execution when switching pages
// and executing the script again.
int GetConversationOptionsInitialized();

// Clears conversation options
void ClearConversationOptions();


void LoadConversationOptions(string sOptionsList, string sValuesList, int nOptionsPerPage=6, int nStartingToken=101)
{
    ClearConversationOptions();

    int initialized = GetLocalInt(OBJECT_SELF, CONV_INIT_VAR);
    if (initialized)
        return;

    int optionsNum = GetStringArraySize(sOptionsList);
    if (optionsNum != GetStringArraySize(sValuesList))
        return;

    SetLocalInt(OBJECT_SELF, CONV_INIT_VAR, TRUE);
    SetLocalInt(OBJECT_SELF, CONV_PAGE_SIZE_VAR, nOptionsPerPage);
    SetLocalInt(OBJECT_SELF, CONV_PAGE_VAR, 0);
    SetLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR, 0);
    SetLocalInt(OBJECT_SELF, CONV_START_TOKEN_VAR, nStartingToken);

    int pagesNum = optionsNum / nOptionsPerPage;
    if (optionsNum % nOptionsPerPage > 0)
        pagesNum++;
    SetLocalInt(OBJECT_SELF, CONV_PAGES_NUM_VAR, pagesNum);

    CopyStringArray(sOptionsList, CONV_OPTIONS, OBJECT_INVALID, OBJECT_SELF);
    CopyStringArray(sValuesList, CONV_VALUES, OBJECT_INVALID, OBJECT_SELF);
    ClearIntArray(sOptionsList);
    ClearIntArray(sValuesList);
}

void SetConversationOptionValue(int nOption)
{
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    int optionsNum = GetStringArraySize(CONV_OPTIONS, OBJECT_SELF);
    int pageSize = GetLocalInt(OBJECT_SELF, CONV_PAGE_SIZE_VAR);

    int optionArrayIndex = pageSize * page + nOption - 1;

    string value = GetStringArrayElement(CONV_VALUES, optionArrayIndex, OBJECT_SELF);
    SetLocalString(OBJECT_SELF, CONV_SELECTED_VAR, value);
}

int GetIsNextPageAvailable()
{
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    int pagesNum = GetLocalInt(OBJECT_SELF, CONV_PAGES_NUM_VAR);

    if (page + 1 < pagesNum)
        return TRUE;
    return FALSE;
}

int GetIsPreviousPageAvailable()
{
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    if (page > 0)
        return TRUE;
    return FALSE;
}

int GetIsFirstPageAvailable()
{
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    int pagesNum = GetLocalInt(OBJECT_SELF, CONV_PAGES_NUM_VAR);

    if (page + 1 == pagesNum && pagesNum > 2)
        return TRUE;
    return FALSE;
}

int GetIsLastPageAvailable()
{
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    int pagesNum = GetLocalInt(OBJECT_SELF, CONV_PAGES_NUM_VAR);

    if (page == 0 && pagesNum > 2)
        return TRUE;
    return FALSE;
}

void LoadNextPage()
{
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    SetLocalInt(OBJECT_SELF, CONV_PAGE_VAR, page+1);
    SetLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR, 0);
}

void LoadFirstPage()
{
    SetLocalInt(OBJECT_SELF, CONV_PAGE_VAR, 0);
    SetLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR, 0);
}

void LoadLastPage()
{
    int lastPage = GetLocalInt(OBJECT_SELF, CONV_PAGES_NUM_VAR) - 1;

    SetLocalInt(OBJECT_SELF, CONV_PAGE_VAR, lastPage);
    SetLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR, 0);
}

void LoadPreviousPage()
{
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    SetLocalInt(OBJECT_SELF, CONV_PAGE_VAR, page-1);
    SetLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR, 0);
}

int InitializeOption()
{
    int option = GetLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR) + 1;
    int page = GetLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    int optionsNum = GetStringArraySize(CONV_OPTIONS, OBJECT_SELF);
    int pageSize = GetLocalInt(OBJECT_SELF, CONV_PAGE_SIZE_VAR);

    //if (option > pageSize)
    //    return FALSE;

    int optionArrayIndex = pageSize * page + option - 1;
    SetLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR, option);

    int token = GetLocalInt(OBJECT_SELF, CONV_START_TOKEN_VAR) - 1 + option;
    string optionString = GetStringArrayElement(CONV_OPTIONS, optionArrayIndex, OBJECT_SELF);
    SetCustomTokenEx(token, optionString);

    if (optionArrayIndex < optionsNum)
        return TRUE;
    return FALSE;
}

string GetSelectedOptionValue()
{
    string value = GetLocalString(OBJECT_SELF, CONV_SELECTED_VAR);

    ClearConversationOptions();

    return value;
}

int GetConversationOptionsInitialized()
{
    return GetLocalInt(OBJECT_SELF, CONV_INIT_VAR);
}

void ClearConversationOptions()
{
    ClearStringArray(CONV_OPTIONS, OBJECT_SELF);
    ClearStringArray(CONV_VALUES, OBJECT_SELF);
    DeleteLocalInt(OBJECT_SELF, CONV_PAGE_SIZE_VAR);
    DeleteLocalInt(OBJECT_SELF, CONV_PAGE_VAR);
    DeleteLocalInt(OBJECT_SELF, CONV_CHECKED_OPTION_VAR);
    DeleteLocalInt(OBJECT_SELF, CONV_INIT_VAR);
    DeleteLocalString(OBJECT_SELF, CONV_SELECTED_VAR);
    DeleteLocalInt(OBJECT_SELF, CONV_START_TOKEN_VAR);
    DeleteLocalInt(OBJECT_SELF, CONV_PAGES_NUM_VAR);
}
