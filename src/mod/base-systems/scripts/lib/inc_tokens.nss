// Custom Token Library
//::////////////////////////////////////////////////////////////////
 
// Sets the custom token identified by the number in iToken to the
// string value specified in sValue. Also duplicates the value as a
// local string stored on the module object so the GetCustomTokenEx
// function can retrieve it at any time.
void SetCustomTokenEx(int nToken, string sValue);
 
// Retrieves the current value of the custom token identified by the
// number in iToken by reading the local string variable containing
// the duplicated token value which was stored on the module object
// by the SetCustomTokenEx function. If the custom token was set using
// the default SetCustomToken function instead of SetCustomTokenEx,
// this function may return an incorrect or blank string.
string GetCustomTokenEx(int nToken);
 
void SetCustomTokenEx(int nToken, string sValue)
{ 
    if (nToken <= 0) 
        return;
 
    // Change the custom token variable and duplicate it.
    // Double set to workaround a bug with conversation nodes not appearing if the displayed options are long and colored tokens
    SetCustomToken(nToken, sValue);
    SetCustomToken(nToken, sValue);
    SetLocalString(GetModule(), "CUSTOM" + IntToString(nToken), sValue);
}
 
string GetCustomTokenEx(int nToken)
{ 
    if (nToken <= 0)
        return "";
 
    // Return the content of the module variable being used to duplicate
    // the token variable.
    return GetLocalString(GetModule(), "CUSTOM" +IntToString(nToken));
}