// Functions that allow you to manipulate spells cast via the Community Patch's spellhook

// Overrides DC for spells cast by oCreature;
// only the latest call of ModifySpellDC or OverrideSpellDC is in effect.
void OverrideSpellDC(object oCreature, int nDC);

// Modifies DC for spells cast by oCreature by the given value;
// only the latest call of ModifySpellDC or OverrideSpellDC is in effect.
void ModifySpellDC(object oCreature, int nDCModifier);

// Clears any override or modification by OverrideSpellDC or ModifySpellDC
void ClearSpellDCOverride(object oCreature);

// Overrides caster level for spells cast by oCreature;
// only the latest call of ModifyCasterLevel or OverrideCasterLevel is in effect.
void OverrideCasterLevel(object oCreature, int nCasterLevel);

// Modifies caster level for spells cast by oCreature by the given value;
// only the latest call of ModifyCasterLevel or OverrideCasterLevel is in effect.
void ModifyCasterLevel(object oCreature, int nCasterLevelModifier);

// Clears any override or modification by OverrideCasterLevel or ModifyCasterLevel
void ClearCasterLevelOverride(object oCreature);

// Overrides metamagic for spells cast by oCreature;
// you can combine multiple metamagic constants with "|";
// only the latest call of AddMetamagic or OverrideMetamagic is in effect.
void OverrideMetamagic(object oCreature, int nMetamagic);

// Adds metamagic to spells cast by oCreature, 
// which will work in addition to other metamagic normally present;
// you can combine multiple metamagic constants with "|";
// only the latest call of AddMetamagic or OverrideMetamagic is in effect.
void AddMetamagic(object oCreature, int nMetamagic);

// Clears any override or modification by OverrideMetamagic or AddMetamagic
void ClearMetamagicOverride(object oCreature);


void OverrideSpellDC(object oCreature, int nDC)
{
    SetLocalInt(oCreature, "SPELLHOOK_DC", nDC);
    DeleteLocalInt(oCreature, "SPELLHOOK_MOD_DC");
}

void ModifySpellDC(object oCreature, int nDCModifier)
{
    SetLocalInt(oCreature, "SPELLHOOK_MOD_DC", nDC);
    DeleteLocalInt(oCreature, "SPELLHOOK_DC");
}

void ClearSpellDCOverride(object oCreature)
{
    DeleteLocalInt(oCreature, "SPELLHOOK_DC");
    DeleteLocalInt(oCreature, "SPELLHOOK_MOD_DC");
}

void OverrideCasterLevel(object oCreature, int nCasterLevel)
{
    SetLocalInt(oCreature, "SPELLHOOK_LVL", nDC);
    DeleteLocalInt(oCreature, "SPELLHOOK_MOD_LVL");
}

void ModifyCasterLevel(object oCreature, int nCasterLevelModifier)
{
    SetLocalInt(oCreature, "SPELLHOOK_MOD_LVL", nDC);
    DeleteLocalInt(oCreature, "SPELLHOOK_LVL");
}

void ClearCasterLevelOverride(object oCreature)
{
    DeleteLocalInt(oCreature, "SPELLHOOK_LVL");
    DeleteLocalInt(oCreature, "SPELLHOOK_MOD_LVL");
}

void OverrideMetamagic(object oCreature, int nMetamagic)
{
    SetLocalInt(oCreature, "SPELLHOOK_META", nDC);
    DeleteLocalInt(oCreature, "SPELLHOOK_MOD_META");
}

void AddMetamagic(object oCreature, int nMetamagic)
{
    SetLocalInt(oCreature, "SPELLHOOK_MOD_META", nDC);
    DeleteLocalInt(oCreature, "SPELLHOOK_META");
}

void ClearMetamagicOverride(object oCreature)
{
    DeleteLocalInt(oCreature, "SPELLHOOK_META");
    DeleteLocalInt(oCreature, "SPELLHOOK_MOD_META");
}