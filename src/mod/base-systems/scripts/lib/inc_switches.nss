const int MODULE_SWITCH_DEBUG_MODE = 1;
const int MODULE_SWITCH_LOGGING = 2;
const int MODULE_SWITCH_PRINT_WARN = 3;
const int MODULE_SWITCH_PRINT_FATAL = 4;

// Sets a module-specific switch to either TRUE or FALSE
void SetSwitch(int nModuleSwitch, int nValue);

// Gets the value of a module-specific switch
int GetSwitch(int nModuleSwitch);

string _GetSwitchName(int nModuleSwitch)
{
    switch (nModuleSwitch)
    {
        case MODULE_SWITCH_DEBUG_MODE: return "SWITCH_DEBUG";
        case MODULE_SWITCH_LOGGING: return "SWITCH_LOGGING";
        case MODULE_SWITCH_PRINT_WARN: return "SWITCH_PRINT_WARN";
        case MODULE_SWITCH_PRINT_FATAL: return "SWITCH_PRINT_FATAL";
    }

    return "";
}

void SetSwitch(int nModuleSwitch, int nValue)
{
    string switchVariable = _GetSwitchName(nModuleSwitch);
    if (switchVariable == "")
        return;
    SetLocalInt(GetModule(), switchVariable, nValue);
}

int GetSwitch(int nModuleSwitch)
{
    string switchVariable = _GetSwitchName(nModuleSwitch);
    if (switchVariable == "")
        return FALSE;
    return GetLocalInt(GetModule(), switchVariable);
}
