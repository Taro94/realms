// Makes oPC enter a targeting mode, letting them select an object as a target 
// If a PC selects a target, it will execute the given script.
void EnterTargetingModeToExecuteScript(object oPC, string sScriptToExecute, int nValidObjectTypes = OBJECT_TYPE_ALL, int nMouseCursorId = MOUSECURSOR_MAGIC, int nBadTargetCursor = MOUSECURSOR_NOMAGIC);

void EnterTargetingModeToExecuteScript(object oPC, string sScriptToExecute, int nValidObjectTypes = OBJECT_TYPE_ALL, int nMouseCursorId = MOUSECURSOR_MAGIC, int nBadTargetCursor = MOUSECURSOR_NOMAGIC)
{
    SetLocalString(oPC, "TARGET_SCRIPT", sScriptToExecute);
    EnterTargetingMode(oPC, nValidObjectTypes, nMouseCursorId, nBadTargetCursor);
}