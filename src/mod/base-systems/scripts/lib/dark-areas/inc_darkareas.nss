#include "inc_common"
#include "inc_debug"

// Library containing a dark/light areas system.
// It works by applying a penalty (-2 attack, -2 AC, -10 spot, -5 search) to PCs and other creatures if an area variable DRK_DARK_AREA is TRUE
// or if it's night time (between 7pm and 5am by default) in an outdoor area and DRK_LIGHT_NIGHT is FALSE.
// There are several ways to avoid the penalty:
// - Darkvision feat
// - Ultravision spell
// - True Seeing effect
// - A source of light in proximity (either a creature with a torch, Light spell effect or a glowing item equipped, or a placeable with a non-zero local int DRK_LIGHT_RADIUS in range determined by the variable)
// - Variable DRK_DARKVISION set to TRUE on a creature
// - Low-light vision feat (only reduces the penalties to -1 attack, -1 AC, -5 spot)

// Verifies if oCreature is under one of the light source creation spells
// and recalculates their light sources if they were subject to one of these spells, but aren't anymore;
// Should be called OnHeartbeat of every creature
void VerifyLightOrContinualFlameSpellEffects(object oCreature);

// Recalculate the penalties related to dark areas for oCreature;
// Should be called in every creature's OnHeartbeat event and in OnEquip and OnUnequip
void RecalculateDarkAreaPenalty(object oCreature);

// This function should be called in OnHeartbeat of NPC creatures ONLY;
// It serves the same purpose of RecalculateEquippedLightSources,
// but because there is no OnEquipped and OnUnequipped event for NPCs,
// we use OnHeartbeat and (to avoid too much code running) perform a check
// to see if any piece of equipment changed before proceeding.
void RecalculateEquippedLightSourcesOnNPCIfNeeded(object oNPC);

// Recalculate light source area of effect emitted by oCreature based on their equipped items and any Light or Continual Flame spells on them;
// Should be called in OnItemEquip and OnItemUnequip (in the latter case, also pass the item being unequipped)
void RecalculateEquippedLightSources(object oCreature, object oUnequippedItem=OBJECT_INVALID);

// Adds a variable to oTarget denoting them as under the effect of the Light or Continual Flame spell
void MarkTargetAsSubjectedToLightOrContinualFlameSpell(object oTarget);

int nDuskHour = 20;
int nDawnHour = 6;

void VerifyLightOrContinualFlameSpellEffects(object oCreature)
{
    //Simplest case and an opportunity to save some code from running
    if (GetLocalInt(oCreature, "DRK_LIGHT_SPELL_PRESENT") == FALSE)
        return;

    //If the creature is not under the Light or Continual Flame spell, remove the var and recalculate light sources
    if (GetHasSpellEffect(SPELL_LIGHT, oCreature) || GetHasSpellEffect(SPELL_CONTINUAL_FLAME, oCreature))
        return;

    DeleteLocalInt(oCreature, "DRK_LIGHT_SPELL_PRESENT");
    SetLocalInt(oCreature, "DRK_LIGHT_RADIUS", 0);
    RecalculateEquippedLightSources(oCreature);
}

int _EquipmentWasChanged(object oNPC)
{
    int inventorySlot;
    int changed = FALSE;
    for (inventorySlot = 0; inventorySlot < 18; inventorySlot++)
    {
        object item = GetItemInSlot(inventorySlot, oNPC);
        string varName = "DRK_ITEM_IN_SLOT_" + IntToString(inventorySlot);
        if (item == GetLocalObject(oNPC, varName))
            continue;

        SetLocalObject(oNPC, varName, item);
        changed = TRUE;
    }
    return changed;
}

void RecalculateEquippedLightSourcesOnNPCIfNeeded(object oNPC)
{
    if (_EquipmentWasChanged(oNPC))
        RecalculateEquippedLightSources(oNPC);
}

effect _CreateDarkAreaPenalty(int nReducedPenalty=FALSE)
{
    effect ef = EffectAttackDecrease(nReducedPenalty ? 1 : 2);
    ef = EffectLinkEffects(ef, EffectACDecrease(nReducedPenalty ? 1 : 2));
    ef = EffectLinkEffects(ef, EffectSkillDecrease(SKILL_SPOT, nReducedPenalty ? 5 : 10));
    if (!nReducedPenalty)
    {
        ef = EffectLinkEffects(ef, EffectSkillDecrease(SKILL_SEARCH, 5));
    }
    ef = SupernaturalEffect(ef);
    ef = TagEffect(ef, "DRK_PENALTY");
    return ef;
}

void RecalculateDarkAreaPenalty(object oCreature)
{
    int subjectedToPenalty = GetLocalInt(oCreature, "DRK_PENALTY_APPLIED");

    object area = GetArea(oCreature);
    int hour = GetTimeHour();
    int areaIsDark = GetLocalInt(area, "DRK_DARK_AREA") ||
                     (!GetLocalInt(area, "DRK_LIGHT_NIGHT")
                      && !GetIsAreaInterior(area)
                      && GetIsAreaAboveGround(area)
                      && (hour >= nDuskHour || hour < nDawnHour));

    //That's the simplest case and an opportunity to save some code from running,
    //which is important, because this function is supposed to run OnHeartbeat of every creature in the game
    if (!areaIsDark && !subjectedToPenalty)
        return;

    //Check for changes to the creature's possession of the low-light vision feat
    int hasLowLightVision = GetHasFeat(FEAT_LOWLIGHTVISION, oCreature);
    int lowVisionFeatChanged = GetLocalInt(oCreature, "DRK_LOWLIGHT_VISION") != hasLowLightVision;
    if (lowVisionFeatChanged)
    {
        SetLocalInt(oCreature, "DRK_LOWLIGHT_VISION", hasLowLightVision);
    }

    //A bunch of other short cases
    int nearLightSource = GetLocalInt(oCreature, "DRK_NEAR_LIGHT");
    int hasDarkvisionFeat = GetHasFeat(FEAT_DARKVISION, oCreature);
    int hasDarkvisionVar = GetLocalInt(oCreature, "DRK_DARKVISION");
    if (!areaIsDark || nearLightSource || hasDarkvisionFeat || hasDarkvisionVar)
    {
        if (subjectedToPenalty)
        {
            RemoveEffectsByTag(oCreature, "DRK_PENALTY");
            DeleteLocalInt(oCreature, "DRK_PENALTY_APPLIED");
        }
        return;
    }

    //At this point we're sure that the area is dark and the creature is not near any light source and has no darkvision feat or variable.
    //We still need to check for an Ultravision or True Seeing effect
    effect ef = GetFirstEffect(oCreature);
    while (GetIsEffectValid(ef))
    {
        int efType = GetEffectType(ef);
        if (efType == EFFECT_TYPE_ULTRAVISION || efType == EFFECT_TYPE_TRUESEEING)
        {
            if (subjectedToPenalty)
            {
                RemoveEffectsByTag(oCreature, "DRK_PENALTY");
                DeleteLocalInt(oCreature, "DRK_PENALTY_APPLIED");
            }
            return;
        }

        ef = GetNextEffect(oCreature);
    }

    //Last possible way to avoid the penalty - check if we're in proximity of a source of light
    location loc = GetLocation(oCreature);
    object objInShape = GetFirstObjectInShape(SHAPE_SPHERE, 20.0, loc, FALSE, OBJECT_TYPE_CREATURE | OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_WAYPOINT);
    while (GetIsObjectValid(objInShape))
    {
        int lightRadius = GetLocalInt(objInShape, "DRK_LIGHT_RADIUS");
        if (lightRadius != 0 && IntToFloat(lightRadius) >= GetDistanceBetween(oCreature, objInShape))
        {
            if (subjectedToPenalty)
            {
                RemoveEffectsByTag(oCreature, "DRK_PENALTY");
                DeleteLocalInt(oCreature, "DRK_PENALTY_APPLIED");
            }
            return;
        }

        objInShape = GetNextObjectInShape(SHAPE_SPHERE, 20.0, loc, FALSE, OBJECT_TYPE_CREATURE | OBJECT_TYPE_PLACEABLE | OBJECT_TYPE_WAYPOINT);
    }

    //We can finally apply the penalty, unless it's already applied
    if (subjectedToPenalty && !lowVisionFeatChanged)
        return;
    else if (subjectedToPenalty)
    {
        RemoveEffectsByTag(oCreature, "DRK_PENALTY");
        DeleteLocalInt(oCreature, "DRK_PENALTY_APPLIED");
    }
    effect penalty = _CreateDarkAreaPenalty(hasLowLightVision);
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, penalty, oCreature);
    SetLocalInt(oCreature, "DRK_PENALTY_APPLIED", TRUE);
}

int _GetItemLightSourceRadius(object oItem)
{
    int bestRadius = 0;
    itemproperty ip = GetFirstItemProperty(oItem);
    while (GetIsItemPropertyValid(ip))
    {
        if (GetItemPropertyType(ip) == ITEM_PROPERTY_LIGHT)
        {
            int costValue = GetItemPropertyCostTableValue(ip);
            int radius = costValue * 5;
            if (radius > bestRadius)
                bestRadius = radius;
        }

        ip = GetNextItemProperty(oItem);
    }
    return bestRadius;
}

int _GetBestEquippedLightSourceRadius(object oCreature, object oUnequippedItem=OBJECT_INVALID)
{
    int inventorySlot;
    int bestRadius = 0;
    for (inventorySlot = 0; inventorySlot < 18; inventorySlot++)
    {
        object item = GetItemInSlot(inventorySlot, oCreature);
        if (item != oUnequippedItem)
        {
            int itemRadius = _GetItemLightSourceRadius(item);
            if (itemRadius > bestRadius)
                bestRadius = itemRadius;
        }
    }
    return bestRadius;
}

void _RecalculateDarkAreaPenaltiesForCreaturesInRadius(object oCenter, int nRadius)
{
    location loc = GetLocation(oCenter);
    object objInShape = GetFirstObjectInShape(SHAPE_SPHERE, IntToFloat(nRadius), loc, FALSE, OBJECT_TYPE_CREATURE);
    while (GetIsObjectValid(objInShape))
    {
        int subjectedToPenalty = GetLocalInt(objInShape, "DRK_PENALTY_APPLIED");
        if (subjectedToPenalty)
        {
            RemoveEffectsByTag(objInShape, "DRK_PENALTY");
            DeleteLocalInt(objInShape, "DRK_PENALTY_APPLIED");
        }

        objInShape = GetNextObjectInShape(SHAPE_SPHERE, IntToFloat(nRadius), loc, FALSE, OBJECT_TYPE_CREATURE);
    }
}

void RecalculateEquippedLightSources(object oCreature, object oUnequippedItem=OBJECT_INVALID)
{
    int previousRadius = GetLocalInt(oCreature, "DRK_LIGHT_RADIUS");
    int radius = GetLocalInt(oCreature, "DRK_LIGHT_SPELL_PRESENT") ? 20 : _GetBestEquippedLightSourceRadius(oCreature, oUnequippedItem);

    if (previousRadius == radius)
        return;

    SetLocalInt(oCreature, "DRK_LIGHT_RADIUS", radius);
    _RecalculateDarkAreaPenaltiesForCreaturesInRadius(oCreature, radius);
}

void MarkTargetAsSubjectedToLightOrContinualFlameSpell(object oTarget)
{
    SetLocalInt(oTarget, "DRK_LIGHT_SPELL_PRESENT", TRUE);
    RecalculateEquippedLightSources(oTarget);
}