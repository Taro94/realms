#include "inc_darkareas"

void main()
{
    object PC = GetPCItemLastEquippedBy();
    RecalculateEquippedLightSources(PC);
    DelayCommand(0.1, RecalculateDarkAreaPenalty(PC));
}