// Set-up areas for the dark areas system: by default any exterior area above ground is marked as dark at night and any underground area is marked as dark.
void main()
{
    object area = GetFirstArea();
    while (GetIsObjectValid(area))
    {
        if (!GetIsAreaInterior(area) && GetIsAreaAboveGround(area))
            SetLocalInt(area, "DRK_DARK_NIGHT", TRUE);
        if (!GetIsAreaAboveGround(area))
            SetLocalInt(area, "DRK_DARK_AREA", TRUE);
        area = GetNextArea();
    }
}