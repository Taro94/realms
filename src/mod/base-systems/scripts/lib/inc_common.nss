#include "inc_debug"

// Various functions and constants with no better place for themselves

const int ACTION_MODE_DEFENSIVE_STANCE = 12;

const int INT_MAX = 2147483647;
const int INT_MIN = -2147483648;

// Send message to all PCs
void SendMessageToAll(string sMessage);

// Sends a floating text string message to all PCs
void FloatingTextStringOnAllPCs(string sMessage);

// Get the number of PCs in the game or in a specific area if oArea is valid
int GetNumberOfPlayers(object oArea=OBJECT_INVALID);

// Presents a popup message to the specified PC
void DisplayPopupMessage(object oPC, string sMessage, string sTitle="");

// Returns TRUE if the game is hosted as a singleplayer game and FALSE otherwise
int GetIsSinglePlayer();

// Returns the pseudo-limbo location (waypoint in an inaccessible area)
location Limbo();

// Set the spellhook script (script to be executed on every spell cast)
void SetSpellhookScript(string sScriptName);

// Get the spellhook script
string GetSpellhookScript();

// Returns a creature's effect by tag
effect GetEffectByTag(object oCreature, string sTag);

// Removes all creature's effects by tag
void RemoveEffectsByTag(object oCreature, string sTag);

// Copies a local int variable named sVarName from oObjectToCopyFrom to oObjectToCopyTo
void CopyLocalInt(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName);

// Copies a local string variable named sVarName from oObjectToCopyFrom to oObjectToCopyTo
void CopyLocalString(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName);

// Copies a local float variable named sVarName from oObjectToCopyFrom to oObjectToCopyTo
void CopyLocalFloat(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName);

// Copies a local object reference variable named sVarName from oObjectToCopyFrom to oObjectToCopyTo
void CopyLocalObject(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName);

// Adds a value to a local integer
void AddToLocalInt(object oObject, string sVarName, int nValueToAdd);

// Subtracts a value from a local integer
void SubtractFromLocalInt(object oObject, string sVarName, int nValueToSubtract);

// Multiplies a local integer by the given multiplier
void MultiplyLocalInt(object oObject, string sVarName, int nMultiplier);

// Divides a local integer by the given divisor
void DivideLocalInt(object oObject, string sVarName, int nDivisor);

// Converts a encoded 0x00000000 hex number to an integer for bitwise operations
// Very useful if used on a 2da field
// * sString - String to convert
int HexStringToInt(string sString);

// Returns TRUE if the string is an integer and false otherwise
int GetIsStringInt(string sString);

// Returns rotated location;
// useful when spawning placeables, because they often have the front rotated by 180 degrees compared to waypoints.
location GetRotatedLocation(location lLoc, float fRotation=180.0);

// Adjust alignment of every PC in the game
void AdjustAligmentOfAllPCs(int nAlignment, int nShift);

// Get final PC master of a creature or OBJECT_INVALID if oCreature has no final PC master;
// oCreature will be returned if oCreature is a PC.
object GetPCMaster(object oCreature);

// Makes the caller face oTarget
void SetFacingObject(object oTarget);

// Makes the caller gracefully stop their current animation (like sitting, laying on the ground, etc.)
void ActionStopAnimation();

// Makes the creature determine combat round (wrapper for function from hench_i0_ai for more convenient use)
void StartCombat();

void ActionStopAnimation()
{
    ActionPlayAnimation(ANIMATION_LOOPING_PAUSE);
}


void SendMessageToAll(string sMessage)
{
    object pc = GetFirstPC();
    while (GetIsObjectValid(pc))
    {
        SendMessageToPC(pc, sMessage);
        pc = GetNextPC();
    }
}

void FloatingTextStringOnAllPCs(string sMessage)
{
    object pc = GetFirstPC();
    while (GetIsObjectValid(pc))
    {
        FloatingTextStringOnCreature(sMessage, pc, FALSE);
        pc = GetNextPC();
    }
}

int GetNumberOfPlayers(object oArea=OBJECT_INVALID)
{
    int num = 0;
    object pc = GetFirstPC();
    while (GetIsObjectValid(pc))
    {
        if (oArea == OBJECT_INVALID || GetArea(pc) == oArea)
            num++;
        pc = GetNextPC();
    }
    return num;
}

void DisplayPopupMessage(object oPC, string sMessage, string sTitle="")
{
    object popupObject = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_invisible", GetLocation(oPC));
    if (sTitle == "")
        sTitle = " ";
    SetName(popupObject, sTitle);
    SetDescription(popupObject, sMessage);
    SetPortraitResRef(popupObject, "po_plc_x0_tme_");

    DelayCommand(0.1, AssignCommand(oPC, ClearAllActions()));
    DelayCommand(0.11, AssignCommand(oPC, ActionExamine(popupObject)));
    DestroyObject(popupObject, 0.5f);
}

int GetIsSinglePlayer()
{
    if (GetNumberOfPlayers() == 0)
        return FALSE;
    string key = GetPCPublicCDKey(GetFirstPC());
    if (key == "")
        return TRUE;
    return FALSE;
}

location Limbo()
{
    return GetLocation(GetWaypointByTag("WP_LIMBO"));
}

void SetSpellhookScript(string sScriptName)
{
    SetLocalString(GetModule(), "X2_S_UD_SPELLSCRIPT", sScriptName);
}

string GetSpellhookScript()
{
    return GetLocalString(GetModule(), "X2_S_UD_SPELLSCRIPT");
}

effect GetEffectByTag(object oCreature, string sTag)
{
    effect invalid;
    effect ef = GetFirstEffect(oCreature);
    while (GetIsEffectValid(ef))
    {
        if (GetEffectTag(ef) == sTag)
            return ef;
        ef = GetNextEffect(oCreature);
    }
    return invalid;
}

void RemoveEffectsByTag(object oCreature, string sTag)
{
    effect ef = GetFirstEffect(oCreature);
    while (GetIsEffectValid(ef))
    {
        if (GetEffectTag(ef) == sTag)
            RemoveEffect(oCreature, ef);
        ef = GetNextEffect(oCreature);
    }
    return;
}

void CopyLocalInt(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName)
{
    SetLocalInt(oObjectToCopyTo, sVarName, GetLocalInt(oObjectToCopyFrom, sVarName));
}

void CopyLocalString(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName)
{
    SetLocalString(oObjectToCopyTo, sVarName, GetLocalString(oObjectToCopyFrom, sVarName));
}

void CopyLocalFloat(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName)
{
    SetLocalFloat(oObjectToCopyTo, sVarName, GetLocalFloat(oObjectToCopyFrom, sVarName));
}

void CopyLocalObject(object oObjectToCopyFrom, object oObjectToCopyTo, string sVarName)
{
    SetLocalObject(oObjectToCopyTo, sVarName, GetLocalObject(oObjectToCopyFrom, sVarName));
}

void AddToLocalInt(object oObject, string sVarName, int nValueToAdd)
{
    SetLocalInt(oObject, sVarName, GetLocalInt(oObject, sVarName)+nValueToAdd);
}

void SubtractFromLocalInt(object oObject, string sVarName, int nValueToSubtract)
{
    SetLocalInt(oObject, sVarName, GetLocalInt(oObject, sVarName)-nValueToSubtract);
}

void MultiplyLocalInt(object oObject, string sVarName, int nMultiplier)
{
    SetLocalInt(oObject, sVarName, GetLocalInt(oObject, sVarName)*nMultiplier);
}

void DivideLocalInt(object oObject, string sVarName, int nDivisor)
{
    SetLocalInt(oObject, sVarName, GetLocalInt(oObject, sVarName)/nDivisor);
}

int HexStringToInt(string sString)
{
    sString = GetStringLowerCase(sString);
    int nResult = 0;
    int nLength = GetStringLength(sString);
    int i;
    for (i = nLength - 1; i >= 0; i--) {
        int n = FindSubString("0123456789abcdef", GetSubString(sString, i, 1));
        if (n == -1)
            return nResult;
        nResult |= n << ((nLength - i -1) * 4);
    }
    return nResult;
}

int GetIsStringInt(string sString)
{
    if (IntToString(StringToInt(sString)) == sString)
        return TRUE;
    return FALSE;
}

location GetRotatedLocation(location lLoc, float fRotation=180.0)
{
    object area = GetAreaFromLocation(lLoc);
    vector wpVector = GetPositionFromLocation(lLoc);
    float wpOrientation = GetFacingFromLocation(lLoc);
    location rotatedLocation = Location(area, wpVector, wpOrientation+fRotation);
    return rotatedLocation;
}

void AdjustAligmentOfAllPCs(int nAlignment, int nShift)
{
    object PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        AdjustAlignment(PC, nAlignment, nShift);
        PC = GetNextPC();
    }
}

object GetPCMaster(object oCreature)
{
    object creature = oCreature;
    while (GetIsObjectValid(creature))
    {
        if (GetIsPC(creature))
            return creature;
        creature = GetMaster(creature);
    }
    return OBJECT_INVALID;
}

void SetFacingObject(object oTarget)
{
    vector vFace = GetPosition(oTarget);
    SetFacingPoint(vFace);
}

void StartCombat()
{
    ExecuteScriptChunk("#include \"hench_i0_ai\" \n void main() { HenchDetermineCombatRound(); }", OBJECT_SELF, FALSE);
}