#include "inc_itemspells"

// Library with functions to help manage epic spells

// Converts an epic spell constant to the corresponding epic spell feat constant,
// returns -1 if nEpicSpell is not an epic spell constant
int EpicSpellToFeat(int nEpicSpell);

// A wrapper for GetHasSpell that also properly accounts for epic spells
int GetHasSpellIncludingEpic(int nSpell, int nIncludeItems=TRUE);

// A wrapper for ActionCastSpellAtObject that also properly handles epic spells
void ActionCastSpellAtObjectIncludingEpic(int nSpell, object oTarget, int nMetaMagic=METAMAGIC_ANY, 
    int bCheat=FALSE, int nDomainLevel=0, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE, int nIncludeItems=TRUE);

// A wrapper for ActionCastSpellAtLocation that also properly handles epic spells
void ActionCastSpellAtLocationIncludingEpic(int nSpell, location lTargetLocation, int nMetaMagic=METAMAGIC_ANY, 
    int bCheat=FALSE, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE, int nIncludeItems=TRUE);

int EpicSpellToFeat(int nEpicSpell)
{
    switch (nEpicSpell)
    {
        case SPELL_EPIC_MUMMY_DUST:
            return FEAT_EPIC_SPELL_MUMMY_DUST;
        case SPELL_EPIC_HELLBALL:
            return FEAT_EPIC_SPELL_HELLBALL;
        case SPELL_EPIC_MAGE_ARMOR:
            return FEAT_EPIC_SPELL_MAGE_ARMOUR;
        case SPELL_EPIC_RUIN:
            return FEAT_EPIC_SPELL_RUIN;
        case SPELL_EPIC_DRAGON_KNIGHT:
            return FEAT_EPIC_SPELL_DRAGON_KNIGHT;
        case 695:
            return FEAT_EPIC_SPELL_EPIC_WARDING;
    }
    return -1;
}

int GetHasSpellIncludingEpic(int nSpell, int nIncludeItems=TRUE)
{
    int epicFeatConst = EpicSpellToFeat(nSpell);
    if (epicFeatConst != -1)
        return GetHasFeat(epicFeatConst);
    return nIncludeItems ? GetHasSpellIncludingItems(nSpell) : GetHasSpell(nSpell);
}

void ActionCastSpellAtObjectIncludingEpic(int nSpell, object oTarget, int nMetaMagic=METAMAGIC_ANY, 
    int bCheat=FALSE, int nDomainLevel=0, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE, int nIncludeItems=TRUE)
{
    int epicSpellFeat = EpicSpellToFeat(nSpell);
    if (epicSpellFeat == -1)
    {
        if (nIncludeItems)
            ActionCastSpellAtObjectIncludingItems(nSpell, oTarget, nMetaMagic, bCheat, nDomainLevel, nProjectilePathType, bInstantSpell);
        else
            ActionCastSpellAtObject(nSpell, oTarget, nMetaMagic, bCheat, nDomainLevel, nProjectilePathType, bInstantSpell);
        return;
    }

    if (!GetHasFeat(epicSpellFeat) && !bCheat)
        return;
    
    if (!bCheat)
        DecrementRemainingFeatUses(OBJECT_SELF, epicSpellFeat);

    ActionCastSpellAtObject(nSpell, oTarget, nMetaMagic, TRUE, nDomainLevel, nProjectilePathType, bInstantSpell);
}

void ActionCastSpellAtLocationIncludingEpic(int nSpell, location lTargetLocation, int nMetaMagic=METAMAGIC_ANY, 
    int bCheat=FALSE, int nProjectilePathType=PROJECTILE_PATH_TYPE_DEFAULT, int bInstantSpell=FALSE, int nIncludeItems=TRUE)
{
    int epicSpellFeat = EpicSpellToFeat(nSpell);
    if (epicSpellFeat == -1)
    {
        if (nIncludeItems)
            ActionCastSpellAtLocationIncludingItems(nSpell, lTargetLocation, nMetaMagic, bCheat, nProjectilePathType, bInstantSpell);
        else
            ActionCastSpellAtLocation(nSpell, lTargetLocation, nMetaMagic, bCheat, nProjectilePathType, bInstantSpell);
        return;
    }

    if (!GetHasFeat(epicSpellFeat) && !bCheat)
        return;
    
    if (!bCheat)
        DecrementRemainingFeatUses(OBJECT_SELF, epicSpellFeat);
    ActionCastSpellAtLocation(nSpell, lTargetLocation, nMetaMagic, TRUE, nProjectilePathType, bInstantSpell);
}