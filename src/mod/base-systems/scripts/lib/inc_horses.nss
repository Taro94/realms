#include "x3_inc_horse"
#include "inc_common"
#include "inc_language"
#include "inc_debug"

// Some extra helper functions to use on top of BioWare's horse system

// Saves important horse variables on its rider so that they are accessible after mount (when horse object disappears);
// Should be called in the script executed pre-mount, which should be exe_premount (see x3_inc_horse for details on pre-mounting hook)
void SaveHorseVariablesOnRider(object oRider, object oHorse);

// Checks if the horse has an owner and if it's an NPC that has a master, gives the horse
// to the master. This is to avoid the annoyance of not being able to give orders to your henchmen's horses.
void VerifyOwnership(object oHorse);

// This function should be called in every area's OnEnter (inc_scriptevents may be helpful here).
// It re-adds all "masterless" (but not ownerless!) horses in the area as henchmen to their masters
// (or the PC masters of their masters, see the VerifyOwnership function for details)
void ReaddHorsesInTheAreaToParty(object oArea);

// Recalculates and re-applies the horse riding ranged penalty.
// Should be called OnEquip and OnUnequip
void RecalculateHorseRangedPenalty(object oCreature, object oItemBeingUnequipped=OBJECT_INVALID);

// Recalculates and re-applies all bonuses and penalties related to horse riding:
// - Mounted Combat AC bonus
// - penalties to skills
// - penalty to ranged attack
// Speed is not included, because the built-in effect is good enough.
// Ensure this function is called on mount, dismount and preferably OnHeartbeat (especially for NPCs)
void RecalculateRidingEffects(object oCreature);

// Enforces recalculation of riding effects after the creature becomes mounted;
// Retries every 0.25 second until success or 200 failed attempts
void EnforceRidingEffectsRecalculationOnMount(object oCreature);

// Enforces recalculation of riding effects after the creature becomes dismounted;
// Retries every 0.25 second until success or 200 failed attempts
void EnforceRidingEffectsRecalculationOnDismount(object oCreature);

// Reapplies riding effects after a creature is raised. Should be called in 70_mod_resurrect (basically after every raise dead / resurrection that may happen in the module).
void ReapplyHorseRidingEffectsOnResurrection(object oCreature);

// A wrapper for JumpToObject that will dismount a rider if they are jumped to an area that disallows riding.
void JumpToObjectRespectingHorses(object oToJumpTo, int nWalkStraightLineToPoint=1);



void SaveHorseVariablesOnRider(object oRider, object oHorse)
{
    //Get horse AC boost
    int extraAC = GetLocalInt(oHorse, "ExtraAC");
    SetLocalInt(oRider, "Horse_ExtraAC", extraAC);
}

void VerifyOwnership(object oHorse)
{
    object master = GetMaster(oHorse);

    if (GetIsPC(master))
        return;

    object masterOfMaster = GetMaster(master);
    if (GetIsPC(masterOfMaster))
    {
        RemoveHenchman(master, oHorse);
        AddHenchman(masterOfMaster, oHorse);
    }
}

void _EnforceHorseInstantMount(object oCreature, object oHorse)
{
    string counter = "HORSE_MOUNTING_ATTEMPTS";
    if (HorseGetIsMounted(oCreature))
    {
        DeleteLocalInt(oCreature, counter);
    }
    else
    {
        int counterValue = GetLocalInt(oCreature, counter);
        if (counterValue > 200)
        {
            DeleteLocalInt(oCreature, counter);
            LogInfo("Over 200 mounting attempts for " + GetName(oCreature) + " (inc_horses, _EnforceHorseInstantMount)");
            return;
        }
        SetLocalInt(oCreature, counter, counterValue+1);
        AssignCommand(oCreature, ClearAllActions());
        AssignCommand(oCreature, HorseMount(oHorse, FALSE, TRUE));
        DelayCommand(0.25, _EnforceHorseInstantMount(oCreature, oHorse));
    }
}

void SetHorseName(object oHorse, object oOwner, int nSetOriginalName=FALSE)
{
    string sName = GetLocalString(oHorse, "sX3_OriginalName");
    if (sName == "")
    {
        sName = GetName(oHorse);
        SetLocalString(oHorse, "sX3_OriginalName", sName);
    }

    if (GetServerLanguage() == LANGUAGE_POLISH)
    {
        string plName = GetLocalString(oHorse, "NamePl");
        if (plName == "")
            plName = sName;
        if (nSetOriginalName)
            SetName(oHorse, plName);
        else
            SetName(oHorse, plName + " ("+GetName(oOwner)+")");
        return;
    }

    if (nSetOriginalName)
        SetName(oHorse);
    else if (GetStringLowerCase(GetStringRight(GetName(oOwner), 1)) == "s" || GetStringLowerCase(GetStringRight(GetName(oOwner), 1)) == "z")
        SetName(oHorse, GetName(oOwner)+"' "+sName);
    else {
        SetName(oHorse, GetName(oOwner)+"'s "+sName);
    }
}

void ReaddHorsesInTheAreaToParty(object oArea)
{
    object horse = GetFirstObjectInArea(oArea);
    while (GetIsObjectValid(horse))
    {
        object owner = HorseGetOwner(horse);
        int wasAutoDismounted = GetLocalInt(horse, "oX3_AutoDismountedOnAreaTransition");
        int wasAutoRemoved = GetLocalInt(horse, "oX3_AutoRemovedFromParty");
        if (GetIsObjectValid(owner) && (wasAutoDismounted || wasAutoRemoved))
        {
            DeleteLocalInt(horse, "oX3_AutoDismountedOnAreaTransition");
            DeleteLocalInt(horse, "oX3_AutoRemovedFromParty");
            if (wasAutoRemoved)
            {
                AddHenchman(owner, horse);
                SetHorseName(horse, owner);
                VerifyOwnership(horse);
            }
            else if (wasAutoDismounted)
            {
                LogInfo("Starting enforcing");
                _EnforceHorseInstantMount(owner, horse);
            }
        }
        horse = GetNextObjectInArea(oArea);
    }
}

void RecalculateHorseRangedPenalty(object oCreature, object oItemBeingUnequipped=OBJECT_INVALID)
{
    string tag = "RidingEffect_RangedPenalty";
    string penaltyVarName = "LAST_CHECK_RANGED_PENALTY";

    int isMounted = HorseGetIsMounted(oCreature);
    int lastPenalty = GetLocalInt(oCreature, penaltyVarName);

    //Apply ranged penalty
    object mainWeapon = GetItemInSlot(INVENTORY_SLOT_RIGHTHAND, oCreature);
    if (isMounted && GetWeaponRanged(mainWeapon) && mainWeapon != oItemBeingUnequipped)
    {
        int penalty = GetHasFeat(FEAT_MOUNTED_ARCHERY, oCreature) ? 2 : 4;
        if (penalty == lastPenalty)
            return;

        RemoveEffectsByTag(oCreature, tag);
        effect rangedPenalty = EffectAttackDecrease(penalty);
        rangedPenalty = ExtraordinaryEffect(rangedPenalty);
        rangedPenalty = TagEffect(rangedPenalty, tag);
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, rangedPenalty, oCreature);
        SetLocalInt(oCreature, penaltyVarName, penalty);
    }
    else if (lastPenalty != 0)
    {
        RemoveEffectsByTag(oCreature, tag);
        SetLocalInt(oCreature, penaltyVarName, 0);
    }
}

void RecalculateRidingEffects(object oCreature)
{
    int isMounted = HorseGetIsMounted(oCreature);
    int wasMounted = GetLocalInt(oCreature, "LAST_CHECK_MOUNTED");

    if (!isMounted && !wasMounted)
        return;

    //Remove old effects
    effect ef = GetFirstEffect(oCreature);
    while (GetIsEffectValid(ef))
    {
        if (GetEffectTag(ef) == "RidingEffect")
            RemoveEffect(oCreature, ef);
        ef = GetNextEffect(oCreature);
    }

    RecalculateHorseRangedPenalty(oCreature);

    if (!isMounted)
    {
        DeleteLocalInt(oCreature, "Horse_ExtraAC");
        SetLocalInt(oCreature, "LAST_CHECK_MOUNTED", FALSE);
        return;
    }

    SetLocalInt(oCreature, "LAST_CHECK_MOUNTED", TRUE);

    //Apply extra AC from horse
    int extraAC = GetLocalInt(oCreature, "Horse_ExtraAC");
    if (extraAC != 0)
    {
        effect acBuff = EffectACIncrease(extraAC);
        acBuff = ExtraordinaryEffect(acBuff);
        acBuff = TagEffect(acBuff, "RidingEffect");
        ApplyEffectToObject(DURATION_TYPE_PERMANENT, acBuff, oCreature);
    }

    //Apply Mounted Combat feat effects
    int nRoll;
    if (GetHasFeat(FEAT_MOUNTED_COMBAT, oCreature))
    {
        nRoll = d20() + GetSkillRank(SKILL_RIDE, oCreature);
        nRoll = nRoll - 10;
        if (nRoll > 4)
        {
            nRoll=nRoll / 5;
            effect mcBuff = EffectACIncrease(nRoll);
            mcBuff = ExtraordinaryEffect(mcBuff);
            mcBuff = TagEffect(mcBuff, "RidingEffect");
            ApplyEffectToObject(DURATION_TYPE_PERMANENT, mcBuff, oCreature);
        }
    }
}

void EnforceRidingEffectsRecalculationOnMount(object oCreature)
{
    string counter = "HORSE_EFFECTS_RECALCULATION_ON_MOUNT_ATTEMPTS";
    if (HorseGetIsMounted(oCreature))
    {
        RecalculateRidingEffects(oCreature);
        DeleteLocalInt(oCreature, counter);
    }
    else
    {
        int counterValue = GetLocalInt(oCreature, counter);
        if (counterValue > 200)
        {
            DeleteLocalInt(oCreature, counter);
            LogInfo("Over 200 riding effects recalculation attempts for " + GetName(oCreature) + " (x3_s3_horse, EnforceRidingEffectsRecalculationOnMount)");
            return;
        }
        SetLocalInt(oCreature, counter, counterValue+1);
        DelayCommand(0.25, EnforceRidingEffectsRecalculationOnMount(oCreature));
    }
}

void EnforceRidingEffectsRecalculationOnDismount(object oCreature)
{
    string counter = "HORSE_EFFECTS_RECALCULATION_ON_DISMOUNT_ATTEMPTS";
    if (!HorseGetIsMounted(oCreature))
    {
        RecalculateRidingEffects(oCreature);
        object horse = HorseGetMyHorse(oCreature);
        SetHorseName(horse, oCreature);
        DeleteLocalInt(oCreature, counter);
    }
    else
    {
        int counterValue = GetLocalInt(oCreature, counter);
        if (counterValue > 200)
        {
            DeleteLocalInt(oCreature, counter);
            LogInfo("Over 200 riding effects recalculation attempts for " + GetName(oCreature) + " (inc_horses, EnforceRidingEffectsRecalculationOnDismount)");
            return;
        }
        SetLocalInt(oCreature, counter, counterValue+1);
        DelayCommand(0.25, EnforceRidingEffectsRecalculationOnDismount(oCreature));
    }
}

void _EnforceHorseMarkedAsAutoRemovedUponDismount(object oCreature)
{
    string counter = "HORSE_MARKING_AS_AUTO_REMOVED_ATTEMPTS";
    if (!HorseGetIsMounted(oCreature))
    {
        object horse = HorseGetMyHorse(oCreature);
        SetLocalInt(horse, "oX3_AutoDismountedOnAreaTransition", TRUE);
        DeleteLocalInt(oCreature, counter);
    }
    else
    {
        int counterValue = GetLocalInt(oCreature, counter);
        if (counterValue > 200)
        {
            DeleteLocalInt(oCreature, counter);
            LogInfo("Over 200 marking horse as auto-removed attempts for " + GetName(oCreature) + " (inc_horses, _EnforceHorseMarkedAsAutoRemovedUponDismount)");
            return;
        }
        SetLocalInt(oCreature, counter, counterValue+1);
        DelayCommand(0.25, _EnforceHorseMarkedAsAutoRemovedUponDismount(oCreature));
    }
}

void ReapplyHorseRidingEffectsOnResurrection(object oCreature)
{
    if (HorseGetIsMounted(oCreature))
    {
        SetLocalInt(oCreature, "LAST_CHECK_RANGED_PENALTY", 0);
        SetLocalInt(oCreature, "LAST_CHECK_MOUNTED", FALSE);
        RecalculateRidingEffects(oCreature);
        HORSE_SupportApplyMountedSkillDecreases(oCreature);
        HORSE_SupportIncreaseSpeed(oCreature, OBJECT_INVALID);
    }
}

void JumpToObjectRespectingHorses(object oToJumpTo, int nWalkStraightLineToPoint=1)
{
    object oClicker=OBJECT_SELF;
    object oTarget=oToJumpTo;
    location lPreJump=HORSE_SupportGetMountLocation(oClicker,oClicker,0.0); // location before jump
    int bAnim=GetLocalInt(OBJECT_SELF,"bDismountFast"); // community requested fast dismount for transitions if variable is not set (use X3_G0_Transition for animated)
    int nN=1;
    object oOb;
    object oAreaHere=GetArea(oClicker);
    object oAreaTarget=GetArea(oTarget);
    object oHitch;
    int bDelayedJump=FALSE;
    int bNoMounts=FALSE;
    float fX3_MOUNT_MULTIPLE=GetLocalFloat(GetArea(oClicker),"fX3_MOUNT_MULTIPLE");
    float fX3_DISMOUNT_MULTIPLE=GetLocalFloat(GetArea(oClicker),"fX3_DISMOUNT_MULTIPLE");
    if (GetLocalFloat(oClicker,"fX3_MOUNT_MULTIPLE")>fX3_MOUNT_MULTIPLE) fX3_MOUNT_MULTIPLE=GetLocalFloat(oClicker,"fX3_MOUNT_MULTIPLE");
    if (fX3_MOUNT_MULTIPLE<=0.0) fX3_MOUNT_MULTIPLE=1.0;
    if (GetLocalFloat(oClicker,"fX3_DISMOUNT_MULTIPLE")>0.0) fX3_DISMOUNT_MULTIPLE=GetLocalFloat(oClicker,"fX3_DISMOUNT_MULTIPLE");
    if (fX3_DISMOUNT_MULTIPLE>0.0) fX3_MOUNT_MULTIPLE=fX3_DISMOUNT_MULTIPLE; // use dismount multiple instead of mount multiple
    float fDelay=0.1*fX3_MOUNT_MULTIPLE;
    if (!GetLocalInt(oAreaTarget,"X3_MOUNT_OK_EXCEPTION"))
    {   // check for global restrictions
        if (GetLocalInt(GetModule(),"X3_MOUNTS_EXTERNAL_ONLY")&&GetIsAreaInterior(oAreaTarget)) bNoMounts=TRUE;
        else if (GetLocalInt(GetModule(),"X3_MOUNTS_NO_UNDERGROUND")&&!GetIsAreaAboveGround(oAreaTarget)) bNoMounts=TRUE;
    } // check for global restrictions
    if (GetLocalInt(oAreaTarget,"X3_NO_MOUNTING")||GetLocalInt(oAreaTarget,"X3_NO_HORSES")||bNoMounts)
    {   // make sure all transitioning are not mounted
        object horse = HorseGetMyHorse(oClicker);
        if (GetIsObjectValid(horse))
            SetLocalInt(horse, "oX3_AutoRemovedFromParty", TRUE);
        if (HorseGetIsMounted(oClicker))
        {   // dismount clicker
            bDelayedJump=TRUE;
            AssignCommand(oClicker,HORSE_SupportDismountWrapper(bAnim,TRUE));
            EnforceRidingEffectsRecalculationOnDismount(oClicker);
            _EnforceHorseMarkedAsAutoRemovedUponDismount(oClicker);
            fDelay=fDelay+0.2*fX3_MOUNT_MULTIPLE;
        } // dismount clicker
        oOb=GetAssociate(ASSOCIATE_TYPE_HENCHMAN,oClicker,nN);
        while(GetIsObjectValid(oOb))
        {
            horse = HorseGetMyHorse(oOb);
            if (GetIsObjectValid(horse))
                SetLocalInt(horse, "oX3_AutoRemovedFromParty", TRUE);
            // check each associate to see if mounted
            if (HorseGetIsMounted(oOb))
            {   // dismount associate
                bDelayedJump=TRUE;
                DelayCommand(fDelay,AssignCommand(oOb,HORSE_SupportDismountWrapper(bAnim,TRUE)));
                EnforceRidingEffectsRecalculationOnDismount(oOb);
                _EnforceHorseMarkedAsAutoRemovedUponDismount(oOb);
                fDelay=fDelay+0.2*fX3_MOUNT_MULTIPLE;
            } // dismount associate
            nN++;
            oOb=GetAssociate(ASSOCIATE_TYPE_HENCHMAN,oClicker,nN);
        } // check each associate to see if mounted
        if (fDelay>0.1) SendMessageToPCByStrRef(oClicker,111989);
        if (bDelayedJump)
        {   // some of the party has/have been mounted, so delay the time to hitch
            fDelay=fDelay+2.0*fX3_MOUNT_MULTIPLE; // non-animated dismount lasts 1.0+1.0=2.0 by default, so wait at least that!
            if (bAnim) fDelay=fDelay+2.8*fX3_MOUNT_MULTIPLE; // animated dismount lasts (X3_ACTION_DELAY+HORSE_DISMOUNT_DURATION+1.0)*fX3_MOUNT_MULTIPLE=4.8 by default, so wait at least that!
        } // some of the party has/have been mounted, so delay the time to hitch
    } // make sure all transitioning are not mounted
    if (GetLocalInt(oAreaTarget,"X3_NO_HORSES")||bNoMounts)
    {   // make sure no horses/mounts follow the clicker to this area
        bDelayedJump=TRUE;
        oHitch=GetNearestObjectByTag("X3_HITCHING_POST",oClicker);
        DelayCommand(fDelay,HorseHitchHorses(oHitch,oClicker,lPreJump));
        if (bAnim) fDelay=fDelay+1.8*fX3_MOUNT_MULTIPLE;
    } // make sure no horses/mounts follow the clicker to this area

    if (bDelayedJump)
    {   // delayed jump
        ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectCutsceneImmobilize(), oClicker, fDelay+0.5);
        DelayCommand(fDelay,AssignCommand(oClicker,ClearAllActions()));
        DelayCommand(fDelay+0.1*fX3_MOUNT_MULTIPLE,AssignCommand(oClicker,JumpToObject(oTarget)));
    } // delayed jump
    else
    {   // quick jump
        ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectCutsceneImmobilize(), oClicker, 1.0);
        AssignCommand(oClicker, ClearAllActions());
        AssignCommand(oClicker, JumpToObject(oTarget));
    } // quick jump
    DelayCommand(fDelay+4.0*fX3_MOUNT_MULTIPLE,HorseMoveAssociates(oClicker));
}

void HorseSetOwnerWithoutAddingToParty(object oHorse,object oOwner,int bAssign=FALSE)
{   // PURPOSE: Set oHorse to be owned by oOwner
    object oPreviousOwner;
    string sName;
    int nHenchmen=HORSE_SupportCountHenchmen(oOwner);
    int nMax=GetLocalInt(GetModule(),"X3_HORSE_MAX_HENCHMEN");
    int bIncHenchmen=GetLocalInt(GetModule(),"X3_HORSE_NO_HENCHMAN_INCREASE");
    int nCurr=GetMaxHenchmen();
    if (GetObjectType(oHorse)==OBJECT_TYPE_CREATURE&&GetObjectType(oOwner)==OBJECT_TYPE_CREATURE)
    {   // valid parameters
        //oPreviousOwner=GetMaster(oHorse);
        //if (oPreviousOwner==oOwner) return; // already is the owner
        if ((HorseGetCanBeMounted(oHorse,oOwner,TRUE)||GetIsPC(oOwner))&&!HorseGetIsAMount(oOwner))
        {   // horse can be mounted
            oPreviousOwner=GetMaster(oHorse);
            if (oPreviousOwner!=oOwner)
            {   // new owner
                if (GetObjectType(oPreviousOwner)==OBJECT_TYPE_CREATURE)
                {   // remove as henchman from previous owner
                    RemoveHenchman(oPreviousOwner,oHorse);
                    if (GetLocalInt(GetModule(),"X3_ENABLE_MOUNT_DB")&&GetIsPC(oPreviousOwner)) SetLocalInt(oPreviousOwner,"bX3_STORE_MOUNT_INFO",TRUE);
                } // remove as henchman from previous owner
                if (nHenchmen==nCurr)
                {   // see if increase possible
                    if (!bIncHenchmen)
                    {   // increase is permissable
                        if (nMax==0||nMax>0)
                        {   // do the increase
                            SetMaxHenchmen(nCurr+1);
                        } // do the increase
                    } // increase is permissable
                } // see if increase possible
                AssignCommand(oHorse,ClearAllActions());
                //AddHenchman(oOwner,oHorse);
                SetLocalObject(oHorse,"oX3_HorseOwner",oOwner);
                //AssignCommand(oHorse,SetAssociateState(NW_ASC_DISTANCE_6_METERS,TRUE));
                //DelayCommand(1.0,SetAssociateState(NW_ASC_DISTANCE_6_METERS,TRUE,oHorse));
                if (bAssign) SetLocalObject(oOwner,"oAssignedHorse",oHorse);
            } // new owner
            else
            {   // make sure variables on oHorse are correct
                SetLocalObject(oHorse,"oX3_HorseOwner",oOwner);
                //AssignCommand(oHorse,SetAssociateState(NW_ASC_DISTANCE_6_METERS,TRUE));
                SetAssociateState(NW_ASC_DISTANCE_6_METERS,TRUE,oHorse);
                if (bAssign) SetLocalObject(oOwner,"oAssignedHorse",oHorse);
                if (GetLocalInt(GetModule(),"X3_ENABLE_MOUNT_DB")&&GetIsPC(oOwner)) SetLocalInt(oOwner,"bX3_STORE_MOUNT_INFO",TRUE);
            } // make sure variables on oHorse are correct
            sName=GetLocalString(oHorse,"sX3_OriginalName");
            if (GetStringLength(sName)<1)
            {   // define original name
                sName=GetName(oHorse);
                SetLocalString(oHorse,"sX3_OriginalName",sName);
            } // define original name
            if (GetMaster(oHorse)==oOwner)
            {   // was set okay
                if (GetStringLowerCase(GetStringRight(GetName(oOwner),1))=="s"||GetStringLowerCase(GetStringRight(GetName(oOwner),1))=="z")
                    SetName(oHorse,GetName(oOwner)+"' "+sName);
                else {
                    SetName(oHorse,GetName(oOwner)+"'s "+sName);
                }
            } // was set okay
        } // horse can be mounted
        else
        {   // not valid
            if (GetIsPC(oOwner))
            {   // PC
                PrintString("X3 HORSE ERROR: Attempt made to set "+GetName(oOwner)+" as owner of "+GetName(oHorse)+" is an invalid assignment.");
            } // PC
            else
            {   // error
                PrintString("X3 HORSE ERROR: Attempt made to set "+GetName(oOwner)+" as owner of "+GetName(oHorse)+" is an invalid assignment.");
            } // error
        } // not valid
    } // valid parameters
} // HorseSetOwner()