#include "inc_debug"
// This library contains functions returning correct text strings based on the language of the game

const int LANGUAGE_ENGLISH = 1;
const int LANGUAGE_POLISH = 2;

// Returns sPolishText if the module is run on a Polish version of the game and sEnglishText otherwise
string GetLocalizedString(string sEnglishText, string sPolishText);

// Returns a LANGUAGE_* constant based on the language of the server
int GetServerLanguage();

// Returns an alignment shortcut (i.e. LG) based on alignment constants
string GetAlignmentString(int alignmentGood, int alignmentLawful);

// Returns a localized race string based on a race constant
string GetRaceString(int race);

// Returns a localized class string based on a class constant
string GetClassString(int class);

// Returns sMaleVariant if nGender is GENDER_MALE and sFemaleVariant otherwise
string GenderedString(string sMaleVariant, string sFemaleVariant, int nGender);



// Returns a localized alignment string (i.e. "Lawful Good (LG)") based on alignment constants
string GetFullAlignmentString(int alignmentGood, int alignmentLawful);

int GetIsGamePolish()
{
    return (GetStringByStrRef(35) == "Ludzie");
}

int GetServerLanguage()
{
    if (GetIsGamePolish())
        return LANGUAGE_POLISH;
    return LANGUAGE_ENGLISH;
}

string GetLocalizedString(string sEnglishText, string sPolishText)
{
    if (GetIsGamePolish())
        return sPolishText;
    return sEnglishText;
}

string GetAlignmentString(int alignmentGood, int alignmentLawful)
{
    if (alignmentLawful == ALIGNMENT_NEUTRAL && alignmentGood == ALIGNMENT_NEUTRAL)
        return "TN";

    string result;
    switch (alignmentLawful)
    {
        case ALIGNMENT_LAWFUL: result = "L"; break;
        case ALIGNMENT_NEUTRAL: result = "N"; break;
        case ALIGNMENT_CHAOTIC: result = "C"; break;
    }
    switch (alignmentGood)
    {
        case ALIGNMENT_GOOD: result += "G"; break;
        case ALIGNMENT_NEUTRAL: result += "N"; break;
        case ALIGNMENT_EVIL: result += "E"; break;
    }
    return result;
}

string GetRaceString(int race)
{
    switch (race)
    {
        case RACIAL_TYPE_HUMAN: return GetLocalizedString("Human", "Cz�owiek");
        case RACIAL_TYPE_ELF: return "Elf";
        case RACIAL_TYPE_DWARF: return GetLocalizedString("Dwarf", "Krasnolud");
        case RACIAL_TYPE_HALFELF: return GetLocalizedString("Half-elf", "P�elf");
        case RACIAL_TYPE_HALFLING: return GetLocalizedString("Halfling", "Nizio�ek");
        case RACIAL_TYPE_HALFORC: return GetLocalizedString("Half-orc", "P�ork");
        case RACIAL_TYPE_GNOME: return GetLocalizedString("Gnome", "Gnom");
    }
    LogWarning("Invalid racial type: %n (inc_language, GetRaceString, race)", race);
    return "";
}

string GetClassString(int class)
{
    switch (class)
    {
        case CLASS_TYPE_FIGHTER: return GetLocalizedString("Fighter", "Wojownik");
        case CLASS_TYPE_MONK: return GetLocalizedString("Monk", "Mnich");
        case CLASS_TYPE_BARBARIAN: return GetLocalizedString("Barbarian", "Barbarzy�ca");
        case CLASS_TYPE_RANGER: return GetLocalizedString("Ranger", "�owca");
        case CLASS_TYPE_BARD: return "Bard";
        case CLASS_TYPE_CLERIC: return GetLocalizedString("Cleric", "Kap�an");
        case CLASS_TYPE_PALADIN: return GetLocalizedString("Paladin", "Paladyn");
        case CLASS_TYPE_DRUID: return "Druid";
        case CLASS_TYPE_WIZARD: return GetLocalizedString("Wizard", "Czarodziej");
        case CLASS_TYPE_SORCERER: return GetLocalizedString("Sorcerer", "Czarownik");
        case CLASS_TYPE_ROGUE: return GetLocalizedString("Rogue", "�otrzyk");
    }
    LogWarning("Invalid class type: %n (inc_language, GetClassString, class)", class);
    return "";
}

string GetFullAlignmentString(int alignmentGood, int alignmentLawful)
{
    if (alignmentLawful == ALIGNMENT_NEUTRAL && alignmentGood == ALIGNMENT_NEUTRAL)
        return GetLocalizedString("True Neutral (TN)", "Prawdziwie Neutralny (TN)");

    string result;
    switch (alignmentLawful)
    {
        case ALIGNMENT_LAWFUL: result = GetLocalizedString("Lawful", "Praworz�dny"); break;
        case ALIGNMENT_NEUTRAL: result = GetLocalizedString("Neutral", "Neutralny"); break;
        case ALIGNMENT_CHAOTIC: result = GetLocalizedString("Chaotic", "Chaotyczny"); break;
    }
    switch (alignmentGood)
    {
        case ALIGNMENT_GOOD: result += GetLocalizedString(" Good", " Dobry"); break;
        case ALIGNMENT_NEUTRAL: result += GetLocalizedString(" Neutral", " Neutralny"); break;
        case ALIGNMENT_EVIL: result += GetLocalizedString(" Evil", " Z�y"); break;
    }

    result = result+" ("+GetAlignmentString(alignmentGood, alignmentLawful)+")";
    return result;
}

string GenderedString(string sMaleVariant, string sFemaleVariant, int nGender)
{
    if (nGender == GENDER_MALE)
        return sMaleVariant;
    return sFemaleVariant;
}