#include "inc_scriptevents"
void main()
{
    object PC = GetLocalObject(OBJECT_SELF, "PC");
    int scrollsBought = GetLocalInt(PC, "CREATOR_SCROLLSBOUGHT");
    if (scrollsBought < 1)
        GiveGoldToCreature(PC, 10000);

    int eventsAdded = GetLocalInt(PC, "CREATOR_ADDEDEVENTS");
    if (!eventsAdded)
    {
        AddEventScript("OnAcquireItem", "mod_acq_scroll");
        SetLocalInt(PC, "CREATOR_ADDEDEVENTS", TRUE);
    }
}
