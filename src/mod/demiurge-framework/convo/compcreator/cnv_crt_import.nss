#include "inc_realmsdb"
void main()
{
    object PC = GetPCSpeaker();
    string dbName = GetLocalString(PC, "CREATOR_IMPORTNAME");
    int result = ImportCompanion(dbName);
    SetLocalInt(PC, "CREATOR_IMPORTRESULT", result);
}
