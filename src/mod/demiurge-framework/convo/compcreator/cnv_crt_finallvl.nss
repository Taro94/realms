#include "inc_companions"
#include "inc_experience"
void main()
{
    object PC = GetPCSpeaker();
    string id = GetLocalString(PC, "CREATOR_ID");
    int currentLvl = GetHitDice(PC);

    AddCompanionSnapshot(id, currentLvl, PC);
    MarkCompanionAsFinished(id);

    DeleteLocalInt(PC, "CREATOR_PROGRESS");
    SetLocalInt(PC, "CREATOR_FINISHED", TRUE);
}
