int StartingConditional()
{
    object PC = GetPCSpeaker();
    int creationInProgress = GetLocalInt(PC, "CREATOR_PROGRESS");
    int expectedLevel = GetLocalInt(PC, "CREATOR_LVL");
    int isWizard = GetLocalInt(PC, "CREATOR_WIZARD");
    int wizardLevels = GetLevelByClass(CLASS_TYPE_WIZARD, PC);

    if (creationInProgress == FALSE)
        return FALSE;

    if (!isWizard && wizardLevels > 0 && expectedLevel <= GetHitDice(PC))
        return TRUE;

    return FALSE;
}
