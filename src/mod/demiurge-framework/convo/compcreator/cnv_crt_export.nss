#include "inc_realmsdb"
void main()
{
    string id = GetLocalString(OBJECT_SELF, "COMPANION_ID");
    struct CompanionInformation companionInfo = GetCompanionInformation(id);

    string fullName = companionInfo.lastName == "" ? companionInfo.firstName : companionInfo.firstName+"_"+companionInfo.lastName;
    string fileName = "rlm_char_"+GetStringLowerCase(fullName)+".sqlite3";

    ExportCompanion(fileName, id);
}
