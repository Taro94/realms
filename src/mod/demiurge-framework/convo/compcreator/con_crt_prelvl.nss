int StartingConditional()
{
    object PC = GetPCSpeaker();
    int creationInProgress = GetLocalInt(PC, "CREATOR_PROGRESS");
    int expectedLevel = GetLocalInt(PC, "CREATOR_LVL");

    if (creationInProgress == FALSE)
        return FALSE;

    if (expectedLevel > GetHitDice(PC))
        return TRUE;

    return FALSE;
}
