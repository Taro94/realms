int StartingConditional()
{
    object PC = GetPCSpeaker();
    SetLocalObject(OBJECT_SELF, "PC", PC); //workaround for GetPCSpeaker() not working in ActionTaken of a single-node convo
    int creationInProgress = GetLocalInt(PC, "CREATOR_PROGRESS");
    int expectedLevel = GetLocalInt(PC, "CREATOR_LVL");

    if (creationInProgress == FALSE)
        return FALSE;

    if (expectedLevel <= GetHitDice(PC))
        return TRUE;

    return FALSE;
}
