#include "inc_colors"
#include "inc_convoptions"
#include "inc_language"
#include "inc_realmsdb"
void main()
{
    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    PrepareCompanionArrays(optionsArray, valuesArray);
    LoadConversationOptions(optionsArray, valuesArray);
}
