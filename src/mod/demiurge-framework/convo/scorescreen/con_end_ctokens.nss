#include "inc_dftokens"
#include "inc_realms"
#include "inc_ruleset"
int StartingConditional()
{
    object realm = GetRealm();
    SetScoreScreenTokens(realm);

    struct Ruleset ruleset = GetRuleset(realm);
    if (ruleset.startingGold != 1.0
        || ruleset.questGold != 1.0
        || ruleset.bountyGold != 1.0
        || ruleset.startingSoulstones != 1.0)
        return TRUE;

    return FALSE;
}
