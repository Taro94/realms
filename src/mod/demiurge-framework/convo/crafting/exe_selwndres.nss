#include "inc_language"
#include "inc_crafting"
#include "inc_realms"

//Script firing when a PC selects an item to be used as a resource in wand crafting
void main()
{
    object item = GetTargetingModeSelectedObject();
    object PC = GetLastPlayerToSelectTarget();

    AssignCommand(PC, ClearAllActions());
    AssignCommand(PC, ActionResumeConversation());

    if (GetItemPossessor(item) != PC)
    {
        string msg = GetLocalizedString("You can only choose an item in your inventory!", "Mo�esz wybra� tylko przedmiot ze swojego ekwipunku!");
        FloatingTextStringOnCreature(msg, PC);
        return;
    }

    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int resourcePoints = GetResourceUnitsOfItemForWandCrafting(realmScript, GetTag(item));

    if (resourcePoints <= 0)
    {
        string msg = GetLocalizedString("The item selected is not a valid wand crafting component!", "Wybrany przedmiot nie nadaje si� jako komponent tworzenia r�d�ek!");
        FloatingTextStringOnCreature(msg, PC);
        return;
    }

    AddResourceToWandCraftingPool(PC, GetTag(item));
}
