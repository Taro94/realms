#include "inc_targeting"

void main()
{
    object PC = GetPCSpeaker();
    AssignCommand(PC, ClearAllActions());
    AssignCommand(PC, ActionPauseConversation());

    EnterTargetingModeToExecuteScript(PC, "exe_selscrres", OBJECT_TYPE_ITEM, MOUSECURSOR_PICKUP, MOUSECURSOR_NOACTION);
}
