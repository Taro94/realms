#include "inc_map"
void main()
{
    object PC = GetPCSpeaker();
    CancelMapDisplay(PC);
    DeleteLocalInt(PC, "TEMP_MAP_X");
    DeleteLocalInt(PC, "TEMP_MAP_Y");
    DeleteLocalInt(PC, "TEMP_MAP_SIZE");
    DeleteLocalInt(PC, "MAP_FLASH");
}
