#include "inc_realmsdb"
void main()
{
    object PC = GetPCSpeaker();
    string param = GetScriptParam("Change");
    int yOffset = GetLocalInt(PC, "TEMP_MAP_Y") + StringToInt(param);
    SetLocalInt(PC, "TEMP_MAP_Y", yOffset);
}
