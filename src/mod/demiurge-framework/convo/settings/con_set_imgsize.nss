#include "inc_dftokens"
#include "inc_realmsdb"
int StartingConditional()
{
    object PC = GetPCSpeaker();
    SetPlayerSettingsTokens(PC);

    if (GetLocalInt(PC, "TEMP_MAP_SIZE") != 0)
        return TRUE;

    struct PlayerSettings settings = GetPlayerSettings(PC);
    SetLocalInt(PC, "TEMP_MAP_X", settings.mapOffsetX);
    SetLocalInt(PC, "TEMP_MAP_Y", settings.mapOffsetY);
    SetLocalInt(PC, "TEMP_MAP_SIZE", settings.mapImageSize);

    return TRUE;
}
