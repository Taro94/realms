#include "inc_realmsdb"
void main()
{
    object PC = GetPCSpeaker();
    string param = GetScriptParam("Change");
    int xOffset = GetLocalInt(PC, "TEMP_MAP_X") + StringToInt(param);
    SetLocalInt(PC, "TEMP_MAP_X", xOffset);
}
