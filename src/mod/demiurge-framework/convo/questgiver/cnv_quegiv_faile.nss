#include "inc_quests"
#include "inc_plots"
#include "inc_stats"

void main()
{
    object realm = GetRealm();
    object oQuest = GetLocalObject(OBJECT_SELF, "QUEST");
    object encounter = GetQuestEncounter(oQuest);
    object specialArea = GetQuestSpecialArea(oQuest);
    object area = GetIsObjectValid(encounter) ? GetEncounterArea(encounter) : specialArea;
    object biome = GetTileBiome(area);

    //Activate next inactive quest
    int inactiveQuestsNum = GetObjectArraySize(BIOME_INACTIVE_QUESTS, biome);
    if (inactiveQuestsNum > 0)
    {
        object nextQuest = GetObjectArrayElement(BIOME_INACTIVE_QUESTS, 0, biome);
        ActivateQuest(nextQuest);
        DeleteObjectArrayElement(BIOME_INACTIVE_QUESTS, 0, biome);
    }
    
    IncrementRegionPastQuests(biome);
    int pastQuests = GetRegionPastQuests(biome);
    if (pastQuests == 7)
    {
        int biomeIndex = GetRegionOrderNumber(biome)-1;
        object realm = GetRealm();
        object plot = GetRealmPlot(realm);
        UpdatePlotForward(plot, PLOT_STATE_FAILED_QUESTS, biomeIndex);
    }
        

    UpdateQuestState(oQuest, QUEST_STATE_AFTER_FAILED);
    UntrackQuest(oQuest);
    CustomQuestFailureLogic(oQuest, GetPCSpeaker());
    RegisterFailedQuest(realm);
}
