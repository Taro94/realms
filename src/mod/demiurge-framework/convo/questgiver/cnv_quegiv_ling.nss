#include "inc_quests"

void main()
{
    object oQuest = GetLocalObject(OBJECT_SELF, "QUEST");
    SetQuestAccepted(oQuest);
    int questState = GetQuestState(oQuest);
    int newState = questState == QUEST_STATE_NONE ? QUEST_STATE_LINGERING : questState;
    UpdateQuestState(oQuest, newState);
    CustomQuestTakingLogic(oQuest, GetPCSpeaker());
}
