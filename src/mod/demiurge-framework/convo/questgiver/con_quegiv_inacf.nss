#include "inc_quests"
int StartingConditional()
{
    object quest = GetLocalObject(OBJECT_SELF, "QUEST");
    return !GetIsQuestActive(quest) && GetGender(OBJECT_SELF) == GENDER_FEMALE;
}
