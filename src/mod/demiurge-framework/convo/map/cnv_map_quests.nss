#include "inc_colors"
#include "inc_convoptions"
#include "inc_language"
#include "inc_realms"
#include "inc_quests"
#include "inc_map"
#include "inc_realmsdb"
void main()
{
    //Display map
    object realm = GetRealm();
    object PC = GetPCSpeaker();
    object quest = GetTrackedQuest(PC);
    int x, y = -1;
    if (GetIsObjectValid(quest))
    {
        if (GetPlotScript(quest) != "")
        {
            //If we're tracking a plot object
            object plotDestination = GetCurrentPlotDestinationTile(quest);
            x = GetTileX(plotDestination);
            y = GetTileY(plotDestination);
        }
        else
        {
            //If we're tracking a regular quest
            object encounter = GetQuestEncounter(quest);
            object specialArea = GetQuestSpecialArea(quest);
            object tile = GetIsObjectValid(encounter) ? GetEncounterArea(encounter) : specialArea;

            if (GetQuestState(quest) == QUEST_STATE_COMPLETED)
            {
                object biome = GetTileBiome(tile);
                object town = GetTownOfBiome(biome);
                x = GetTileX(town);
                y = GetTileY(town);
            }
            else
            {
                x = GetTileX(tile);
                y = GetTileY(tile);
            }
        }
    }
    struct PlayerSettings settings = GetPlayerSettings(PC);
    DisplayMap(PC, realm, TRUE, x, y, FALSE, settings.mapOffsetX, settings.mapOffsetY, settings.mapImageSize);

    if (GetConversationOptionsInitialized())
        return;

    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    //First option is "None"
    string option = GetLocalizedString("[None]", "[�adne]");
    option = ColorStringWithStoredColor(option, "10");
    string value = "";
    AddStringArrayElement(optionsArray, option);
    AddStringArrayElement(valuesArray, value);

    //Second option is the plot quest
    object plot = GetRealmPlot(realm);
    struct PlotJournalEntries plotJournalEntries = GetPlotInstanceJournalEntries(plot);
    option = GetLocalizedString("Main quest", "Zadanie g��wne");
    option = "["+option+" - \""+plotJournalEntries.journalName+"\"]";
    option = ColorStringWithStoredColor(option, "10");
    value = ObjectToString(plot);
    AddStringArrayElement(optionsArray, option);
    AddStringArrayElement(valuesArray, value);

    //Add options for all quests in progress
    int questsNum = GetObjectArraySize(REALM_QUESTS_ARRAY, realm);
    int i;
    for (i = 0; i < questsNum; i++)
    {
        object quest = GetObjectArrayElement(REALM_QUESTS_ARRAY, i, realm);
        int questState = GetQuestState(quest);
        if ((questState == QUEST_STATE_LINGERING || questState == QUEST_STATE_COMPLETED) && GetIsQuestAccepted(quest))
        {
            option = GetQuestNameWithQuestgiver(quest);
            option = "[" + option + "]";
            option = ColorStringWithStoredColor(option, "10");
            value = ObjectToString(quest);
            AddStringArrayElement(optionsArray, option);
            AddStringArrayElement(valuesArray, value);
        }
    }

    LoadConversationOptions(optionsArray, valuesArray);
}
