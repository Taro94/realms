#include "inc_convoptions"
#include "inc_quests"
void main()
{
    int option = StringToInt(GetScriptParam("Option"));
    SetConversationOptionValue(option);

    string value = GetSelectedOptionValue();
    object questToTrack = StringToObject(value);

    object oPC = GetPCSpeaker();
    SetTrackedQuest(oPC, questToTrack);
}
