int StartingConditional()
{
    string option = GetScriptParam("Option");
    if (GetLocalInt(OBJECT_SELF, option) < 300)
        return TRUE;
    return FALSE;
}
