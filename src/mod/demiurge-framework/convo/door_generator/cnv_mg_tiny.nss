#include "inc_arrays"
#include "inc_companions"
void main()
{
    SetLocalInt(OBJECT_SELF, "MAPSIZE", 4);
    CreateStringArray("CompanionNames");
    CreateStringArray("CompanionIds");
    PrepareCompanionArrays("SelectableNames", "SelectableIds", FALSE);
}
