#include "inc_chat"
#include "inc_random"

void main()
{
    string lastMsg = GetLastMessage(GetPCSpeaker());
    int seed = GetSeedFromString(lastMsg);
    SetLocalString(OBJECT_SELF, "SEED", lastMsg);
}
