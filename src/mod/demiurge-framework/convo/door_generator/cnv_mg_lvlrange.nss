#include "inc_colors"
#include "inc_convoptions"
#include "inc_language"
void main()
{
    if (GetConversationOptionsInitialized())
        return;

    string optionsArray = "optionsArray";
    string valuesArray = "valuesArray";

    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    DeleteLocalInt(OBJECT_SELF, "STARTLEVEL");

    int i;
    for (i = 0; i < 8; i++)
    {
        string option = GetLocalizedString("[Level ", "[Poziom ") + IntToString(i+1) + "]";
        option = ColorStringWithStoredColor(option, "10");

        string value = IntToString(i+1);
        AddStringArrayElement(optionsArray, option);
        AddStringArrayElement(valuesArray, value);
    }

    LoadConversationOptions(optionsArray, valuesArray);
}
