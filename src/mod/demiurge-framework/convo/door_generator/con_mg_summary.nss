#include "inc_language"
#include "inc_tokens"
int StartingConditional()
{
    string size;
    int mapSize = GetLocalInt(OBJECT_SELF, "MAPSIZE");
    switch (mapSize)
    {
        case 4: size = GetLocalizedString("tiny", "bardzo ma�y"); break;
        case 8: size = GetLocalizedString("small", "ma�y"); break;
        case 12: size = GetLocalizedString("medium", "�redni"); break;
        default: size = GetLocalizedString("large", "du�y"); break;
    }
    SetCustomTokenEx(101, GetLocalizedString("random", "losowe"));
    SetCustomTokenEx(102, size);
    SetCustomTokenEx(103, IntToString(GetLocalInt(OBJECT_SELF, "STARTLEVEL")));
    SetCustomTokenEx(104, IntToString(GetLocalInt(OBJECT_SELF, "PLAYERS")));
    return TRUE;
}
