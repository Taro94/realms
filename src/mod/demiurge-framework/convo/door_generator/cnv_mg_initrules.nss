void main()
{
    SetLocalInt(OBJECT_SELF, "STARTING_GOLD", 100);
    SetLocalInt(OBJECT_SELF, "QUEST_GOLD", 100);
    SetLocalInt(OBJECT_SELF, "BOUNTY_GOLD", 100);
    SetLocalInt(OBJECT_SELF, "STARTING_LIVES", 100);
    SetLocalString(OBJECT_SELF, "SEED", "0");
}
