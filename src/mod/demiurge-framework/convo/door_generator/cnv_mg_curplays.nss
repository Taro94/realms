void main()
{
    int pcNum = 0;
    object PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        pcNum++;
        PC = GetNextPC();
    }

    SetLocalInt(OBJECT_SELF, "PLAYERS", pcNum);
}
