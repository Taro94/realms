int StartingConditional()
{
    string option = GetScriptParam("Option");
    if (GetLocalInt(OBJECT_SELF, option) > 0)
        return TRUE;
    return FALSE;
}
