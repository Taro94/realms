#include "inc_towns"
#include "inc_tiles"

void TeleportToHitchingPost(object hitchingPostWp)
{
    ClearAllActions();
    JumpToObject(hitchingPostWp);
}

void main()
{
    //Send the horse to the last visited town
    object PC = GetPCSpeaker();
    object respawnWp = GetPCRespawnWaypoint(PC);
    object town = GetTile(GetArea(respawnWp));
    object hitchingPostWp = GetTileWaypoint(town, "wp_hitchpost");

    RemoveHenchman(PC, OBJECT_SELF);
    ActionMoveAwayFromObject(PC, TRUE);
    DelayCommand(3.0f, TeleportToHitchingPost(hitchingPostWp));
}
