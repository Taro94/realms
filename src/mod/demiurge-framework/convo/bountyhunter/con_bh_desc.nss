#include "inc_dftokens"
#include "inc_debug"

int StartingConditional()
{
    string description = GetLocalString(OBJECT_SELF, "ChampionDescription");
    SetChampionDescriptionToken(description);
    return TRUE;
}
