#include "inc_champions"
#include "inc_encounters"
#include "inc_realms"
#include "inc_tiles"

int StartingConditional()
{
    object thisArea = GetArea(OBJECT_SELF);
    object thisTile = GetTile(thisArea);
    object thisBiome = GetTileBiome(thisTile);
    object PC = GetPCSpeaker();
    object item = GetFirstItemInInventory(PC);
    while (GetIsObjectValid(item))
    {
        object encounter = GetEncounterOfTrophy(item);
        if (encounter != OBJECT_INVALID)
        {
            object area = GetEncounterArea(encounter);
            object tile = GetTile(area);
            object biome = GetTileBiome(tile);
            if (biome == thisBiome)
            {
                return TRUE;
            }
        }
        item = GetNextItemInInventory(PC);
    }
    return FALSE;
}
