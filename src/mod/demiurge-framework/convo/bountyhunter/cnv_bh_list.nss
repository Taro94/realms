#include "inc_biomes"
#include "inc_towns"
#include "inc_encounters"
#include "inc_champions"
#include "inc_tiles"
#include "inc_arrays"
#include "inc_convoptions"

void main()
{
    object area = GetArea(OBJECT_SELF);
    object tile = GetTile(area);
    object biome = GetBiomeOfTown(tile);

    string optionsArray = "Options";
    string valuesArray = "Values";
    CreateStringArray(optionsArray);
    CreateStringArray(valuesArray);

    int i;
    for (i = 0; i < GetObjectArraySize(BIOME_ENCOUNTERS_WITH_CHAMPIONS, biome); i++)
    {
        object encounter = GetObjectArrayElement(BIOME_ENCOUNTERS_WITH_CHAMPIONS, i, biome);
        object trophy = GetTrophyOfEncounter(encounter);
        if (GetIsObjectValid(trophy))
        {
            string championName = GetChampionNameOfTrophy(trophy);
            string championDescription = GetChampionDescriptionOfTrophy(trophy);
            AddStringArrayElement(optionsArray, championName);
            AddStringArrayElement(valuesArray, championDescription);
        }
    }

    LoadConversationOptions(optionsArray, valuesArray);
}
