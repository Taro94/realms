#include "inc_tiles"
#include "inc_facilities"
#include "inc_horses"

void main()
{
    LogInfo("Transporting to facility...");
    object PC = GetLastUsedBy();
    object tile = GetTile(OBJECT_SELF);
    object wp = GetTileWaypoint(tile, "wp_facexit");

    if (GetIsObjectValid(wp))
        LogInfo("Valid WP");
    else
        LogWarning("Invalid WP");

    AssignCommand(PC, ClearAllActions());
    AssignCommand(PC, JumpToObjectRespectingHorses(wp));
    AssignCommand(PC, ClearAllActions());
}
