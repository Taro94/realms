#include "inc_hndvars"

string OnGetOverworldBiomeScript(int nIndex);
string OnGetRandomRealmName(string sSeedName);
string OnGetRealmPlotScript(int nIndex);
int OnGetResourceUnitsRequiredToCraftWand(int nSpell, int nSpellLevel);
int OnGetResourceUnitsRequiredToBrewPotion(int nSpell, int nSpellLevel);
int OnGetResourceUnitsRequiredToScribeScroll(int nSpell, int nSpellLevel);
int OnGetResourceUnitsOfItemForWandCrafting(string sItemTag);
int OnGetResourceUnitsOfItemForPotionBrewing(string sItemTag);
int OnGetResourceUnitsOfItemForScrollScribing(string sItemTag);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfOverworldBiomes
    {        
        SetLocalInt(mod, funcResult, overworldBiomeNumber);
    }
    else if (handler == 1) //GetNumberOfLevelsPerRegion
    {        
        SetLocalInt(mod, funcResult, levelsPerRegion);
    }
    else if (handler == 2) //GetOverworldBiomeScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetOverworldBiomeScript(nIndex));
    }
    else if (handler == 4) //GetRandomRealmName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetRandomRealmName(sSeedName));
    }
    else if (handler == 7) //GetNumberOfPlots
    {        
        SetLocalInt(mod, funcResult, plotsNumber);
    }
    else if (handler == 8) //GetRealmPlotScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetRealmPlotScript(nIndex));
    }
    else if (handler == 9) //GetRealmStartingLevel
    {        
        SetLocalInt(mod, funcResult, startingLevel);
    }
    else if (handler == 10) //GetResourceUnitsRequiredToCraftWand
    {
        int nSpell = GetLocalInt(mod, funcArg1);
        int nSpellLevel = GetLocalInt(mod, funcArg2);
        SetLocalInt(mod, funcResult, OnGetResourceUnitsRequiredToCraftWand(nSpell, nSpellLevel));
    }
    else if (handler == 11) //GetResourceUnitsRequiredToBrewPotion
    {
        int nSpell = GetLocalInt(mod, funcArg1);
        int nSpellLevel = GetLocalInt(mod, funcArg2);
        SetLocalInt(mod, funcResult, OnGetResourceUnitsRequiredToBrewPotion(nSpell, nSpellLevel));
    }
    else if (handler == 12) //GetResourceUnitsRequiredToScribeScroll
    {
        int nSpell = GetLocalInt(mod, funcArg1);
        int nSpellLevel = GetLocalInt(mod, funcArg2);
        SetLocalInt(mod, funcResult, OnGetResourceUnitsRequiredToScribeScroll(nSpell, nSpellLevel));
    }
    else if (handler == 13) //GetResourceUnitsOfItemForWandCrafting
    {
        string sItemTag = GetLocalString(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetResourceUnitsOfItemForWandCrafting(sItemTag));
    }
    else if (handler == 14) //GetResourceUnitsOfItemForPotionBrewing
    {
        string sItemTag = GetLocalString(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetResourceUnitsOfItemForPotionBrewing(sItemTag));
    }
    else if (handler == 15) //GetResourceUnitsOfItemForScrollScribing
    {
        string sItemTag = GetLocalString(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetResourceUnitsOfItemForScrollScribing(sItemTag));
    }
}