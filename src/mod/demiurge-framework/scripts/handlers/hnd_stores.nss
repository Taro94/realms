#include "inc_hndvars"

string OnGetStoreAreaResRef(int nIndex);
int OnGetStoreDayTrack(int nIndex);
int OnGetStoreNightTrack(int nIndex);
int OnGetStoreBattleTrack(int nIndex);
string OnGetRandomStoreName(string sSeedName, int nLanguage);
string OnGetStorekeeperResRef(int nIndex);
object OnSpawnStoreInventory(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);
string OnGetRandomStoreOwnerFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomStoreOwnerLastName(int nRacialType, int nGender, int nLanguage, string sSeedName);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 4) //GetTownStoresNumber
    {
        SetLocalInt(mod, funcResult, areasNumber);
    }
    else if (handler == 11) //GetStoreDayTracksNumber
    {
        SetLocalInt(mod, funcResult, storeDayTracksNumber);
    }
    else if (handler == 12) //GetStoreNightTracksNumber
    {
        SetLocalInt(mod, funcResult, storeNightTracksNumber);
    }
    else if (handler == 13) //GetStoreBattleTracksNumber
    {
        SetLocalInt(mod, funcResult, storeBattleTracksNumber);
    }
    else if (handler == 17) //GetStoreAreaResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetStoreAreaResRef(nIndex));
    }
    else if (handler == 24) //GetStoreDayTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetStoreDayTrack(nIndex));
    }
    else if (handler == 25) //GetStoreNightTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetStoreNightTrack(nIndex));
    }
    else if (handler == 26) //GetStoreBattleTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetStoreBattleTrack(nIndex));
    }
    else if (handler == 29) //GetRandomStoreName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetRandomStoreName(sSeedName, nLanguage));
    }
    else if (handler == 31) //GetStoreStorekeepersNumber
    {
        SetLocalInt(mod, funcResult, storekeepersNumber);
    }
    else if (handler == 34) //GetStorekeeperResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetStorekeeperResRef(nIndex));
    }
    else if (handler == 35) //SpawnStoreInventory
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nBiomeStartingLevel = GetLocalInt(mod, funcArg2);
        object oDestinationWaypoint = GetLocalObject(mod, funcArg3);
        SetLocalObject(mod, funcResult, OnSpawnStoreInventory(sSeedName, nBiomeStartingLevel, oDestinationWaypoint));
    }
    else if (handler == 36) //GetRandomStoreOwnerFirstName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomStoreOwnerFirstName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 37) //GetRandomStoreOwnerLastName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomStoreOwnerLastName(nRacialType, nGender, nLanguage, sSeedName));
    }
}