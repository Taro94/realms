#include "inc_hndvars"

string OnGetBossAreaResRef(int nIndex, object oBiome, int nExitsFlag);
void OnInitializeBossArea(string sSeedName, object oBiome, object oBossArea, int nBiomeStartingLevel);
string OnGetBossAreaRumor(object oBossArea, int nIndex, int nLanguage);
string OnGetRandomBossAreaName(string sSeedName, int nLanguage);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetNumberOfBossAreas
    {
        SetLocalInt(mod, funcResult, areasNumber);
    }
    else if (handler == 1) //GetBossAreaMinPlayerLevel
    {
        SetLocalInt(mod, funcResult, minPlayerLevel);
    }
    else if (handler == 2) //GetBossAreaMaxPlayerLevel
    {
        SetLocalInt(mod, funcResult, maxPlayerLevel);
    }
    else if (handler == 3) //GetBossAreaRumorsNumber
    {
        SetLocalInt(mod, funcResult, rumorsNumber);
    }
    else if (handler == 4) //GetBossAreaResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        object oBiome = GetLocalObject(mod, funcArg2);
        int nExitsFlag = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetBossAreaResRef(nIndex, oBiome, nExitsFlag));
    }
    else if (handler == 5) //InitializeBossArea
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        object oBiome = GetLocalObject(mod, funcArg2);
        object oBossArea = GetLocalObject(mod, funcArg3);
        int nBiomeStartingLevel = GetLocalInt(mod, funcArg4);
        OnInitializeBossArea(sSeedName, oBiome, oBossArea, nBiomeStartingLevel);
    }
    else if (handler == 6) //GetBossAreaRumor
    {
        object oBossArea = GetLocalObject(mod, funcArg1);
        int nIndex = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        SetLocalString(mod, funcResult, OnGetBossAreaRumor(oBossArea, nIndex, nLanguage));
    }
    else if (handler == 7) //GetRandomBossAreaName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetRandomBossAreaName(sSeedName, nLanguage));
    }
}