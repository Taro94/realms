#include "inc_hndvars"

string OnGetTownCommonerResRef(int nIndex);
string OnGetTownStableMasterResRef(int nIndex);
string OnGetTownBountyHunterResRef(int nIndex);
string OnGetTownAreaResRef(int nIndex, int nExitsFlag);
string OnGetTavernAreaResRef(int nIndex);
string OnGetTownStoreScript(int nIndex);
int OnGetTownDayTrack(int nIndex);
int OnGetTownNightTrack(int nIndex);
int OnGetTownBattleTrack(int nIndex);
int OnGetTavernDayTrack(int nIndex);
int OnGetTavernNightTrack(int nIndex);
int OnGetTavernBattleTrack(int nIndex);
string OnGetRandomTownName(string sSeedName, int nLanguage);
string OnGetRandomTavernName(string sSeedName, int nLanguage);
string OnGetInnkeeperResRef(int nIndex);
object OnSpawnTavernInventory(string sSeedName, object oDestinationWaypoint);
object OnSpawnStableInventory(string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);
string OnGetTownFacilityScript(int nIndex);
string OnGetRandomCommonerFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomCommonerLastName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomInnkeeperFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomInnkeeperLastName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomBountyHunterFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomBountyHunterLastName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomStableMasterFirstName(int nRacialType, int nGender, int nLanguage, string sSeedName);
string OnGetRandomStableMasterLastName(int nRacialType, int nGender, int nLanguage, string sSeedName);

void ScriptHandler()
{
    object mod = GetModule();
    int handler = GetLocalInt(mod, funcHandler);

    if (handler == 0) //GetTownAreasIdentifier
    {
        SetLocalString(mod, funcResult, areasId);
    }
    else if (handler == 1) //GetTownAreasNumber
    {
        SetLocalInt(mod, funcResult, areasNumber);
    }
    else if (handler == 2) //GetTownCommonersNumber
    {
        SetLocalInt(mod, funcResult, commonersNumber);
    }
    else if (handler == 39) //GetTownBountyHuntersNumber
    {
        SetLocalInt(mod, funcResult, bountyHuntersNumber);
    }
    else if (handler == 49) //GetTownStableMastersNumber
    {
        SetLocalInt(mod, funcResult, stableMastersNumber);
    }
    else if (handler == 3) //GetTownTavernsNumber
    {
        SetLocalInt(mod, funcResult, tavernsNumber);
    }
    else if (handler == 4) //GetTownStoresNumber
    {
        SetLocalInt(mod, funcResult, storesNumber);
    }
    else if (handler == 5) //GetTownDayTracksNumber
    {
        SetLocalInt(mod, funcResult, dayTracksNumber);
    }
    else if (handler == 6) //GetTownNightTracksNumber
    {
        SetLocalInt(mod, funcResult, nightTracksNumber);
    }
    else if (handler == 7) //GetTownBattleTracksNumber
    {
        SetLocalInt(mod, funcResult, battleTracksNumber);
    }
    else if (handler == 8) //GetTavernDayTracksNumber
    {
        SetLocalInt(mod, funcResult, tavernDayTracksNumber);
    }
    else if (handler == 9) //GetTavernNightTracksNumber
    {
        SetLocalInt(mod, funcResult, tavernNightTracksNumber);
    }
    else if (handler == 10) //GetTavernBattleTracksNumber
    {
        SetLocalInt(mod, funcResult, tavernBattleTracksNumber);
    }
    else if (handler == 14) //GetTownCommonerResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetTownCommonerResRef(nIndex));
    }
    else if (handler == 40) //GetTownBountyHunterResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetTownBountyHunterResRef(nIndex));
    }
    else if (handler == 50) //GetTownStableMasterResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetTownStableMasterResRef(nIndex));
    }
    else if (handler == 15) //GetTownAreaResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        int nExitsFlag = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetTownAreaResRef(nIndex, nExitsFlag));
    }
    else if (handler == 16) //GetTavernAreaResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetTavernAreaResRef(nIndex));
    }
    else if (handler == 18) //GetTownDayTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetTownDayTrack(nIndex));
    }
    else if (handler == 19) //GetTownNightTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetTownNightTrack(nIndex));
    }
    else if (handler == 20) //GetTownBattleTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetTownBattleTrack(nIndex));
    }
    else if (handler == 21) //GetTavernDayTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetTavernDayTrack(nIndex));
    }
    else if (handler == 22) //GetTavernNightTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetTavernNightTrack(nIndex));
    }
    else if (handler == 23) //GetTavernBattleTrack
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalInt(mod, funcResult, OnGetTavernBattleTrack(nIndex));
    }
    else if (handler == 27) //GetRandomTownName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetRandomTownName(sSeedName, nLanguage));
    }
    else if (handler == 28) //GetRandomTavernName
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nLanguage = GetLocalInt(mod, funcArg2);
        SetLocalString(mod, funcResult, OnGetRandomTavernName(sSeedName, nLanguage));
    }
    else if (handler == 30) //GetTavernInnkeepersNumber
    {
        SetLocalInt(mod, funcResult, innkeepersNumber);
    }
    else if (handler == 32) //GetInnkeeperResRef
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetInnkeeperResRef(nIndex));
    }
    else if (handler == 33) //SpawnTavernInventory
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        object oDestinationWaypoint = GetLocalObject(mod, funcArg2);
        SetLocalObject(mod, funcResult, OnSpawnTavernInventory(sSeedName, oDestinationWaypoint));
    }
    else if (handler == 51) //SpawnStableInventory
    {
        string sSeedName = GetLocalString(mod, funcArg1);
        int nBiomeStartingLevel = GetLocalInt(mod, funcArg2);
        object oDestinationWaypoint = GetLocalObject(mod, funcArg3);
        SetLocalObject(mod, funcResult, OnSpawnStableInventory(sSeedName, nBiomeStartingLevel, oDestinationWaypoint));
    }
    else if (handler == 36) //GetTownStoreScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetTownStoreScript(nIndex));
    }
    else if (handler == 37) //GetTownFacilitiesNumber
    {
        SetLocalInt(mod, funcResult, facilitiesNumber);
    }
    else if (handler == 38) //GetTownFacilityScript
    {
        int nIndex = GetLocalInt(mod, funcArg1);
        SetLocalString(mod, funcResult, OnGetTownFacilityScript(nIndex));
    }
    else if (handler == 41) //GetRandomCommonerFirstName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomCommonerFirstName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 42) //GetRandomCommonerLastName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomCommonerLastName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 43) //GetRandomInnkeeperFirstName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomInnkeeperFirstName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 44) //GetRandomInnkeeperLastName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomInnkeeperLastName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 45) //GetRandomBountyHunterFirstName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomBountyHunterFirstName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 46) //GetRandomBountyHunterLastName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomBountyHunterLastName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 47) //GetRandomStableMasterFirstName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomStableMasterFirstName(nRacialType, nGender, nLanguage, sSeedName));
    }
    else if (handler == 48) //GetRandomStableMasterLastName
    {
        int nRacialType = GetLocalInt(mod, funcArg1);
        int nGender = GetLocalInt(mod, funcArg2);
        int nLanguage = GetLocalInt(mod, funcArg3);
        string sSeedName = GetLocalString(mod, funcArg4);
        SetLocalString(mod, funcResult, OnGetRandomStableMasterLastName(nRacialType, nGender, nLanguage, sSeedName));
    }
}
