#include "inc_debug"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_towns"
#include "inc_plots"
void main()
{
    object PC = GetEnteringObject();
    if (!GetIsPC(PC))
        return;

    ExploreAreaForPlayer(OBJECT_SELF, PC);

    LogInfo("Entered town");
    object tile = GetTile(OBJECT_SELF);
    object biome = GetTileBiome(tile);
    object town = GetTownOfBiome(biome);
    object tavern = GetTavernOfTown(town);
    int biomeIndex = GetRegionOrderNumber(biome)-1;
    object realm = GetRealm();
    object plot = GetRealmPlot(realm);
    UpdatePlotForward(plot, PLOT_STATE_DOING_QUESTS, biomeIndex);
    SetPCRespawn(PC, town);

    //Disable rest in the tavern if no PCs are left there
    PC = GetFirstPC();
    while (GetIsObjectValid(PC))
    {
        if (GetArea(PC) == tavern)
            return;
        PC = GetNextPC();
    }
    DeleteLocalInt(tavern, "CanRest");
}
