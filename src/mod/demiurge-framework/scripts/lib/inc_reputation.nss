#include "inc_language"
#include "inc_common"

// Functions for modifying and accessing the adventuring party's reputation.
// Reputation can be seen as the good/evil alignment of the party as seen by others.
// As such, it may correspond to the actual good/evil alignment of the PCs, but doesn't have to.
// Reputation should be increased or decreased based on PC actions that are likely to become known to others.
// An act of pickpocketing, for example, shouldn't decrease the party's reputation (unless the culprit was detected),
// because the general population doesn't know about it. On the other hand, there may be situations likely to change your reputation
// even if your morality is unaffected, or even affected to the contrary. 
// NPCs witnessing PCs kill an innocent bystander is a good reason for a decrease in the PCs' reputation,
// even if the victim was actually a murderer the party was hunting down and because of which their good/evil alignment wasn't affected.
// The party starts with 0 reputation and can achieve reputation as low as -50 or as high as 50.

// Returns the reputation of the party (min. -50, max. 50)
int GetPartyReputation();

// Adds the given number to the reputation of the party. The reputation can't go lower than -50 or higher than 50.
void ModifyPartyReputation(int nModification);

// Returns a descriptive assessment of the party's reputation score. To be displayed by an NPC (inn keeper? me?) asked by a PC about the party reputation.
string GetDescriptivePartyReputation();

// Returns a number between -5 and 5 based on party's reputation that can be used as a modifier for checks for skills such as Persuade, Bluff and Intimidate.
int GetReputationDCModifier();


int GetPartyReputation()
{
    return GetLocalInt(GetModule(), "PARTY_REPUTATION");
}

void ModifyPartyReputation(int nModification)
{
    int current = GetPartyReputation();
    int new = current+nModification;

    int max = 50;
    int min = -50;

    if (new > max)
        new = max;
    else if (new < min)
        new = min;

    if (new > current)
        SendMessageToAll(GetLocalizedString("Party reputation increased.", "Reputacja dru�yny wzros�a."));
    else if (new < current)
        SendMessageToAll(GetLocalizedString("Party reputation decreased.", "Reputacja dru�yny zmala�a."));

    SetLocalInt(GetModule(), "PARTY_REPUTATION", new);
}

string GetDescriptivePartyReputation()
{
    int reputation = GetPartyReputation();
    if (reputation < -30)
        return GetLocalizedString(
            "I hate to say it, but yer considered a ruthless, evil piece of work. Out of those who know 'bout ya, most would want ya dead. Don't... don't look at me that way, I'm only telling ya what others say!", 
            "Nie chc� tego m�wi�, ale ludzie uwa�aj� ci� za bezwzgl�dn� i zepsut� jednostk�. Z tych, kt�rzy o tobie cokolwiek wiedz�, wi�kszo�� �yczy ci rych�ej �mierci. Nie... nie patrz na mnie w ten spos�b, ja tylko m�wi�, co s�dz� inni!");
    if (reputation < -10)
        return GetLocalizedString(
            "Ye don't have good reputation in the realm at all. People be wary of you, some are afraid. I'd keep a low profile if I were you. Better yet, show the folk you're a good-hearted fellow after all.", 
            "Nie masz dobrej opinii w krainie. Ludno�� zachowuje wobec ciebie ostro�no��, niekt�rzy si� ciebie obawiaj�. Mo�e staraj si� nie rzuca� za bardzo w oczy. Albo lepiej, udowodnij plotkuj�cym, �e w g��bi serca stoisz po w�a�ciwej stronie.");
    if (reputation <= 10)
        return GetLocalizedString(
            "People don't talk much about ya and they don't really have opinions on your deeds. Maybe it's better that way, huh?", 
            "Ludzie wiele o tobie nie m�wi� i nie maj� wyrobionego zdania na tw�j temat. Mo�e to i lepiej?");
    if (reputation <= 30)
        return GetLocalizedString(
            "Ya have the people's favor, as ye do mine! Yer doing some good for those who need help, keep it that way!", 
            "Ludno�� darzy ci� sympati�, tak jak i ja! Robisz wiele dobrego dla tych, kt�rym twoja pomoc jest potrzebna, tak trzymaj!");
    else
        return GetLocalizedString(
            "Do ye even have to ask? You're a local hero! Not a single soul would dare say a bad word about you and if they did, I'd kick them out of my place!", 
            "Musisz pyta�? Ludzie maj� ci� za bohatera! Pojedyncza duszyczka nie �mia�aby powiedzie� o tobie z�ego s�owa, a gdyby spr�bowa�a, nie mia�aby wi�cej wst�pu do mojego przybytku!");
}

int GetReputationDCModifier()
{
    int rep = GetPartyReputation();
    int sign = rep < 0 ? -1 : 1;
    int value = rep / 10;
    return sign*value;
}