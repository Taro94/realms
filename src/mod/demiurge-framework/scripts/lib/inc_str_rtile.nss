#include "inc_arrays"

//Constants
const int TILE_NO_PATH = 0;
const int TILE_CURRENT_PATH = 1;
const int TILE_PREV_PATH = 2;
const int TILE_TYPE_NORMAL = 0; //regular area with encounters (though some of them may be special)
const int TILE_TYPE_START = 1; //starting village
const int TILE_TYPE_SETTLEMENT = 2; //a village or a town
const int TILE_TYPE_SPECIAL = 3; //a special area
const int TILE_TYPE_BOSS = 4; //a boss area

const int PATH_NORTH = 1;
const int PATH_EAST = 2;
const int PATH_SOUTH = 4;
const int PATH_WEST = 8;

//Headers
void CreateRealmTileArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID);
void ClearRealmTileArray(string sName, object oObject=OBJECT_INVALID);
void AddRealmTileArrayElement(string sName, struct RealmTile tile, int nBeginning=FALSE, object oObject=OBJECT_INVALID);
void DeleteRealmTileArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);
void SetRealmTileArrayElement(string sName, int nIndex, struct RealmTile tile, object oObject=OBJECT_INVALID);
struct RealmTile GetRealmTileArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID);
void SetRealmTileArrayElementField(string sName, int nIndex, string sField, int nValue, object oObject=OBJECT_INVALID);
void SetRealmTileStringArrayElementField(string sName, int nIndex, string sField, string sValue, object oObject=OBJECT_INVALID);
int GetRealmTileArrayElementField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID);
string GetRealmTileArrayStringElementField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID);
int GetRealmTileArraySize(string sName, object oObject=OBJECT_INVALID);

//Struct
struct RealmTile
{
    int x; //0-based x coordinate of the tile, with 0 being on the far left
    int y; //0-based y coordinate of the tile, with 0 being at the top
    int index; //this is an ID of a tile calculated at map generation based on x and y - as such, it's redundant, but is helpful for storage of references to tiles (as a region's array for example)
    string biome; //script name of the biome of this tile
    int region; //integer denoting region ID of this tile
    int paths; //four binary flags representing connections with other tiles
    int blocked; //four binary flags representing sides that can't be used to create paths, because a neighboring tile is already defined (or doesn't exist)
    int set; //integer that should be 2 if the tile has been analyzed before already, 1 if it's been set within currently generated path and 0 otherwise
    int type; //integer denoting type of the tile
    string distances; //an int array representing distance that remains in this tile to get closer to the n-th tile, where n is the integer's index in the array
    int roadToTown; //a boolean denoting whether the tile in question is located en route to the the regional town from the region's entrance (to prioritize encounters that are not en route to the town for quest generation)
};

//Definitions
void CreateRealmTileArray(string sName, int nArraySize=0, object oObject=OBJECT_INVALID)
{
    CreateIntArray(sName+"_x", nArraySize, oObject);
    CreateIntArray(sName+"_y", nArraySize, oObject);
    CreateIntArray(sName+"_index", nArraySize, oObject);
    CreateStringArray(sName+"_biome", nArraySize, oObject);
    CreateIntArray(sName+"_region", nArraySize, oObject);
    CreateIntArray(sName+"_paths", nArraySize, oObject);
    CreateIntArray(sName+"_blocked", nArraySize, oObject);
    CreateIntArray(sName+"_set", nArraySize, oObject);
    CreateIntArray(sName+"_type", nArraySize, oObject);
    CreateStringArray(sName+"_distances", nArraySize, oObject);
    CreateIntArray(sName+"_roadtotown", nArraySize, oObject);
}

void ClearRealmTileArray(string sName, object oObject=OBJECT_INVALID)
{
    ClearIntArray(sName+"_x", oObject);
    ClearIntArray(sName+"_y", oObject);
    ClearIntArray(sName+"_index", oObject);
    ClearStringArray(sName+"_biome", oObject);
    ClearIntArray(sName+"_region", oObject);
    ClearIntArray(sName+"_paths", oObject);
    ClearIntArray(sName+"_blocked", oObject);
    ClearIntArray(sName+"_set", oObject);
    ClearIntArray(sName+"_type", oObject);
    ClearStringArray(sName+"_distances", oObject);
    ClearIntArray(sName+"_roadtotown", oObject);
}

void AddRealmTileArrayElement(string sName, struct RealmTile tile, int nBeginning=FALSE, object oObject=OBJECT_INVALID)
{
    AddIntArrayElement(sName+"_x", tile.x, nBeginning, oObject);
    AddIntArrayElement(sName+"_y", tile.y, nBeginning, oObject);
    AddIntArrayElement(sName+"_index", tile.index, nBeginning, oObject);
    AddStringArrayElement(sName+"_biome", tile.biome, nBeginning, oObject);
    AddIntArrayElement(sName+"_region", tile.region, nBeginning, oObject);
    AddIntArrayElement(sName+"_paths", tile.paths, nBeginning, oObject);
    AddIntArrayElement(sName+"_blocked", tile.blocked, nBeginning, oObject);
    AddIntArrayElement(sName+"_set", tile.set, nBeginning, oObject);
    AddIntArrayElement(sName+"_type", tile.type, nBeginning, oObject);
    AddStringArrayElement(sName+"_distances", tile.distances, nBeginning, oObject);
    AddIntArrayElement(sName+"_roadtotown", tile.roadToTown, nBeginning, oObject);
}

void DeleteRealmTileArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    DeleteIntArrayElement(sName+"_x", nIndex, oObject);
    DeleteIntArrayElement(sName+"_y", nIndex, oObject);
    DeleteIntArrayElement(sName+"_index", nIndex, oObject);
    DeleteStringArrayElement(sName+"_biome", nIndex, oObject);
    DeleteIntArrayElement(sName+"_region", nIndex, oObject);
    DeleteIntArrayElement(sName+"_paths", nIndex, oObject);
    DeleteIntArrayElement(sName+"_blocked", nIndex, oObject);
    DeleteIntArrayElement(sName+"_set", nIndex, oObject);
    DeleteIntArrayElement(sName+"_type", nIndex, oObject);
    DeleteStringArrayElement(sName+"_distances", nIndex, oObject);
    DeleteIntArrayElement(sName+"_roadtotown", nIndex, oObject);
}

void SetRealmTileArrayElement(string sName, int nIndex, struct RealmTile tile, object oObject=OBJECT_INVALID)
{
    SetIntArrayElement(sName+"_x", nIndex, tile.x, oObject);
    SetIntArrayElement(sName+"_y", nIndex, tile.y, oObject);
    SetIntArrayElement(sName+"_index", nIndex, tile.index, oObject);
    SetStringArrayElement(sName+"_biome", nIndex, tile.biome, oObject);
    SetIntArrayElement(sName+"_region", nIndex, tile.region, oObject);
    SetIntArrayElement(sName+"_paths", nIndex, tile.paths, oObject);
    SetIntArrayElement(sName+"_blocked", nIndex, tile.blocked, oObject);
    SetIntArrayElement(sName+"_set", nIndex, tile.set, oObject);
    SetIntArrayElement(sName+"_type", nIndex, tile.type, oObject);
    SetStringArrayElement(sName+"_distances", nIndex, tile.distances, oObject);
    SetIntArrayElement(sName+"_roadtotown", nIndex, tile.roadToTown, oObject);
}

struct RealmTile GetRealmTileArrayElement(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    struct RealmTile tile;
    tile.x = GetIntArrayElement(sName+"_x", nIndex, oObject);
    tile.y = GetIntArrayElement(sName+"_y", nIndex, oObject);
    tile.index = GetIntArrayElement(sName+"_index", nIndex, oObject);
    tile.biome = GetStringArrayElement(sName+"_biome", nIndex, oObject);
    tile.region = GetIntArrayElement(sName+"_region", nIndex, oObject);
    tile.paths = GetIntArrayElement(sName+"_paths", nIndex, oObject);
    tile.blocked = GetIntArrayElement(sName+"_blocked", nIndex, oObject);
    tile.set = GetIntArrayElement(sName+"_set", nIndex, oObject);
    tile.type = GetIntArrayElement(sName+"_type", nIndex, oObject);
    tile.distances = GetStringArrayElement(sName+"_distances", nIndex, oObject);
    tile.roadToTown = GetIntArrayElement(sName+"_roadtotown", nIndex, oObject);

    return tile;
}

void SetRealmTileArrayElementField(string sName, int nIndex, string sField, int nValue, object oObject=OBJECT_INVALID)
{
    SetIntArrayElement(sName+"_"+sField, nIndex, nValue, oObject);
}

void SetRealmTileArrayStringElementField(string sName, int nIndex, string sField, string sValue, object oObject=OBJECT_INVALID)
{
    SetStringArrayElement(sName+"_"+sField, nIndex, sValue, oObject);
}

int GetRealmTileArrayElementField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID)
{
    return GetIntArrayElement(sName+"_"+sField, nIndex, oObject);
}

string GetRealmTileArrayStringElementField(string sName, int nIndex, string sField, object oObject=OBJECT_INVALID)
{
    return GetStringArrayElement(sName+"_"+sField, nIndex, oObject);
}

void SetRealmTileArrayElementDistancesField(string sName, int nIndex, string sValue, object oObject=OBJECT_INVALID)
{
    SetStringArrayElement(sName+"_distances", nIndex, sValue, oObject);
}

string GetRealmTileArrayElementDistancesField(string sName, int nIndex, object oObject=OBJECT_INVALID)
{
    return GetStringArrayElement(sName+"_distances", nIndex, oObject);
}

int GetRealmTileArraySize(string sName, object oObject=OBJECT_INVALID)
{
    return GetIntArraySize(sName+"_x", oObject);
}