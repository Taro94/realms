// Library containing functions related to Realms' crafting system

#include "inc_common"
#include "inc_arrays"
#include "inc_debug"
#include "inc_partyskills"
#include "inc_realms"

const int CRAFT_TYPE_WEAPON = 1;
const int CRAFT_TYPE_ARMOR = 2;
const int CRAFT_TYPE_TRAP = 3;

struct CasterClassAndSpellLevel
{
    int casterClass;
    int spellLvl;
};

// Sets oCreature as the current crafter for oPC.
// oPC should be the crafter if they're using the crafting dialogue to craft items themselves.
// oPC's henchman should be the crafter if the PC is talking to them and wants *them* to craft something.
void SetCrafterCreature(object oPC, object oCreature);

// Returns the crafter currently set for oPC.
object GetCrafterCreature(object oPC);

// Adds a single unit of resource with resref sResourceTag to the wand crafting resource pool.
// Decreases the number of that resource oPC has by 1.
// If sResourceTag is not a valid wand crafting resource, nothing happens and the function returns FALSE,
// otherwise it returns TRUE.
int AddResourceToWandCraftingPool(object oPC, string sResourceTag);

// Adds a single unit of resource with resref sResourceTag to the potion brewing resource pool.
// Decreases the number of that resource oPC has by 1.
// If sResourceTag is not a valid potion brewing resource, nothing happens and the function returns FALSE,
// otherwise it returns TRUE.
int AddResourceToPotionBrewingPool(object oPC, string sResourceTag);

// Adds a single unit of resource with resref sResourceTag to the scroll writing resource pool.
// Decreases the number of that resource oPC has by 1.
// If sResourceTag is not a valid scroll writing resource, nothing happens and the function returns FALSE,
// otherwise it returns TRUE.
int AddResourceToScrollWritingPool(object oPC, string sResourceTag);

// Clears oPC's wand crafting pool, returning resources added to it to oPC.
void ClearWandCraftingPool(object oPC);

// Clears oPC's potion brewing pool, returning resources added to it to oPC.
void ClearPotionBrewingPool(object oPC);

// Clears oPC's scroll writing pool, returning resources added to it to oPC.
void ClearScrollWritingPool(object oPC);

// Deletes items from the potion brewing resource pool of oPC until their total resource value
// is equal or greater to nResourcePointsToConsume.
// Does nothing if the total resource value of the pool is less than nResourcePointsToConsume.
void ConsumeItemsFromPotionBrewingPool(object oPC, int nResourcePointsToConsume);

// Deletes items from the scroll writing resource pool of oPC until their total resource value
// is equal or greater to nResourcePointsToConsume.
// Does nothing if the total resource value of the pool is less than nResourcePointsToConsume.
void ConsumeItemsFromScrollWritingPool(object oPC, int nResourcePointsToConsume);

// Deletes items from the wand crafting resource pool of oPC until their total resource value
// is equal or greater to nResourcePointsToConsume.
// Does nothing if the total resource value of the pool is less than nResourcePointsToConsume.
void ConsumeItemsFromWandCraftingPool(object oPC, int nResourcePointsToConsume);

// Returns the total resource value of the wand crafting pool of oPC.
int GetWandCraftingPoolValue(object oPC);

// Returns the total resource value of the potion brewing pool of oPC.
int GetPotionBrewingPoolValue(object oPC);

// Returns the total resource value of the scroll writing pool of oPC.
int GetScrollWritingPoolValue(object oPC);

// Returns TRUE if oCrafter has the Craft Wand feat, has nSpell ready to cast, the spell can be used on wands
// and oMaterialsOwner has the bone wand item and added resources necessary to craft a wand of nSpell's level (defined by the realm script) to the resource pool.
// Returns FALSE otherwise.
// Setting nVerifyResourcePoints to FALSE will make the function not check whether enough resources have been added to the pool.
int CanCraftWand(object oMaterialsOwner, object oCrafter, int nSpell, int nVerifyResourcePoints=TRUE);

// Returns TRUE if oCrafter has the Brew Potion feat, has nSpell ready to cast, nSpell is non-hostile, can target the caster and can be used on potions
// and oMaterialsOwner has the empty vial item and added resources necessary to brew a potion of nSpell's level (defined by the realm script) to the resource pool.
// Returns FALSE otherwise.
// Setting nVerifyResourcePoints to FALSE will make the function not check whether enough resources have been added to the pool.
int CanBrewPotion(object oMaterialsOwner, object oCrafter, int nSpell, int nVerifyResourcePoints=TRUE);

// Returns TRUE if oCrafter has the Scribe Scroll feat, has nSpell ready to cast
// and oMaterialsOwner has the blank scroll item and added resources necessary to brew a potion of nSpell's level (defined by the realm script) to the resource pool.
// Returns FALSE otherwise.
// Setting nVerifyResourcePoints to FALSE will make the function not check whether enough resources have been added to the pool.
int CanWriteScroll(object oMaterialsOwner, object oCrafter, int nSpell, int nVerifyResourcePoints=TRUE);

// Creates a wand of nSpell with d20 + oCaster's casting level number of charges.
// All conditions checked by CanCraftWand must be met or nothing will happen.
// Uses up required components and a spell slot of oCrafter with nSpell / capable of casting nSpell and plays a casting animation.
void CraftWand(object oMaterialsOwner, object oCrafter, int nSpell);

// Brews a potion of nSpell.
// All conditions checked by CanBrewPotion must be met or nothing will happen.
// Uses up required components and a spell slot of oCrafter with nSpell / capable of casting nSpell and plays a casting animation.
void BrewPotion(object oMaterialsOwner, object oCrafter, int nSpell);

// Writes a scroll of nSpell.
// All conditions checked by CanWriteScroll must be met or nothing will happen.
// Uses up required components and a spell slot of oCrafter with nSpell / capable of casting nSpell and plays a casting animation.
void WriteScroll(object oMaterialsOwner, object oCrafter, int nSpell);

// Attempts to craft an item from a recipe in oMaterialsOwner's inventory.
// oMaterialsOwner must possess all components required by the recipe
// and pass a crafting skill test with DC defined by the recipe.
// Failing the test will destroy the components without creating the item.
// The test itself is a party check, i.e. the best skill rank among oMaterialsOwner and their associates is used.
// Recipe must be an object with various parameters defined via local variables,
// as noted below:
// CRAFT_RESREF - string denoting the item to be crafted from the recipe
// CRAFT_RESREF_NUM - optional integer denoting the number of items with resref CRAFT_RESREF that will be created from the recipe. If not given, defaults to 1.
// CRAFT_SKILL - integer denoting the crafting skill required: 26 for Craft Weapon, 25 for Craft Armor and 22 for Craft Trap
// CRAFT_DC - integer denoting the DC of the skill check
// COMPONENT_X - string denoting the tag of x-th required item, where X follows the progression from 1 to any number;
//               for example, a recipe's component tags can be defined in string variables COMPONENT_1, COMPONENT_2 and COMPONENT_3;
//               ensure there are no breaks, i.e. COMPONENT_3 won't be respected if there is no COMPONENT_2 defined.
// COMPONENT_X_NUM - optional integer denoting the number of items with tag COMPONENT_X required. If not given, defaults to 1.
// Function exit codes:
// 0 - oRecipe is an invalid recipe (not all parameter variables are properly defined)
// 1 - oMaterialsOwner doesn't have all required components
// 2 - Skill check failed (components destroyed)
// 3 - Skill check successful (item created)
int CraftItemFromRecipe(object oMaterialsOwner, object oRecipe);

void SetCrafterCreature(object oPC, object oCreature)
{
    SetLocalObject(oPC, "Crafter_Object", oCreature);
}

object GetCrafterCreature(object oPC)
{
    object crafter = GetLocalObject(oPC, "Crafter_Object");
    if (!GetIsObjectValid(crafter))
        crafter = oPC;
    return crafter;
}

int AddResourceToWandCraftingPool(object oPC, string sResourceTag)
{
    object item = GetItemPossessedBy(oPC, sResourceTag);
    if (!GetIsObjectValid(item))
        return FALSE;

    string array = "WAND_CRAFT_POOL";
    if (GetStringArraySize(array, oPC) == 0)
    {
        CreateStringArray(array, 0, oPC);
        DeleteLocalInt(oPC, array);
    }

    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int resourceValue = GetResourceUnitsOfItemForWandCrafting(realmScript, sResourceTag);
    if (resourceValue <= 0)
        return FALSE;

    int size = GetItemStackSize(item);
    if (size == 1)
        DestroyObject(item);
    else
        SetItemStackSize(item, size-1);

    AddStringArrayElement(array, sResourceTag, FALSE, oPC);
    AddToLocalInt(oPC, array, resourceValue);

    return TRUE;
}

int AddResourceToPotionBrewingPool(object oPC, string sResourceTag)
{
    object item = GetItemPossessedBy(oPC, sResourceTag);
    if (!GetIsObjectValid(item))
        return FALSE;

    string array = "POTION_CRAFT_POOL";
    if (GetStringArraySize(array, oPC) == 0)
    {
        CreateStringArray(array, 0, oPC);
        DeleteLocalInt(oPC, array);
    }

    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int resourceValue = GetResourceUnitsOfItemForPotionBrewing(realmScript, sResourceTag);
    if (resourceValue == 0)
        return FALSE;

    int size = GetItemStackSize(item);
    if (size == 1)
        DestroyObject(item);
    else
        SetItemStackSize(item, size-1);

    AddStringArrayElement(array, sResourceTag, FALSE, oPC);
    AddToLocalInt(oPC, array, resourceValue);

    return TRUE;
}

int AddResourceToScrollWritingPool(object oPC, string sResourceTag)
{
    object item = GetItemPossessedBy(oPC, sResourceTag);
    if (!GetIsObjectValid(item))
        return FALSE;

    string array = "SCROLL_CRAFT_POOL";
    if (GetStringArraySize(array, oPC) == 0)
    {
        CreateStringArray(array, 0, oPC);
        DeleteLocalInt(oPC, array);
    }

    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int resourceValue = GetResourceUnitsOfItemForScrollScribing(realmScript, sResourceTag);
    if (resourceValue == 0)
        return FALSE;

    int size = GetItemStackSize(item);
    if (size == 1)
        DestroyObject(item);
    else
        SetItemStackSize(item, size-1);

    AddStringArrayElement(array, sResourceTag, FALSE, oPC);
    AddToLocalInt(oPC, array, resourceValue);

    return TRUE;
}

void ClearWandCraftingPool(object oPC)
{
    int i;
    string array = "WAND_CRAFT_POOL";
    for (i = 0; i < GetStringArraySize(array, oPC); i++)
    {
        string tag = GetStringArrayElement(array, i, oPC);
        CreateItemOnObject(tag, oPC);
    }
    ClearStringArray(array, oPC);
    DeleteLocalInt(oPC, array);
}

void ClearPotionBrewingPool(object oPC)
{
    int i;
    string array = "POTION_CRAFT_POOL";
    for (i = 0; i < GetStringArraySize(array, oPC); i++)
    {
        string tag = GetStringArrayElement(array, i, oPC);
        CreateItemOnObject(tag, oPC);
    }
    ClearStringArray(array, oPC);
    DeleteLocalInt(oPC, array);
}

void ClearScrollWritingPool(object oPC)
{
    int i;
    string array = "SCROLL_CRAFT_POOL";
    for (i = 0; i < GetStringArraySize(array, oPC); i++)
    {
        string tag = GetStringArrayElement(array, i, oPC);
        CreateItemOnObject(tag, oPC);
    }
    ClearStringArray(array, oPC);
    DeleteLocalInt(oPC, array);
}

void ConsumeItemsFromPotionBrewingPool(object oPC, int nResourcePointsToConsume)
{
    if (GetPotionBrewingPoolValue(oPC) < nResourcePointsToConsume)
        return;

    int i;
    string array = "POTION_CRAFT_POOL";
    int pointsConsumed = 0;
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    for (i = GetStringArraySize(array, oPC)-1; i >= 0; i--)
    {
        string tag = GetStringArrayElement(array, i, oPC);
        int resourceValue = GetResourceUnitsOfItemForPotionBrewing(realmScript, tag);
        pointsConsumed += resourceValue;
        SubtractFromLocalInt(oPC, array, resourceValue);
        DeleteStringArrayElement(array, i, oPC);
        if (pointsConsumed >= nResourcePointsToConsume)
            return;
    }
}

void ConsumeItemsFromScrollWritingPool(object oPC, int nResourcePointsToConsume)
{
    if (GetScrollWritingPoolValue(oPC) < nResourcePointsToConsume)
        return;

    int i;
    string array = "SCROLL_CRAFT_POOL";
    int pointsConsumed = 0;
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    for (i = GetStringArraySize(array, oPC)-1; i >= 0; i--)
    {
        string tag = GetStringArrayElement(array, i, oPC);
        int resourceValue = GetResourceUnitsOfItemForScrollScribing(realmScript, tag);
        pointsConsumed += resourceValue;
        SubtractFromLocalInt(oPC, array, resourceValue);
        DeleteStringArrayElement(array, i, oPC);
        if (pointsConsumed >= nResourcePointsToConsume)
            return;
    }
}

void ConsumeItemsFromWandCraftingPool(object oPC, int nResourcePointsToConsume)
{
    if (GetWandCraftingPoolValue(oPC) < nResourcePointsToConsume)
        return;

    int i;
    string array = "WAND_CRAFT_POOL";
    int pointsConsumed = 0;
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    for (i = GetStringArraySize(array, oPC)-1; i >= 0; i--)
    {
        string tag = GetStringArrayElement(array, i, oPC);
        int resourceValue = GetResourceUnitsOfItemForWandCrafting(realmScript, tag);
        pointsConsumed += resourceValue;
        SubtractFromLocalInt(oPC, array, resourceValue);
        DeleteStringArrayElement(array, i, oPC);
        if (pointsConsumed >= nResourcePointsToConsume)
            return;
    }
}

int GetWandCraftingPoolValue(object oPC)
{
    return GetLocalInt(oPC, "WAND_CRAFT_POOL");
}

int GetPotionBrewingPoolValue(object oPC)
{
    return GetLocalInt(oPC, "POTION_CRAFT_POOL");
}

int GetScrollWritingPoolValue(object oPC)
{
    return GetLocalInt(oPC, "SCROLL_CRAFT_POOL");
}

struct CasterClassAndSpellLevel _GuessCasterClassAndSpellLevel(object oCaster, int nSpell)
{
    struct CasterClassAndSpellLevel result;

    int wizardLevel = GetLevelByClass(CLASS_TYPE_WIZARD, oCaster);
    int sorcererLevel = GetLevelByClass(CLASS_TYPE_SORCERER, oCaster);
    int bardLevel = GetLevelByClass(CLASS_TYPE_BARD, oCaster);
    int clericLevel = GetLevelByClass(CLASS_TYPE_CLERIC, oCaster);
    int paladinLevel = GetLevelByClass(CLASS_TYPE_PALADIN, oCaster);
    int druidLevel = GetLevelByClass(CLASS_TYPE_DRUID, oCaster);
    int rangerLevel = GetLevelByClass(CLASS_TYPE_RANGER, oCaster);

    //If caster only has one casting class, then that class must be in the first slot and we can be sure it's the casting class used to cast the spell
    if (GetLevelByPosition(1, oCaster) == wizardLevel + sorcererLevel + bardLevel + clericLevel + paladinLevel + druidLevel + rangerLevel)
    {
        result.casterClass = GetClassByPosition(1, oCaster);

        string spellTableCol = Get2DAString("classes", "SpellTableColumn", result.casterClass);
        string spellLvlString = Get2DAString("spells", spellTableCol, nSpell);

        result.spellLvl = StringToInt(spellLvlString);
        return result;
    }

    //Otherwise, let's check the possible caster classes of nSpell out of the caster's classes
    int i;
    CreateIntArray("PotentialCasters");
    for (i = 1; i <= 3; i++)
    {
        int class = GetClassByPosition(i, oCaster);
        if (class == CLASS_TYPE_INVALID)
            break;

        string spellTableCol = Get2DAString("classes", "SpellTableColumn", class);
        string spellLvlString = Get2DAString("spells", spellTableCol, nSpell);
        if (spellLvlString == "****")
            continue;

        int spellLvl = StringToInt(spellLvlString);
        int classLvl = GetLevelByPosition(i, oCaster);
        int maxCastableSpellLvl = 0;
        switch (class)
        {
            case CLASS_TYPE_WIZARD:
            case CLASS_TYPE_CLERIC:
            case CLASS_TYPE_DRUID:
                maxCastableSpellLvl = classLvl / 2 + classLvl % 2;
                break;
            case CLASS_TYPE_SORCERER:
                maxCastableSpellLvl = classLvl / 2;
                break;
            case CLASS_TYPE_RANGER:
            case CLASS_TYPE_PALADIN:
                maxCastableSpellLvl = classLvl < 10 ? classLvl / 4 : classLvl / 3;
                break;
            case CLASS_TYPE_BARD:
                maxCastableSpellLvl = classLvl % 3 == 0 ? classLvl / 3 : classLvl / 3 + 1;
                break;
        }
        if (maxCastableSpellLvl < spellLvl)
            continue;

        AddIntArrayElement("PotentialCasters", class);
    }

    //If we have multiple candidates, just take the class with the lowest spell level
    //in order to minimize resources needed (the only instance in which our guess may turn to be wrong)
    int lowestSpellLevel = 10;
    int selectedClass = CLASS_TYPE_INVALID;
    for (i = 0; i < GetIntArraySize("PotentialCasters"); i++)
    {
        int class = GetIntArrayElement("PotentialCasters", i);
        string spellTableCol = Get2DAString("classes", "SpellTableColumn", class);
        string spellLvlString = Get2DAString("spells", spellTableCol, nSpell);
        int spellLvl = StringToInt(spellLvlString);
        if (spellLvl < lowestSpellLevel)
        {
            selectedClass = class;
            lowestSpellLevel = spellLvl;
        }
    }
    ClearIntArray("PotentialCasters");

    result.casterClass = selectedClass;
    result.spellLvl = lowestSpellLevel;
    return result;
}

int _GetIsSpellHostile(int nSpell)
{
    string hostileSetting = Get2DAString("spells", "HostileSetting", nSpell);
    if (hostileSetting == "1")
        return TRUE;
    return FALSE;
}

int _GetCanSpellTargetCaster(int nSpell)
{
    string targetType = Get2DAString("spells", "TargetType", nSpell);
    int targetTypeNumber = HexStringToInt(targetType);
    return targetTypeNumber & 1;
}

int _GetIsCastSpellConstantCompatibleWithPotions(int nCastSpellConstant)
{
    string compatibleString = Get2DAString("iprp_spells", "PotionUse", nCastSpellConstant);
    if (compatibleString == "1")
        return TRUE;
    return FALSE;
}

int _GetIsCastSpellConstantCompatibleWithWands(int nCastSpellConstant)
{
    string compatibleString = Get2DAString("iprp_spells", "WandUse", nCastSpellConstant);
    if (compatibleString == "1")
        return TRUE;
    return FALSE;
}

int _SpellToCastSpellConstant(int nSpell, int nCasterLvl)
{
    int i;
    int selectedIpSpell = -1;
    int closestCasterLvl = 99;
    for (i = 0; i < 600; i++)
    {
        string spellIndex = Get2DAString("iprp_spells", "SpellIndex", i);
        if (spellIndex != IntToString(nSpell))
            continue;

        //Find cast spell item property with caster level closest to nCasterLvl - preferrably lower than nCasterLvl, if possible
        string casterLvlString = Get2DAString("iprp_spells", "CasterLvl", i);
        int casterLvl = StringToInt(casterLvlString);
        if (closestCasterLvl <= nCasterLvl)
        {
            if (casterLvl <= nCasterLvl && casterLvl > closestCasterLvl)
            {
                closestCasterLvl = casterLvl;
                selectedIpSpell = i;
            }
        }
        else
        {
            if (casterLvl < closestCasterLvl)
            {
                closestCasterLvl = casterLvl;
                selectedIpSpell = i;
            }
        }
    }

    if (selectedIpSpell == -1)
        LogWarning("_SpellToCastSpellConstant returned -1 for nSpell = %n, nCasterLvl = %n", nSpell, nCasterLvl);
    return selectedIpSpell;
}

int CanCraftWand(object oMaterialsOwner, object oCrafter, int nSpell, int nVerifyResourcePoints=TRUE)
{
    if (!GetHasSpell(nSpell, oCrafter) || !GetHasFeat(FEAT_CRAFT_WAND, oCrafter))
        return FALSE;

    if (!GetIsObjectValid(GetItemPossessedBy(oMaterialsOwner, "it_bonewand")))
        return FALSE;

    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    struct CasterClassAndSpellLevel classAndSpellLvl = _GuessCasterClassAndSpellLevel(oCrafter, nSpell);
    int requiredResourcePoints = GetResourceUnitsRequiredToCraftWand(realmScript, nSpell, classAndSpellLvl.spellLvl);
    if (GetWandCraftingPoolValue(oMaterialsOwner) < requiredResourcePoints && nVerifyResourcePoints)
        return FALSE;

    int casterLevel = GetLevelByClass(classAndSpellLvl.casterClass, oCrafter);
    int castSpellConstant = _SpellToCastSpellConstant(nSpell, casterLevel);
    if (!_GetIsCastSpellConstantCompatibleWithWands(castSpellConstant))
        return FALSE;

    return TRUE;
}

int CanBrewPotion(object oMaterialsOwner, object oCrafter, int nSpell, int nVerifyResourcePoints=TRUE)
{
    if (!GetHasSpell(nSpell, oCrafter) || !GetHasFeat(FEAT_BREW_POTION, oCrafter))
        return FALSE;

    if (_GetIsSpellHostile(nSpell) || !_GetCanSpellTargetCaster(nSpell))
        return FALSE;

    if (!GetIsObjectValid(GetItemPossessedBy(oMaterialsOwner, "it_emptybottle")))
        return FALSE;

    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    struct CasterClassAndSpellLevel classAndSpellLvl = _GuessCasterClassAndSpellLevel(oCrafter, nSpell);
    int requiredResourcePoints = GetResourceUnitsRequiredToBrewPotion(realmScript, nSpell, classAndSpellLvl.spellLvl);
    if (GetPotionBrewingPoolValue(oMaterialsOwner) < requiredResourcePoints && nVerifyResourcePoints)
        return FALSE;

    int casterLevel = GetLevelByClass(classAndSpellLvl.casterClass, oCrafter);
    int castSpellConstant = _SpellToCastSpellConstant(nSpell, casterLevel);
    if (!_GetIsCastSpellConstantCompatibleWithPotions(castSpellConstant))
        return FALSE;

    return TRUE;
}

int CanWriteScroll(object oMaterialsOwner, object oCrafter, int nSpell, int nVerifyResourcePoints=TRUE)
{
    if (!GetHasSpell(nSpell, oCrafter) || !GetHasFeat(FEAT_SCRIBE_SCROLL, oCrafter))
        return FALSE;

    if (!GetIsObjectValid(GetItemPossessedBy(oMaterialsOwner, "it_blankscroll")))
        return FALSE;

    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    struct CasterClassAndSpellLevel classAndSpellLvl = _GuessCasterClassAndSpellLevel(oCrafter, nSpell);
    int requiredResourcePoints = GetResourceUnitsRequiredToScribeScroll(realmScript, nSpell, classAndSpellLvl.spellLvl);
    if (GetScrollWritingPoolValue(oMaterialsOwner) < requiredResourcePoints && nVerifyResourcePoints)
        return FALSE;

    return TRUE;
}

void CraftWand(object oMaterialsOwner, object oCrafter, int nSpell)
{
    if (!CanCraftWand(oMaterialsOwner, oCrafter, nSpell))
        return;

    struct CasterClassAndSpellLevel classAndSpellLvl = _GuessCasterClassAndSpellLevel(oCrafter, nSpell);
    int casterLevel = GetLevelByClass(classAndSpellLvl.casterClass, oCrafter);
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int requiredResourcePoints = GetResourceUnitsRequiredToCraftWand(realmScript, nSpell, classAndSpellLvl.spellLvl);

    //Destroy components
    ConsumeItemsFromWandCraftingPool(oMaterialsOwner, requiredResourcePoints);

    //Destroy base item
    object baseItem = GetItemPossessedBy(oMaterialsOwner, "it_bonewand");
    DestroyObject(baseItem);

    //Create the item
    object crafted = CreateItemOnObject("x2_it_pcwand", oMaterialsOwner);

    //Set wand charges
    int charges = casterLevel + d20();
    if (GetLocalInt(GetModule(), "X2_SWITCH_ENABLE_50_WAND_CHARGES"))
        charges = 50;
    SetItemCharges(crafted, charges);

    //Add item property
    int castSpellConstant = _SpellToCastSpellConstant(nSpell, casterLevel);
    itemproperty ip = ItemPropertyCastSpell(castSpellConstant, IP_CONST_CASTSPELL_NUMUSES_1_CHARGE_PER_USE);
    AddItemProperty(DURATION_TYPE_PERMANENT, ip, crafted);

    //Add class restrictions
    if (classAndSpellLvl.casterClass == CLASS_TYPE_WIZARD
            || classAndSpellLvl.casterClass == CLASS_TYPE_SORCERER
            || classAndSpellLvl.casterClass == CLASS_TYPE_BARD)
    {
        itemproperty restriction = ItemPropertyLimitUseByClass(IP_CONST_CLASS_BARD);
        AddItemProperty(DURATION_TYPE_PERMANENT, restriction, crafted);
        restriction = ItemPropertyLimitUseByClass(IP_CONST_CLASS_WIZARD);
        AddItemProperty(DURATION_TYPE_PERMANENT, restriction, crafted);
        restriction = ItemPropertyLimitUseByClass(IP_CONST_CLASS_SORCERER);
        AddItemProperty(DURATION_TYPE_PERMANENT, restriction, crafted);
    }
    else
    {
        itemproperty restriction = ItemPropertyLimitUseByClass(IP_CONST_CLASS_PALADIN);
        AddItemProperty(DURATION_TYPE_PERMANENT, restriction, crafted);
        restriction = ItemPropertyLimitUseByClass(IP_CONST_CLASS_RANGER);
        AddItemProperty(DURATION_TYPE_PERMANENT, restriction, crafted);
        restriction = ItemPropertyLimitUseByClass(IP_CONST_CLASS_CLERIC);
        AddItemProperty(DURATION_TYPE_PERMANENT, restriction, crafted);
        restriction = ItemPropertyLimitUseByClass(IP_CONST_CLASS_DRUID);
        AddItemProperty(DURATION_TYPE_PERMANENT, restriction, crafted);
    }

    //Get subspell as a fake spell to cast if the spell has any
    string subSpell = Get2DAString("spells", "SubRadSpell1", nSpell);
    int spellToCast = subSpell == "" ? nSpell : StringToInt(subSpell);

    //Simulate spell being cast
    DecrementRemainingSpellUses(oCrafter, nSpell);
    AssignCommand(oCrafter, ClearAllActions());
    AssignCommand(oCrafter, ActionCastFakeSpellAtObject(spellToCast, oCrafter));
}

void BrewPotion(object oMaterialsOwner, object oCrafter, int nSpell)
{
    if (!CanBrewPotion(oMaterialsOwner, oCrafter, nSpell))
        return;

    struct CasterClassAndSpellLevel classAndSpellLvl = _GuessCasterClassAndSpellLevel(oCrafter, nSpell);
    int casterLevel = GetLevelByClass(classAndSpellLvl.casterClass, oCrafter);
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int requiredResourcePoints = GetResourceUnitsRequiredToBrewPotion(realmScript, nSpell, classAndSpellLvl.spellLvl);

    //Destroy components
    ConsumeItemsFromPotionBrewingPool(oMaterialsOwner, requiredResourcePoints);

    //Destroy base item
    object baseItem = GetItemPossessedBy(oMaterialsOwner, "it_emptybottle");
    DestroyObject(baseItem);

    //Create the item
    object crafted = CreateItemOnObject("x2_it_pcpotion", oMaterialsOwner);

    //Add item property
    int castSpellConstant = _SpellToCastSpellConstant(nSpell, casterLevel);
    itemproperty ip = ItemPropertyCastSpell(castSpellConstant, IP_CONST_CASTSPELL_NUMUSES_SINGLE_USE);
    AddItemProperty(DURATION_TYPE_PERMANENT, ip, crafted);

    //Get subspell as a fake spell to cast if the spell has any
    string subSpell = Get2DAString("spells", "SubRadSpell1", nSpell);
    int spellToCast = subSpell == "" ? nSpell : StringToInt(subSpell);

    //Simulate spell being cast
    DecrementRemainingSpellUses(oCrafter, nSpell);
    AssignCommand(oCrafter, ClearAllActions());
    AssignCommand(oCrafter, ActionCastFakeSpellAtObject(spellToCast, oCrafter));
}

void WriteScroll(object oMaterialsOwner, object oCrafter, int nSpell)
{
    if (!CanWriteScroll(oMaterialsOwner, oCrafter, nSpell))
        return;

    struct CasterClassAndSpellLevel classAndSpellLvl = _GuessCasterClassAndSpellLevel(oCrafter, nSpell);
    int casterLevel = GetLevelByClass(classAndSpellLvl.casterClass, oCrafter);
    object realm = GetRealm();
    string realmScript = GetRealmScript(realm);
    int requiredResourcePoints = GetResourceUnitsRequiredToScribeScroll(realmScript, nSpell, classAndSpellLvl.spellLvl);

    //Destroy components
    ConsumeItemsFromScrollWritingPool(oMaterialsOwner, requiredResourcePoints);

    //Destroy base item
    object baseItem = GetItemPossessedBy(oMaterialsOwner, "it_blankscroll");
    DestroyObject(baseItem);

    //Get class string for des_crft_scroll.2da
    string class = "";
    switch (classAndSpellLvl.casterClass)
    {
        case CLASS_TYPE_WIZARD:
        case CLASS_TYPE_SORCERER:
            class = "Wiz_Sorc";
            break;
        case CLASS_TYPE_CLERIC:
            class = "Cleric";
            break;
        case CLASS_TYPE_PALADIN:
            class = "Paladin";
            break;
        case CLASS_TYPE_DRUID:
            class = "Druid";
            break;
        case CLASS_TYPE_RANGER:
            class = "Ranger";
            break;
        case CLASS_TYPE_BARD:
            class = "Bard";
            break;
    }

    //Create the item
    string resref = Get2DAString("des_crft_scroll", class, nSpell);
    object crafted = CreateItemOnObject(resref, oMaterialsOwner);

    //Get subspell as a fake spell to cast if the spell has any
    string subSpell = Get2DAString("spells", "SubRadSpell1", nSpell);
    int spellToCast = subSpell == "" ? nSpell : StringToInt(subSpell);

    //Simulate spell being cast
    DecrementRemainingSpellUses(oCrafter, nSpell);
    AssignCommand(oCrafter, ClearAllActions());
    AssignCommand(oCrafter, ActionCastFakeSpellAtObject(spellToCast, oCrafter));
}

int _CountItemsPossessedBy(object oCreature, string sTag)
{
    if (GetItemPossessedBy(oCreature, sTag) == OBJECT_INVALID)
        return 0;

    int num = 0;
    object item = GetFirstItemInInventory(oCreature);
    while (GetIsObjectValid(item))
    {
        if (GetTag(item) == sTag)
            num += GetItemStackSize(item);
        item = GetNextItemInInventory(oCreature);
    }
    return num;
}

void _DestroyNumberOfItemsPossessedBy(object oCreature, string sTag, int nNumber)
{
    object item = GetFirstItemInInventory(oCreature);
    while (GetIsObjectValid(item))
    {
        if (GetTag(item) == sTag)
        {
            int stackSize = GetItemStackSize(item);
            if (stackSize <= nNumber)
            {
                DestroyObject(item);
                if (stackSize == nNumber)
                    return;
                nNumber -= stackSize;
            }
            else
            {
                SetItemStackSize(item, stackSize-nNumber);
                return;
            }
        }
        item = GetNextItemInInventory(oCreature);
    }
}

int CraftItemFromRecipe(object oMaterialsOwner, object oRecipe)
{
    int dc = GetLocalInt(oRecipe, "CRAFT_DC");
    int skill = GetLocalInt(oRecipe, "CRAFT_SKILL");
    string resref = GetLocalString(oRecipe, "CRAFT_RESREF");
    int resrefNum = GetLocalInt(oRecipe, "CRAFT_RESREF_NUM");
    if (resrefNum == 0)
        resrefNum = 1;

    //Verify the recipe is properly defined
    if (resref == "" || dc == 0 || (skill != SKILL_CRAFT_ARMOR && skill != SKILL_CRAFT_WEAPON && skill != SKILL_CRAFT_TRAP))
        return 0;

    //Get all required components and stop the process if the crafter doesn't have everything required
    CreateStringArray("CraftingComponents");
    CreateIntArray("CraftingComponentsNums");
    int i = 1;
    while (TRUE)
    {
        string varName = "COMPONENT_" + IntToString(i);
        string varNumName = varName + "_NUM";
        string tag = GetLocalString(oRecipe, varName);
        if (tag == "")
            break;

        int num = GetLocalInt(oRecipe, varNumName);
        if (num <= 0)
            num = 1;

        int itemsPossessed = _CountItemsPossessedBy(oMaterialsOwner, tag);
        if (itemsPossessed < num)
        {
            ClearStringArray("CraftingComponents");
            ClearIntArray("CraftingComponentsNums");
            return 1;
        }

        AddStringArrayElement("CraftingComponents", tag);
        AddIntArrayElement("CraftingComponentsNums", num);
        i++;
    }

    //Destroy required components
    for (i = 0; i < GetStringArraySize("CraftingComponents"); i++)
    {
        string tag = GetStringArrayElement("CraftingComponents", i);
        int num = GetIntArrayElement("CraftingComponentsNums", i);
        _DestroyNumberOfItemsPossessedBy(oMaterialsOwner, tag, num);
    }

    //Clear arrays
    ClearStringArray("CraftingComponents");
    ClearIntArray("CraftingComponentsNums");

    //Roll for the skill
    int skillCheck = GetIsPartySkillSuccessful(oMaterialsOwner, skill, dc);
    if (!skillCheck)
        return 2;

    //Create item(s)
    object item = CreateItemOnObject(resref, oMaterialsOwner, resrefNum);
    SetIdentified(item, TRUE);
    return 3;
}
