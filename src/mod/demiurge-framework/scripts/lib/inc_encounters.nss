#include "inc_debug"
#include "inc_hndvars"
#include "inc_arrays"
#include "inc_tiles"
#include "inc_biomes"

//Library for using various encounter implementations dynamically

// Getter for the encounter script's regularQuestsNumber field
int GetNumberOfEncounterRegularQuests(string sEncounterScript);

// Getter for the encounter script's specialtyQuestsNumber field
int GetNumberOfEncounterSpecialtyQuests(string sEncounterScript);

// Getter for the encounter script's championsNumber field
int GetNumberOfEncounterChampions(string sEncounterScript);

// Getter for the encounter script's minPlayerLevel field
int GetEncounterMinPlayerLevel(string sEncounterScript);

// Getter for the encounter script's maxPlayerLevel field
int GetEncounterMaxPlayerLevel(string sEncounterScript);

// Spawns an encounter in a designated area for a given encounter waypoint set number for a given number ofplayers at a given level
void SpawnEncounter(object oEncounter, string sSeedName);

// Returns the encounter's regular quest script name corresponding to index nIndex in range [0, regularQuestsNumber-1]
string GetEncounterRegularQuestScript(string sEncounterScript, int nIndex);

// Returns the encounter's specialty quest script name corresponding to index nIndex in range [0, specialtyQuestsNumber-1]
string GetEncounterSpecialtyQuestScript(string sEncounterScript, int nIndex);

// Getter for the encounter's rumorsNumber field
int GetEncounterRumorsNumber(string sEncounterScript);

// Returns the encounter's rumor corresponding to index nIndex in range [0, rumorsNumber-1]
string GetEncounterRumor(object oEncounter, int nIndex, int nLanguage);

// Returns TRUE if the given encounter script has a low quest generation priority,
// in which case it will not generate quests unless only encounters of low quest generation priority are available
int GetEncounterHasLowQuestGenerationPriority(string sEncounterScript);

// Returns the encounter's champion script name corresponding to index nIndex in range [0, championsNumber-1]
string GetEncounterChampion(object oEncounter, int nIndex);

//Instance functions

// Encounter object constructor - creates an encounter object
object CreateEncounterInstance(string sEncounterScript, int nLowQuestGenerationPriority, int nEnRouteToTown, object oRealm, object oEncounterArea, int nLevel);

// Returns an object representation of a quest that was generated for this encounter (OBJECT_INVALID if no quest was generated)
object GetQuestOfEncounter(object oEncounter);

// Returns a champion creature spawned for this encounter (OBJECT_INVALID if no champion was generated)
object GetChampionOfEncounter(object oEncounter);

// Returns a trophy item of the champion creature spawned for this encounter (OBJECT_INVALID if no champion was generated or there is no bounty on it)
object GetTrophyOfEncounter(object oEncounter);

// Returns the given encounter's area
object GetEncounterArea(object oEncounter);

// Returns the given encounter's script name
string GetEncounterScript(object oEncounter);

// Returns the given encounter's realm object
object GetEncounterRealm(object oEncounter);

// Sets a generated quest represented as an object as this encounter's quest
void SetQuestOfEncounter(object oEncounter, object oQuest);

// Sets a champion creature of this encounter
void SetChampionOfEncounter(object oEncounter, object oChampion);

// Returns the given encounter's designated PC level
int GetEncounterLevel(object oEncounter);

// Returns a location in bounds defined by the eight encounter waypoints (which themselves should form a square of size 12.0x12.0).
// fX and fY are internal coordinates of the encounter area and can have values between 0.0f and 12.0f inclusive.
// The south-western waypoint, i.e. the lower left corner of the square, is located at [0.0, 0.0] and the north-eastern one is located at [12.0, 12.0].
// For example, in order to retrieve the center of the encounter area, use coordinates [6.0, 6.0].
// fOrientation is the orientation angle expressed in degrees between 0.0 and 360.0 inclusive with 0.0 pointing north.
location GetLocationWithinEncounterBounds(object oEncounter, float fX, float fY, float fOrientation=0.0f);


//Script functions
int GetNumberOfEncounterRegularQuests(string sEncounterScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sEncounterScript);
    return GetLocalInt(mod, funcResult);
}

int GetNumberOfEncounterSpecialtyQuests(string sEncounterScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 8);
    ExecuteScript(sEncounterScript);
    return GetLocalInt(mod, funcResult);
}

int GetEncounterMinPlayerLevel(string sEncounterScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sEncounterScript);
    return GetLocalInt(mod, funcResult);
}

int GetEncounterMaxPlayerLevel(string sEncounterScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sEncounterScript);
    return GetLocalInt(mod, funcResult);
}

void SpawnEncounter(object oEncounter, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    SetLocalObject(mod, funcArg1, oEncounter);
    SetLocalString(mod, funcArg2, sSeedName);
    string encounterScript = GetEncounterScript(oEncounter);
    ExecuteScript(encounterScript);
}

string GetEncounterRegularQuestScript(string sEncounterScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sEncounterScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid regular quest script name returned in encounter " + sEncounterScript + ", nIndex = %n", nIndex);
    return result;
}

string GetEncounterSpecialtyQuestScript(string sEncounterScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 9);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sEncounterScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid specialty quest script name returned in encounter " + sEncounterScript + ", nIndex = %n", nIndex);
    return result;
}

int GetEncounterRumorsNumber(string sEncounterScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 5);
    ExecuteScript(sEncounterScript);
    return GetLocalInt(mod, funcResult);
}

string GetEncounterRumor(object oEncounter, int nIndex, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 6);
    SetLocalObject(mod, funcArg1, oEncounter);
    SetLocalInt(mod, funcArg2, nIndex);
    SetLocalInt(mod, funcArg3, nLanguage);
    string sEncounterScript = GetEncounterScript(oEncounter);
    ExecuteScript(sEncounterScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid rumor returned in encounter " + sEncounterScript + ", nIndex = %n, nLanguage = %n", nIndex, nLanguage);
    return result;
}

int GetEncounterHasLowQuestGenerationPriority(string sEncounterScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    ExecuteScript(sEncounterScript);
    return GetLocalInt(mod, funcResult);
}

int GetNumberOfEncounterChampions(string sEncounterScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 10);
    ExecuteScript(sEncounterScript);
    return GetLocalInt(mod, funcResult);
}

string GetEncounterChampion(object oEncounter, int nIndex)
{
    object mod = GetModule();
    string sEncounterScript = GetEncounterScript(oEncounter);
    SetLocalInt(mod, funcHandler, 11);
    SetLocalObject(mod, funcArg1, oEncounter);
    SetLocalInt(mod, funcArg2, nIndex);
    ExecuteScript(sEncounterScript);
    return GetLocalString(mod, funcResult);
}

//Instance functions

object _GetAreaWaypoint(object oArea, string sWaypointTag)
{
    object obj = GetFirstObjectInArea(oArea);
    while (GetIsObjectValid(obj))
    {
        if (GetObjectType(obj) == OBJECT_TYPE_WAYPOINT && GetTag(obj) == sWaypointTag)
            return obj;
        obj = GetNextObjectInArea(oArea);
    }
    return OBJECT_INVALID;
}

object _GetEncounterWaypoint(object oEncounter, string sEncounterWaypointConstant)
{
    object oRegularArea = GetEncounterArea(oEncounter);
    string resref = "wp_encounter_" + sEncounterWaypointConstant;
    object wp = _GetAreaWaypoint(oRegularArea, resref);
    if (wp == OBJECT_INVALID)
        LogWarning("Invalid waypoint with ResRef: " + resref + ", area ResRef: " + GetResRef(oRegularArea));
    return wp;
}

location GetLocationWithinEncounterBounds(object oEncounter, float fX, float fY, float fOrientation=0.0f)
{
    location initialLoc = GetLocation(_GetEncounterWaypoint(oEncounter, "sw"));

    if (fX < 0.0 || fY < 0.0)
    {
        LogWarning("Invalid (negative in either X or Y axis) encounter location, encounter script: " + GetEncounterScript(oEncounter));
        return initialLoc;
    }

    if (fX > 12.0f || fY > 12.0f)
    {
        LogWarning("Invalid (over 12.0f in either X or Y axis) encounter location, encounter script: " + GetEncounterScript(oEncounter));
        return initialLoc;
    }

    vector vec = GetPositionFromLocation(initialLoc);
    vec = Vector(vec.x + fX, vec.y + fY, vec.z);

    object area = GetEncounterArea(oEncounter);
    location result = Location(area, vec, fOrientation);

    return result;
}


//Constructor
object CreateEncounterInstance(string sEncounterScript, int nLowQuestGenerationPriority, int nEnRouteToTown, object oRealm, object oEncounterArea, int nLevel)
{
    object encounterObject = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_invisible", GetStartingLocation());
    SetLocalString(encounterObject, "ENCOUNTER_SCRIPT", sEncounterScript);
    SetLocalObject(encounterObject, "ENCOUNTER_AREA", oEncounterArea);
    SetLocalInt(encounterObject, "ENCOUNTER_LEVEL", nLevel);
    SetLocalObject(encounterObject, "ENCOUNTER_REALM", oRealm);

    AddObjectArrayElement(TILE_ENCOUNTERS_ARRAY, encounterObject, FALSE, oEncounterArea);
    object biome = GetTileBiome(oEncounterArea);
    string encountersForQuestsArray;
    if (!nLowQuestGenerationPriority && !nEnRouteToTown)
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_1;
    else if (nLowQuestGenerationPriority && !nEnRouteToTown)
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_2;
    else if (!nLowQuestGenerationPriority && nEnRouteToTown)
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_3;
    else
        encountersForQuestsArray = BIOME_ENCOUNTERS_WITH_QUEST_PRIORITY_4;
    AddObjectArrayElement(encountersForQuestsArray, encounterObject, FALSE, biome);

    return encounterObject;
}

//Instance functions
object GetQuestOfEncounter(object oEncounter)
{
    return GetLocalObject(oEncounter, "ENCOUNTER_QUEST");
}

object GetChampionOfEncounter(object oEncounter)
{
    return GetLocalObject(oEncounter, "ENCOUNTER_CHAMPION");
}

object GetEncounterArea(object oEncounter)
{
    return GetLocalObject(oEncounter, "ENCOUNTER_AREA");
}

string GetEncounterScript(object oEncounter)
{
    return GetLocalString(oEncounter, "ENCOUNTER_SCRIPT");
}

void SetQuestOfEncounter(object oEncounter, object oQuest)
{
    SetLocalObject(oEncounter, "ENCOUNTER_QUEST", oQuest);
}

void SetChampionOfEncounter(object oEncounter, object oChampion)
{
    SetLocalObject(oEncounter, "ENCOUNTER_CHAMPION", oChampion);
}

int GetEncounterLevel(object oEncounter)
{
    return GetLocalInt(oEncounter, "ENCOUNTER_LEVEL");
}

object GetEncounterRealm(object oEncounter)
{
    return GetLocalObject(oEncounter, "ENCOUNTER_REALM");
}

object GetTrophyOfEncounter(object oEncounter)
{
    return GetLocalObject(oEncounter, "ENCOUNTER_TROPHY");
}