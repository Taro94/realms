#include "inc_debug"
#include "inc_hndvars"
#include "inc_arrays"
#include "inc_tiles"
#include "inc_biomes"
#include "inc_dftokens"
#include "inc_realms"

//Library for using various plot implementations dynamically

struct PlotJournalEntries
{
    string journalName; //journal entry name
    string journalPlotDescription; //every entry's initial line describing the overall plot
    string journalStart; //entry's line displayed at the beginning
    string journalFinishedRegionQuests; //entry's line displayed upon completion of 3 quests in the region, prompting the player to visit the next region
    string journalFailedRegionQuests; //entry's line displayed upon failing to complete 3 quests in the region, prompting the player to visit the next region
    string journalReachedNextVillage; //entry's line displayed upon reaching a new village, prompting the player to attempt some quests there
    string journalFinishedLastRegionQuests; //entry's line displayed upon completion of 3 quests in the final region (containing the boss area), prompting the player to find the boss area
    string journalFailedLastRegionQuests; //entry's line displayed upon failing to complete 3 quests in the final region (containing the boss area), prompting the player to find the boss area
};

const int PLOT_STATE_DOING_QUESTS = 1;
const int PLOT_STATE_DONE_QUESTS = 2;
const int PLOT_STATE_FAILED_QUESTS = 3;


// Getter for the plot script's bossAreasNumber field
int GetNumberOfPlotBossAreas(string sPlotScript);

// Getter for the plot script's encountersNumber field
int GetPlotEncountersNumber(string sPlotScript);

// Getter for the plot script's minPlayerLevel field
int GetPlotMinPlayerLevel(string sPlotScript);

// Getter for the plot script's maxPlayerLevel field
int GetPlotMaxPlayerLevel(string sPlotScript);

// Getter for the plot's rumorsNumber field
int GetPlotRumorsNumber(string sPlotScript);

// Returns a PlotJournalEntries struct (containing all strings to be display in the PC's journal in the plot entry)
// based on a quest object, a seed name and a LANGUAGE_* constant from inc_language
struct PlotJournalEntries GetPlotJournalEntries(object oPlot, string sSeedName, int nLanguage);

// Returns the plot's regular boss area script name corresponding to index nIndex in range [0, bossAreasNumber-1]
string GetPlotBossAreaScript(string sPlotScript, int nIndex);

// Returns the plot's rumor corresponding to index nIndex in range [0, rumorsNumber-1]
string GetPlotRumor(object oPlot, int nIndex, int nLanguage);

// Returns the plot's encounter script corresponding to index nIndex in range [0, encountersNumber-1]
string GetPlotEncounterScript(string sPlotScript, int nIndex);

//Instance functions

// Encounter object constructor - creates a plot object
object CreatePlotInstance(string sPlotScript, object oRealm);

// Returns an object representation of a boss area that was generated for this plot
object GetBossAreaOfPlot(object oPlot);

// Sets a boss area as the plot's boss area
void SetBossAreaOfPlot(object oPlot, object oBossArea);

// Returns the given plot's script name
string GetPlotScript(object oPlot);

// Returns the index of the biome that the PC party has reached
int GetPlotCurrentBiomeIndex(object oPlot);

// Set a plot's journal entries
void SetPlotJournalEntries(object oPlot, struct PlotJournalEntries entries);

// Get a plot's journal entries
struct PlotJournalEntries GetPlotInstanceJournalEntries(object oPlot);

// Returns the plot's current state, which together with the plot's current biome index constitues full information on the game overall progress
int GetPlotState(object oPlot);

// Updates the plot's state and current biome index, updating PCs' journal entries in the process.
// This will work only if the new combination of nState and nBiomeIndex represent a point of further game overall progression,
// for example if the plot's state is DOING_QUESTS and the biome index is 2, you can update it to DONE_QUESTS/FAILED_QUESTS with biome index 2 or higher,
// but you can't update it to any state with biome index lower than 2. Similarly, you can't update from DONE_QUESTS/FAILED_QUESTS with index 2 to DOING_QUESTS with index 2.
void UpdatePlotForward(object oPlot, int nState, int nBiomeIndex);

// Returns a tile object that is the current destination of the plot quest (to be used in map tracking).
// Returns the current biome's village's tile if the plot's state is PLOT_STATE_DOING_QUESTS, otherwise returns the next biome's village's tile
// or (in case of the last biome) the boss area's tile.
object GetCurrentPlotDestinationTile(object oPlot);



//Script functions
int GetNumberOfPlotBossAreas(string sPlotScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 0);
    ExecuteScript(sPlotScript);
    return GetLocalInt(mod, funcResult);
}

int GetPlotEncountersNumber(string sPlotScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 7);
    ExecuteScript(sPlotScript);
    return GetLocalInt(mod, funcResult);
}

int GetPlotMinPlayerLevel(string sPlotScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 1);
    ExecuteScript(sPlotScript);
    return GetLocalInt(mod, funcResult);
}

int GetPlotMaxPlayerLevel(string sPlotScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 2);
    ExecuteScript(sPlotScript);
    return GetLocalInt(mod, funcResult);
}

int GetPlotRumorsNumber(string sPlotScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 3);
    ExecuteScript(sPlotScript);
    return GetLocalInt(mod, funcResult);
}

struct PlotJournalEntries GetPlotJournalEntries(object oPlot, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    string plotScript = GetPlotScript(oPlot);
    SetLocalInt(mod, funcHandler, 4);
    SetLocalObject(mod, funcArg1, oPlot);
    SetLocalString(mod, funcArg2, sSeedName);
    SetLocalInt(mod, funcArg3, nLanguage);
    ExecuteScript(plotScript);

    struct PlotJournalEntries result;
    result.journalName = GetLocalString(mod, funcResult);
    result.journalPlotDescription = GetLocalString(mod, funcResult2);
    result.journalStart = GetLocalString(mod, funcResult3);
    result.journalFinishedRegionQuests = GetLocalString(mod, funcResult4);
    result.journalFailedRegionQuests = GetLocalString(mod, funcResult5);
    result.journalReachedNextVillage = GetLocalString(mod, funcResult6);
    result.journalFinishedLastRegionQuests = GetLocalString(mod, funcResult7);
    result.journalFailedLastRegionQuests = GetLocalString(mod, funcResult8);
    return result;
}

string GetPlotBossAreaScript(string sPlotScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 5);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sPlotScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid boss area script name returned in plot " + sPlotScript + ", nIndex = %n", nIndex);
    return result;
}

string GetPlotRumor(object oPlot, int nIndex, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 6);
    SetLocalObject(mod, funcArg1, oPlot);
    SetLocalInt(mod, funcArg2, nIndex);
    SetLocalInt(mod, funcArg3, nLanguage);
    string plotScript = GetPlotScript(oPlot);
    ExecuteScript(plotScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid rumor returned in plot " + plotScript + ", nIndex = %n, nLanguage = %n", nIndex, nLanguage);
    return result;
}

string GetPlotEncounterScript(string sPlotScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 8);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sPlotScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Plot " + sPlotScript + " returned invalid encounter script name, nIndex = %n", nIndex);
    return result;
}

//Instance functions

//Constructor
object CreatePlotInstance(string sPlotScript, object oRealm)
{
    object plotObject = CreateObject(OBJECT_TYPE_PLACEABLE, "obj_invisible", GetStartingLocation());
    SetLocalString(plotObject, "PLOT_SCRIPT", sPlotScript);
    SetLocalInt(plotObject, "PLOT_STATE", PLOT_STATE_DOING_QUESTS);
    SetLocalInt(plotObject, "PLOT_BIOMEINDEX", -1);
    SetRealmPlot(oRealm, plotObject);

    return plotObject;
}

//Instance functions
void SetPlotJournalEntries(object oPlot, struct PlotJournalEntries entries)
{
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES1", entries.journalName);
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES2", entries.journalPlotDescription);
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES3", entries.journalStart);
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES4", entries.journalFinishedRegionQuests);
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES5", entries.journalFailedRegionQuests);
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES6", entries.journalReachedNextVillage);
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES7", entries.journalFinishedLastRegionQuests);
    SetLocalString(oPlot, "PLOT_JOURNALENTRIES8", entries.journalFailedLastRegionQuests);
}

struct PlotJournalEntries GetPlotInstanceJournalEntries(object oPlot)
{
    struct PlotJournalEntries entries;
    entries.journalName = GetLocalString(oPlot, "PLOT_JOURNALENTRIES1");
    entries.journalPlotDescription = GetLocalString(oPlot, "PLOT_JOURNALENTRIES2");
    entries.journalStart = GetLocalString(oPlot, "PLOT_JOURNALENTRIES3");
    entries.journalFinishedRegionQuests = GetLocalString(oPlot, "PLOT_JOURNALENTRIES4");
    entries.journalFailedRegionQuests = GetLocalString(oPlot, "PLOT_JOURNALENTRIES5");
    entries.journalReachedNextVillage = GetLocalString(oPlot, "PLOT_JOURNALENTRIES6");
    entries.journalFinishedLastRegionQuests = GetLocalString(oPlot, "PLOT_JOURNALENTRIES7");
    entries.journalFailedLastRegionQuests = GetLocalString(oPlot, "PLOT_JOURNALENTRIES8");
    return entries;
}

object GetBossAreaOfPlot(object oPlot)
{
    return GetLocalObject(oPlot, "PLOT_BOSSAREA");
}

void SetBossAreaOfPlot(object oPlot, object oBossArea)
{
    SetLocalObject(oPlot, "PLOT_BOSSAREA", oBossArea);
}

string GetPlotScript(object oPlot)
{
    return GetLocalString(oPlot, "PLOT_SCRIPT");
}

int GetPlotCurrentBiomeIndex(object oPlot)
{
    return GetLocalInt(oPlot, "PLOT_BIOMEINDEX");
}

int GetPlotState(object oPlot)
{
    return GetLocalInt(oPlot, "PLOT_STATE");
}

void UpdatePlotForward(object oPlot, int nState, int nBiomeIndex)
{
    int currentState = GetPlotState(oPlot);
    int currentIndex = GetPlotCurrentBiomeIndex(oPlot);

    if (currentIndex > nBiomeIndex || (currentIndex == nBiomeIndex && (nState == PLOT_STATE_DOING_QUESTS || nState+currentState == 5)))
        return;

    SetLocalInt(oPlot, "PLOT_STATE", nState);
    SetLocalInt(oPlot, "PLOT_BIOMEINDEX", nBiomeIndex);

    struct PlotJournalEntries entries = GetPlotInstanceJournalEntries(oPlot);
    object realm = GetRealm();
    int biomesNum = GetObjectArraySize(REALM_BIOMES_ARRAY, realm);
    int state = GetPlotState(oPlot);
    int biomeIndex = GetPlotCurrentBiomeIndex(oPlot);

    string secondParagraph;
    if (biomeIndex == 0 && nState == PLOT_STATE_DOING_QUESTS)
        secondParagraph = entries.journalStart;
    else if (nState == PLOT_STATE_DOING_QUESTS)
        secondParagraph = entries.journalReachedNextVillage;
    else if (biomeIndex != biomesNum-1 && nState == PLOT_STATE_DONE_QUESTS)
        secondParagraph = entries.journalFinishedRegionQuests;
    else if (biomeIndex != biomesNum-1 && nState == PLOT_STATE_FAILED_QUESTS)
        secondParagraph = entries.journalFailedRegionQuests;
    else if (nState == PLOT_STATE_DONE_QUESTS)
        secondParagraph = entries.journalFinishedLastRegionQuests;
    else
        secondParagraph = entries.journalFailedLastRegionQuests;

    RemoveJournalQuestEntry("main", GetFirstPC(), TRUE, TRUE);
    SetPlotJournalTokens(entries.journalName, entries.journalPlotDescription, secondParagraph);
    AddJournalQuestEntry("main", 1, GetFirstPC(), TRUE, TRUE, TRUE);
}

object GetCurrentPlotDestinationTile(object oPlot)
{
    LogInfo("Getting plot destination file");
    int state = GetPlotState(oPlot);
    object realm = GetRealm();
    int biomeIndex = GetPlotCurrentBiomeIndex(oPlot);
    object currentBiome = GetObjectArrayElement(REALM_BIOMES_ARRAY, biomeIndex, realm);
    object currentTown = GetTownOfBiome(currentBiome);
    if (state == PLOT_STATE_DOING_QUESTS)
        return GetTile(currentTown);

    int biomesNum = GetObjectArraySize(REALM_BIOMES_ARRAY, realm);
    if (biomeIndex == biomesNum-1)
    {
        return GetBossAreaOfPlot(oPlot);
    }

    object nextBiome = GetObjectArrayElement(REALM_BIOMES_ARRAY, biomeIndex+1, realm);
    object nextTown = GetTownOfBiome(nextBiome);
    return GetTile(nextTown);
}