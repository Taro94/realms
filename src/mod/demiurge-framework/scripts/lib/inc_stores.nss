#include "inc_debug"
#include "inc_tiles"
#include "inc_hndvars"

//Library for using various store implementations dynamically

// Getter for the store script's areasNumber field
int GetStoreAreasNumber(string sStoreScript);

// Getter for the store script's storeDayTracksNumber field
int GetStoreDayTracksNumber(string sStoreScript);

// Getter for the store script's storeNightTracksNumber field
int GetStoreNightTracksNumber(string sStoreScript);

// Getter for the store script's storeBattleTracksNumber field
int GetStoreBattleTracksNumber(string sStoreScript);

// Returns a ResRef of a store area to spawn corresponding to the index nIndex in range [0, storeAreasNumber-1]
string GetStoreAreaResRef(string sStoreScript, int nIndex);

// Returns a store's day music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, storeDayTracksNumber-1]
int GetStoreDayTrack(string sStoreScript, int nIndex);

// Returns a store's night music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, storeNightTracksNumber-1]
int GetStoreNightTrack(string sStoreScript, int nIndex);

// Returns a store's battle music track's constant (TRACK_*, but it can also be an integer that has no constant assigned) corresponding to the index nIndex in range [0, storeBattleTracksNumber-1]
int GetStoreBattleTrack(string sStoreScript, int nIndex);

// Returns a random store name for a given store script, based on a LANGUAGE_* constant from inc_language
string GetRandomStoreName(string sStoreScript, string sSeedName, int nLanguage);

// Getter for the store script's storekeepersNumber field
int GetStoreStorekeepersNumber(string sStoreScript);

// Returns a ResRef of an storekeeper creature to spawn corresponding to the index nIndex in range [0, storekeepersNumber-1]
string GetStorekeeperResRef(string sStoreScript, int nIndex);

// Creates and returns a general shop store object at oDestinationWaypoint based on a given town script, a randomness seed and biome level
object SpawnStoreInventory(string sStoreScript, string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint);

// Returns a random store owner first name
string GetRandomStoreOwnerFirstName(string sStoreScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

// Returns a random store owner last name
string GetRandomStoreOwnerLastName(string sStoreScript, int nRacialType, int nGender, int nLanguage, string sSeedName);

//Instance functions

// Store object initialization
void InitializeStoreInstance(string sStoreScript, object oStoreArea, object oTown);

// Returns the given store's script name
string GetStoreScript(object oStore);

// Returns the store's town
object GetTownOfStore(object oStore);


int GetStoreAreasNumber(string sStoreScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 4);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Store " + sStoreScript + " returned invalid areasNumber value: %n", result);
    return result;
}

int GetStoreDayTracksNumber(string sStoreScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 11);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Store " + sStoreScript + " returned invalid storeDayTracksNumber value: %n", result);
    return result;
}

int GetStoreNightTracksNumber(string sStoreScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 12);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Store " + sStoreScript + " returned invalid storeNightTracksNumber value: %n", result);
    return result;
}

int GetStoreBattleTracksNumber(string sStoreScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 13);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Store " + sStoreScript + " returned invalid storeBattleTracksNumber value: %n", result);
    return result;
}

string GetStoreAreaResRef(string sStoreScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 17);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sStoreScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Store " + sStoreScript + " returned invalid store area ResRef");
    return result;
}

int GetStoreDayTrack(string sStoreScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 24);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetStoreNightTrack(string sStoreScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 25);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

int GetStoreBattleTrack(string sStoreScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 26);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    return result;
}

string GetRandomStoreName(string sStoreScript, string sSeedName, int nLanguage)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 29);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nLanguage);
    ExecuteScript(sStoreScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid store name returned in " + sStoreScript + ", sSeedName = " + sSeedName + ", nLanguage = %n", nLanguage);
    return result;
}

int GetStoreStorekeepersNumber(string sStoreScript)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 31);
    ExecuteScript(sStoreScript);
    int result = GetLocalInt(mod, funcResult);
    if (result <= 0)
        LogWarning("Store " + sStoreScript + " returned invalid storekeepersNumber value: %n", result);
    return result;
}

string GetStorekeeperResRef(string sStoreScript, int nIndex)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 34);
    SetLocalInt(mod, funcArg1, nIndex);
    ExecuteScript(sStoreScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid storekeeper creature ResRef returned in store " + sStoreScript + ", nindex = %n", nIndex);
    return result;
}

object SpawnStoreInventory(string sStoreScript, string sSeedName, int nBiomeStartingLevel, object oDestinationWaypoint)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 35);
    SetLocalString(mod, funcArg1, sSeedName);
    SetLocalInt(mod, funcArg2, nBiomeStartingLevel);
    SetLocalObject(mod, funcArg3, oDestinationWaypoint);
    ExecuteScript(sStoreScript);
    object result = GetLocalObject(mod, funcResult);
    if (result == OBJECT_INVALID)
        LogWarning("Invalid store inventory store object returned in store " + sStoreScript + ", sSeedName = " + sSeedName);
    return result;
}

string GetRandomStoreOwnerFirstName(string sStoreScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 36);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sStoreScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid store owner first name returned in store " + sStoreScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

string GetRandomStoreOwnerLastName(string sStoreScript, int nRacialType, int nGender, int nLanguage, string sSeedName)
{
    object mod = GetModule();
    SetLocalInt(mod, funcHandler, 37);
    SetLocalInt(mod, funcArg1, nRacialType);
    SetLocalInt(mod, funcArg2, nGender);
    SetLocalInt(mod, funcArg3, nLanguage);
    SetLocalString(mod, funcArg4, sSeedName);
    ExecuteScript(sStoreScript);
    string result = GetLocalString(mod, funcResult);
    if (result == "")
        LogWarning("Invalid store owner last name returned in store " + sStoreScript + ", nRacialType = %n, nGender = %n, nLanguage = %n", nRacialType, nGender, nLanguage);
    return result;
}

//Instance functions

void InitializeStoreInstance(string sStoreScript, object oStoreArea, object oTown)
{
    SetLocalObject(oStoreArea, "STORE_TOWN", oTown);
    SetLocalObject(oTown, "TOWN_STORE", oStoreArea);
    AddAreaToTile(oTown, oStoreArea);
    SetLocalString(oStoreArea, "STORE_SCRIPT", sStoreScript);
}

string GetStoreScript(object oStore)
{
    return GetLocalString(oStore, "STORE_SCRIPT");
}

object GetTownOfStore(object oStore)
{
    return GetLocalObject(oStore, "STORE_TOWN");
}
