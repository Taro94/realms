#include "inc_generation"
void main()
{
    object oPC = GetClickingObject();
    object tavernArea = GetArea(OBJECT_SELF);
    object tile = GetLocalObject(tavernArea, "Tile");
    object destinationWp = GetTileWaypoint(tile, "wp_tavern");
    AssignCommand(oPC, JumpToObject(destinationWp));
}
