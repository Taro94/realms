#include "inc_debug"
#include "inc_switches"
#include "inc_chat"
#include "inc_map"
#include "inc_realms"
#include "inc_realmsdb"
#include "inc_language"

/*
Map display command. Usage:
/map <full> <x y>
full - if provided, the full map is displayed (not just the explored parts),
x y - if provided, a path to the tile with the given coordinates will be displayed.
Debug mode must be on for the parameters to work.
If no parameters are provided or debug mode is off, the regular map dialogue is opened.
Calling the command again will clear the display.
*/

void main()
{
    object PC = OBJECT_SELF;
    int debug = GetSwitch(MODULE_SWITCH_DEBUG_MODE);
    string params = GetChatCommandParameters();

    //do nothing if we're in the starting area
    object area = GetArea(PC);
    if (area == GetAreaFromLocation(GetStartingLocation()) || GetTag(area) == "companion_area")
    {
        string msg = GetLocalizedString("You can only use the map after starting the adventure!", "Mo�esz u�y� mapy dopiero po rozpocz�ciu przygody!");
        FloatingTextStringOnCreature(msg, PC, FALSE);
        return;
    }

    //start map convo if not in debug mode or if no parameters are provided
    if (!debug || params == "")
    {
        DeleteLocalInt(PC, "MapDisplayed");
        ClearAllActions();
        ActionStartConversation(OBJECT_SELF, "map", TRUE, FALSE);
        return;
    }

    struct PlayerSettings settings = GetPlayerSettings(PC);
    object mapObject = GetRealm();
    int politicalMode = TRUE;

    //just cancel map displaying if it's on
    if (GetLocalInt(PC, "MapDisplayed") == TRUE)
    {
        DeleteLocalInt(PC, "MapDisplayed");
        CancelMapDisplay(PC);
        return;
    }

    SetLocalInt(PC, "MapDisplayed", TRUE);

    //if params' value is simply "full" (and it's debug mode) display the full map
    if (params == "full")
    {
        DisplayMap(PC, mapObject, politicalMode, -1, -1, TRUE, settings.mapOffsetX, settings.mapOffsetY, settings.mapImageSize);
        return;
    }

    //if no params were provided, just display the map without pathing
    int spaceIndex = FindSubString(params, " ");
    if (spaceIndex == -1)
    {
        DisplayMap(PC, mapObject, politicalMode, -1, -1, FALSE, settings.mapOffsetX, settings.mapOffsetY, settings.mapImageSize);
        return;
    }

    //if params start with "full " (and it's debug mode) remember to display the full map (and get rid of the now unnecessary part of params)
    int fullMap = FALSE;
    if (GetStringLeft(params, 5) == "full ")
    {
        fullMap = TRUE;
        params = GetSubString(params, 5, 9999);
    }

    spaceIndex = FindSubString(params, " ");
    string xPart = GetStringLeft(params, spaceIndex);
    string yPart = GetSubString(params, spaceIndex+1, 9999);
    int x = StringToInt(xPart);
    int y = StringToInt(yPart);

    //simply display the map if the coordinates given were wrong
    if ((x == 0 && xPart != "0") || (y == 0 && yPart != "0"))
    {
        DisplayMap(PC, mapObject, politicalMode, -1, -1, fullMap, settings.mapOffsetX, settings.mapOffsetY, settings.mapImageSize);
        return;
    }

    //and if the tile does not exist
    object tile = GetTileArea(x, y, mapObject);
    if (!GetIsObjectValid(tile))
    {
        DisplayMap(PC, mapObject, politicalMode, -1, -1, fullMap, settings.mapOffsetX, settings.mapOffsetY, settings.mapImageSize);
        return;
    }

    DisplayMap(PC, mapObject, politicalMode, x, y, fullMap, settings.mapOffsetX, settings.mapOffsetY, settings.mapImageSize);
}
