#include "inc_language"

void main()
{
    object mod = GetModule();
    int checksum = GetLocalInt(mod, "MAP_CHECKSUM");
    SendMessageToPC(OBJECT_SELF, GetLocalizedString("Generated realm's checksum:", "Suma kontrolna wygenerowanej mapy:"));
    SendMessageToPC(OBJECT_SELF, IntToString(checksum));
}