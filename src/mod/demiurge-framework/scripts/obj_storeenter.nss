#include "inc_generation"
#include "inc_horses"
void main()
{
    object oPC = GetClickingObject();
    object townArea = GetArea(OBJECT_SELF);
    object tile = GetLocalObject(townArea, "Tile");
    object destinationWp = GetTileWaypoint(tile, "wp_storeexit");
    AssignCommand(oPC, JumpToObjectRespectingHorses(destinationWp));
}
