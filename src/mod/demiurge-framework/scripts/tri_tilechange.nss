#include "inc_generation"
#include "inc_realms"
#include "inc_biomes"
#include "inc_stats"
#include "inc_transitions"
#include "inc_tiles"

//Script for travelling between tiles

object GetNextTile(int x, int y, int direction, object oRealm)
{
    switch (direction)
    {
        case PATH_NORTH: y--; break;
        case PATH_EAST: x++; break;
        case PATH_SOUTH: y++; break;
        case PATH_WEST: x--; break;
        default: LogWarning("Wrong direction (tri_tilechange, GetNextTile, direction)");
    }
    return GetTileArea(x, y, oRealm);
}

string GetEnterWaypointTag(int direction)
{
    switch (direction)
    {
        case PATH_NORTH: return "wp_south";
        case PATH_EAST: return "wp_west";
        case PATH_SOUTH: return "wp_north";
        case PATH_WEST: return "wp_east";
    }
    LogWarning("Wrong direction (tri_tilechange, GetEnterWaypointTag, direction)");
    return "";
}

void StubbornTeleportPC(object oPC, object oWP)
{
    if (GetArea(oPC) == GetArea(oWP))
        AssignCommand(oPC, ClearAllActions());
    else
    {
        AssignCommand(oPC, JumpToObject(oWP, FALSE));
        DelayCommand(0.1, StubbornTeleportPC(oPC, oWP));
    }
}

void main()
{
    object realm = GetRealm();
    object oPC = GetEnteringObject();
    if (!GetIsPC(oPC))
        return;

    object area = GetArea(OBJECT_SELF);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    int level = GetRegionStartingLevel(biome);
    int direction = GetLocalInt(OBJECT_SELF, "TRANSITION_DIRECTION");
    int x = GetLocalInt(GetArea(OBJECT_SELF), "X");
    int y = GetLocalInt(GetArea(OBJECT_SELF), "Y");
    object nextTile = GetNextTile(x, y, direction, GetRealm());
    string wpTag = GetEnterWaypointTag(direction);
    object targetWp = GetTileWaypoint(nextTile, wpTag);

    if (!GetIsObjectValid(targetWp))
    {
        LogWarning("Bad transition for data below (tri_tilechange, main, targetWp)");
        LogWarning("Direction: %n", direction);
        LogWarning("Original area resref: " + GetResRef(GetArea(OBJECT_SELF)));
        LogWarning("Target area resref: " + GetResRef(GetArea(nextTile)));
    }

    ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectCutsceneImmobilize(), oPC, 1.0);
    AssignCommand(oPC, ClearAllActions());
    //AssignCommand(oPC, JumpToObject(targetWp));
    StubbornTeleportPC(oPC, targetWp);
    AssignCommand(oPC, ApplyPursuitDamage(oPC, level, TRUE));
    SetMapTileExplored(nextTile);
    if (GetLocalInt(oPC, "InTransition") == FALSE)
    {
        RegisterTileTransition(realm);
        SetLocalInt(oPC, "InTransition", TRUE);
        DelayCommand(0.5, DeleteLocalInt(oPC, "InTransition"));
    }
}
