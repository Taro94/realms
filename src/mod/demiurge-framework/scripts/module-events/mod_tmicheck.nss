#include "inc_common"
#include "inc_language"

void StubbornDisplayPopup(object PC)
{
    if (GetTag(GetArea(PC)) != "start")
    {
        DelayCommand(0.1f, StubbornDisplayPopup(PC));
        return;
    }

    DisplayPopupMessage(PC, GetLocalizedString(
            "It looks like you haven't increased your game's instruction limit in the options.\n\nThis module requires you to do so in order to play. Go to \"Game Options\" -> \"Game\" -> \"Scripts\" -> \"Advanced\" and set \"Vm Runtime Limits Instructions\" to the maximum possible value, then restart the module.", 
            "Wygl�da na to, �e nie zwi�kszy�e� limitu instrukcji gry w opcjach.\n\nJest to wymagane przez ten modu�, aby m�c zagra�. Wejd� w \"Opcje gry\" -> \"Gra\" -> \"Skrypty\" -> \"Zaawansowane\" i ustaw \"Vm Runtime Limits Instructions\" na maksymaln� mo�liw� warto��, a nast�pnie uruchom modu� ponownie."), 
            GetLocalizedString("Increase instruction limit", "Zwi�ksz limit instrukcji"));
}

void main()
{
    object PC = GetEnteringObject();
    int tmiIncreased = GetLocalInt(GetModule(), "TMI_INCREASED");
    if (tmiIncreased)
        return;
    
    object door = GetObjectByTag("door_generator");
    SetEventScript(door, EVENT_SCRIPT_DOOR_ON_CLICKED, "mod_tmicheck");

    StubbornDisplayPopup(PC);
}