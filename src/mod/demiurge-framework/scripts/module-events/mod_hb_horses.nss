#include "x3_inc_horse"

void ApplyMountedCombatEffects(object oPC)
{
    int nRoll;
    if (GetHasFeat(FEAT_MOUNTED_COMBAT, oPC) && HorseGetIsMounted(oPC))
    {
        nRoll = d20() + GetSkillRank(SKILL_RIDE, oPC);
        nRoll = nRoll - 10;
        if (nRoll > 4)
        {
            nRoll=nRoll / 5;
            ApplyEffectToObject(DURATION_TYPE_TEMPORARY, EffectACIncrease(nRoll), oPC, 7.0);
        }
    }
}

void main()
{
    object oPC = GetFirstPC();
    while(GetIsObjectValid(oPC))
    {
        ApplyMountedCombatEffects(oPC);
        oPC=GetNextPC();
    }
}
