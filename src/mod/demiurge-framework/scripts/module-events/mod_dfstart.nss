#include "inc_tokens"
#include "inc_colors"
#include "x3_inc_string"
#include "inc_scriptevents"
#include "x2_inc_switches"
#include "inc_switches"
#include "inc_realms"
#include "inc_rngnames"
#include "inc_realmsdb"
#include "inc_common"
#include "inc_weather"
// Framework-specific startup code

void main()
{
    if (GetIsModuleLoadedFromSaveFile())
    {
        RestoreRealmWeather();
        return;
    }

    //Registering event scripts
    AddEventScript(SUBEVENT_MODULE_ON_LOAD, "mod_start");
    AddEventScript(SUBEVENT_MODULE_ON_LOAD, "mod_regareas");
    AddEventScript(SUBEVENT_MODULE_ON_HEARTBEAT, "mod_hb_horses");
    AddEventScript(SUBEVENT_MODULE_ON_HEARTBEAT, "mod_hb_weather");
    AddEventScript(SUBEVENT_MODULE_ON_CLIENT_ENTER, "mod_playenter");
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_TARGET, "mod_playtarget");
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_DEATH, "mod_death");
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_DYING, "mod_dying");
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_RESPAWN, "mod_respawn");
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_REST, "mod_rest");
    AddEventScript(SUBEVENT_MODULE_ON_UNACQUIRE_ITEM, "x2_mod_def_unaqu");
    AddEventScript(SUBEVENT_MODULE_ON_UNACQUIRE_ITEM, "mod_trophydrop");
    AddEventScript(SUBEVENT_MODULE_ON_ACQUIRE_ITEM, "mod_trophypick");

    //Custom quest event hooks
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_DEATH, "mod_que_death");
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_RESPAWN, "mod_que_respawn");

    //Dark areas subsystem
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_EQUIP_ITEM, "mod_drkequip");
    AddEventScript(SUBEVENT_MODULE_ON_PLAYER_UNEQUIP_ITEM, "mod_drkunequip");
    AddEventScript(SUBEVENT_MODULE_ON_LOAD, "mod_drkstart");

    //Crafting recipes
    AddEventScript(SUBEVENT_MODULE_ON_ACTIVATE_ITEM, "mod_userecipe");

    //Keeping note of players count and difficulty
    AddEventScript(SUBEVENT_MODULE_ON_HEARTBEAT, "mod_hb_realmdif");
    AddEventScript(SUBEVENT_MODULE_ON_CLIENT_ENTER, "mod_realmenter");

    //Counting TMI
    DelayCommand(0.1f, ExecuteScript("mod_tmicount"));
    AddEventScript(SUBEVENT_MODULE_ON_CLIENT_ENTER, "mod_tmicheck");

    //Registering spellhook
    SetSpellhookScript("mod_spellhook");

    //Generate an identifier of this playthrough
    SetLocalString(OBJECT_SELF, "SESSION_ID", GetRandomUUID());

    //Regular NWN switches
    SetModuleSwitch(MODULE_VAR_AI_STOP_EXPERTISE_ABUSE, TRUE);
    SetModuleSwitch(MODULE_SWITCH_ENABLE_TAGBASED_SCRIPTS, TRUE);
    SetLocalInt(OBJECT_SELF, "X2_L_NOTREASURE", TRUE);

    //Horse-related switches
    SetLocalInt(OBJECT_SELF, "bX3_MOUNT_NO_ZAXIS", TRUE);
    SetLocalInt(OBJECT_SELF, "X3_MOUNTS_EXTERNAL_ONLY", TRUE); //No horses allowed in internal areas
    SetLocalInt(OBJECT_SELF, "X3_MOUNTS_NO_UNDERGROUND", TRUE); //No horses allowed in underground areas

    //Up to 4 henchmen (and their and PC's horses)
    SetMaxHenchmen(9);

    //Register token used for storing a lighter highlight color (the default dark blue is hardly visible in most conversations) as well as a gold color
    string rgb = RGBToString(64, 233, 255);
    StoreColor("10", rgb);
    SetCustomTokenEx(10, "<c" + rgb + ">");
    rgb = RGBToString(204, 136, 0);
    StoreColor("11", rgb);
    SetCustomTokenEx(11, "<c" + rgb + ">");

    //Choose a script implementing a realm template (can be easily changed to another for debugging)
    string realmScript = "rea_realm";

    //Create the realm object
    object realm = CreateRealmInstance(realmScript);
    SetLocalObject(OBJECT_SELF, "MAPOBJECT", realm);

    //Create the Realms db
    InitializeSavDb();
    InitializeDb();
    DeleteUnfinishedCompanions();
}
