// Register areas by ResRef to make them easily accessible. This should be run on module's first load and is meant to work in conjunction with functions from inc_areas.

void main()
{
    object mod = GetModule();
    object area = GetFirstArea();
    while (GetIsObjectValid(area))
    {
        string resref = GetResRef(area);
        SetLocalObject(mod, "STORED_AREA_"+resref, area);
        area = GetNextArea();
    }
}