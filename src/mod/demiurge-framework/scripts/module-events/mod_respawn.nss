#include "nw_i0_plot"
#include "inc_towns"
#include "inc_horses"
#include "inc_stats"
#include "inc_realms"

void main()
{
    object PC = GetLastRespawnButtonPresser();

    if (!GetIsDead(PC))
        return;

    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectResurrection(), PC);
    ApplyEffectToObject(DURATION_TYPE_INSTANT, EffectHeal(GetMaxHitPoints(PC)), PC);
    RemoveEffects(PC);

    //Sanity check, ensure the PC has soulstones
    object soulstone = GetItemPossessedBy(PC, "it_soulstone");
    int canRespawn = soulstone != OBJECT_INVALID;
    if (!canRespawn)
    {
        //This shouldn't happen, but off you go to the land of the dead again!
        effect eDeath = EffectDeath(FALSE, FALSE);
        ApplyEffectToObject(DURATION_TYPE_INSTANT, eDeath, PC);
        return;
    }

    //Consume a soulstone
    int stackSize = GetItemStackSize(soulstone);
    if (stackSize == 1)
        DestroyObject(soulstone);
    else
        SetItemStackSize(soulstone, stackSize-1);

    //Dismount PC and their henchmen, destroying the horses
    if (HorseGetIsMounted(PC))
        HorseInstantDismount(PC);
    int i = 1;
    object henchman = GetHenchman(PC, i);
    while (GetIsObjectValid(henchman))
    {
        if (HorseGetIsMounted(henchman))
            HorseInstantDismount(henchman);
        henchman = GetHenchman(PC, ++i);
    }

    //Teleport to town
    object respawnWp = GetPCRespawnWaypoint(PC);
    AssignCommand(PC, JumpToLocation(GetLocation(respawnWp)));

    //Register respawn
    object realm = GetRealm();
    RegisterRespawn(realm);
}