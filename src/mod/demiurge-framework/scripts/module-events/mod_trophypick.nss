#include "inc_champions"
#include "inc_encounters"
#include "inc_biomes"
#include "inc_towns"
#include "inc_tiles"
#include "inc_common"

void main()
{
    if (!GetIsPC(GetModuleItemAcquiredBy()))
        return;

    object trophy = GetModuleItemAcquired();
    object trophyEncounter = GetEncounterOfTrophy(trophy);
    if (trophyEncounter == OBJECT_INVALID)
        return;

    object area = GetEncounterArea(trophyEncounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);
    object village = GetTownOfBiome(biome);
    object hunter = GetBountyHunterOfTown(village);

    effect questMarker = GetEffectByTag(hunter, "QuestMark");
    RemoveEffect(hunter, questMarker);
    effect newQuestMarker = TagEffect(EffectVisualEffect(682), "QuestMark"); //green question mark
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, newQuestMarker, hunter);
}