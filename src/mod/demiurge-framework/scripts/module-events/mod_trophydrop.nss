#include "inc_champions"
#include "inc_encounters"
#include "inc_biomes"
#include "inc_towns"
#include "inc_tiles"
#include "inc_common"
#include "inc_arrays"

void main()
{
    if (!GetIsPC(GetModuleItemLostBy()))
        return;

    object trophy = GetModuleItemLost();
    object trophyEncounter = GetEncounterOfTrophy(trophy);
    if (trophyEncounter == OBJECT_INVALID)
        return;

    object area = GetEncounterArea(trophyEncounter);
    object tile = GetTile(area);
    object biome = GetTileBiome(tile);

    int i;
    for (i = 0; i < GetObjectArraySize(BIOME_ENCOUNTERS_WITH_CHAMPIONS, biome); i++)
    {
        object encounter = GetObjectArrayElement(BIOME_ENCOUNTERS_WITH_CHAMPIONS, i, biome);
        object biomeTrophy = GetTrophyOfEncounter(encounter);
        if (trophy == biomeTrophy)
            continue;

        object owner = GetItemPossessor(biomeTrophy);
        if (GetIsObjectValid(owner) && GetIsPC(owner))
            return;
    }

    object village = GetTownOfBiome(biome);
    object hunter = GetBountyHunterOfTown(village);

    effect questMarker = GetEffectByTag(hunter, "QuestMark");
    RemoveEffect(hunter, questMarker);
    effect newQuestMarker = TagEffect(EffectVisualEffect(677), "QuestMark"); //green exclamation mark
    ApplyEffectToObject(DURATION_TYPE_PERMANENT, newQuestMarker, hunter);
}