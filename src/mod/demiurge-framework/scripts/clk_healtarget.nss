#include "inc_debug"

void main()
{
    object PC = GetLastPlayerToSelectTarget();
    object caster = GetLocalObject(PC, "SpellCasterCompanion");
    object target = GetTargetingModeSelectedObject();
    SetLocalObject(caster, "Henchman_Spell_Target", target);
    ExecuteScript("x2_d1_unsetgroup", caster);
    ExecuteScript("hench_o0_heal", caster);
}
